--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

local gamemode = GAMEMODE or GM

Monolith.LocaleBuilder( "en", true )

--@TODO: put the translations in subtables (has to be mirrored in the places that use them)

gamemode_version = "Version " .. gamemode.Version -- @TODO: Find all instances and remove

time_long_format = "%H:%M:%S - %e %B %Y"
time_abrev_format = "%H:%M:%S - %d/%m/%Y"

monolith.version 						= "Update " .. gamemode.Version

ok 										= "OK"
yes 									= "Yes"
no 										= "No"
on 										= "On"
off 									= "Off"
cancel 									= "Cancel"
err 									= "Error"
confirm 								= "Confirm"
del 									= "Delete"
moneySymbol 							= "$"
moneySymbol.side 						= "LEFT" -- DO NOT TRANSLATE! Enter "LEFT" if you say $100 or "RIGHT" if you display 100$

trash.reward = "You received %s for cleaning the trash."
trash.isEmpty = "This garbage can is empty."

init.notif.lifeAlert 					= "Your life alert made an automatic 911 call to the hospital and the police!"
init.notif.noParamedicsorcops 			= "There are currently no paramedics or officers available. Your life-alert charge will not be used."
init.notif.savedMedical 				= "You've been saved by medical services, but you're experiencing a strange case of amnesia... (NLR ACTIVE)"
init.notif.noPermission					= "You do not have permission to use this functionality."

init.dispatch.lifeAlert 				= "This is an automatic life alert! Available units please respond to this call!"

init.notif.panicButton 					= "You pressed the panic button! An emergency broadcast has been made to the police!"
init.dispatch.panicButton				= "This is an automatic panic button! Available units please reference the police computer map!"

weapon.lockpick.notif.dispatch 			= "A break-in has been detected at this location. Available units please respond!"

kernel.ui.elements.noNotifications 		= "You have no new notifications"
kernel.ui.elements.close 				= "CLOSE ✕"
kernel.ui.elements.min 					= "MIN ▽"
kernel.ui.elements.open 				= "OPEN △"
kernel.ui.elements.allNotifications		= "All Notifications"

kernel.player.waitinglist				= "Server full! you are #%d in queue.\nKeep trying to connect to keep your spot."

anims.raiseHands 	= "%s raises their hands."
anims.lowerHands 	= "%s lowers their hands."
anims.log.raiseHands 	= "%s raised their hands."
anims.log.lowerHands 	= "%s lowered their hands."

taser.low 				= "Left Click to low-voltage tase"
taser.high 				= "Right Click to high-voltage tase"
taser.recharge			= "RECHARGING"

gambling.notAllowed = "You are not allowed to gamble."

admin.actionNotAllowed = "You are not allowed to perform this action."
admin.characterReset = "Your character data was reset."
admin.missingId = "Missing steamId."
admin.characterResetCooldown = "You can only wipe your character once every month."
