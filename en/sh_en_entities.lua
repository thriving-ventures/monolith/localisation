--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

Monolith.LocaleBuilder( "en", true )

--[[
	SWEPS
--]]

-- Hands
sweps.handcuffsinhand.printName 			= "Handcuffs"

sweps.handcuffsinhand.actions.beginCuffing 	= "%s starts putting cuffs on %s's wrists."
sweps.handcuffsinhand.actions.placedCuffs	= "%s places cuffs on %s's wrists."
sweps.handcuffsinhand.actions.handCuffing	= "HANDCUFFING \n%s"
sweps.handcuffsinhand.actions.removeCuffs	= "%s removes cuffs from %s's wrists."

sweps.handcuffsinhand.gui.checkOwner 		= "Press R to get owner"
sweps.handcuffsinhand.gui.noOwner			= "No owner"

sweps.handcuffsinhand.log.cuffed			= "%s cuffed %s"
sweps.handcuffsinhand.log.unCuffed			= "%s un-cuffed %s"

-- Restrained

sweps.handcuffsonplayer.printName 			= "Restrained"

-- Bong
sweps.sentBong.gui.weed 					= "Weed"
sweps.sentBong.gui.quality					= "Quality"

-- Zipties
sweps.zipties.printName 					= "Zipties"

sweps.zipties.actions.beginZip 				= "%s starts putting zipties on %s."
sweps.zipties.actions.finishZip 			= "%s finishes putting zipties on %s."
sweps.zipties.actions.zipTie 				= "ZIP TYING \n%s"

sweps.zipties.log.ziptied					= "%s ziptied %s"

-- Axe
sweps.axe.printName 						= "Wood Axe"

-- Defib
sweps.defib.actions.stabalizing				= "STABILISING\n${name}"

sweps.defib.gui.dead 						= "The patient appears to be deceased."
sweps.defib.gui.stable						= "He's already stabilised!"
sweps.defib.gui.critical					= "Patient must be critical for him to be stabilised!"
sweps.defib.gui.downtime					= "You have gotten here quick enough! Because of that, the patient didn't sustain big injuries"
sweps.defib.gui.hospitalwarning				= "It seems like he has been stabilised, take him to hospital!"

sweps.defib.logger.revived 					= "%s revived: %s"

-- Axe
sweps.fireaxe.gui.assistance 				= "Your assistance is not required here!"

-- Keys
sweps.keys.printName 						= "Keys"

sweps.keys.gui.state						= "Do you want to ${state} access to your cars/doors?"
sweps.keys.gui.stateRevoke					= "revoke"
sweps.keys.gui.stateGrant					= "grant"
sweps.keys.gui.access						= "Access"
sweps.keys.gui.thisPerson					= "this person"
sweps.keys.gui.yes							= "Yes"
sweps.keys.gui.no							= "No"
sweps.keys.gui.revokeOwner					= "You revoked ownership for "
sweps.keys.gui.grantOwner					= "You granted ownership for "

--weapon_lockpick
sweps.lockpick.printName         = "Lockpick"

--weapon_lockpick
sweps.extinguisher.printName 		= "Extinguisher"

--weapon_mono_citizen_id
sweps.citizenid.printName        = "Civilian ID Card"

--weapon_mono_department
sweps.departmentTablet.printName = "Department's Tablet"
sweps.departmentTablet.desc      = "Use it to get high" -- ? ...

--weapon_fireaxe
sweps.fireaxe.printName 		= "Fire Axe"

-- weapon_defibrillator
sweps.defibrillator.printName 	= "Defibrillator"

--weapon_mono_fists
sweps.fists.printName            = "Hands"
sweps.fists.instructions         = "Primary Fire: Punch\nSecondary Fire: Knock/Pickup"
sweps.fists.purpose              = "Hitting things and knocking on doors."

-- Medkit
sweps.medkit.printName 						= "First Aid"

sweps.medkit.gui.healing					= "HEALING"
sweps.medkit.gui.sterilizing 				= "STERILIZING\nWOUNDS"
sweps.medkit.gui.cauterizing 				= "CAUTERIZING\nWOUNDS"
sweps.medkit.gui.applyingBandages 			= "APPLYING\nBANDAGES"
sweps.medkit.gui.healingMe 					= "HEALING\nYOURSELF"
sweps.medkit.notif.playerInCombat			= "You cannot heal being in combat!"
sweps.medkit.notif.targetInCombat			= "The target is in combat!"

--weapon_mono_police_id
sweps.policeid.printName   = "Police ID Card"

--weapon_mono_tablet
sweps.monotablet.printName = "Mono-Tablet"

--weapon_pickaxe
sweps.pickaxe.printName    = "Pickaxe"
sweps.pickaxe.instructions = "Primary attack: Swing - Secondary attack : Push"

--weapon_radargun
sweps.radargun.printName   = "Radar Gun"
sweps.radargun.purpose     = "Left to scan speed"

--weapon_ramdoor
sweps.ramdoor.printName = "Battering Ram"
sweps.ramdoor.purpose = "Help"

--weapon_taser
sweps.taser.printName = "Taser"
sweps.taser.log = "%s tasered %s."

-- weapon_mono_vape
-- @TODO configure the flavor juices in the weapon_mono_vape.lua directly in the JuicyVapeJuices table
sweps.monovape.printName 					= "Vape"
sweps.monovape.gui.loadedFlavorJuice 		= "Loaded %s flavor juice"

sweps.monovape.juices.dew				= "Mountain Dew"
sweps.monovape.juices.cheetos				= "Cheetos"
sweps.monovape.juices.razzleberry				= "Razzleberry"
sweps.monovape.juices.banana				= "Banana"
sweps.monovape.juices.blacklicorice				= "Black Licorice"
sweps.monovape.juices.churro				= "Churro"
sweps.monovape.juices.skittles				= "Skittles"
sweps.monovape.juices.normal				= "Normal"

-- weapon_nightstick
sweps.nightstick.printName 					= "Baton"
sweps.nightstick.log.knockOut 				= "%s has knocked out %s with a police baton."

-- weapon_policetape
sweps.policetape.printName 					= "Police Tape"

sweps.policetape.gui.distance				=  "Distance"
sweps.policetape.gui.rightClickCancel		= "Right click to cancel."
sweps.policetape.gui.ropeCantBeThatLong 	= "Your rope can't be that long!"
sweps.policetape.gui.pickedUpSegmentTape 	= "You have picked up this segment of the tape."
sweps.policetape.gui.clearedAllTapes 		= "You have cleared all of your police tapes."
sweps.policetape.gui.maxTapes 				= "You have reached the limit of maximum tapes!" -- @TODO money symbol line 139
sweps.policetape.log.hasPlacedRope 			= "%s has placed a police tape at %s"
sweps.policetape.log.removedPoliceTape 		= "%s removed his police tape at %s"
sweps.policetape.log.removedOtherPoliceTape = "%s removed %s's police tape at %s"
sweps.policetape.log.clearedAllTapes		= "%s  cleared all of his police tapes."

-- weapon_radargun
-- @TODO maybe we should add the speed unit in this file

-- weapon_ticket
sweps.weaponticket.printName 					= "Ticket Book"
sweps.weaponticket.purpose 						= "Use it to write tickets and such"

sweps.weaponticket.gui.cantGiveTicketToPlayer = "You cannot give a ticket to this player for another %s seconds."
sweps.weaponticket.gui.cantGiveTicket		  = "You cannot give a ticket for another %s seconds."
sweps.weaponticket.gui.officierIssuedIt 	  = "Officer who Issued it"
sweps.weaponticket.gui.timeSinceIssued		  = "Time since issued"
sweps.weaponticket.gui.lawyer 				  = "Lawyer"
sweps.weaponticket.gui.selectTicket 		  = "Select a ticket"
sweps.weaponticket.gui.police 				  = "Police"
sweps.weaponticket.gui.minutesAgo 			  = "%s minute(s) ago"
sweps.weaponticket.gui.resolve				  = "Resolve"
sweps.weaponticket.gui.ticket				  = "Ticket"
sweps.weaponticket.gui.payTicket			  = "Pay this ticket ($%s)"
sweps.weaponticket.gui.pay					  = "Pay"
sweps.weaponticket.gui.deny					  = "Deny"
sweps.weaponticket.gui.appealTicketIfUnfair   = "Appeal this ticket if you believe this was unfair."
sweps.weaponticket.gui.smallViolation 		  = "First/Small Violation"
sweps.weaponticket.gui.repeated 			  = "Repeated/Moderated"
sweps.weaponticket.gui.excessive			  = "Excessive/Severe"
sweps.weaponticket.gui.appeal 				  = "Appeal"
sweps.weaponticket.gui.amountToPay 			  = "Amount to pay" -- @TODO money symbol line 246
sweps.weaponticket.gui.ticketsFromHim 		  = "Tickets from him"
sweps.weaponticket.gui.insertText 			  = "<Insert text>"
sweps.weaponticket.gui.beMoreDescriptive 	  = "You need to be more descriptive with your reason."
sweps.weaponticket.gui.insertNumber 		  = "You have to insert a number!"
sweps.weaponticket.gui.ok 					  = "Ok"
sweps.weaponticket.gui.cantFineMore 		  = "You can't fine more than $4,500!" -- @TODO maybe adding this to a configuration or something like that line 313 and 227
sweps.weaponticket.gui.cantFineMoreThan		  = "You can't fine more than %s!"
sweps.weaponticket.gui.areYouSure 			  = "Are you sure that you want to create this ticket?"
sweps.weaponticket.gui.cancel 				  = "Cancel"
sweps.weaponticket.gui.accept 				  = "Accept"
sweps.weaponticket.gui.id 					  = "ID"
sweps.weaponticket.gui.kind 				  = "Kind"
sweps.weaponticket.gui.invalidReason		  = "You cannot issue a ticket with the default reason text."
sweps.weaponticket.gui.targetName			  = "Name"
sweps.weaponticket.gui.creatorName			  = "Officer"


-- weapon_Lawbook
sweps.lawbook.printName			  				= "Law Book"
sweps.lawbook.purpose 							= "Left CLick to open the latest Law book version"

-- weapon_trowel
sweps.weapontrowel.printName 				  = "Gardening Trowel"
sweps.weapontrowel.gui.cantPlantHere 		  = "You can not plant here."
sweps.weapontrowel.gui.tooClose 			  = "You are trying to plant too close to something!"

-- weapon_watercan
sweps.watercan.printName = "Watering Can"
sweps.watercan.purpose = "A weapon used to water dirt piles"

-- weapon_mono_drugtest
sweps.drugtest.printName = "Drug Tester"
sweps.drugtest.purpose = "A testing kit used to test for drugs."


--[[
	ENTITIES
--]]

-- areacreator
tools.areacreator.name = "Area Creator"
tools.areacreator.notDefined = "Not Defined"

-- nodecreator
tools.nodecreator.name = "Node Creator"
tools.nodecreator.instructions = "LMB: Create node - RMB: Set Path"
tools.nodecreator.selectNode = "Select another node"

-- permaprops
tools.permaprops.name = "PermaProps"
tools.permaprops.description = "Save a props permanently"
tools.permaprops.instructions = "LeftClick: Add RightClick: Remove Reload: Update"
tools.permaprops.adminCan = "Admin can touch permaprops"
tools.permaprops.adminCant = "Admin can't touch permaprops !"
tools.permaprops.superAdminCan = "SuperAdmin can touch PermaProps"
tools.permaprops.superAdminCant = "SuperAdmin can't touch permaprops !"
tools.permaprops.onlySA = "Only Super Admin can touch permaprops"
tools.permaprops.inValidEnt = "That is not a valid entity !"
tools.permaprops.isPlayer = "That is a player !"
tools.permaprops.already = "That entity is already permanent !"
tools.permaprops.saved = "You saved %s with model %s to the database."
tools.permaprops.erased = "You erased %s with a model of %s from the database."
tools.permaprops.reload = "You have reload all PermaProps !"
tools.permaprops.updated = "You updated the %s you selected in the database."
tools.permaprops.saveProps = "Save a props for server restarts\nBy Malboro"
tools.permaprops.header = "------ Configuration ------"
tools.permaprops.header2 = "-------- Functions --------"
tools.permaprops.removeAll = "Remove all PermaProps"
tools.permaprops.erasedAll = "You erased all props from the map"

-- propertycreator
tools.propertycreator.name = "Property Creator"
tools.propertycreator.desc = "Edit The Property Data"
tools.propertycreator.propertyName = "Name: "
tools.propertycreator.hammerIdentifier = "Hammer ID: "
tools.propertycreator.propertyUID = "UID: "
tools.propertycreator.propertyCategory = "Category: "
tools.propertycreator.propertyPrice = "Price: "
tools.propertycreator.propertyJobCat = "Job Category: "
tools.propertycreator.propertyElectricMode = "Electricity Node: "
tools.propertycreator.noDataGiven = "No data is given"
tools.propertycreator.noDataCamGiven = "No cam data is given"
tools.propertycreator.noDataBoundGiven = "No boundarie data is given"
tools.propertycreator.noDataSendGiven = "No send data is given"
tools.propertycreator.noDataIDGiven = "No id is given"
tools.propertycreator.noDataNameGiven = "No name is given"
tools.propertycreator.noDataPriceGiven = "No price is given"
tools.propertycreator.noDataCatGiven = "No category is given"
tools.propertycreator.noDataJCatGiven = "No jobCategory given on unownable property"
tools.propertycreator.savedCon = "Saved %s as existing configuration"
tools.propertycreator.failedSave = "Failed to save %s as existing configuration. ERROR: "
tools.propertycreator.noError = "no error"
tools.propertycreator.saved = "Saved "
tools.propertycreator.reload = "Reload: Open Property Editor"
tools.propertycreator.leftC = "Left-Click: "
tools.propertycreator.rightC = "Right-Click: "
tools.propertycreator.none = "None"

tools.propertycreator.gui.propertyEditor = "Property Creator"
tools.propertycreator.gui.editBound = "Edit Boundaries"
tools.propertycreator.gui.setMaxPos = "Set Max Pos"
tools.propertycreator.gui.setMinPos = "Set Min Pos"
tools.propertycreator.gui.editDoors = "Edit Doors"
tools.propertycreator.gui.noDoorFound = "No door found at hit location. Make sure you are aiming at the door"
tools.propertycreator.gui.addedDoor = "Added door to property"
tools.propertycreator.gui.unableToRmove = "Unable to remove door position from property. Door position not found in property door table"
tools.propertycreator.gui.addDoor = "Add Door"
tools.propertycreator.gui.removeDoor = "Remove Door"
tools.propertycreator.gui.editCam = "Edit Camera"
tools.propertycreator.gui.setCam = "Set Camera Position"
tools.propertycreator.gui.editPower = "Edit Power Outlets"
tools.propertycreator.gui.addOutlet = "Add Outlet"
tools.propertycreator.gui.removeOutlet = "Remove Outlet"
tools.propertycreator.gui.editBins = "Edit Trash Bins"
tools.propertycreator.gui.addBin = "Add Bin"
tools.propertycreator.gui.removeBin = "Remove Removebin"
tools.propertycreator.gui.editPrisoner = "Edit Prisoner Toilets"
tools.propertycreator.gui.addPrisoner = "Add Prisoner Toilet"
tools.propertycreator.gui.removePrisoner = "Remove Prisoner Toilet"
tools.propertycreator.gui.editTask = "Edit Prisoner Tasks"
tools.propertycreator.gui.addTask = "Add Prisoner Task"
tools.propertycreator.gui.removeTask = "Remove Prisoner Task"
tools.propertycreator.gui.saveProperty = "Save Property Changes"
tools.propertycreator.gui.deselectProperty = "Exit Property"
tools.propertycreator.gui.addChildren = "Add Children Houses"
tools.propertycreator.gui.setParentID = "Set The Parent ID"
tools.propertycreator.gui.putParentID = "Put the Parent ID here."
tools.propertycreator.gui.setChildrenCount = "Set The Children Count"
tools.propertycreator.gui.setChildrenCountBelow = "Set the children count of the properys below."
tools.propertycreator.gui.editParentID = "Edit Parent ID"
tools.propertycreator.gui.putNewParentID = "Put the new Parent ID here."
tools.propertycreator.gui.quickPropertyPicker = "Quick Property Picker"
tools.propertycreator.gui.automaticDoors = "Automatically Set Doors"

-- textscreen
tools.textscreen.name = "3D2D Textscreen"
tools.textscreen.desc = "Create a textscreen with multiple lines, font colours and sizes."
tools.textscreen.instructions = "Left Click: Spawn a textscreen Right Click: Update textscreen with settings"
tools.textscreen.undoneText = "Undone textscreen"
tools.textscreen.textScreens = "Textscreens"
tools.textscreen.cleanedUpText = "Cleaned up all textscreens"
tools.textscreen.hitLimit = "You've hit the textscreen limit!"

tools.textscreen.gui.reset = "Reset all"
tools.textscreen.gui.resetColors = "Reset colors"
tools.textscreen.gui.resetSizes = "Reset sizes"
tools.textscreen.gui.resetText = "Reset textboxes"
tools.textscreen.gui.resetEverything = "Reset everything"
tools.textscreen.gui.resetLine = "Reset line"
tools.textscreen.gui.resetAllLines = "Reset all lines"
tools.textscreen.gui.line = "Line "
tools.textscreen.gui.fontColor = " font color"
tools.textscreen.gui.fontSize = "Font size"

--[[
	ENTITIES
--]]

-- ent_dumpster
entities.dumpster.waitBeforeDiving = "You must wait %s before dumpster diving again."
entities.dumpster.dumpsterSearched = "This dumpster has already been searched recently. You must wait %s"
entities.dumpster.foundNothing = "You found nothing in the dumpster."
entities.dumpster.foundX = "You found a(n) %s in the dumpster!"
entities.dumpster.dumpsterDiving = "Dumpster Diving..."

-- sent_workbench
entities.workbench.printName                = "Workbench"

-- sent_woodworkbench
entities.woodworkbench.printName            = "Woodworking Table"

-- sent_textscreen
entities.textscreen.printName               = "Text Screen"
entities.textscreen.editTextTitle           = "Edit Text Screen"
entities.textscreen.editColors              = "Edit Colors"
entities.textscreen.abort                   = "Abort"
entities.textscreen.setColor                = "Set Color"
entities.textscreen.colorChanged            = "The text screen its colour has been changed."
entities.textscreen.edit                    = "Edit"
entities.textscreen.textLengthError         = "Text too short/long (<2 >18)"
entities.textscreen.pressEToChange          = "Press E to change"

-- sent_stretcher
entities.stretcher.printName                = "Stretcher"
entities.stretcher.patientStabilise         = "The patient must be stabilised first to do that!"
entities.stretcher.ledByAnyone              = "The stretcher must not be led by anyone to do this!"
entities.stretcher.strappedOntop            = "%s strapped %s's body ontop of a stretcher"
entities.stretcher.unstrapped               = "%s unstrapped %s's body from a stretcher."
entities.stretcher.sameVehicle              = "You must put the stretcher into the same vehicle it came from!"
entities.stretcher.backDoorClosed           = "The back doors are closed! Open them first."
entities.stretcher.notEMSPolice             = "You can't do that if you're not an EMS/Police."
entities.stretcher.notYourVehicle           = "It's not your vehicle!"
entities.stretcher.notYourAmbulance         = "It's not your ambulance!"
entities.stretcher.blockingDeploy           = "There's something blocking the deploy position!"

-- sent_road_sign
entities.roadsign.printName                 = "Police Road Sign"
entities.roadsign.policeDepartment          = "Police Department"
entities.roadsign.mustBePolice              = "You must be a member of the Police to edit this sign"
entities.roadsign.pickingUp                 = "PICKING UP"
entities.roadsign.policeSign                = "Police Sign"
entities.roadsign.update                    = "Update"

-- sent_road_sign_small
entities.roadsignsmall.printName            = "Police Road Sign - Small"
entities.roadsignsmall.roadClosed           = "ROAD CLOSED"
entities.roadsignsmall.memberPolice         = "You must be a member of the Police to edit this sign"
entities.roadsignsmall.policeSign           = "Police Sign"
entities.roadsignsmall.update               = "Update"

entities.roadsignsmall.log.changeText		= "%s changed a roadsign's text to: %s."

-- sent_residential_extension_outlet
entities.extensionoutlet.printName          = "Residential Extension Outlet"
entities.extensionoutlet.errorAttempt       = "Attempt to use SetBreaker() on a sent_residential_extension_outlet"

-- sent_repair_bench
entities.repairbench.printName              = "Repair Bench"

-- sent_property_bin
entities.propertybin.printName              = "Property Bin"
entities.propertybin.garbageCan             = "Garbage Can"
entities.propertybin.percentFull            = "% full"

-- sent_power_source
entities.powersource.printName              = "Property Power Source"

-- sent_power_outlet
entities.poweroutlet.printName              = "Power Outlet"

-- sent_power_converter
entities.powerconverter.printName           = "Power Converter (Two-Way)"

-- sent_pot
entities.pot.printName                      = "Pot"
entities.pot.weedBag                        = "Weed Bag"
entities.pot.useOtherPots                   = "You cannot use other individual's pots."
entities.pot.dehydrated                     = "Dehydrated"
entities.pot.thirsty                        = "Thirsty"
entities.pot.neutral                        = "Neutral"
entities.pot.healthy                        = "Healthy"
entities.pot.insertSoil                     = "Insert Soil"
entities.pot.insertSeed                     = "Insert Seed"
entities.pot.harvest                        = "Harvest"
entities.pot.properSeeds                    = "You do not have any proper seeds to plant."
entities.pot.gardeningSoil                  = "You do not have any gardening soil to use."
entities.pot.plantedSeed                    = "You have planted a seed in the pot."
entities.pot.filledPot                      = "You have filled the pot with gardening soil."
entities.pot.gardeningPot                   = "Gardening Pot"
entities.pot.alreadySoil                    = "The pot already has soil"
entities.pot.soilAvailable                  = "You have %s soil available to use"
entities.pot.seedsAvailable                 = "You have %s seed%s available to plant"
entities.pot.plant                          = "Plant"
entities.pot.properSeeds                    = "You do not have any proper seeds to plant."
entities.pot.addSoil                        = "Add Soil"

-- sent_plug
entities.plug.printName                     = "Plug"

-- sent_money
entities.money.printName                    = "Money"
entities.money.youGrabbed                   = "You grabbed $"
entities.money.pickedUp                     = " picked up "

-- sent_metalworkbench
entities.metalworkbench.printName           = "Metalworking Table"

-- sent_metalworkbench
entities.metalworkbench.printName           = "Metalworking Table"

-- sent_lsd
entities.lsd.printName                      = "LSD"

-- sent_lsd_pyro
entities.lsdpyro.printName                  = "Bunson burner"

-- sent_lsd_powder
entities.lsdpowder.printName                = "LSD Powder material"

-- sent_lsd_paper
entities.lsdpaper.printName                 = "Adventure Paper"

-- sent_lsd_gas
entities.lsdgas.printName                   = "Gas can"

-- sent_lsd_freezer
entities.lsdfreezer.printName               = "Fridge"
entities.lsdfreezer.holdPickUp              = "(Hold) Pick up "
entities.lsdfreezer.insertFlask             = "Insert a flask"
entities.lsdfreezer.takeOut                 = "I'm freezing! Take me out"

-- sent_lsd_flask
entities.lsdflask.printName                 = "Flask"
entities.lsdflask.stage1                    = "Insert Powder"
entities.lsdflask.stage2                    = "I need heat"
entities.lsdflask.stage3                    = "Wait for the burner to complete"
entities.lsdflask.stage4                    = "Insert liquid"
entities.lsdflask.stage5                    = "Shake me gently"
entities.lsdflask.stage6                    = "Cool me"
entities.lsdflask.stage7                    = "Put me on a piece of paper"

-- sent_lsd_flank_support
entities.lsdflanksupport.printName          = "Flask support"
entities.lsdflanksupport.fireStarted        = "[Fire Quest] LSD Fire has started!"
entities.lsdflanksupport.insertBurner       = "Insert a burner"
entities.lsdflanksupport.insertGas          = "Insert a gas canister"
entities.lsdflanksupport.insertFlask        = "Insert a flask"
entities.lsdflanksupport.progress           = "Progress: "

-- sent_lsd_bottle
entities.lsdbottle.printName                = "LSD Bottle material"

-- sent_infinite_power_source
entities.infinitepower.printName            = "Infinite Power Source"
entities.infinitepower.powerSource          = "Power Source"
entities.infinitepower.infinite             = "Infinite"

-- sent_fire_spark
entities.firespark.printName                = "Drug"

-- sent_fire_source
entities.firesource.printName               = "Fire"

-- sent_factory_furnace
entities.factoryfurnace.printName           = "Furnace"
entities.factoryfurnace.busy                = "Factory is busy."
entities.factoryfurnace.takeAllComp         = "You need to take all components out before picking this up."
entities.factoryfurnace.readyForUse         = "Ready for use"
entities.factoryfurnace.needsConveyor       = "Needs a conveyor"
entities.factoryfurnace.needsPower          = "Needs a power source"

-- sent_factory_crusher
entities.factorycrusher.printName           = "Crusher"
entities.factorycrusher.busyCrushing        = "The Crusher is busy crushing an item."
entities.factorycrusher.takeAllBefore       = "You need to take all items from the Container out before picking this up."
entities.factorycrusher.preciousMats        = "A container to hold precious materials given by the Crusher."
entities.factorycrusher.turnOffNotif        = "The Crusher will turn off after it has finished."
entities.factorycrusher.crushing            = "Crushing "
entities.factorycrusher.insertChunks        = "Insert Chunks"
entities.factorycrusher.unplugged           = "Unplugged"

-- sent_factory_conveyor
entities.factoryconveyor.printName          = "Conveyor"
entities.factoryconveyor.factoryBusy        = "The Factory is busy."
entities.factoryconveyor.turnMeOn           = "Turn me on"
entities.factoryconveyor.active             = "Active"
entities.factoryconveyor.secs               = " secs"

-- sent_factory_cell
entities.factorycell.printName              = "Power Cell"

-- sent_factory_box
entities.factorybox.printName               = "Storage Box"
entities.factorybox.containerHold           = "A container to hold factory items given by the Furnace."
entities.factorybox.takeAllItems            = "You need to take all items from the Storage Box out before picking it up."

-- sent_factory_base
entities.factorybase.printName              = "Factory Base"

-- sent_drug_bag
entities.drugbag.printName                  = "Weed Bag"

-- sent_door_charge
entities.doorcharge.printName               = "Door Charge"

-- sent_cooktable
entities.cooktable.printName                = "Cooking Stove"
entities.cooktable.takeAllItems            	= "You need to take all items from the prep table out before picking it up."

-- sent_cocaine_stove
entities.cocainestove.printName             = "Electric Stove"

-- sent_cocaine_pot
entities.cocainpot.printName                = "Dirty Cooking Pot"
entities.cocainpot.temperature              = "Temperature: "
entities.cocainpot.waitAbout                = "Wait about %d Seconds"

-- sent_cocaine_packingbox
entities.cocainepackingbox.printName        = "Packing Box"
entities.cocainepackingbox.cocaLeaves       = "Coca Leaves: "
entities.cocainepackingbox.boxFull          = "Box is full"
entities.cocainepackingbox.packing          = "Packing"
entities.cocainepackingbox.notEnoughLeaves  = "You do not have enough leaves."
entities.cocainepackingbox.boxFullEmpty     = "Box is full, please empty the box."
entities.cocainepackingbox.noLeaves         = "You do not have any leaves."

-- sent_cocaine_kerosene
entities.cocainkerosene.printName           = "Kerosene"
entities.cocainkerosene.shakeIt             = "Shake it: "
entities.cocainkerosene.readyToUse          = "Ready to use"

-- sent_cocaine_jerrycan
entities.cocainejerrycan.printName          = "Jerry Can"
entities.cocainejerrycan.ready              = "Ready"
entities.cocainejerrycan.pleaseWait         = "Please wait %d Seconds"

-- sent_cocaine_gas
entities.cocainegas.printName               = "Gas"
entities.cocainegas.filled                  = "Filled: "

-- sent_cocaine_drafted
entities.cocainedrafted.printName           = "Drafted Leaves"
entities.cocainedrafted.shakeIt             = "Shake it: "
entities.cocainedrafted.ready               = "Ready to use"
entities.cocainedrafted.pleaseWait          = "Please wait %d Seconds"

-- sent_cocaine_acid
entities.cocaineacid.printName              = "Sulfuric Acid"
entities.cocaineacid.pleaseWait             = "Please wait %d Seconds"
entities.cocaineacid.ready                  = "Ready to use"

-- sent_christmas_tree
entities.christmastree.printName            = "Christmas Tree"

-- sent_brewingbarrelbench
entities.brewingbarrelbench.printName       = "Brewing Barrel"

-- sent_bountyboard
entities.bountyboard.printName              = "Bountyboard"
entities.bountyboard.placeBounty            = "Place a bounty"
entities.bountyboard.name                   = "Name"
entities.bountyboard.reward                 = "Reward"
entities.bountyboard.dateAssigned           = "Date Assigned"

-- sent_base_lsd
entities.baselsd.printName                  = "Drug"

-- base_item_stackable
entities.baseitemstackable.printName        = "Base item"
entities.baseitemstackable.beingArrested    = "You cannot pickup items while being arrested."
entities.baseitemstackable.inventorySpace   = "You do not have space in your inventory to purchase that!"
entities.baseitemstackable.boughtItem       = "You purchased an item. %s was subtracted from your wallet."
entities.baseitemstackable.shopFull         = "This shop is already full!"
entities.baseitemstackable.dontHaveItem     = "You don't have this item in your inventory!"
entities.baseitemstackable.onlyHadThis      = "You only had %s of this item!"

--ent_atm_screen
entities.atmscreen.printName                = "ATM SCREEN"
entities.atmscreen.withdraw                 = "Withdraw"
entities.atmscreen.withdrawAll				= "Withdraw All"
entities.atmscreen.deposit                  = "Deposit"
entities.atmscreen.depositAll               = "Deposit All"
entities.atmscreen.monoBank                 = "MONO-BANK"
entities.atmscreen.balance                  = "Balance: "
entities.atmscreen.accountHolder            = "Account Holder: %s"
entities.atmscreen.fiftyUSD                 = "$50" -- idk what you expect me to use here
entities.atmscreen.fiveHundredUSD           = "$500"
entities.atmscreen.tenThousandUSD           = "$10000"
entities.atmscreen.clear                    = "C"
entities.atmscreen.lessThan                 = "<"
entities.atmscreen.USD                      = "$"
entities.atmscreen.accessDenied             = "ATM access denied."
entities.atmscreen.insufficientFunds        = "You do not have the sufficient funds."
entities.atmscreen.UseWithdraw				= "Please Use the Withdraw option for that."
entities.atmscreen.UseDeposit				= "Please Use the Deposit option for that."
entities.atmscreen.confirm                  = "Confirm"

--ent_atm
entities.atm.printName                      = "ATM"

--ent_atm_craftable
entities.atmcraftable.printName                      = "Craftable ATM"

--ent_boat_base
entities.boatbase.printName                 = "Boat Base"
entities.boatbase.containerDesc             = "A container to hold cargo in the hold of a Boat."

--ent_boat_motor_engine
entities.boatmotorengine.printName          = "Boat Motor Engine"

--ent_boat_rhib
entities.boatrhib.printName                 = "RHIB"

--ent_boat_rowboat
entities.rowboat.printName                  = "Row Boat"


--ent_callsign_display
entities.callsigndisplay.printName          = "Callsign Display Board"
entities.callsigndisplay.yourCallsign       = "Your callsign is %s"
entities.callsigndisplay.noCallsign         = "You don't have a callsign"
entities.callsigndisplay.deactivate         = "Detective"

--ent_carspawnsign
entities.carspawnsign.warnning              = "— WARNING —" --written 2 years ago by kyle goodale not me
entities.carspawnsign.seriousInjury         = "To avoid serious injury,"
entities.carspawnsign.remain10ft            = "remain at least 10 feet"
entities.carspawnsign.fromThisSign          = "away from this sign..."
entities.carspawnsign.ty                    = "Thank you."

--ent_circle_trigger
entities.circletrigger.printName            = "Delivery Point"
entities.circletrigger.depotPrintName       = "Return Point"

--ent_busstop_trigger
entities.busstoptrigger.printName			= "Bus Stop"

--ent_teleporter_trigger
entities.teleportertrigger.printName		= "Teleporter Trigger"
entities.teleportertrigger.text				= "Validating ID card"
entities.teleportertrigger.notAllowed		= "You are not allowed to access the shooting range at this time."

--ent_detector_trigger
entities.detectortrigger.printName          = "Metal Detector Trigger"

--ent_fire_hydrant
entities.firehydrant.printName              = "Fire Hydrant"

--ent_fire_pumphose
entities.pumphose.printName                 = "Hose Pump"
entities.pumphose.detachingHose             = "DETACHING HOSE"

--ent_fire_waterhose
entities.waterhose.printName                = "Water Hose"
entities.waterhose.unequipFirst             = "Unequip your hose first!"

--ent_garage_tv
entities.garagetv.printName                 = "Garage TV"

--ent_garage
entities.garage.printName                   = "Garage"
entities.garage.modeDisabled                = "This mode is currently disabled. Please use the Legacy vehicles system by pressing F3."

--ent_gascan
entities.gascan.printName                   = "Gas Can"
entities.gascan.pourThis                    = "Pour this into your vehicle."

--ent_gaspump
entities.gaspump.printName                  = "Gas pump"
entities.gaspump.poor                       = "You can't afford this!"
entities.gaspump.needToWait                 = "You have to wait %s seconds!"

--ent_house_alarm
entities.housealarm.printName               = "Map" -- weir name for a house alarm

--ent_instanced_apt_circle
entities.instancedaptcircle.printName       = "Testing Text"

--ent_invisible_window
entities.invisibleWindow.printName          = "Invisible window"

--ent_itembox
entities.itembox.printName                  = "Premium Locker"
entities.itembox.restricted                 = "Restricted to Premium Members only. (!store)"

--ent_map
entities.map.printName                      = "Map"
entities.map.cityHall                       = "City Hall"
entities.map.superMarket                    = "Supermarket"
entities.map.policeStation                  = "Police Station"
entities.map.fireStation                    = "Fire Station"
entities.map.clothingStore                  = "Clothing Store"
entities.map.generalHospital                = "General Hospital"
entities.map.modShop                        = "Mod Shop"
entities.map.realtyOffice                   = "Realty Office"
entities.map.realEstateOffice               = "Real Estate Agency"
entities.map.carDealership                  = "Car Dealership"
entities.map.cityBank                       = "City Bank"
entities.map.miningRavine                   = "Mining Ravine"
entities.map.transitCentre                  = "Transit Center"
entities.map.impoundLot                     = "Impound Lot"
entities.map.vehicleImpound                 = "Vehicle Impound"
entities.map.fishingDock                    = "Fishing Dock"
entities.map.biteRestaurant                 = "Bite Restaurant"
entities.map.woodcuttingForest              = "Woodcutting Forest"
entities.map.gasStation                     = "Gas Station"
entities.map.gasStationCity                 = "Gas Station (City)"
entities.map.gasStationCountry              = "Gas Station (Country)"
entities.map.gasStationIndustiral           = "Gas Station (Industrial)"
entities.map.deliveryDepot                  = "Delivery Depot"
entities.map.drugAlley                      = "Drug Alley"
entities.map.cityMines                      = "City Mines"
entities.map.highEndMines                   = "High End Mines"
entities.map.fishingStore                   = "Fishing Store"
entities.map.casino                         = "Casino"
entities.map.monoRailStop                   = "Mono-Rail Stop"
entities.map.gunStore                       = "Gun Store"
entities.map.hardwareStore                  = "Hardware Store"
entities.map.woodCuttingPark                = "Woodcutting Park"
entities.map.marketStreet                   = "Market Street"
entities.map.garage                         = "Garage"
entities.map.cityGarage                     = "City Garage"
entities.map.alderGarage                    = "Alder Garage"
entities.map.miningRow                      = "Mining Row"
entities.map.woodcuttingRow                 = "Woodcutting Row"
entities.map.resourceHaven                  = "Resource Haven"
entities.map.mineshaft                      = "Mineshaft"
entities.map.hospital                       = "Hospital"
entities.map.mechanicShop                   = "Mechanic Shop"
entities.map.automobileDealer               = "Automobile Dealer"
entities.map.drugDealers                    = "Drug Dealers"
entities.map.truckersCo                     = "Truckers Co."
entities.map.deliveryService                = "Delivery Services"
entities.map.mexiGrill                      = "Mexi Grill"
entities.map.ronnies                      	= "Ronnie's"
entities.map.bank                           = "Bank"
entities.map.lake                           = "Lake"
entities.map.truenorthCounty                = "Truenorth County"
entities.map.pineCounty                		= "Pine County"
entities.map.monofordCounty					= "Monoford County"
entities.map.truenorthCentral               = "Truenorth Central"
entities.map.taxiCompany                    = "Taxi Company"
entities.map.publicworks 					= "Public Works"
entities.map.openMap                        = "Open Map!"
entities.map.MonoMap                        = "MONO-MAP"
entities.map.legend                         = "LEGEND"
entities.map.you                            = "You"
entities.map.setDestination                 = "Set Destination"
entities.map.mcc                			= "Convention Center"
entities.map.applianceStore                	= "Appliance Store"

--ent_metal_detector
entities.metalDetector.printName            = "Metal Detector"

--ent_police_database
entities.policeDatabase.printName           = "Police Database"
entities.policeDatabase.accessPoliceDB      = "Access Police Database"
entities.policeDatabase.noAccess            = "No Access"

--ent_police_jailer
entities.policeJailer.printName             = "Police Computer"
entities.policeJailer.accessPolicePC        = "Access Police Computer"
entities.policeJailer.noAccess              = "No Access"

--ent_prisoner_toiler
entities.prisonerToilet.printName           = "Prison Toilet Brush"
entities.prisonerToilet.digIn               = "Dig in for a surprise"
entities.prisonerToilet.ewDisgusting        = "Ew, Disgusting!"
entities.prisonerToilet.looksEmpty          = "The toilet looks empty..."
entities.prisonerToilet.noWay               = "Ugh, I cannot imagine even touching that toilet!"
entities.prisonerToilet.cannotSearch        = "You cannot search toilets for lockpicks when there are no Prison Guards on duty."
entities.prisonerToilet.alreadyHave         = "I already have a lockpick, why would I do it?"
entities.prisonerToilet.startedDigging      = "%s started digging through toilet's contents."
entities.prisonerToilet.dangIt              = "I've found a lockpick but it slipped away into the toilet... Dang it."
entities.prisonerToilet.dugUpLockpick       = "You have dug up a lockpick! Neat!"
entities.prisonerToilet.foundNothing        = "You found nothing."
entities.prisonerToilet.diggingThrough      = "DIGGING THROUGH"

--ent_prisoner_task
entities.prisonerTask.printName				= "Prisoner Task"
entities.prisonerTask.notif.notYou			= "That's not for you, luckily."
entities.prisonerTask.notif.justDone 		= "This task has just been done. Find another one, or wait a little while."
entities.prisonerTask.notif.cooldown		= "Wait %s seconds before doing another prison task."

entities.prisonerTask.laundry.hintText		= "Do the dirty laundry."
entities.prisonerTask.laundry.actionText	= "Doing laundry..."
entities.prisonerTask.dishes.hintText 		= "Clean some filthy dishes."
entities.prisonerTask.dishes.actionText 	= "Doing dishes..."
entities.prisonerTask.cooking.hintText 		= "Cook some nasty meals."
entities.prisonerTask.cooking.actionText 	= "Cooking..."
entities.prisonerTask.fallbackHintText		= "Perform prison task."

--ent_respawner
entities.respawner.printName                = "Equipment Locker"
entities.respawner.cannotOpen               = "You are not able to open this locker."
entities.respawner.readyToUse               = "Ready to use"
entities.respawner.seconds                  = "seconds"
entities.respawner.noAccess                 = "No Access"

--ent_source_base
entities.sourceBase.printName               = "Resource Base"
entities.sourceBase.gatherWeaponName        = "Weapon"
entities.sourceBase.cannotGather            = "You cannot gather while employed!"
entities.sourceBase.noMoreResources         = "This source doesn't have more resources"
entities.sourceBase.skillLevelRequired      = "Level %s %s required!"
entities.sourceBase.source                  = "%s Source"
entities.sourceBase.sourceAvailable         = "Available %s: %s%%"
entities.sourceBase.level                   = "Level %s"

--ent_source_rock
entities.sourceRock.printName               = "Rock"
entities.sourceRock.resourceName            = "Stone"
entities.sourceRock.cannotMine              = "You cannot mine while employed!"
entities.sourceRock.noMoreStone             = "This rock doesn't have more stone"
entities.sourceRock.higherSkillLevel        = "You need a higher skill level to mine this ore!"

--ent_source_tree
entities.sourceTree.printName               = "Tree"
entities.sourceTree.resourceName            = "Wood"
entities.sourceTree.gatherWeaponName        = "Axe"
entities.sourceTree.messageEmployed         = "You cannot log while employed!"
entities.sourceTree.noMoreWood              = "This tree doesn't have more wood"

--ent_spawned_furniture
entities.spawnedFurniture.printName         = "Furniture"

--ent_spawned_prop
entities.spawnedProp.printName              = "Prop"
entities.spawnedProp.pickingUp              = "PICKING UP"
entities.spawnedProp.health                 = "Health: %s%%"

--ent_spray_plate
entities.sprayPlate.printName               = "Spray Plate"
entities.sprayPlate.sprayingTag             = "SPRAYING TAG"
entities.sprayPlate.pleaseWait              = "Please wait until applying spray again, it's on cooldown."
entities.sprayPlate.tagYourCrew             = "Tag your Crew's identity to the wall"

--ent_tapedummy
entities.tapeDummy.printName                = "Tape Dummy Object"

--ent_towtruck_bed
entities.towtruck.reelTheWinch				= "Reel the winch in before locking the bed."
entities.towtruck.slideTheBack				= "Slide the bed back before locking the bed."
entities.towtruck.attachToSelf				= "You wish."

--ent_unarrest_trigger
entities.unarrestTrigger.printName          = "Unarrest Trigger"

--ent_use_trigger
entities.useTrigger.printName               = "Use Trigger"

--ent_vehicle_oopvs  ( specifies it's disabled but adding it in just in case )
entities.vehicleOOPVS.printName             = "Vehicle OOPVS View"

--ent_vehicle_showcase
entities.vehicleShowcase.printName          = "Vehicle Showcase"

--ent_voting
entities.votingComputer.printName           = "Voting Computer"
entities.votingComputer.cannotVote          = "You cannot vote until next the election period."
entities.votingComputer.mayorRating         = "Mayor's Rating: "
entities.votingComputer.generalTaxes        = "General Taxes:"
entities.votingComputer.yourJobTaxes        = "Your Job Taxes:"
entities.votingComputer.aveJobTaxes         = "Ave. Job Taxes:"

--ent_wheel_repair_kit
entities.wheelRepairKit.printName           = "Wheel Repair Kit"

--mono_camera
entities.monoCamera.printName               = "Personal CCTV Camera"

--monolith_npc
entities.monoNPC.printName                  = "NPC"

--npc_injuredciv
entities.injuredCiv.printName               = "Injured Citizen"
entities.injuredCiv.innocentCivilian        = "This civilian is innocent."

--npc_robber
entities.npcRobber.criminal 				= "Criminal"
entities.npcRobber.mustSurrenderFirst       = "The robber must have surrendered first."

--sent_base_gonzo
entities.baseGonzo.printName                = "Drug"

-- the_weed_foil
entities.weedfoil.printName                 = "Foil machine"

-- the_weed_foildryhash
entities.weedfoildryhash.printName          = "Foil Dry Hash"
entities.weedfoildryhash.foiledDryHash      = "Foiled Dry Hash"
entities.weedfoildryhash.consumeWeed        = "You can't consume foiled weed."

-- the_weed_hash
entities.weedhash.printName                 = "Wet Hash"
entities.weedhash.wetWeed                   = "You can not consume the weed yet, you need to dry it."

-- the_weed_jar
entities.weedjar.printName                  = "Jar"

-- the_weed_microwave
entities.weedmicrowave.printName            = "Microwave"
entities.weedmicrowave.connectApp           = "You need to connect this appliance's plug to a Residential Socket."

-- the_weed_plant
entities.weedplant.printName                = "Pot"
entities.weedplant.soilInPot                = "You have put some soil into the pot."
entities.weedplant.soilPack                 = "You need to have a soil pack in your inventory to use this."
entities.weedplant.plantedSeed              = "You have planted a seed into the pot."
entities.weedplant.marjIsBigSeed            = "You need to have a marijuana seed pack in your inventory to use this."
entities.weedplant.harvestSuccess           = "You succesfully harvested the weed plant."
entities.weedplant.badTeam                  = "You are not the correct team to harvest the plant."
entities.weedplant.somethingStrange         = "You smell something strange. You take a moment to clear your throat."

-- the_weed_tent
entities.weedtent.printName                 = "Tent"
entities.weedtent.sockResi                  = "You need to connect this appliance's plug to a Residential Socket."
entities.weedtent.toggleSystem              = "You have toggled the system "
entities.weedtent.on                        = "on."
entities.weedtent.off                       = "off."
entities.weedtent.bigBarrel                 = "You need a big barrel of water to do this."
entities.weedtent.refillWater               = "You have refilled the water container."
entities.weedtent.waterSys                  = "Watering System ("
entities.weedtent.ON                        = "ON)"
entities.weedtent.OFF                       = "OFF)"
entities.weedtent.waterLeft                 = "Water left"
entities.weedtent.liters                    = " liters"
entities.weedtent.refill                    = "Press E on the monitor to refill"
entities.weedtent.altE                      = "Hold ALT and E on the monitor to toggle watering"

--ent_dirt_pile
entities.dirtpile.printName                 = "Dirt Pile"
entities.dirtpile.harvesting                = "HARVESTING"
entities.dirtpile.stealing                  = "STEALING"
entities.dirtpile.noInventorySpace          = "You do not have any inventory space to harvest this!"
entities.dirtpile.soilQualityNoBueno        = "The soil quality is so bad, that you have destroyed your plot by accident!"
entities.dirtpile.stealingAttempt           = "/me attempts to steal crops from a dirt pile." -- NOTE: Keep the /me prefix, this formats the text when typed in the chat
entities.dirtpile.stealingSeeds             = "STEALING\nSEEDS"

--ent_easter_egg
entities.easteregg.printName                = "Easter Egg"
entities.easteregg.claimPrize               = "Press\n\n\nAnd claim your prize!"
entities.easteregg.nothing                  = "nothing"
entities.easteregg.youReceived              = "You received %s"
entities.easteregg.congratsYouReceived      = "Congratulations! You received\n%s"

--ent_wol_machine
entities.wolmachine.printName               = "Wheel Of Luck"
entities.wolmachine.congratsYouWon          = "Congratulations! You just won %s"
entities.wolmachine.chatPrefix              = "[Wheel Of Money]"
entities.wolmachine.justWonTheJackpot       = "just won the jackpot of"
entities.wolmachine.onTheWheelOfLuck        = "on the Wheel Of Luck!"
entities.wolmachine.cantWhenWorking         = "You can't use that machine when you're working!"
entities.wolmachine.currDisabled            = "The Wheel Of Luck is disabled right now."
entities.wolmachine.pleaseWait              = "Please wait %s seconds before switching machines."
entities.wolmachine.pleaseWaitTurn          = "Please wait for %s to finish their turn or find a new machine."
entities.wolmachine.cannotAfford            = "You cannot afford to use this machine, you need at least %s."

--ent_wol_reel
entities.wolreel.printName                  = "Wheel Of Luck Reel"

--the_weed_dirt
entities.theweeddirt.printName              = "Soil Pack"
entities.theweeddirt.chargesLeft            = "%s - %s Charges Left"

--the_weed_dryhash
entities.theweeddryhash.printName           = "Hash"

--ent_pplant_barrel
entities.pplantBarrel.printName           	= "Power Plant Barrel"
entities.pplantBarrel.extractAttempt        = "Siphoning chemicals..."
entities.pplantBarrel.empty           		= "This barrel seems to be temporarily empty."
entities.pplantBarrel.startExtract			= "%s started siphoning chemicals out of the barrel."
entities.pplantBarrel.siphon				= "To siphon chemicals"
entities.pplantBarrel.noSiphon				= "You do not have a siphon pump."
entities.pplantBarrel.police				= "You cant do this."
entities.pplantBarrel.notEnough				= "Warning: Not enough security online"
entities.pplantBarrel.broken				= "Siphon pump is broken."

-- Monoford keycard entities
entities.keypad.startSwiping				= "%s swipes a keycard on the electronic door lock."
entities.keypad.swipeAttempt				= "Swiping keycard..."

-- mailbox shit

entities.mailboxlocker.printName			= "Mailbox Locker"
entities.mailbox.printName					= "Mailbox"
-- DHeist entities
entities.shipCrate.foundX 					= "You found a(n) %s in the crate!"
entities.shipCrate.foundBP 					= "You found a %s Blueprint in the crate!"
entities.robEntity.log.received				= "%s received a(n) %s (x%s)"

-- Meth entities

entities.zmlab2.table.pickupFailed 			= "You can't pick up this table while it has items on it."

-- Automatic water sprinkler
entities.water_sprinkler.name 				= "Automatic Water Sprinkler"
entities.water_sprinkler.description 		= "Water your plants, automatically™"

-- Leaderboard entities
entities.leaderboard.printName = "Money Leaderboard"
entities.leaderboard.title = "Richest Players"

