--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

--[[ @NOTE: Addon language configurations locations for the following addons are diplayed:
		- CasinoKit: casinokit/lua/casinokit/custom/langs
			- Also for each sub addon
		- DHeists: dheists_/lua/dheists/config/language
		- gsigns: gsigns/lua/metasign_config.lua
		- RRL: rrl/lua/rrl/config
]]

-- @TODO:
		-- firetruck-ladder
		-- blue's casino stuff
		-- fishingmod
		-- raptor-ui
		-- [any SWEP PrintNames]
		-- [any AddNotification]
		-- rrl
		-- v-fire
		-- Zero's arcade pack
		-- Check GetJobCategory again.

Monolith.LocaleBuilder( "en", true )

-- CasinoKit
addon.casino.notif.allowedMax5					= "You are only allowed to place a maximum of 5 bets per table."

-- Tow Truck Driver
addon.towtruck.notif.noVehicle					= "There is no vehicle behind the tow truck!"
addon.towtruck.notif.attached 					= "Vehicle successfully attached!"
addon.towtruck.notif.unattached					= "Vehicle successfully unattached!"
addon.towtruck.notif.lookTowTruck				= "You must be looking at a tow truck to attach nearby vehicles!"

-- Firetruck Ladder
addon.ftladder.notif.needFirefighter		= "You need to be a firefighter to do this!"

-- Fishingmod
addon.fishing.notif.hookedCaught				= "You hooked a fish! It's now in your inventory."
addon.fishing.notif.hookedAway 					= "You hooked a fish, but it got away."
addon.fishing.notif.noSpace						= "You do not have space in your inventory!"
addon.fishing.notif.noCatch						= "You didnt catch anything."
addon.fishing.notif.noBait						= "You have no more bait."
addon.fishing.notif.pleaseWait					= "Please wait %s to cast again."
addon.fishing.notif.rmbReel						= "[RMB] REEL IN"
addon.fishing.notif.gotScrap					= "You found some scrap metal!"
addon.fishing.notif.exoticFishCaught 			= "You caught some exotic Fish!"

-- GSigns
addon.gsigns.notif.err 							= "An error occured. Please check your console!"

addon.gsigns.action.pickingUp 					= "PICKING UP"

-- RRL
addon.rrl.notif.removedWarning 					= "You removed a warning from %s"
addon.rrl.notif.warnedBy						= "You were warned by %s for %s, you not have %s warning points."
addon.rrl.notif.willKicked						= "You will be kicked if you receive %s more points"
addon.rrl.notif.willKicked.any 					= "any"
addon.rrl.notif.youWarned 						= "You warned %s for %s."
addon.rrl.notif.reportDoesntExist 				= "The report you are trying to access does not exist."
addon.rrl.notif.noAccess						= "You do not have access to this feature"
addon.rrl.notif.removedReportTicket 			= "You removed a report ticket (#%s)"
addon.rrl.notif.supportTicketRemoved			= "Your support ticket was removed (#%s)"
addon.rrl.notif.cantCloseOwnReport				= "You cannot close your own report."
addon.rrl.notif.reportTicketClosed 				= "Your report ticket was closed (#%s)"
addon.rrl.notif.cannotOpenOwnReport				= "You cannot open your own report"
addon.rrl.notif.yourReportTicketOpened 			= "Your report ticket was opened (#%s)"
addon.rrl.notif.alreadyClaimed 					= "You cannot claim this report as it is already claimed"
addon.rrl.notif.cannotClaimOwn 					= "You cannot claim your own report."
addon.rrl.notif.staffMemberClaimed 				= "A staff member claimed your report ticket (#%s)"
addon.rrl.notif.claimedReportTicket				= "You claimed a report ticket (#%s)"
addon.rrl.notif.staffMemberUnclaimed			= "A staff member unclaimed your report ticket (#%s)"
addon.rrl.notif.youUnclaimed					= "You unclaimed a report ticket (#%s)"
addon.rrl.notif.submittedNewReport				= "You submitted a new report (#%s)"

-- Whomekit_media
addon.whomekit_media.notif.needConnect			= "You need to connect this appliance's plug to a residential socket."
addon.whomekit_media.notif.blacklist 			= "You are blacklisted from playing any media content."
addon.whomekit_media.log.startedPlaying 		= "%s started playing: %s on a media source. (%s)"

-- zcrga
addon.zcrga.notif.cantAfford					= "You can't afford this!"
addon.zcrga.notif.youWonX						= "You won %s!"
