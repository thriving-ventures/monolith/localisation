--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

Monolith.LocaleBuilder( "tr", true )

--[[
	SWEPS
--]]

-- Hands
sweps.handcuffsinhand.printName 			= "Kelepçeler"

sweps.handcuffsinhand.actions.beginCuffing 	= "%s kelepçeleri %s kişisinin bileklerine takmaya başlar."
sweps.handcuffsinhand.actions.placedCuffs	= "%s kelepçeleri %s kişisinin bileklerine takıyor."
sweps.handcuffsinhand.actions.handCuffing	= "%s\nKELEPÇELENİYOR"
sweps.handcuffsinhand.actions.removeCuffs	= "%s, kelepçeleri %s kişisinin bileklerinden çıkarır."

sweps.handcuffsinhand.gui.checkOwner 		= "Sahibi görmek için R tuşuna basın"
sweps.handcuffsinhand.gui.noOwner			= "Sahipsiz"

sweps.handcuffsinhand.log.cuffed			= "%s, %s kişisini kelepçeledi"
sweps.handcuffsinhand.log.unCuffed			= "%s, %s kişisinin kelepçesini açtı"

-- Restrained

sweps.handcuffsonplayer.printName 			= "Kısıtlı"

-- Bong
sweps.sentBong.gui.weed 					= "Ot"
sweps.sentBong.gui.quality					= "Kalite"

-- Zipties
sweps.zipties.printName 					= "Ziptie"

sweps.zipties.actions.beginZip 				= "%s, %s kişisini bağlamaya başlar."
sweps.zipties.actions.finishZip 			= "%s, %s kişisini başarıyla bağladı."
sweps.zipties.actions.zipTie 				= "%s\nBAĞLANIYOR"

sweps.zipties.log.ziptied					= "%s, %s kişisini bağladı"

-- Axe
sweps.axe.printName 						= "Balta"

-- Defib
sweps.defib.actions.stabalizing				= "STABİLİZE EDİLİYOR\n${name}"

sweps.defib.gui.dead 						= "Hasta ölmüş görünüyor."
sweps.defib.gui.stable						= "Zaten stabilize oldu!"
sweps.defib.gui.critical					= "Hasta stabilize olabilmesi için kritik olmalı!"
sweps.defib.gui.downtime					= "Buraya yeterince çabuk ulaştın! Bu nedenle hasta büyük yaralanmalara maruz kalmadı"
sweps.defib.gui.hospitalwarning				= "Görünüşe göre stabilize edilmiş, onu hastaneye götürün!"

sweps.defib.logger.revived 					= "%s canlandırdı: %s"

-- Axe
sweps.fireaxe.gui.assistance 				= "Yardımınız burada gerekli değildir!"

-- Keys
sweps.keys.printName 						= "Anahtarlar"

sweps.keys.gui.state						= "${state} arabanıza/kapılarınıza erişmesini istiyor musunuz?"
sweps.keys.gui.stateRevoke					= "izni iptal et"
sweps.keys.gui.stateGrant					= "izin ver"
sweps.keys.gui.access						= "İzin"
sweps.keys.gui.thisPerson					= "bu kişi"
sweps.keys.gui.yes							= "Evet"
sweps.keys.gui.no							= "Hayır"
sweps.keys.gui.revokeOwner					= "Sahipliği iptal ettiniz: "
sweps.keys.gui.grantOwner					= "Sahipliği verdiniz: "

--weapon_lockpick
sweps.lockpick.printName         = "Maymuncuk"

--weapon_lockpick
sweps.extinguisher.printName 		= "Yangın Söndürücü"

--weapon_mono_citizen_id
sweps.citizenid.printName        = "Kimlik Kartı"

--weapon_mono_department
sweps.departmentTablet.printName = "Departman Tableti"
sweps.departmentTablet.desc      = "Şehir denetimi için kullanınız" -- ? ...

--weapon_fireaxe
sweps.fireaxe.printName 		= "İtfaiyeci Baltası"

-- weapon_defibrillator
sweps.defibrillator.printName 	= "Defibrilatör"

--weapon_mono_fists
sweps.fists.printName            = "Eller"
sweps.fists.instructions         = "Sol Tık: Yumruk\nSağ Tık: Kapıyı Çal/Eşyayı Tut"
sweps.fists.purpose              = "Bir şeylere vurmaya, kapı çalmaya, eşyaları tutmayı sağlar."

-- Medkit
sweps.medkit.printName 						= "İlk Yardım"

sweps.medkit.gui.healing					= "İYİLEŞTİRİLİYOR"
sweps.medkit.gui.sterilizing 				= "YARALAR\nSTERİLİZE EDİLİYOR"
sweps.medkit.gui.cauterizing 				= "YARALAR\nYAKILIYOR"
sweps.medkit.gui.applyingBandages 			= "BANDAJ\nUYGULANIYOR"
sweps.medkit.gui.healingMe 					= "KENDİNİ\nİYİLEŞTİRİYORSUN"

--weapon_mono_police_id
sweps.policeid.printName   = "Polis Kimlik Kartı"

--weapon_mono_tablet
sweps.monotablet.printName = "Mono-Tablet"

--weapon_pickaxe
sweps.pickaxe.printName    = "Kazma"
sweps.pickaxe.instructions = "Sol tık: Salla - Sağ tık: İttir"

--weapon_radargun
sweps.radargun.printName   = "Hız Ölçer"
sweps.radargun.purpose     = "Sol tık ile hızı tara"

--weapon_ramdoor
sweps.ramdoor.printName = "Koçbaşı"
sweps.ramdoor.purpose = "Yardım"

--weapon_taser
sweps.taser.printName = "Elektroşok"

-- weapon_mono_vape
-- @TODO configure the flavor juices in the weapon_mono_vape.lua directly in the JuicyVapeJuices table
sweps.monovape.printName 					= "Elektronik Sigara"
sweps.monovape.gui.loadedFlavorJuice 		= "%s likit dolduruldu"

sweps.monovape.juices.dew					= "Mountain Dew"
sweps.monovape.juices.cheetos				= "Çitos"
sweps.monovape.juices.razzleberry			= "Razzleberry"
sweps.monovape.juices.banana				= "Muz"
sweps.monovape.juices.blacklicorice			= "Siyah Meyankökü"
sweps.monovape.juices.churro				= "Churro"
sweps.monovape.juices.skittles				= "Skittles"
sweps.monovape.juices.normal				= "Normal"

-- weapon_nightstick
sweps.nightstick.printName 					= "Cop"
sweps.nightstick.log.knockOut 				= "%s, %s kişisini polis copuyla yere serdi."

-- weapon_policetape
sweps.policetape.printName 					= "Polis Şeridi"

sweps.policetape.gui.distance				= "Mesafe"
sweps.policetape.gui.rightClickCancel		= "İptal etmek için sağ tıklayın."
sweps.policetape.gui.ropeCantBeThatLong 	= "Şerit bu kadar uzun olamaz!"
sweps.policetape.gui.pickedUpSegmentTape 	= "Şeridin bu bölümünü aldınız."
sweps.policetape.gui.clearedAllTapes 		= "Tüm polis şeritlerini temizledin."
sweps.policetape.gui.maxTapes 				= "Maksimum şerit sınırına ulaştınız!" -- @TODO money symbol line 139
sweps.policetape.log.hasPlacedRope 			= "%s, %s adresine bir polis şeridini yerleştirdi"
sweps.policetape.log.removedPoliceTape 		= "%s, %s adresindeki polis şeridini kaldırdı"
sweps.policetape.log.clearedAllTapes		= "%s tüm polis şeritlerini sildi."

-- weapon_radargun
-- @TODO maybe we should add the speed unit in this file

-- weapon_ticket
sweps.weaponticket.printName 					= "Ceza Defteri"
sweps.weaponticket.purpose 						= "Cezaları yazman için bir defter"

sweps.weaponticket.gui.cantGiveTicketToPlayer = "Bu oyuncuya %s saniye daha ceza yazamazsınız."
sweps.weaponticket.gui.cantGiveTicket		  = "Başka bir %s saniye boyunca ceza yazamazsınız."
sweps.weaponticket.gui.officierIssuedIt 	  = "Bunu Veren Memur"
sweps.weaponticket.gui.timeSinceIssued		  = "Veriliş tarihi"
sweps.weaponticket.gui.lawyer 				  = "Avukat"
sweps.weaponticket.gui.selectTicket 		  = "Bir ceza seçin"
sweps.weaponticket.gui.police 				  = "Polis"
sweps.weaponticket.gui.minutesAgo 			  = "%s dakika önce"
sweps.weaponticket.gui.resolve				  = "Çöz"
sweps.weaponticket.gui.ticket				  = "Ceza"
sweps.weaponticket.gui.payTicket			  = "Bu cezayı öde ($%s)"
sweps.weaponticket.gui.pay					  = "Öde"
sweps.weaponticket.gui.deny					  = "Reddet"
sweps.weaponticket.gui.appealTicketIfUnfair   = "Adil olmadığını düşünüyorsan cezaya itiraz et."
sweps.weaponticket.gui.smallViolation 		  = "İlk/Küçük İhlal"
sweps.weaponticket.gui.repeated 			  = "Tekrarlanan/Orta"
sweps.weaponticket.gui.excessive			  = "Sürekli/Şiddetli"
sweps.weaponticket.gui.appeal 				  = "Temyiz"
sweps.weaponticket.gui.amountToPay 			  = "Ödenecek Miktar" -- @TODO money symbol line 246
sweps.weaponticket.gui.ticketsFromHim 		  = "Ondan cezalar"
sweps.weaponticket.gui.insertText 			  = "<Not ekle>"
sweps.weaponticket.gui.beMoreDescriptive 	  = "Nedeninizle daha açıklayıcı olmalısınız."
sweps.weaponticket.gui.insertNumber 		  = "Bir sayı girmelisin!"
sweps.weaponticket.gui.ok 					  = "Tamam"
sweps.weaponticket.gui.cantFineMore 		  = "4.500₺'den fazlasını yazamazsınız!" -- @TODO maybe adding this to a configuration or something like that line 313 and 227
sweps.weaponticket.gui.cantFineMoreThan		  = "%s'den fazla ceza yazamazsın!"
sweps.weaponticket.gui.areYouSure 			  = "Bu bileti oluşturmak istediğinizden emin misiniz?"
sweps.weaponticket.gui.cancel 				  = "İptal"
sweps.weaponticket.gui.accept 				  = "Kabul et"
sweps.weaponticket.gui.id 					  = "ID"
sweps.weaponticket.gui.kind 				  = "Çeşit"
sweps.weaponticket.gui.invalidReason		  = "Geçersiz neden."
sweps.weaponticket.gui.targetName			  = "İsim"

-- weapon_trowel
sweps.weapontrowel.printName 				  = "Bahçe Malası"
sweps.weapontrowel.gui.cantPlantHere 		  = "Buraya bitki dikemezsin."
sweps.weapontrowel.gui.tooClose 			  = "Bir şeye çok yakın ekmeye çalışıyorsun!"

-- weapon_watercan
sweps.watercan.printName 	= "Sulama Kabı"
sweps.watercan.purpose 		= "Ekinlerini sulamak ve yetiştirmek için kullan"


--[[
	ENTITIES
--]]

-- areacreator
tools.areacreator.name = "Area Creator"
tools.areacreator.notDefined = "Not Defined"

-- nodecreator
tools.nodecreator.name = "Node Creator"
tools.nodecreator.instructions = "LMB: Create node - RMB: Set Path"
tools.nodecreator.selectNode = "Select another node"

-- permaprops
tools.permaprops.name = "PermaProps"
tools.permaprops.description = "Save a props permanently"
tools.permaprops.instructions = "LeftClick: Add RightClick: Remove Reload: Update"
tools.permaprops.adminCan = "Admin can touch permaprops"
tools.permaprops.adminCant = "Admin can't touch permaprops !"
tools.permaprops.superAdminCan = "SuperAdmin can touch PermaProps"
tools.permaprops.superAdminCant = "SuperAdmin can't touch permaprops !"
tools.permaprops.onlySA = "Only Super Admin can touch permaprops"
tools.permaprops.inValidEnt = "That is not a valid entity !"
tools.permaprops.isPlayer = "That is a player !"
tools.permaprops.already = "That entity is already permanent !"
tools.permaprops.saved = "You saved %s with model %s to the database."
tools.permaprops.erased = "You erased %s with a model of %s from the database."
tools.permaprops.reload = "You have reload all PermaProps !"
tools.permaprops.updated = "You updated the %s you selected in the database."
tools.permaprops.saveProps = "Save a props for server restarts\nBy Malboro"
tools.permaprops.header = "------ Configuration ------"
tools.permaprops.header2 = "-------- Functions --------"
tools.permaprops.removeAll = "Remove all PermaProps"
tools.permaprops.erasedAll = "You erased all props from the map"

-- propertycreator
tools.propertycreator.name = "Property Creator"
tools.propertycreator.desc = "Edit The Property Data"
tools.propertycreator.propertyName = "Name: "
tools.propertycreator.hammerIdentifier = "Hammer ID: "
tools.propertycreator.propertyUID = "UID: "
tools.propertycreator.propertyCategory = "Category: "
tools.propertycreator.propertyPrice = "Price: "
tools.propertycreator.propertyJobCat = "Job Category: "
tools.propertycreator.propertyElectricMode = "Electricity Node: "
tools.propertycreator.noDataGiven = "No data is given"
tools.propertycreator.noDataCamGiven = "No cam data is given"
tools.propertycreator.noDataBoundGiven = "No boundarie data is given"
tools.propertycreator.noDataSendGiven = "No send data is given"
tools.propertycreator.noDataIDGiven = "No id is given"
tools.propertycreator.noDataNameGiven = "No name is given"
tools.propertycreator.noDataPriceGiven = "No price is given"
tools.propertycreator.noDataCatGiven = "No category is given"
tools.propertycreator.noDataJCatGiven = "No jobCategory given on unownable property"
tools.propertycreator.savedCon = "Saved %s as existing configuration"
tools.propertycreator.failedSave = "Failed to save %s as existing configuration. ERROR: "
tools.propertycreator.noError = "no error"
tools.propertycreator.saved = "Saved "
tools.propertycreator.reload = "Reload: Open Property Editor"
tools.propertycreator.leftC = "Left-Click: "
tools.propertycreator.rightC = "Right-Click: "
tools.propertycreator.none = "None"

tools.propertycreator.gui.propertyEditor = "Property Creator"
tools.propertycreator.gui.editBound = "Edit Boundaries"
tools.propertycreator.gui.setMaxPos = "Set Max Pos"
tools.propertycreator.gui.setMinPos = "Set Min Pos"
tools.propertycreator.gui.editDoors = "Edit Doors"
tools.propertycreator.gui.noDoorFound = "No door found at hit location. Make sure you are aiming at the door"
tools.propertycreator.gui.addedDoor = "Added door to property"
tools.propertycreator.gui.unableToRmove = "Unable to remove door position from property. Door position not found in property door table"
tools.propertycreator.gui.addDoor = "Add Door"
tools.propertycreator.gui.removeDoor = "Remove Door"
tools.propertycreator.gui.editCam = "Edit Camera"
tools.propertycreator.gui.setCam = "Set Camera Position"
tools.propertycreator.gui.editPower = "Edit Power Outlets"
tools.propertycreator.gui.addOutlet = "Add Outlet"
tools.propertycreator.gui.removeOutlet = "Remove Outlet"
tools.propertycreator.gui.editBins = "Edit Trash Bins"
tools.propertycreator.gui.addBin = "Add Bin"
tools.propertycreator.gui.removeBin = "Remove Removebin"
tools.propertycreator.gui.editPrisoner = "Edit Prisoner Toilets"
tools.propertycreator.gui.addPrisoner = "Add Prisoner Toilet"
tools.propertycreator.gui.removePrisoner = "Remove Prisoner Toilet"
tools.propertycreator.gui.editTask = "Edit Prisoner Tasks"
tools.propertycreator.gui.addTask = "Add Prisoner Task"
tools.propertycreator.gui.removeTask = "Remove Prisoner Task"
tools.propertycreator.gui.saveProperty = "Save Property Changes"
tools.propertycreator.gui.deselectProperty = "Exit Property"
tools.propertycreator.gui.addChildren = "Add Children Houses"
tools.propertycreator.gui.setParentID = "Set The Parent ID"
tools.propertycreator.gui.putParentID = "Put the Parent ID here."
tools.propertycreator.gui.setChildrenCount = "Set The Children Count"
tools.propertycreator.gui.setChildrenCountBelow = "Set the children count of the properys below."
tools.propertycreator.gui.editParentID = "Edit Parent ID"
tools.propertycreator.gui.putNewParentID = "Put the new Parent ID here."
tools.propertycreator.gui.quickPropertyPicker = "Quick Property Picker"
tools.propertycreator.gui.automaticDoors = "Automatically Set Doors"

-- textscreen
tools.textscreen.name = "3D2D Textscreen"
tools.textscreen.desc = "Create a textscreen with multiple lines, font colours and sizes."
tools.textscreen.instructions = "Left Click: Spawn a textscreen Right Click: Update textscreen with settings"
tools.textscreen.undoneText = "Undone textscreen"
tools.textscreen.textScreens = "Textscreens"
tools.textscreen.cleanedUpText = "Cleaned up all textscreens"
tools.textscreen.hitLimit = "You've hit the textscreen limit!"

tools.textscreen.gui.reset = "Reset all"
tools.textscreen.gui.resetColors = "Reset colors"
tools.textscreen.gui.resetSizes = "Reset sizes"
tools.textscreen.gui.resetText = "Reset textboxes"
tools.textscreen.gui.resetEverything = "Reset everything"
tools.textscreen.gui.resetLine = "Reset line"
tools.textscreen.gui.resetAllLines = "Reset all lines"
tools.textscreen.gui.line = "Line "
tools.textscreen.gui.fontColor = " font color"
tools.textscreen.gui.fontSize = "Font size"

--[[
	ENTITIES
--]]


-- ent_dumpster
entities.dumpster.waitBeforeDiving = "Tekrar çöpü karıştırmadan önce %s beklemeniz gerekir."
entities.dumpster.dumpsterSearched = "Bu çöpü zaten karıştırdın. %s Beklemelisin"
entities.dumpster.foundNothing = "Çöpten bir şey çıkmadı."
entities.dumpster.foundX = "Çöpten %s buldun!"
entities.dumpster.dumpsterDiving = "Çöpü karıştırıyorsun..."

-- sent_workbench
entities.workbench.printName                = "Çalışma Masası"

-- sent_woodworkbench
entities.woodworkbench.printName            = "Odun İşleme Masası"

-- sent_textscreen
entities.textscreen.printName               = "Metin Ekranı"
entities.textscreen.editTextTitle           = "Metin Ekranını Düzenle"
entities.textscreen.editColors              = "Renkleri Düzenle"
entities.textscreen.abort                   = "İptal"
entities.textscreen.setColor                = "Renk Ayarla"
entities.textscreen.colorChanged            = "Metin ekranının rengi değiştirildi."
entities.textscreen.edit                    = "Düzenle"
entities.textscreen.textLengthError         = "Metin çok kısa/uzun (<2> 18)"
entities.textscreen.pressEToChange          = "Değiştirmek için E tuşuna basın"

-- sent_stretcher
entities.stretcher.printName                = "Sedye"
entities.stretcher.patientStabilise         = "Bunu yapmak için önce hasta stabilize edilmelidir!"
entities.stretcher.ledByAnyone              = "Sedye bunu yapmak için hiç kimse tarafından yönlendirilmemelidir!"
entities.stretcher.strappedOntop            = "%s, %s gövdesini bir sedyenin üstüne bağladı"
entities.stretcher.unstrapped               = "%s %s kişisinin gövdesini bir sedyeden ayırdı."
entities.stretcher.sameVehicle              = "Sedyeyi geldiği araca koymalısın!"
entities.stretcher.backDoorClosed           = "Arka kapılar kapalı! Önce onları aç."
entities.stretcher.notEMSPolice             = "EMS/Polis değilseniz bunu yapamazsınız."
entities.stretcher.notYourVehicle           = "Bu senin araban değil!"
entities.stretcher.notYourAmbulance         = "Bu senin ambulansın değil!"
entities.stretcher.blockingDeploy           = "Konuşlanma konumunu engelleyen bir şey var!"

-- sent_road_sign
entities.roadsign.printName                 = "Polis Yol Levhası"
entities.roadsign.policeDepartment          = "Polis Departmanı"
entities.roadsign.mustBePolice              = "Bu işareti düzenlemek için Polis üyesi olmalısınız"
entities.roadsign.pickingUp                 = "TOPLAMA"
entities.roadsign.policeSign                = "Polis İşareti"
entities.roadsign.update                    = "Güncelleme"

-- sent_road_sign_small
entities.roadsignsmall.printName            = "Polis yol işareti - küçük"
entities.roadsignsmall.roadClosed           = "YOL KAPALI"
entities.roadsignsmall.memberPolice         = "Bu işareti düzenlemek için Polis üyesi olmalısınız"
entities.roadsignsmall.policeSign           = "Polis İşareti"
entities.roadsignsmall.update               = "Güncelleme"

entities.roadsignsmall.log.changeText		= "%s changed a roadsign's text to: %s."

-- sent_residential_extension_outlet
entities.extensionoutlet.printName          = "Konut Uzatma Çıkışı"
entities.extensionoutlet.errorAttempt       = "Bir sent_residential_extension_outlet üzerinde SetBreaker() kullanmaya çalışıldı"

-- sent_repair_bench
entities.repairbench.printName              = "Tamir Tezgahı"

-- sent_property_bin
entities.propertybin.printName              = "Mülk Kutusu"
entities.propertybin.garbageCan             = "Çöp Tenekesi"
entities.propertybin.percentFull            = "% dolu"

-- sent_power_source
entities.powersource.printName              = "Mülkiyet Güç Kaynağı"

-- sent_power_outlet
entities.poweroutlet.printName              = "Priz"

-- sent_power_converter
entities.powerconverter.printName           = "Güç Dönüştürücü (İki Yönlü)"

-- sent_pot
entities.pot.printName                      = "Tencere"
entities.pot.weedBag                        = "Ot Torbası"
entities.pot.useOtherPots                   = "Başka bireyin saksılarını kullanamazsınız."
entities.pot.dehydrated                     = "Susuz"
entities.pot.thirsty                        = "Susuz"
entities.pot.neutral                        = "Nötr"
entities.pot.healthy                        = "Sağlıklı"
entities.pot.insertSoil                     = "Toprak Ekle"
entities.pot.insertSeed                     = "Tohum Ekleme"
entities.pot.harvest                        = "Hasat"
entities.pot.properSeeds                    = "Dikilecek uygun tohumunuz yok."
entities.pot.gardeningSoil                  = "Kullanacağınız bahçe toprağı yok."
entities.pot.plantedSeed                    = "Tencereye bir tohum ekledin."
entities.pot.filledPot                      = "Tencereyi bahçe toprağıyla doldurdun."
entities.pot.gardeningPot                   = "Bahçe Saksısı"
entities.pot.alreadySoil                    = "Tencerede zaten toprak var"
entities.pot.soilAvailable                  = "You have %s soil available to use"
entities.pot.seedsAvailable                 = "You have %s seed%s available to plant"
entities.pot.plant                          = "Bitki"
entities.pot.properSeeds                    = "Dikilecek uygun tohumunuz yok."
entities.pot.addSoil                        = "Toprak Ekle"

-- sent_plug
entities.plug.printName                     = "Fiş"

-- sent_money
entities.money.printName                    = "Para"
entities.money.youGrabbed                   = "Yerden aldın ₺"
entities.money.pickedUp                     = " aldı "

-- sent_metalworkbench
entities.metalworkbench.printName           = "Metal İşleme Masası"

-- sent_metalworkbench
entities.metalworkbench.printName           = "Metal İşleme Masası"

-- sent_lsd
entities.lsd.printName                      = "LSD"

-- sent_lsd_pyro
entities.lsdpyro.printName                  = "Bunsen brülörü"

-- sent_lsd_powder
entities.lsdpowder.printName                = "LSD Toz malzemesi"

-- sent_lsd_paper
entities.lsdpaper.printName                 = "Macera Kağıdı"

-- sent_lsd_gas
entities.lsdgas.printName                   = "Gaz kutusu"

-- sent_lsd_freezer
entities.lsdfreezer.printName               = "Buzdolabı"
entities.lsdfreezer.holdPickUp              = "(Basılı Tut) Al "
entities.lsdfreezer.insertFlask             = "Bir şişe yerleştirin"
entities.lsdfreezer.takeOut                 = "Donuyorum! Beni dışarı çıkar"

-- sent_lsd_flask
entities.lsdflask.printName                 = "Flask"
entities.lsdflask.stage1                    = "Toz Ekle"
entities.lsdflask.stage2                    = "Isıya ihtiyacım var"
entities.lsdflask.stage3                    = "Brülörün tamamlanmasını bekleyin"
entities.lsdflask.stage4                    = "Sıvı ekleyin"
entities.lsdflask.stage5                    = "Beni nazikçe salla"
entities.lsdflask.stage6                    = "Sakin ol"
entities.lsdflask.stage7                    = "Beni bir kağıda koy"

-- sent_lsd_flank_support
entities.lsdflanksupport.printName          = "Şişe desteği"
entities.lsdflanksupport.fireStarted        = "[Yangın Görevi] LSD Ateşi başladı!"
entities.lsdflanksupport.insertBurner       = "Brülör yerleştirin"
entities.lsdflanksupport.insertGas          = "Bir gaz tüpü takın"
entities.lsdflanksupport.insertFlask        = "Bir şişe takın"
entities.lsdflanksupport.progress           = "İlerleme: "

-- sent_lsd_bottle
entities.lsdbottle.printName                = "LSD Şişe malzemesi"

-- sent_infinite_power_source
entities.infinitepower.printName            = "Sonsuz Güç Kaynağı"
entities.infinitepower.powerSource          = "Güç kaynağı"
entities.infinitepower.infinite             = "Sonsuz"

-- sent_fire_spark
entities.firespark.printName                = "İlaç"

-- sent_fire_source
entities.firesource.printName               = "Ateş"

-- sent_factory_furnace
entities.factoryfurnace.printName           = "Fırın"
entities.factoryfurnace.busy                = "Fabrika meşgul."
entities.factoryfurnace.takeAllComp         = "Bunu almadan önce tüm bileşenleri çıkarmanız gerekiyor."
entities.factoryfurnace.readyForUse         = "Kullanıma hazır"
entities.factoryfurnace.needsConveyor       = "Konveyöre ihtiyaç var"
entities.factoryfurnace.needsPower          = "Bir güç kaynağına ihtiyaç var"

-- sent_factory_crusher
entities.factorycrusher.printName           = "Kırıcı"
entities.factorycrusher.busyCrushing        = "Kırıcı bir öğeyi ezmekle meşgul."
entities.factorycrusher.takeAllBefore       = "Bunu almadan önce Konteyner'deki tüm eşyaları çıkarmalısın."
entities.factorycrusher.preciousMats        = "Kırıcı tarafından verilen değerli malzemeleri tutmak için bir kap."
entities.factorycrusher.turnOffNotif        = "Kırıcı bittikten sonra kapanacaktır."
entities.factorycrusher.crushing            = "Ezme"
entities.factorycrusher.insertChunks        = "Parçalar Ekle"
entities.factorycrusher.unplugged           = "Takılı değil"

-- sent_factory_conveyor
entities.factoryconveyor.printName          = "Taşıyıcı"
entities.factoryconveyor.factoryBusy        = "Fabrika meşgul."
entities.factoryconveyor.turnMeOn           = "Beni tahrik et"
entities.factoryconveyor.active             = "Aktif"
entities.factoryconveyor.secs               = "saniye"

-- sent_factory_cell
entities.factorycell.printName              = "Güç hücresi"

-- sent_factory_box
entities.factorybox.printName               = "Saklama kutusu"
entities.factorybox.containerHold           = "Fırın tarafından verilen fabrika öğelerini tutmak için bir kap."
entities.factorybox.takeAllItems            = "Almadan önce Saklama Kutusundaki tüm eşyaları çıkarmanız gerekiyor."

-- sent_factory_base
entities.factorybase.printName              = "Fabrika Üssü"

-- sent_drug_bag
entities.drugbag.printName                  = "Ot Torbası"

-- sent_door_charge
entities.doorcharge.printName               = "Kapı Şarjı"

-- sent_cooktable
entities.cooktable.printName                = "Pişirme ocağı"

-- sent_cocaine_stove
entities.cocainestove.printName             = "Elektrikli Soba"

-- sent_cocaine_pot
entities.cocainpot.printName                = "Tencere" -- @TODO: Change
entities.cocainpot.temperature              = "Sıcaklık: "
entities.cocainpot.waitAbout                = "%d saniye bekleyin"

-- sent_cocaine_packingbox
entities.cocainepackingbox.printName        = "Ambalaj Kutusu"
entities.cocainepackingbox.cocaLeaves       = "Koka yaprakları: "
entities.cocainepackingbox.boxFull          = "Kutu dolu"
entities.cocainepackingbox.packing          = "Paketleme"
entities.cocainepackingbox.notEnoughLeaves  = "Yeterli yaprak yok."
entities.cocainepackingbox.boxFullEmpty     = "Kutu dolu, lütfen kutuyu boşaltın."
entities.cocainepackingbox.noLeaves         = "Hiç yaprağın yok."

-- sent_cocaine_kerosene
entities.cocainkerosene.printName           = "Gazyağı"
entities.cocainkerosene.shakeIt             = "Salla: "
entities.cocainkerosene.readyToUse          = "Kullanıma hazır"

-- sent_cocaine_jerrycan
entities.cocainejerrycan.printName          = "Yakıt bidonu"
entities.cocainejerrycan.ready              = "Hazır"
entities.cocainejerrycan.pleaseWait         = "Lütfen %d saniye bekleyin"

-- sent_cocaine_gas
entities.cocainegas.printName               = "Gaz"
entities.cocainegas.filled                  = "Dolu:"

-- sent_cocaine_drafted
entities.cocainedrafted.printName           = "Hazırlanan Yapraklar"
entities.cocainedrafted.shakeIt             = "Salla: "
entities.cocainedrafted.ready               = "Kullanıma hazır"
entities.cocainedrafted.pleaseWait          = "Lütfen %d saniye bekleyin"

-- sent_cocaine_acid
entities.cocaineacid.printName              = "Sülfürik asit"
entities.cocaineacid.pleaseWait             = "Lütfen %d saniye bekleyin"
entities.cocaineacid.ready                  = "Kullanıma hazır"

-- sent_christmas_tree
entities.christmastree.printName            = "Noel ağacı"

-- sent_brewingbarrelbench
entities.brewingbarrelbench.printName       = "Bira Fıçısı"

-- sent_bountyboard
entities.bountyboard.printName              = "Ödül Kurulu"
entities.bountyboard.placeBounty            = "Ödül ver"
entities.bountyboard.name                   = "Ad"
entities.bountyboard.reward                 = "Ödül"
entities.bountyboard.dateAssigned           = "Atanma Tarihi"

-- sent_base_lsd
entities.baselsd.printName                  = "İlaç"

-- base_item_stackable
entities.baseitemstackable.printName        = "Temel öğe"
entities.baseitemstackable.beingArrested    = "Tutuklanırken eşya toplayamazsınız."
entities.baseitemstackable.inventorySpace   = "Envanterinizde bunu satın almak için yer yok!"
entities.baseitemstackable.boughtItem       = "Bir ürün satın aldınız. %s cüzdanınızdan çıkarıldı."
entities.baseitemstackable.shopFull         = "Bu dükkan zaten dolu!"
entities.baseitemstackable.dontHaveItem     = "Bu eşya envanterinde yok!"
entities.baseitemstackable.onlyHadThis      = "You only had %s of this item!"

--ent_atm_screen
entities.atmscreen.printName                = "ATM EKRANI"
entities.atmscreen.withdraw                 = "Çek"
entities.atmscreen.deposit                  = "Yatır"
entities.atmscreen.monoBank                 = "MONO-BANKA"
entities.atmscreen.balance                  = "Bakiye: "
entities.atmscreen.accountHolder            = "Hesap sahipleri"
entities.atmscreen.fiftyUSD                 = "50₺" -- idk what you expect me to use here
entities.atmscreen.fiveHundredUSD           = "500₺"
entities.atmscreen.tenThousandUSD           = "10000₺"
entities.atmscreen.clear                    = "C"
entities.atmscreen.lessThan                 = "<"
entities.atmscreen.USD                      = "₺"
entities.atmscreen.accessDenied             = "ATM erişimi reddedildi."
entities.atmscreen.insufficientFunds        = "Yeterli paranız yok."
entities.atmscreen.confirm                  = "Doğrula"

--ent_atm
entities.atm.printName                      = "ATM"

--ent_boat_base
entities.boatbase.printName                 = "Tekne Üssü"
entities.boatbase.containerDesc             = "Kargo bir teknenin ambarında tutmak için bir konteyner."

--ent_boat_motor_engine
entities.boatmotorengine.printName          = "Tekne Motor Motoru"

--ent_boat_rhib
entities.boatrhib.printName                 = "RHIB"

--ent_boat_rowboat
entities.rowboat.printName                  = "Sıra Teknesi"


--ent_callsign_display
entities.callsigndisplay.printName          = "Callsign Display Board"
entities.callsigndisplay.yourCallsign       = "Çağrı imzanız: %s"
entities.callsigndisplay.noCallsign         = "Çağrı imzanız yok"
entities.callsigndisplay.deactivate         = "Dedektif"

--ent_carspawnsign
entities.carspawnsign.warnning              = "- UYARI -" --written 2 years ago by kyle goodale not me
entities.carspawnsign.seriousInjury         = "Ciddi yaralanmaları önlemek için,"
entities.carspawnsign.remain10ft            = "en az 10 adım uzak kal"
entities.carspawnsign.fromThisSign          = "bu işaretten uzağa ..."
entities.carspawnsign.ty                    = "Teşekkür ederim."

--ent_circle_trigger
entities.circletrigger.printName            = "Teslim noktası"
entities.circletrigger.depotPrintName       = "Dönüş Noktası"

--ent_busstop_trigger
entities.busstoptrigger.printName			= "Bus Stop"

--ent_detector_trigger
entities.detectortrigger.printName          = "Metal Dedektör Tetikleyici"

--ent_teleporter_trigger
entities.teleportertrigger.printName		= "Shooting Range Trigger" -- TODO
entities.teleportertrigger.text				= "Validating ID Card"	   -- TODO
entities.teleportertrigger.notAllowed		= "You are not allowed to access the shooting range at this time."	-- TODO

--ent_fire_hydrant
entities.firehydrant.printName              = "Yangın Hidrantı"

--ent_fire_pumphose
entities.pumphose.printName                 = "Hortum Pompası"
entities.pumphose.detachingHose             = "HİDROLİK HORTUM"

--ent_fire_waterhose
entities.waterhose.printName                = "Hortum"
entities.waterhose.unequipFirst             = "Önce hortumunu aç!"

--ent_garage_tv
entities.garagetv.printName                 = "Garaj TV"

--ent_garage
entities.garage.printName                   = "Garaj"
entities.garage.modeDisabled                = "Bu mod şu anda devre dışı. Lütfen F3'e basarak Eski araçlar sistemini kullanın."

--ent_gascan
entities.gascan.printName                   = "Gaz Kutusu"
entities.gascan.pourThis                    = "Bunu aracınıza dökün."

--ent_gaspump
entities.gaspump.printName                  = "Gaz pompası"
entities.gaspump.poor                       = "Bunu karşılayamazsın!"
entities.gaspump.needToWait                 = "%d saniye beklemelisin!"

--ent_house_alarm
entities.housealarm.printName               = "Harita" -- weir name for a house alarm

--ent_instanced_apt_circle
entities.instancedaptcircle.printName       = "Test Metni"

--ent_invisible_window
entities.invisibleWindow.printName          = "Görünmez Pencere"

--ent_itembox
entities.itembox.printName                  = "Premium Sandık"
entities.itembox.restricted                 = "Yalnızca Premium Üyelerle sınırlıdır. (!store)"

--ent_map
entities.map.printName                      = "Harita"
entities.map.cityHall                       = "Belediye binası"
entities.map.superMarket                    = "Süpermarket"
entities.map.policeStation                  = "Polis Merkezi"
entities.map.fireStation                    = "İtfaiye"
entities.map.clothingStore                  = "Giyim mağazası"
entities.map.generalHospital                = "Genel Hastane"
entities.map.modShop                        = "Mod Shop"
entities.map.realtyOffice                   = "Gayrimenkul Ofisi"
entities.map.realEstateOffice               = "Emlâk ofisi"
entities.map.carDealership                  = "Araba satış bayiliği"
entities.map.cityBank                       = "Merkez Bankası"
entities.map.miningRavine                   = "Madencilik Ravine"
entities.map.transitCentre                  = "Transit Merkezi"
entities.map.impoundLot                     = "Haciz"
entities.map.vehicleImpound                 = "Araç İçi"
entities.map.fishingDock                    = "Balık İskelesi"
entities.map.biteRestaurant                 = "Lokma Restoranı"
entities.map.woodcuttingForest              = "Odun Ormanı"
entities.map.gasStation                     = "Gaz istasyonu"
entities.map.gasStationCity                 = "Benzin İstasyonu (Şehir)"
entities.map.gasStationCountry              = "Benzin İstasyonu (Ülke)"
entities.map.gasStationIndustiral           = "Benzin İstasyonu (Endüstriyel)"
entities.map.deliveryDepot                  = "Teslimat deposu"
entities.map.drugAlley                      = "Uyuşturucu Yolu"
entities.map.cityMines                      = "Şehir Madenleri"
entities.map.highEndMines                   = "İleri Teknoloji Madenler"
entities.map.fishingStore                   = "Balıkçılık Mağazası"
entities.map.casino                         = "Gazino"
entities.map.monoRailStop                   = "Tek Raylı Durak"
entities.map.gunStore                       = "Silah Mağazası"
entities.map.hardwareStore                  = "Donanım mağazası"
entities.map.woodCuttingPark                = "Gravür Parkı"
entities.map.marketStreet                   = "Market Sokağı"
entities.map.garage                         = "Garaj"
entities.map.cityGarage                     = "Şehir Garajı"
entities.map.miningRow                      = "Madencilik Satırı"
entities.map.woodcuttingRow                 = "Gravür Satırı"
entities.map.resourceHaven                  = "Kaynak Cenneti"
entities.map.mineshaft                      = "Mineshaft"
entities.map.hospital                       = "Hastane"
entities.map.mechanicShop                   = "Tamirci dükkanı"
entities.map.automobileDealer               = "Otomobil Satıcısı"
entities.map.drugDealers                    = "Uyuşturucu satıcıları"
entities.map.truckersCo                     = "Kamyoncu A.Ş."
entities.map.deliveryService                = "Teslimat Hizmetleri"
entities.map.mexiGrill                      = "Mexi Izgara"
entities.map.ronnies                      	= "Ronnie"
entities.map.bank                           = "Banka"
entities.map.lake                           = "Göl"
entities.map.truenorthCounty                = "Truenorth County"
entities.map.pineCounty                		= "Pine County"
entities.map.monofordCounty					= "Monoford İlçesi"
entities.map.truenorthCentral               = "Truenorth Merkez"
entities.map.taxiCompany                    = "Taksi Şirketi"
entities.map.openMap                        = "Haritayı Aç!"
entities.map.MonoMap                        = "Mono-Harita"
entities.map.legend                         = "EFSANE"
entities.map.you                            = "Sen"
entities.map.setDestination                 = "Hedef belirle"
entities.map.mcc                			= "Convention Center"
entities.map.applianceStore                	= "Appliance Store"

--ent_metal_detector
entities.metalDetector.printName            = "Metal dedektörü"

--ent_police_database
entities.policeDatabase.printName           = "Polis Veritabanı"
entities.policeDatabase.accessPoliceDB      = "Polis Veri Tabanına eriş"
entities.policeDatabase.noAccess            = "Erişim Yok"

--ent_police_jailer
entities.policeJailer.printName             = "Polis Bilgisayarı"
entities.policeJailer.accessPolicePC        = "Polis Bilgisayarına Erişim"
entities.policeJailer.noAccess              = "Erişim Yok"

--ent_prisoner_toiler
entities.prisonerToilet.printName           = "Hapishane Tuvalet Fırçası"
entities.prisonerToilet.digIn               = "Sürpriz için giriş yap"
entities.prisonerToilet.ewDisgusting        = "Ah, iğrenç!"
entities.prisonerToilet.looksEmpty          = "Tuvalet boş görünüyor ..."
entities.prisonerToilet.noWay               = "Ah, o tuvalete bile dokunmayı hayal bile edemiyorum!"
entities.prisonerToilet.cannotSearch        = "Nöbetçi Hapishane Muhafızları olmadığında tuvaletlerde kilitli arama yapamazsınız."
entities.prisonerToilet.alreadyHave         = "Zaten bir kilit tıkım var, neden yapayım ki?"
entities.prisonerToilet.startedDigging      = "%s tuvaletin içindekileri araştırmaya başladı."
entities.prisonerToilet.dangIt              = "Bir kilit buldum ama tuvalete girdi ... Lanet olsun."
entities.prisonerToilet.dugUpLockpick       = "Bir kilit tıkladı! Düzgün!"
entities.prisonerToilet.foundNothing        = "Hiçbir şey bulamadın."
entities.prisonerToilet.diggingThrough      = "ARA KESİM"

--ent_prisoner_task
entities.prisonerTask.printName				= "Mahkum Görevi"
entities.prisonerTask.notif.notYou			= "Neyse ki bu senin için değil."
entities.prisonerTask.notif.justDone 		= "Bu görev daha yeni tamamlandı. Bir tane daha bulun veya biraz bekleyin."
entities.prisonerTask.notif.cooldown		= "Başka bir hapishane görevi yapmadan önce %s saniye bekleyin."

entities.prisonerTask.laundry.hintText		= "Kirli çamaşırları yıka."
entities.prisonerTask.laundry.actionText	= "Çamaşır yıkamak..."
entities.prisonerTask.dishes.hintText 		= "Pis bulaşıkları temizleyin."
entities.prisonerTask.dishes.actionText 	= "Bulaşık yıkamak..."
entities.prisonerTask.cooking.hintText 		= "Güzel yemekler pişirin."
entities.prisonerTask.cooking.actionText 	= "Yemek pişirme..."
entities.prisonerTask.fallbackHintText		= "Hapishane görevini yerine getir."

--ent_respawner
entities.respawner.printName                = "Ekipman Dolabı"
entities.respawner.cannotOpen               = "Bu dolabı açamazsınız."
entities.respawner.readyToUse               = "Kullanıma hazır"
entities.respawner.seconds                  = "Saniye"
entities.respawner.noAccess                 = "Erişim Yok"

--ent_source_base
entities.sourceBase.printName               = "Kaynak Tabanı"
entities.sourceBase.gatherWeaponName        = "Silah"
entities.sourceBase.cannotGather            = "Çalışırken toplanamazsın!"
entities.sourceBase.noMoreResources         = "Bu kaynağın daha fazla kaynağı yok"
entities.sourceBase.skillLevelRequired      = "Seviye %s %s gerekli!"
entities.sourceBase.source                  = "%s Kaynak"
entities.sourceBase.sourceAvailable         = "Mevcut %s: %s%%"
entities.sourceBase.level                   = "Seviye %s"

--ent_source_rock
entities.sourceRock.printName               = "Kaya"
entities.sourceRock.resourceName            = "Taş"
entities.sourceRock.cannotMine              = "İşteyken maden kazamazsın!"
entities.sourceRock.noMoreStone             = "Bu kayanın daha fazla taşı yok"
entities.sourceRock.higherSkillLevel        = "Bu cevheri madencilik için daha yüksek bir beceri seviyesine ihtiyacın var!"

--ent_source_tree
entities.sourceTree.printName               = "Ağaç"
entities.sourceTree.resourceName            = "Odun"
entities.sourceTree.gatherWeaponName        = "Balta"
entities.sourceTree.messageEmployed         = "Çalışırken giriş yapamazsınız!"
entities.sourceTree.noMoreWood              = "Bu ağacın daha fazla odunu yok"

--ent_spawned_furniture
entities.spawnedFurniture.printName         = "Mobilya"

--ent_spawned_prop
entities.spawnedProp.printName              = "Prop"
entities.spawnedProp.pickingUp              = "TOPLAMA"
entities.spawnedProp.health                 = "Can: %s%%"

--ent_spray_plate
entities.sprayPlate.printName               = "Püskürtme Plakası"
entities.sprayPlate.sprayingTag             = "PÜSKÜRTME ETİKETİ"
entities.sprayPlate.pleaseWait              = "Lütfen tekrar sprey uygulanana kadar bekleyin, dolum süresinde."
entities.sprayPlate.tagYourCrew             = "Ekibinizi duvara etiketleyin"

--ent_tapedummy
entities.tapeDummy.printName                = "Bant Kukla Nesnesi"

--ent_unarrest_trigger
entities.unarrestTrigger.printName          = "Tutuklama Tetikleyicisi"

--ent_use_trigger
entities.useTrigger.printName               = "Tetikleyiciyi Kullan"

--ent_vehicle_oopvs  ( specifies it's disabled but adding it in just in case )
entities.vehicleOOPVS.printName             = "Vehicle OOPVS Görüntüle"

--ent_vehicle_showcase
entities.vehicleShowcase.printName          = "Araç Vitrini"

--ent_voting
entities.votingComputer.printName           = "Oylama Bilgisayarı"
entities.votingComputer.cannotVote          = "Seçim dönemine kadar oy kullanamazsınız."
entities.votingComputer.mayorRating         = "Başkanın Derecesi:"
entities.votingComputer.generalTaxes        = "Genel Vergiler:"
entities.votingComputer.yourJobTaxes        = "İş Vergileriniz:"
entities.votingComputer.aveJobTaxes         = "Ave. İş Vergileri:"

--ent_wheel_repair_kit
entities.wheelRepairKit.printName           = "Tekerlek Tamir Takımı"

--mono_camera
entities.monoCamera.printName               = "Kişisel CCTV Kamera"

--monolith_npc
entities.monoNPC.printName                  = "NPC"

--npc_injuredciv
entities.injuredCiv.printName               = "Yaralı Vatandaş"
entities.injuredCiv.innocentCivilian        = "Bu sivil masum."

--npc_robber
entities.npcRobber.criminal 				= "Suçlu"
entities.npcRobber.mustSurrenderFirst       = "Soyguncu önce teslim olmalıydı."

--sent_base_gonzo
entities.baseGonzo.printName                = "İlaç"

-- the_weed_foil
entities.weedfoil.printName                 = "Folyo makinesi"

-- the_weed_foildryhash
entities.weedfoildryhash.printName          = "Folyo Kuru Karma"
entities.weedfoildryhash.foiledDryHash      = "Kuru Kuru Karma"
entities.weedfoildryhash.consumeWeed        = "Folyo otu tüketemezsin."

-- the_weed_hash
entities.weedhash.printName                 = "Islak Karma"
entities.weedhash.wetWeed                   = "Otu henüz tüketemezsiniz, kurutmanız gerekir."

-- the_weed_jar
entities.weedjar.printName                  = "Kavanoz"

-- the_weed_microwave
entities.weedmicrowave.printName            = "Mikrodalga"
entities.weedmicrowave.connectApp           = "Bu cihazın fişini Konut Prizine bağlamanız gerekiyor."

-- the_weed_plant
entities.weedplant.printName                = "Tencere"
entities.weedplant.soilInPot                = "Tencereye biraz toprak koydun."
entities.weedplant.soilPack                 = "Bunu kullanmak için envanterinde bir toprak paketine sahip olmalısın."
entities.weedplant.plantedSeed              = "Tencereye bir tohum ekledin."
entities.weedplant.marjIsBigSeed            = "Bunu kullanabilmek için envanterinizde kenevir tohumu bulundurmanız gerekiyor."
entities.weedplant.harvestSuccess           = "Ot bitkisini başarıyla hasat ettin."
entities.weedplant.badTeam                  = "Bitkiyi hasat etmek için doğru takım değilsin."
entities.weedplant.somethingStrange         = "Garip bir şey kokuyorsun. Boğazını temizlemek için biraz zaman ayırıyorsun."

-- the_weed_tent
entities.weedtent.printName                 = "Çadır"
entities.weedtent.sockResi                  = "Bu cihazın fişini Konut Prizine bağlamanız gerekiyor."
entities.weedtent.toggleSystem              = "Sistemi değiştirdiniz"
entities.weedtent.on                        = "Üzerine."
entities.weedtent.off                       = "Kapalı."
entities.weedtent.bigBarrel                 = "Bunu yapmak için büyük bir varil suya ihtiyacın var."
entities.weedtent.refillWater               = "Su kabını yeniden doldurdun."
entities.weedtent.waterSys                  = "Sulama Sistemi ("
entities.weedtent.ON                        = "AÇIK)"
entities.weedtent.OFF                       = "KAPALI)"
entities.weedtent.waterLeft                 = "Su kaldı"
entities.weedtent.liters                    = "litre"
entities.weedtent.refill                    = "Doldurmak için ekranda E tuşuna basın"
entities.weedtent.altE                      = "Sulamayı değiştirmek için ALT ve E tuşlarını ekranda tutun"

--ent_dirt_pile
entities.dirtpile.printName                 = "Toprak Yığını"
entities.dirtpile.harvesting                = "HASAT"
entities.dirtpile.stealing                  = "Çalmakla"
entities.dirtpile.noInventorySpace          = "Bunu hasat etmek için envanter alanınız yok!"
entities.dirtpile.soilQualityNoBueno        = "Toprak kalitesi o kadar kötü ki, planınızı kazara yok ettiniz!"
entities.dirtpile.stealingAttempt           = "/me çapalanan yerden ekinleri çalmaya çalışır." -- NOTE: Keep the /me prefix, this formats the text when typed in the chat
entities.dirtpile.stealingSeeds             = "TOHUMLAR\nÇALINIYOR"

--ent_easter_egg
entities.easteregg.printName                = "Paskalya Yumurtası"
entities.easteregg.claimPrize               = "Bas\n\n\nVe ödülünü al!"
entities.easteregg.nothing                  = "hiçbir şey"
entities.easteregg.youReceived              = "%s aldın"
entities.easteregg.congratsYouReceived      = "Tebrikler!\n%s aldın"

--ent_wol_machine
entities.wolmachine.printName               = "Şans Çarkı"
entities.wolmachine.congratsYouWon          = "Tebrikler! %s kazandınız"
entities.wolmachine.chatPrefix              = "[Para Çarkı]"
entities.wolmachine.justWonTheJackpot       = "ikramiyeyi az önce kazandı"
entities.wolmachine.onTheWheelOfLuck        = "Şans Çarkında!"
entities.wolmachine.cantWhenWorking         = "Bu makineyi çalışırken kullanamazsın!"
entities.wolmachine.currDisabled            = "Şans Çarkı şu anda devre dışı."
entities.wolmachine.pleaseWait              = "Lütfen makineleri değiştirmeden önce %s saniye bekleyin."
entities.wolmachine.pleaseWaitTurn          = "Lütfen %s'nin sırasını bitirmesini veya yeni bir makine bulmasını bekleyin."
entities.wolmachine.cannotAfford            = "Bu makineyi kullanamazsınız, en az %s gerekir."

--ent_wol_reel
entities.wolreel.printName                  = "Çarkıfelek"

--the_weed_dirt
entities.theweeddirt.printName              = "Toprak Paketi"
entities.theweeddirt.chargesLeft            = "%s - %s kullanımı kaldı"

--the_weed_dryhash
entities.theweeddryhash.printName           = "Esrar"

--ent_pplant_barrel TODO: Needs Translation
entities.pplantBarrel.printName           	= "Santral Fıçısı"
entities.pplantBarrel.extractAttempt        = "Sifon kimyasalları..."
entities.pplantBarrel.empty           		= "Bu varil boş görünüyor."
entities.pplantBarrel.startExtract			= "%s kimyasalları varilden sifonlamaya başladı."
entities.pplantBarrel.siphon				= "Kimyasalları sifonlamak için"
entities.pplantBarrel.noSiphon				= "Bir sifon pompanız yok."
entities.pplantBarrel.police				= "You cant do this." -- TODO: Needs Translation
entities.pplantBarrel.notEnough				= "Warning: Not enough security online" -- TODO: Needs Translation
entities.pplantBarrel.broken				= "Siphon pump is broken." -- TODO: Needs Translation
