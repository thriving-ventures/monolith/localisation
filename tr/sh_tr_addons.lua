--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

--[[ @NOTE: Addon language configurations locations for the following addons are diplayed:
		- CasinoKit: casinokit/lua/casinokit/custom/langs
			- Also for each sub addon
		- DHeists: dheists_/lua/dheists/config/language
		- gsigns: gsigns/lua/metasign_config.lua
		- RRL: rrl/lua/rrl/config
]]

-- @TODO:
-- firetruck-ladder
-- blue's casino stuff
-- fishingmod
-- raptor-ui
-- [any SWEP PrintNames]
-- [any AddNotification]
-- rrl
-- v-fire
-- Zero's arcade pack
-- Check GetJobCategory again.

Monolith.LocaleBuilder( "tr", true )

-- CasinoKit
addon.casino.notif.allowedMax5					= "Masa başına en fazla 5 bahis koyabilirsiniz."

-- Tow Truck Driver
addon.towtruck.notif.noVehicle					= "Çekici arkasında araç yok!"
addon.towtruck.notif.attached 					= "Araç başarıyla bağlandı!"
addon.towtruck.notif.unattached					= "Aracı çekmeyi bıraktın!"
addon.towtruck.notif.lookTowTruck				= "Yakındaki araçları bağlamak için bir çekiciye bakmalısın!"

-- Firetruck Ladder
addon.ftladder.notif.needFirefighter		= "Bunu yapabilmek için itfaiyeci olmalısın!"

-- Fishingmod
addon.fishing.notif.hookedCaught				= "Bir balık yakaladın! Artık envanterinde."
addon.fishing.notif.hookedAway 					= "Bir balığı yakalamıştın, ama kurtuldu ve kaçtı."
addon.fishing.notif.noSpace						= "Envanterinde yeterli alan yok!"
addon.fishing.notif.noCatch						= "Bir şey yakalayamadın."
addon.fishing.notif.noBait						= "Yemin bitmiş."
addon.fishing.notif.pleaseWait					= "Oltanı tekrar sallamak için %s bekle."
addon.fishing.notif.rmbReel						= "[Sağ Tık] Balığı Çek"


-- GSigns
addon.gsigns.notif.err 							= "Bir hata oluştu. Lütfen konsolunuzu kontrol edin!"

addon.gsigns.action.pickingUp 					= "TOPLANIYOR"

-- RRL
addon.rrl.notif.removedWarning 					= "%s kişisinin bir uyarısını sildin"
addon.rrl.notif.warnedBy						= "%s tarafından %s sebebiyle uyarıldın, %s uyarı puanın yok."
addon.rrl.notif.willKicked						= "%s tane daha uyarı puanın alırsan atılacaksın"
addon.rrl.notif.willKicked.any 					= "herhangi"
addon.rrl.notif.youWarned 						= "%s kişisini %s sebebiyle uyardın."
addon.rrl.notif.reportDoesntExist 				= "Erişmeye çalıştığınız rapor mevcut değil."
addon.rrl.notif.noAccess						= "Bu özelliğe erişiminiz yok"
addon.rrl.notif.removedReportTicket 			= "Bir raporu kaldırdınız (#%s)"
addon.rrl.notif.supportTicketRemoved			= "Raporunuz kaldırıldı (#%s)"
addon.rrl.notif.cantCloseOwnReport				= "Kendi raporunu kapatamazsın."
addon.rrl.notif.reportTicketClosed 				= "Raporunuz kapatıldı (#%s)"
addon.rrl.notif.cannotOpenOwnReport				= "Kendi raporunu açamazsın"
addon.rrl.notif.yourReportTicketOpened 			= "Raporun açıldı (#%s)"
addon.rrl.notif.alreadyClaimed 					= "Zaten alındığı için bu raporu alamazsınız"
addon.rrl.notif.cannotClaimOwn 					= "Kendi raporunuzu talep edemezsiniz."
addon.rrl.notif.staffMemberClaimed 				= "Bir yetkili raporunuzu aldı (#%s)"
addon.rrl.notif.claimedReportTicket				= "Bir raporu aldınız (#%s)"
addon.rrl.notif.staffMemberUnclaimed			= "Bir yetkili raporunuzu bıraktı (#%s)"
addon.rrl.notif.youUnclaimed					= "Bir raporu bıraktın (#%s)"
addon.rrl.notif.submittedNewReport				= "Yeni bir rapor oluşturdun (#%s)"

-- Whomekit_media
addon.whomekit_media.notif.needConnect			= "Bu cihazın fişini konut prizine bağlamanız gerekir."

addon.whomekit_media.log.startedPlaying 		= "%s çalmaya başladı: %s bir medya kaynağında. (%s)"

-- zcrga
addon.zcrga.notif.cantAfford					= "Bunu karşılayamazsın!"
addon.zcrga.notif.youWonX						= "%s Kazandın!"
