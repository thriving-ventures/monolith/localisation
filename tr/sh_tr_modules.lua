--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

Monolith.LocaleBuilder( "tr", true )

-- Actions Module
actions.timeremaining 					= "KALAN SÜRE"
actions.movingcancel 					= "HAREKET ETMEK BU EYLEMİ İPTAL EDECEK"
actions.cancelled 						= "Hareket ettiniz ve mevcut işleminiz iptal edildi"

actions.log.invalid 					= "[EYLEM] %s geçersiz bir eşya üzerinde işlem yapmaya çalıştı"
actions.debug.invalid 					= "[EYLEM] Oyuncu: %s geçersiz bir eşya üzerinde işlem yapmaya çalıştı"

-- Areas Module
-- @TODO: Map-specific areas

-- Banking Module
banking.gui.deposit						= "Yatır"
banking.gui.withdraw					= "Çek"
banking.gui.specify 					= "Lütfen %s için ne kadar para istediğinizi belirtin" -- %s = banking.gui.deposit or banking.gui.withdraw
banking.gui.funds 						= "SİZİN PARANIZ"
banking.gui.balance						= "Banka hesap bakiyesi"
banking.gui.account 					= "BANKA HESABI"

banking.notif.combat 					= "Çatışmadayken bunu yapamazsın"
banking.notif.full 						= "Bankanız dolu! Daha fazla ürün eklemeden önce yer açmanız gerekecek"
banking.notif.jobonly 					= "İş eşyalarını bankana koyamazsın."
banking.notif.alrdydeployed				= "Koymaya çalıştırdığınız eşya yerleştirilmiş. Lütfen önce onu alın." -- TODO: Translation needed.
banking.notif.depositInvalidAmount 		= "Para yatırma işlemi başarısız oldu. Geçersiz tutar."
banking.notif.depositInvalidSlip		= "Para yatırma başarısız oldu. Geçersiz para yatırma fişi. Hata BANK_01"
banking.notif.withdrawInvalidAmount		= "Para çekme başarısız oldu. Geçersiz tutar istendi."
banking.notif.withdrawInvalidSlip		= "Para çekme başarısız oldu. Geçersiz para çekme makbuzu. Hata BANK_02"
banking.notif.deposit 					= "%s yatırdınız" -- %s = formatted money
banking.notif.depositInsufficient 		= "Bu parayı yatırmak için yeterli paranız yok."
banking.notif.withdraw 					= "%s çektiniz" -- %s = formatted money
banking.notif.withdrawError 			= "Para çekilirken bir şeyler ters gitti. Hata Kodu: BANK_02"
banking.notif.withdrawInsufficient		= "Bu para çekme işlemini yapmak için yeterli paranız yok."
banking.notif.loadingError 				= "Oyuncu verilerinizin yüklenmesi ile ilgili bir sorun oluştu. Lütfen tekrar katılın."

banking.log.deposit 					= "%s yatırılan banka öğesi: %s"
banking.log.withdraw 					= "%s banka ürününü aldı: %s"
banking.log.depositMoney 				= "%s yatırılan para %s"
banking.log.withdrawMoney 				= "%s para çekildi %s"

-- Budget Module
budget.gui.locker 						= "Ekipman Dolabı"
budget.gui.equipLoadout					= "Ekipmanı Donat"
budget.gui.primary 						= "Birincil Silahlar"
budget.gui.secondary 					= "İkincil Silahlar"
budget.gui.costs						= "Ücret: %s"
budget.gui.deptBudget 					= "%s bölümün bütçesi: % "
budget.gui.full							= "Dolu"
budget.gui.locked 						= "KİLİTLİ, BAŞKAN YOK"

budget.notif.ratingGain					= "%s%% Başkanlık Değeri kazandınız."
budget.notif.ratingLoss					= "%s%% Başkanlık Değeri kaybettiniz."
budget.notif.tooLow 					= "Bakanlığın bütçesi bu eylemi gerçekleştiremeyecek kadar düşük."
budget.notif.refill 					= "'%s' departmanın bütçesinin yeniden dolması gerekiyor."
budget.notif.lowCityBudget				= "'Şehir Bütçesi' düşük"
budget.notif.lowDepartment 				= "'%s' departmanın bütçesi düşük, yeniden doldurulması gerekiyor."
budget.notif.lowBudget 					= "Departmanın bütçesi çok düşük!"

-- Carm modification module
carmod.gui.noVehicles 					= "Değiştirilecek aracınız yok"
carmod.gui.selectCar 					= "ARAÇ SEÇİN"
carmod.gui.modificationsTitle 			= "DEĞİŞİKLİKLER"
carmod.gui.bodygroups 					= "Bodygroups"
carmod.gui.modifications 				= "Araç Modifikasyonları"
carmod.gui.purchasePartPrompt 			= "Bu parçayı satın almak istediğinizden emin misiniz?"
carmod.gui.noAdditionalParts 			= "Ek parça yok."
carmod.gui.parts						= "Parçaları"
carmod.gui.engineUpgradePrompt 			= "Bu motor yükseltmesini satın almak istediğinizden emin misiniz?"
carmod.gui.engine						= "Motor"
carmod.gui.resprayPurchasePrompt 		= "Bu ödülü satın almak istediğinizden emin misiniz?"
carmod.gui.respray 						= "Respray"
carmod.gui.skins 						= "Skinler"
carmod.gui.purchase 					= "SATIN ALMA"
carmod.gui.wrapNum						= "#%s'yi kaydır"
carmod.gui.wrapPurchasePrompt 			= "Bu paketi satın almak istediğinizden emin misiniz?"
carmod.gui.wraps 						= "Ambalajlar"
carmod.gui.underglow 					= "Neon"
carmod.gui.underglowPurchasePrompt 		= "Bu çalıları satın almak istediğinizden emin misiniz?"
carmod.gui.licensePlates 				= "Araç plakası"
carmod.gui.licensePurchasePrompt 		= "Bu plakayı satın almak istediğinizden emin misiniz?"

carmod.gui.colors.stock 				= "Stok"
carmod.gui.colors.white 				= "Beyaz"
carmod.gui.colors.red 					= "Kırmızı"
carmod.gui.colors.lime 					= "Misket Limonu"
carmod.gui.colors.green 				= "Yeşil"
carmod.gui.colors.blue 					= "Mavi"
carmod.gui.colors.orange 				= "Portakal"
carmod.gui.colors.yellow				= "Sarı"
carmod.gui.colors.cyan					= "Mavi"
carmod.gui.colors.purple 				= "Mor"
carmod.gui.colors.pink					= "Pembe"

carmod.gui.upgrades.stock 				= "Stok"
carmod.gui.upgrades.street				= "Sokak"
carmod.gui.upgrades.race 				= "Yarış"
carmod.gui.upgrades.pro 				= "Profesyonel"
carmod.gui.upgrades.super 				= "Süper"

carmod.notif.premium 					= "Bunu satın almak için Premium Üye olmanız gerekir. (Daha fazla bilgi için !store yazın)"
carmod.notif.engineUpgrade 				= "Araç motorunuz %s değerine yükseltildi"
carmod.notif.cannotAffordRespray 		= "Bu ödülü karşılayamazsın."
carmod.notif.resprayBought 				= "%s için bir ödül aldın"
carmod.notif.cannotAffordEngine 		= "Bu motor yükseltmesini göze alamazsınız."
carmod.notif.currentEngine 				= "Bu sizin mevcut motor güncellemeniz."
carmod.notif.engineReimburse 			= "Mevcut %s motor yükseltmeniz için %s geri ödendi"
carmod.notif.engineBought 				= "%s motor yükseltmesi satın aldınız. Maliyeti %s"
carmod.notif.cannotAffordUnderglow 		= "Bu neonu karşılayamıyorsun"
carmod.notif.underglowBought 			= "%s neonu satın aldın aldın. Maliyeti %s"
carmod.notif.cannotAffordBodygroup 		= "Bu vücut grubunu karşılayamazsınız"
carmod.notif.bodygroupBought 			= "Bir vücut grubu satın aldınız. Maliyeti %s"
carmod.notif.licenseLong 				= "Plakanız çok uzun. %s karakterini aşmadığından emin olun."
carmod.notif.licenseShort 				= "Plakanız çok kısa. %s karakterini aştığından emin olun."
carmod.notif.licenseProfanity 			= "Plakanız küfür içeremez."
carmod.notif.cannotAffordLicense		= "Bu plakayı karşılayamazsınız"
carmod.notif.purchaseLicense 			= "Bir lisans plakası satın aldın. Maliyeti %s"
carmod.notif.skinDisabled 				= "Skin deaktif"
carmod.notif.cannotAffordSkin 			= "Bu skini karşılayamıyorsun."
carmod.notif.purchaseVehicleSkin 		= "Bir araç kaplaması satın aldın. Maliyeti %s"

carmod.keybind.underglow 				= "Neon Açık/Kapalı"

-- Chatbox module
chatbox.keybind.timestamps 				= "Sohbet Zaman Damgası"

-- Christmas module
christmas.gui.title 					= "On İki Gün Noel"
christmas.gui.titleRewards 				= "On İki Gün Noel - Ödüller"
christmas.gui.rewards 					= "Hediyenizden aldığınız ödüller:"
christmas.gui.gift.opened 				= "Zaten Açılmış"
christmas.gui.gift.missed 				= "Kaçırılmış Hediye"
christmas.gui.calendar 					= "Takvim"

christmas.reward.skillBoost 			= "%sx %s yetenek arttırma (%s)" -- e.g. "2x Chemistry Skill Booster for 12 hours"

christmas.notif.christmas 				= "Noel"
christmas.notif.activeReward 			= "Alabileceğiniz etkin bir takvim ödülünüz var! Almak için Mono Menüyü açın."
christmas.notif.itemReward 				= "Bir hediye açtın ve eşya kazandın: %s"
christmas.notif.cashReward 				= "Bir hediye açtın ve para kazandın: %s"

-- Client Settings module
clientsettings.gui.settings 			= "Ayarlar"
clientsettings.gui.keybinds 			= "Tuş Atamaları"
clientsettings.gui.other 				= "Diğer"
clientsettings.gui.subheader 			= "Monolith içindeki tüm seçeneklerin yeri."
clientsettings.gui.confirm 				= "Doğrula"
clientsettings.gui.change				= "Değiştir"

clientsettings.gui.multicore 			= "Çoklu Çekirdek"
clientsettings.gui.darkMode 			= "Gece Modu"
clientsettings.gui.performance 			= "Performans"
clientsettings.gui.viewRange 			= "Görüş Uzaklığı"
clientsettings.gui.viewRange.low		= "Düşük"
clientsettings.gui.viewRange.medium		= "Orta"
clientsettings.gui.viewRange.high 		= "Yüksek"
clientsettings.gui.apperance 			= "Görünüş"
clientsettings.gui.vignette 			= "Kenar Siyahlıkları"
clientsettings.gui.blur 				= "Bulanıklık"
clientsettings.gui.betaNameplates 		= "Beta İsim Levhaları"
clientsettings.gui.gameHints 			= "Oyun İpuçları"
clientsettings.gui.minimapStyle 		= "Minimap Tarzı"
clientsettings.gui.minimapStyle.normal 	= "Normal"
clientsettings.gui.minimapStyle.fancy 	= "Fancy"
clientsettings.gui.minimapStyle.fancy3D	= "Fancy 3D"
clientsettings.gui.streamer 			= "Yayıncı Modu"
clientsettings.gui.insurance 			= "Sigorta Talepleri"
clientsettings.gui.enableFoodSounds 	= "Açlık/Susuzluk seslerini aç"
clientsettings.gui.chatSettings 		= "Sohbet Ayarları"
clientsettings.gui.enableGovMissions	= "Enable government missions"
clientsettings.gui.doSeatbeltSounds		= "Enable seatbelt warning sounds"
clientsettings.gui.showSpeedo			= "Show vehicle speedometer"

-- Commands module
commands.notif.noPlayer 				= "Oyuncu bulunamadı"
commands.notif.invalidModel 			= "Geçersiz model yolu"
commands.notif.invalidScale 			= "Geçersiz boyut"
commands.notif.syncingNames 			= "Rol isimleri senkronize ediliyor..."
commands.notif.cooldown 				= "Bu komutu kullanabilmek için %s saniye beklemeniz gerekiyor."
commands.notif.entitiesFixed 			= "Varlıklar düzeltildi! (umarız)"
commands.notif.noDeaths 				= "Bu oyuncunun karşılanacak ölümü yok."
commands.notif.theyRefunded 			= "Başarıyla %s karşılandı."
commands.notif.youRefunded 				= "Ölümünüz bir yönetici tarafından karşılandı! Hak talebinde bulunmak için Geri Ödemeler ve Koleksiyonlar NPC'sini ziyaret edin."
commands.notif.targetNotPlayer 			= "Oyuncu olmayan bir şeyi hedeflediniz"
commands.notif.targetTooFar 			= "Hedefinizden çok uzaktasınız"
commands.notif.cantAfford 				= "Bunu karşılayamazsınız veya yanlış tutarı girdiniz"
commands.notif.youGave 					= "Birisine %s verdin"
commands.notif.gaveYou 					= "Birisi sana %s verdi"
commands.notif.noTarget					= "Your target doesn't have access to toggle event mode."
commands.notif.enabledEventMode			= "You toggled event mode for %s."
commands.notif.disableEventModeSelf		= "You disabled event mode for yourself."

commands.roll.prefix 					= "[SALLAMA] " -- keep the spaces
commands.roll.rolled 					= " salladı "
commands.roll.cooldown 					= "Tekrar sallamak için %s saniye bekle."

commands.unstuck.sitting 				= "Otururken bunu yapamazsın"
commands.unstuck.quickly 				= "Bunu çok hızlı yapamazsın"
commands.unstuck.attempting 			= "Seni çıkartmaya çalışacağız"
commands.unstuck.notStuck 				= "Sıkışık değilsin"
commands.unstuck.couldntFind 			= "Uygun bir yer bulunamadı. Bir yöneticiyi arayın."
commands.unstuck.youCantDoThis 			= "You can't do this."

commands.log.refund 					= "%s, %s parayı %s ölümü yüzünden karşıladı." -- [admin] refunded [money] to [player] for their death
commands.log.give 						= "%s, %s kişisine %s give komutuyla verdi."
commands.log.unstuck 					= "%s kendisini sıkışmaktan kurtarmayı denedi."
commands.log.enabledEventMode			= "%s toggled Event Mode for %s."
commands.log.disableEventModeSelf		= "%s disabled Event Mode for themself."

commands.fps.prompt 					= "FPS değerini iyileştirmek için oyun ayarlarınızı otomatik olarak belirlememizi ister misiniz? Bu, eski sistemlerde kare hızınızı düşürebilir.\nBu ayarları korumak istiyorsanız, daha sonra konsolda `host_writeconfig` komutunu yürütün."
commands.fps.prompt.title 				= "FPS"
commands.fps.notif.applied 				= "FPS Komutları uygulandı. Şuanda daha iyi FPS değerin olmalı."
-- @TODO: Should we localize command names too?

commands.unwarrant.couldntFindTarget	= "Couldn't find target."
commands.unwarrant.targetNotWarranted	= "The player is not warranted."

-- Communication module
-- @TODO: Should we translate chat commands?
communication.notif.freqLimit 			= "Radyo kanalları 100 ve 200 frekansları arasında olmalıdır"
communication.notif.radioAccess 		= "Bu radyo kanalına erişiminiz yok"
communication.notif.radioSet 			= "Radyo kanalını %s yaptın"
communication.notif.radioWasSet 		= "Radyo frekansın '%s' olarak ayarlandı"
communication.notif.msgRestrained 		= "Kısıtlanmış durumdayken iMessage gönderemezsiniz."
communication.notif.noComm 				= "Şu anda iletişim cihazınız yok."
communication.notif.spam 				= "Spamlamayı bırak yoksa susturulacaksın."
communication.notif.muted 				= "Spamladığın için 1 dakika susturuldun."
communication.notif.unmuted 			= "Susturman açıldı."
communication.notif.oocBlacklist 		= "OOC kullanman yasaklandı"
communication.notif.loocBlacklist 		= "You are blacklisted from using LOOC." -- TODO
communication.notif.generalBlacklist 	= "Bu komutu kullanman yasaklandı."
communication.notif.cooldown 			= "OOC bekletmede. Lütfen yazmadan önce %s saniye bekleyin."
communication.notif.advertCooldown		= "ADs are on cooldown. Please wait %s before creating another one." -- TODO
communication.notif.profanity 			= "Metniniz küfür içeremez!"
-- @TODO: Add language-specific profanity filter
communication.notif.restrained 			= "Kısıtlanmış durumdayken iletişim kuramazsınız."
communication.notif.advertCharge 		= "Reklam için %s ücret ödedin."
communication.notif.advertInsufficient	= "Reklam vermek için üzerinde %s yok."
communication.notif.useRPAd             = "Use /ad to issue a roleplay advertisement. Do not advert raids, questions, or such."
communication.notif.noRadio 			= "Radyonuz yok."
communication.notif.enableRadio 		= "Radyonuzu etkinleştirmelisiniz."
communication.notif.radioRestrained 	= "Kısıtlanmış durumdayken radyo ile iletişim kuramazsınız."
communication.notif.needCrew 			= "Bunu yapmak için bir ekibe ihtiyacınız var."
communication.notif.orgCantComm 		= "Şu anda kuruluşunuzla iletişim kuramıyorsunuz."
communication.notif.orgRestrained 		= "Kısıtlıyken kuruluşunuzla iletişim kuramazsınız."
communication.notif.cantDead 			= "Ölüyken bunu yapamazsın"

communication.commands.says 			= " diyor "
communication.commands.yells 			= " bağırıyor "
communication.commands.whispers 		= " fısıldıyor "

communication.chat.ooc 					= "OOC"
communication.chat.looc 				= "LOOC"
communication.chat.ad					= "AD"
communication.chat.radio 				= "Radyo Sohbeti"
communication.chat.org 					= "ORG"
communication.chat.gov 					= "GOV"
communication.chat.somebody  			= "Birisi"
communication.chat.unconscious 			= "Ölü veya bilinçsizken konuşamazsın."
communication.chat.somebody 			= "Birisi"

communication.channels.publicServices 	= "Toplum Servisleri"
communication.channels.police 			= "Monoford PD"
communication.channels.pinePolice 		= "Pine County PD"
communication.channels.guards			= "Hapishane Gardiyanları"
communication.channels.emergency 		= "Acil Servis"
communication.channels.ss 				= "Özel Servis"
communication.channels.security 		= "Özel Güvenlik" -- TODO: Needs Translation
communication.channels.military 		= "Askeri"
communication.channels.swat 			= "SWAT" -- TODO: Needs Translation
communication.channels.corrections 		= "Ceza İnfaz Kurumu"
-- @TODO: Emojis? line 496 @ sh_chat.lua

communication.log.msgSent 				= "%s, '%s' kişisine iMesaj gönderdi. Yazdığı: '%s'"
communication.log.spamming 				= "%s spam nedeniyle susturuldu."
communication.log.chatCmd 				= "%s sohbet komutu yürüttü: %s"

communication.gui.iMessage 				= "iMesaj"
communication.gui.quickReplyBind 		= "Hızlı Cevap (F6)"
communication.gui.quickReply 			= "Hızlı Cevap"
communication.gui.radio 				= "RADYO"
communication.gui.close 				= "KAPAT ✕"
communication.gui.frequency 			= "FREKANS - %s"
communication.gui.setCustomFreq 		= "Özel Frekans Ayarla"
communication.gui.setFreq 				= "Frekans Ayarla"
communication.gui.inputFreq 			= "Yapmak istediğin frekansı gir. (100-200 arası olmalı)"
communication.gui.confirmFreq 			= "Frekansını '%s' yapmak istediğine emin misin?"
communication.gui.switchFreq 			= "FREKANSI DEĞİŞ (%s)"
communication.gui.existing 				= "Mevcut Kanallar"

communication.chat.newMessage 			= "%s kişisinden yeni bir mesaj"

communication.binds.radioTalk 			= "Radyo Konuşması"

-- Cosmetics module
cosmetics.gui.title 					= "KOZMETİKLER"
cosmetics.gui.equip 					= "Takmak"
cosmetics.gui.unequip 					= "Çıkarmak"
cosmetics.gui.limited 					= "LİMİTLİ" -- as in, limited edition
cosmetics.gui.free 						= "BEDAVA"
cosmetics.gui.promptEquip 				= "%s istediğine emin misin? (%s)" -- Do you wish to [equip] your [item name]
cosmetics.gui.promptPurchase 			= "%s aksesuarını %s fiyata satın alacak mısın?" -- Do you wish to purchase [name] for [price]
cosmetics.gui.version 					= "VERSİYON"
cosmetics.gui.updateLog 				= "Güncelleme günlüğümüzü görüntülemek için buraya tıklayın!"

-- Cooking module (@TODO)
cooking.ent.pot 						= "Cooking Pot"
cooking.ent.stove 						= "Stove"
cooking.ent.table 						= "Prep Table"

cooking.gui.title 						= "PREP TABLE"
cooking.gui.subTitle 					= "Prepare your ingredients for cooking"
cooking.gui.prepTable 					= "Prep Table"
cooking.gui.cookingPot					= "Cooking Pot"
cooking.gui.insertIngredients			= "Insert your ingredients"

cooking.notif.preparedX 				= "You've prepared %s!"
cooking.notif.noItemInCenter 			= "No item in the center slot to prep!"
cooking.notif.prepTableInUse			= "This prep table is currently in-use."
cooking.notif.tooFarAway 				= "You're too far away!"
cooking.notif.onlyOpenOwn 				= "You can only open your own prep table."
cooking.notif.plugInStove 				= "You must plug the stove in before turning on the burners."
cooking.notif.reservedPremium			= "These burners are reserved for players with Premium%s."
cooking.notif.reservedPremium.level		= " or cooking level %s"
cooking.notif.reservedLevel				= "These burners are reserved for players with cooking level %s%s."
cooking.notif.reservedLevel.premium		= " or Premium"
cooking.notif.cantOperate				= "Only the owner of the stove may operate it."
cooking.notif.emptyPot 					= "All you did was heat an empty pot up, dummy."
cooking.notif.failedRecipe				= "You failed to follow the recipe, and your cooking has resulted in inedible mush."
cooking.notif.goodRecipe				= "You've successfully cooked %s!"
cooking.notif.ownPots					= "You can only use your own cooking pots!"
cooking.notif.farAway					= "You're too far away from the cooking pot!"

-- CPR module
cpr.notif.emergencyOnly 				= "Sadece acil servisler insanlar üzerinde CPR yapabilir!"
cpr.notif.stabalized 					= "Bu oyuncunun durumu dengelidir ve güçlü bir nabız hissedebilirsiniz."
cpr.notif.noPulse 						= "Nabız hissetmiyorsunuz"
cpr.notif.almostWake 					= "Bu kişi bilinçli ve uyanmak üzere."
cpr.notif.feelPulse 					= "Bir %s nabız hissetmiyorsunuz" -- %s = cpr.strength.XXX
cpr.notif.alreadyStable 				= "Bu oyuncu zaten dengelendi!"
cpr.notif.tooFarGone 					= "İşe yaramıyor, çok ileri gitti."
cpr.notif.alreadyConscious 				= "Bilinçli bir insanda CPR yapamazsınız!"
cpr.notif.alreadyPerforming 			= "Birisi bu kişi üzerinde zaten bir CPR yapıyor!"
cpr.notif.needKit 						= "Bu işlemi gerçekleştirmek için bir CPR kitine ihtiyacınız var!"
cpr.notif.kitRunOut 					= "Meslek medkitiniz bitti, lütfen Ekipman Dolabınızda doldurun."
cpr.notif.startedCPR 					= "%s, %s kişisi üzerinde CPR yapmaya başladı"

cpr.strength.veryStrong 				= "çok güçlü"
cpr.strength.barely 					= "neredeyse hiç"
cpr.strength.weak 						= "zayıf"
cpr.strength.veryWeak 					= "çok zayıf"
cpr.strength.strong 					= "güçlü"

cpr.action.checkPulse 					= "%s, %s kişisinin nabzını kontrol eder."
cpr.action.succeedCPR 					= "%s, %s kişisi üzerinde CPR yaptı."
cpr.action.failedCPR 					= "%s, %s kişisi üzerinde CPR yapamadı."
cpr.action.youSucceedCPR 				= "Hastayı CPR yapmayı başardınız!"
cpr.action.youFailedCPR 				= "Hastayı CPR yapmada başarısız oldunuz!"
cpr.action.noComplete 					= "%s, %s üzerinde CPR yapamadı."
cpr.action.performing 					= "CPR YAPILIYOR"

-- Crafting Module
crafting.gui.title 						= "ÜRETİM MENÜSÜ"
crafting.gui.queue 						= "ÜRETİM SIRASI"
crafting.gui.search 					= "Ara"
crafting.gui.claim 						= "EŞYALARI AL"
crafting.gui.duration 					= "Süre: "
crafting.gui.amount 					= "Miktar: %s"
crafting.gui.skill 						= "Yetenek: %s"
crafting.gui.craft 						= "ÜRET"
crafting.gui.requires 					= "ÜRET (%s GEREKTİRİYOR)"
crafting.gui.notEnough 					= "ÜRET (YETERSİZ KAYNAK)"
crafting.gui.amt 						= "Miktar"
crafting.gui.ing 						= "Malzeme"
crafting.gui.inv 						= "Envanter"
crafting.gui.noCatItems 				= "Bu kategoride ürün yok. Doğru bir çalışma tezgahı veya filtre kullanın."
crafting.gui.allRecipes 				= "Tüm Tarifler"

crafting.type.crafting 					= "Üretim Masası"
crafting.type.woodcutting 				= "Odun İşleme Masası"
crafting.type.metalworking 				= "Metal İşleme Masası"
crafting.type.cooking 					= "Pişirme Ocağı"

crafting.notif.noResources 				= "Bu öğeyi oluşturmak için gerekli kaynaklara sahip değilsiniz."
crafting.notif.noLevel 					= "Bu eşyayı yapmak için gereken seviyeye sahip değilsiniz."
crafting.notif.queueLimit 				= "Sıra limitine ulaştınız."
crafting.notif.someoneStole 			= "%s tezgahını çaldı!"
crafting.notif.youStole 				= "%s, %s kişisinin tezgahını çaldın!"
crafting.notif.youCrafted 				= "%s ürettin."
crafting.notif.noInventory 				= "%s almak için envanter alanınız yok."
crafting.notif.cantClaimDidntCraft 		= "Eşyaları alamazsınız çünkü tezgahtaki eşyaları üretmediniz."

-- Crews module
crews.gui.crewName 						= "Ekip İsmi"
crews.gui.defaultName 					= "Lorem Ipsum"
crews.gui.listOnline 					= "Şu anda çevrimiçi ekip üyelerinin bir listesi."
crews.gui.invalidRank 					= "Geçersiz Rütbe"
crews.gui.promptKick 					= "%s kişisini ekibinden atmak istediğine emin misin?"
crews.gui.setRank 						= "Rütbe Ayarla"
crews.gui.promptRank 					= "%s kişisini %s yapmak istediğine emin misin?"
crews.gui.transferLeadership 			= "Liderliği Transfer Et"
crews.gui.promptTransfer 				= "%s adlı kişiyi yeni lider yapmak istediğine emin misin?"
crews.gui.invalidName 					= "Geçersiz İsim"
crews.gui.offlineMembers 				= "ÇEVRİMDIŞI ÜYELER"
crews.gui.noOffline 					= "Şu anda çevrimdışı ekip üyesi yok."
crews.gui.listOffline 					= "Şu anda çevrimdışı ekip üyelerinin bir listesi."
crews.gui.loading 						= "Yükleniyor..."
crews.gui.inviteMembers 				= "Üye Davet Et"
crews.gui.inviteDesc 					= "Yeni bir üye davet etmek için yer."
crews.gui.search 						= "Ara: "
crews.gui.promptInvite 					= "%s kişisini ekibine davet etmek istediğine emin misin?\nNotice: This will make them the rank with lowest immunity."
crews.gui.information 					= "BİLGİLENDİRME"
crews.gui.numMembers 					= "Şuanda %s üye var. (Grup: %s)"
crews.gui.yourRank 						= "'%s' olarak rütbelendirildin"
crews.gui.actions 						= "EYLEMLER"
crews.gui.editName 						= "Ekip Adını Düzenle"
crews.gui.promptEditName 				= "Ekip adını neyle değiştirmek istiyorsunuz?"
crews.gui.editColor 					= "Ekip Rengi Düzenle"
crews.gui.color 						= "RENK DÜZENLE"
crews.gui.modifyColor 					= "Ekibinizin renginde değişiklikler yapın."
crews.gui.setColor 						= "Renk Seç"
crews.gui.promptColor 					= "Bunun yeni ekip renginiz olmasını istediğinizden emin misiniz?"
crews.gui.editLogo 						= "Ekip Logosu Düzenle"
crews.gui.editLogoCaps 					= "LOGO DÜZENLE"
crews.gui.clickLogo 					= "Şuanki logonu güncellemek için bir logoya tıkla."
crews.gui.haventSelectedLogo 			= "Bir logo belirlemedin!"
crews.gui.promptLogo 					= "Bu logo ekibinin yeni logosu olması istediğine emin misin?"
crews.gui.viewMembers 					= "Üye Listesini Görüntüle"
crews.gui.leaveCrew 					= "Ayrıl: %s"
crews.gui.promptLeave 					= "%s ekibinden ayrılmak istediğine emin misin?"
crews.gui.promptOwnerLeave 				= "Tekrar soracağım: %s lideri olarak, bu ekipten ayrıldığında ekibin dağılacak. Bu geri döndürülemez. Emin misin?"
crews.gui.anotherMembersList 			= "%s üyeleri"
crews.gui.crew 							= "Ekip"
crews.gui.pending 						= "Bekleyen ekip"
crews.gui.defaultrank 					= "Owner's Rank Name"
crews.gui.nameColon 					= "İsim: "
crews.gui.colorColon 					= "Renk: "
crews.gui.logoColon 					= "Logo: "
crews.gui.createAndCost 				= "Oluştur (%s)"
crews.gui.promptCreate 					= "Bu ekibi kurmak istediğine emin misin? Ücreti %s olacak."
crews.gui.crews							= "Ekipler"
crews.gui.sectionSubhead 				= "Buradan ekibinizin yönetim eylemleri gerçekleştirin."

crews.notif.notInCrew 					= "Bir ekipte değilsin."
crews.notif.noPermInvite 				= "Ekibe üye davet etmeye yetkin yok."
crews.notif.alreadyInCrew 				= "%s zaten bir ekipte."
crews.notif.defaultError 				= "Bu eylemi gerçekleştiremezsin"
crews.notif.youWereInvited 				= "${crewName} ekibine ${name} tarafından davet edildin"
crews.notif.youInvited 					= "${name} kişisini ${crewName} ekibine davet ettin"
crews.notif.noPerm 						= "Bu işlemi gerçekleştirme izniniz yok."
crews.notif.higherImmunity 				= "Bu üyeyi eklemeye çalıştığınız rütbenin kendi rütbenizden daha yüksek bir dokunulmazlığı var."
crews.notif.updatedRank 				= "Üyenin rütbesini güncelledin."
crews.notif.somethingWrong 				= "Üye sıralamasını güncellerken bir şeyler ters gitti"
crews.notif.cantKickYourself 			= "Kendini atamazsın."
crews.notif.noRightsKick 				= "Bu üyeyi atmaya yetkin yok."
crews.notif.youKicked 					= "Birisini ekipten attın."
crews.notif.gotKicked 					= "Ekipten ${name} tarafından atıldın."
crews.notif.youJoined 					= "${name} ekibine katıldın."
crews.notif.acceptedInvitation 			= "${name} ekip davetiyeni kabul etti."
crews.notif.noInvite 					= "Bu ekibe şu an için davetiye yok."
crews.notif.transferLeadership 			= "${crewName} ekibinin liderliğini transfer ettiniz."
crews.notif.nowLeader 					= "Artık ${crewName} ekibinin liderisiniz, tebrikler!"
crews.notif.nameLongShort 				= "Ekip ismi çok uzun veya kısa"
crews.notif.cantAfford 					= "Ekip kurmaya paran yetmiyor"
crews.notif.crewCreated 				= "Yeni ekibini kurdun."
crews.notif.disbanded 					= "Dağıttın: %s"
crews.notif.founderRank					= "You cannot invite a player to the founder (default) rank. Make a new rank."


crews.ranks.user 						= "Üye"
crews.ranks.mod 						= "Moderatör"
crews.ranks.admin 						= "Yönetici"
crews.ranks.leader 						= "Lider"

-- Daily Rewards
drewards.gui.title 						= "Günlük Ödüller"
drewards.gui.rewardsTitle 				= "Bugünün Ödülleri"
drewards.gui.skillBooster 				= "%s%% yetenek arttırıcı (%s)" -- 50% skill booster for 30:00
drewards.gui.possibleRewardsHeader 		= "Bugün alabileceğiniz ödüller"
drewards.gui.rewardsMessage 			= "Ödüller her gün talep edilebilir. Art arda talep edilen her ödül, ödül seviyenizi artıracaktır."
drewards.gui.claimReward 				= "Ödül Al"

drewrads.rewards.money 					= "Para Ödülü"
drewards.rewards.booster 				= "%s Arttırıcı"

drewards.notif.activeReward 			= "Alabildiğin bir günlük ödül var! F3 basarak alabilirsin."
drewards.notif.alreadyClaimed 			= "Zaten bugünün ödülünü kazandın!"
drewards.notif.received 				= "Günlük ödülden %s kazandın!"
drewards.notif.notEnoughSpace			= "You do not have enough space in your inventory to receive the reward. Operation Cancelled." -- TODO

drewards.log.claimReward				= "%s claimed a reward now, with the following assets: %s."

-- Dispatch Module
dispatch.gui.title 						= "Yeni Çağrı Geliyor!"
dispatch.gui.accept						= "KABUL ET (O)"
dispatch.gui.dismiss 					= "REDDET (N)"
dispatch.gui.caller						= "Çağıran: %s"
dispatch.gui.description 				= "Açıklama: %s"

dispatch.notif.leaveToNotify 			= "Tekrar çağrılar hakkında bildirimler almak istiyorsanız, acil durum/servis uygulamanıza gidin ve mevcut çağrıdan çıkın."
dispatch.notif.newCall 					= "Yeni bir 911 çağrısı yapıldı! Acil durum uygulamanızı kontrol edin!"
dispatch.notif.newCallout 				= "Yeni bir ek bilgi mevcut! Tabletinizi kontrol edin!"
dispatch.notif.callConcluded 			= "Katıldığınız 911 çağrısı sonuçlandı."
dispatch.notif.automatic 				= "Otomatik Çağrı"
dispatch.notif.alreadyInCallout 		= "Zaten çağrıda yer alıyorsunuz"
dispatch.notif.youreResponder 			= "Aramanın ilk yanıtlayıcısı sensin! İşiniz bittiğinde aramayı bitirmeyi unutmayın!"
dispatch.notif.onlyOne 					= "Bu çağrıya yalnızca bir kişi katılabilir!"
dispatch.notif.callDelay 				= "Yeni bir 911 çağrısı yapmadan önce lütfen ${second} saniye bekleyin."
dispatch.notif.useRadio 				= "Bir devlet/acil durum işinde arama yapamazsınız, iş arkadaşlarınızla iletişim kurmak için radyo kullanın!"
dispatch.notif.autoClose 				= "Çağrı, ${minutes} dakika sonra otomatik olarak kapanacak!"
dispatch.notif.tooEarly 				= "Bir çağrıyı bu kadar erken bitiremezsiniz!"
dispatch.notif.jobChangeClear 			= "İş değişikliğiniz nedeniyle 911 çağrılarınız silindi!"

dispatch.action.newCall 				= "%s yeni bir 911 çağrısı oluşturdu."

dispatch.log.newCall 					= "%s adlı kişi 911 çağrısı oluşturdu. (Departman: ${department}) (Açıklama: \"${description}\")"

-- Durability Module
durability.gui.title 					= "TAMIR MASASI"
durability.gui.subTitle 				= "Onarım maliyetinin ne kadar olacağını kontrol etmek için bir öğe ekleyin."
durability.gui.brokenCantRepair 		= "Bu eşya kırık ve tamir edilemez."
durability.gui.noNeedRepair 			= "Bunu tamir etmeye ihtiyacın yok"
durability.gui.promptRepair 			= "Bu eşyayı tamir etmek istediğine emin misin?"
durability.gui.repair 				 	= "Tamir et"

durability.notif.cantRepair 			= "Bu eşyayı tamir edemezsin."
durability.notif.noSelect 				= "Tamir edilecek bir eşya seçmedin."
durability.notif.noNeedRepair 			= "Bu eşyayı tamir etmeye ihtiyacın yok."
durability.notif.completelyBroken 		= "Tamamen kırık eşyaları tamir edemezsin."
durability.notif.cantAffordRepair 		= "Bu tamiri karşılayamıyorsun."
durability.notif.youRepaired 			= "%s tamir ettin"
durability.notif.thingBroken 			= "%s kırık. Tamir masasında tamir et!"
durability.notif.equipHandcuff 			= "Kelepçeliyken eşyaları donanamazsın"
durability.notif.notJammed 				= "Silahın sıkışmadı."

durability.action.unJamming 			= "ÇÖZÜLÜYOR"

durability.binds.unjamWeapon 			= "Silahı çöz"

-- EXP module
exp.gui.xp 								= "xp"
exp.gui.jobEXP 							= "< Mexlek EXP"

-- Factory module
factory.gui.close 						= "KAPAT ✕"
factory.gui.none 						= "Hiç"
factory.gui.active 						= "Aktif"
factory.gui.notActive 					= "Aktif Değil"
factory.gui.conveyorColon 				= "Konveyör Bandı: %s"
factory.gui.powerConsumption 			= "Güç tüketimi: %s W"
factory.gui.placed 						= "Yerleştirildi"
factory.gui.status						= "DURUM"
factory.gui.ingredient 					= "Malzeme"
factory.gui.amount						= "Miktar"
factory.gui.available 					= "Mevcut"
factory.gui.craft 						= "Üret"
factory.gui.metalIngots 				= "Metal Barlar"
factory.gui.searchFailed 				= "Aramanda bir eşya bulunamadı"
factory.gui.cantCraftComp 				= "Mevcut bileşenlerle hiçbir şey yapamazsınız"
factory.gui.removeQueue 				= "Kuyruktan kaldır"
factory.gui.furnace						= "Ocak"
factory.gui.highEndManufacturing 		= "Üst düzey üretim."
factory.gui.items						= "Eşyalar"
factory.gui.listItems 					= "Üretebileceğiniz öğelerin listesi."
factory.gui.factoryQueue 				= "FABRİKA KUYRUĞU"
factory.gui.search 						= "Ara"
factory.gui.inspect 					= "İNCELE"
factory.gui.invItemsSub 				= "Envanterinizdeki bileşen listesine ekleyebileceğiniz öğeler."
factory.gui.components 					= "BİLEŞENLER"
factory.gui.usedCreateNew 				= "Yeni öğeler oluşturmak için kullanılan öğeler."
factory.gui.take 						= "Al"
factory.gui.noItemsInFurnace			= "Fırında eşya yok"
factory.gui.insert 						= "Yerleştir"
factory.gui.inventory 					= "ENVANTER"
factory.gui.storageBoxColon 			= "Depo Kutusu: %s"

factory.notif.entFarExist 				= "Eşya yok veya fırın yok veya çok uzakta"
factory.notif.failedCraft 				= "Eşya üretilemedi"
factory.notif.furnaceTakeItems 			= "Fırından bazı eşyalar aldınız."
factory.notif.somethingWrong 			= "Bir şeyler yanlış gitti."
factory.notif.furnaceCancelCratf 		= "Bir öğeyi oluşturmayı iptal ettiniz. Gerekli tüm bileşenleri Fırın Bileşenlerine geri yerleştirildi."
factory.notif.itemNotFound 				= "Eşya bulunamadı"
factory.notif.elseUsing 				= "%s kişisi senin fabrikanı kullanmaya başladı"
factory.notif.usingSomeonesFactory 		= "%s kişisinin fabrikasını kullanmaya başladın."
factory.notif.noConveyor 				= "Konveyör bandı yok, bir tane tak."
factory.notif.notFactoryItem 			= "Bir fabrika eşyası değil."
factory.notif.lvlInsuff 				= "Bu işlemi gerçekleştirmek için seviye gereksinimlerini karşılamıyorsunuz"
factory.notif.coneyorInactive 			= "Konveyör bandı aktif değil, değiştirin."
factory.notif.cantAffordManufacture	 	= "Bu eşyanın üretimini karşılayamıyorsun"
factory.notif.tooManyQueue 				= "Üretim kuyruğunda çok öğe var."
factory.notif.addedQueue 				= "Üretim Kuyruğuna bir öğe eklediniz."

factory.misc.energyUnit 				= "J"

-- HUD module
hud.notif.noPlayerFound 				= "Oyuncu bulunamadı"
hud.notif.noTaxiDrivers 				= "Şu anda taksi şoförü yok."
hud.notif.silentToggle 					= "Tabletinizde Sessiz Durum değiştirildi."
hud.notif.blacklistedVoice				= "Sesli sohbet kullanmanız yasaklandı."

hud.gui.outOfStock 						= "Stokta kalmadı!"
hud.gui.unknown 						= "Bilinmeyen"
hud.gui.noAmmo 							= "Mermi Yok"
hud.gui.levelStuff 						= "Seviye: %s - Kalan EXP: %s"
hud.gui.levelReached 					= "%s seviyeye ulaşıldı!"
hud.gui.male 							= "ERKEK"
hud.gui.female 							= "KADIN"
hud.gui.noResidence 					= "KONUT YOK"
hud.gui.policeTape 						= "Polis Bandı"
hud.gui.nobody 							= "Kimse"
hud.gui.frozenHead 						= "Bir yetkili tarafından DONDURULDUN. Lütfen sakin kalın."
hud.gui.frozenSub 						= "Adil olmadığını düşünüyorsanız lütfen !report kullanın."
hud.gui.invisible 						= "Görünmez"

hud.gui.election.header 				= "BAŞKANLIK SEÇİMİ"
hud.gui.election.text 					= "%s, %s saniye içinde bitecektir."
hud.gui.election.registration 			= "Kayıt"
hud.gui.election.voting 				= "Oylama"
hud.gui.election.err 					= "HATA"

hud.gui.emergency.active 				= "Şuanda Aktif"
hud.gui.emergency.header 				= "ACİL DURUM"
hud.gui.emergency.reason 				= "Sebep: \"%s\""

hud.scoreboard.players 					= "Oyuncular: %s/%s"
hud.scoreboard.premiumStore 			= "Premium Market"
hud.scoreboard.serverUpdates 			= "Sunucu Güncellemeleri"
hud.scoreboard.guides 					= "Rehberler"
hud.scoreboard.forums 					= "Forumlar"
hud.scoreboard.discord 					= "Discord"
hud.scoreboard.steamGroup 				= "Steam Grubu"
hud.scoreboard.apply 					= "Yetkili Başvurusu"
hud.scoreboard.serverFallback 			= "SUNUCU 1"
-- @TODO: Replace serverguard's howtoapply command in cl_scoreboard.lua line 200
-- @TODO: Change spookfest name to localized in line 229 of cl_scoreboard.lua

-- don't know which of these might change w/ your guys' server, so i'm adding them all:
hud.links.servers 						= "https://monolithservers.com/#servers"
hud.links.premium 						= "https://monolithtr.com/store/premium"
hud.links.forums 						= "https://monolithservers.com/forums"
hud.links.updates 						= "https://monolithservers.com/forums/forums/updates.43/"
hud.links.guides 						= "https://steamcommunity.com/id/fatal1st/myworkshopfiles/?section=guides"
hud.links.discord 						= "https://discord.gg/9MYyTTUwSZ"
hud.links.group 						= "https://steamcommunity.com/groups/monolithtr"

hud.menu.copySteamName 					= "Steam İsmi Kopyala"
hud.menu.copyRPName 					= "RP İsmi Kopyala"
hud.menu.copySteamID 					= "SteamID Kopyala"
hud.menu.openProfile 					= "Profili Aç"
hud.menu.bringPlayer 					= "Oyuncu Çek"
hud.menu.gotoPlayer 					= "Oyuncuya Işınlan"
hud.menu.cancel 						= "İptal"

hud.tablet.taxi 						= "Taksi"
hud.tablet.taxi.call					= "TAKSİ ÇAĞIR"
hud.tablet.taxi.inside 					= "Taksinin içine gir"
hud.tablet.licenses 					= "Lisanslar"
hud.tablet.licenses.request 			= "İstek"
hud.tablet.imessage.notify.youblocked	= "You can't send a message to someone you blocked." -- Todo
hud.tablet.imessage.notify.areblocked	= "Failed to send message, you are blocked by the recipient." -- Todo
hud.tablet.imessage.unblock				= "Unblock" -- Todo
hud.tablet.imessage.setblocked			= "Block"	-- Todo
hud.tablet.imessage			 			= "iMesaj"
hud.tablet.imessage.chatOffline 		= "Çevrimdışı kişilerle sohbet edemezsin"
hud.tablet.imessage.available 			= "Mevcut"
hud.tablet.imessage.online 				= "Çevrimiçi"
hud.tablet.imessage.offline 			= "Çevrimdışı"
hud.tablet.trading 						= "Takas"
hud.tablet.trading.selectPlayer 		= "Takas yapacağın oyuncuyu seç"
hud.tablet.trading.sentRequest 			= "%s kişisine takas isteği gönderdin."
hud.tablet.laws							= "Yasalar"
hud.tablet.laws.permanent 				= "Kalıcı Yasalar"
hud.tablet.laws.temporary 				= "Geçici Yasalar"
hud.tablet.monomap 						= "Mono-Harita"
hud.tablet.monomap.layer 				= "Bölge %s"
hud.tablet.monotablet 					= "Mono-Tablet"
hud.tablet.depttablet					= "Dept-Tablet"
hud.tablet.settings 					= "Ayarlar"
hud.tablet.settings.suppressSMS 		= "SMS Mesajlarını Gizle"
hud.tablet.emergency 					= "Servisler & 911"
hud.tablet.emergency.callouts 			= "GÜNCEL ÇAĞRILAR"
hud.tablet.emergency.endCallout 		= "SON"
hud.tablet.emergency.leave 				= "AYRIL"
hud.tablet.emergency.focus 				= "FOKUS"
hud.tablet.emergency.noOne 				= "Kimse!"
hud.tablet.emergency.info 				= [[Ek bilgi kimliği: ${calloutID}
Yanıtlayan birimler: ${officerAmt}
İlk yanıtlayan: ${firstResponder}
Çağıran: ${caller}
Departman: ${department}

Açıklama: ${description}]]
hud.tablet.emergency.message			= "ACİL DURUM MESAJI"
hud.tablet.emergency.dept.fire 			= "İtfaiye Dep."
hud.tablet.emergency.dept.police 		= "Polis Dep."
hud.tablet.emergency.dept.ems 			= "EMS Dep."
hud.tablet.emergency.dept.tow 			= "Çekici Şirketi"
hud.tablet.emergency.request 			= "YARDIM İSTE"
hud.tablet.emergency.unit 				= "%s Birim"
hud.tablet.moveDesktop 					= "Masaüstüne taşı"
hud.tablet.successMove 					= "Uygulama başarıyla masaüstünüze taşındı."

hud.misc.hz 							= "%s MHz" -- Hertz abbreviation
-- @TODO: modules/hud/sh_toast.lua zones

-- Hunger module
hunger.notif.hungry 					= "Şuanda aç hissediyorsun."
hunger.notif.thirsty 					= "Şuanda susuz hissediyorsun."

-- Instanced housing module
instancedhousing.gui.promptEnter 		= "Bu eve girmek ister misin?"
instancedhousing.gui.youWillTP			= "Yeni bir lokasyona ışınlanacaksın."

instancedhousing.notif.entered 			= "Evine girdin"
instancedhousing.notif.left 			= "Evinden çıktın"

instancedhousing.notif.purchased		= "You purchased a property for %s"

-- Instanced layers module
instancedlayers.notif.cantDeploy 		= "Öğeleri başka bir durumda açamazsınız"
instancedlayers.notif.cantDrop 			= "Öğeleri başka bir durumda bırakamazsınız"
instancedlayers.notif.anotherInstance	= "%s başka bir durumda. Ona katıldın."
instancedlayers.notif.returning 		= "Eski durumunuza geri dönüyorsunuz."

-- Insurance module
insurance.gui.unemployed 				= "İşsiz"
insurance.gui.understandClose 			= "Anladım, kapat."
insurance.gui.treatmentWouldHave		= "Tedavinin maliyeti %s olacaktı, ancak sigortanız daha fazla ayrıntı için aşağıyı okuyun;\n\n"
insurance.gui.fullRate 					= "Ödenebilir %s cüzdanın 100%% oranından.\n"
insurance.gui.someRate 					= "Ödenebilir %s (indirimler sonrası) bankandan 70%% veya premium ise 40%% oranda.\n"
insurance.gui.premiumApplied 			= "Prim ücreti uygulanır, banka ücreti düşürülmelidir.\n"
insurance.gui.wantedNoDiscount 			= "Aranıyorsunuz, bu yüzden indirim uygulanmadı.\n"
insurance.gui.tip 						= "\nİPUCU: Bankadan ödenen ücretler düşürülür - Paradan tasarruf etmek için yalnızca bankadan ücret ödemeye çalışın."

insurance.notif.newbie 					= "Burada yeni olduğunuz için hastane ücretleriniz Devlet tarafından karşılandı. 40. seviyenin üzerindeyken sigorta ücretlerinizi ödemeniz gerekeceğini unutmayın."
insurance.notif.paidWallet 				= "Hastane ücretlerini (%s) cüzdanınızdan ödediniz."
insurance.notif.paidPartWallet 			= "Hastane ücretinizin bir kısmını (%s) cüzdanınızdan ödediniz."
insurance.notif.paidPartBank 			= "Hastane ücretinizin (%s) bir kısmını bankanızdan ödediniz."
insurance.notif.paidBank 				= "Hastane ücretlerinizi (%s) bankanızdan ödediniz."

insurance.log.paidTreatment	  			= "%s, tedavisi için %s ödedi."

-- Intro Screen Module
intro.gui.startLife 					= "YENİ HAYATINA BAŞLA"
intro.gui.rpName 						= "ROLEPLAY İSMİ"
intro.gui.fallbackName 					= "Ali Akbulut"
intro.gui.warning 						= [[
	 • Lütfen gerçekçi bir rol ismi seçin.
	 • İsimler, dalga/siyasi isimler olmamalı.
	 • Bu kuralları takip etmemeniz ban ile sonuçlanır.]]
intro.gui.gender 						= "CİNSİYET"
intro.gui.face							= "YÜZ"
intro.gui.variants 						= " BİÇİM (PREMİUM ÖZEL)"
intro.gui.presents 						= "Sunar..."
intro.gui.spookfest.sponsored 			= "Sponsorlayan..."
intro.gui.spookfest.spooky 				= "Spooky Scary Skeletons"
intro.gui.undocumented 					= "Belgesiz"
intro.gui.citizen 						= "Sivil"
intro.gui.example 						= "Örnek"
intro.gui.name 							= "İsim"
intro.gui.referralsProgram				= "REFERRALS PROGRAM"						-- TODO
intro.gui.referralsQuestion				= "WERE YOU REFERRED TO MONOLITH?"			-- TODO
intro.gui.refereeSteamID				= "REFEREE'S STEAMID64"						-- TODO
-- @TODO: Update profanity filter intro_screen/sh_charactercreation.lua
-- @TODO: Expand invalid characters check in intro_screen/sh_charactercreation.lua

intro.notif.invalidName 				= "Geçersiz bir ad girdiniz (En az 3 harf, En çok 15. Karakter A-Z, küfür yok.)"
intro.notif.invalidReferral				= "You have entered an invalid steamID64. Please try again." -- TODO

-- Inventory module (imported from legacy)
inventory.inventory 					= "Envanter"
inventory.inventory_description 		= "Envanterin, içinde %i eşya olabilir."

inventory.actions 						= "Eylemler"
inventory.stats 						= "İstatistik"
inventory.confiscatable 				= "El koyulabilir"
inventory.drop_on_death 				= "Ölünce Düşer"
inventory.deploy_instructions 			= "Sol tık ile doğ, CTRL ile iptal et"
inventory.character_load_failure 		= "Karakter verisi yüklenemedi"
inventory.notifications 				= "Bildirimler"
inventory.cant_put_x_on_y 				= "%s öğesini %s üzerine koyamazsınız"

inventory.equip 						= "Donan"
inventory.use 							= "Kullan"
inventory.deploy 						= "Yerleştir"
inventory.consume 						= "Tüket"
inventory.sell_with_price 				= "Sat (%s)"
inventory.sell_all						= "Sell All"	-- TODO
inventory.sell_all_confirmation			= "Are you sure you want to do this?\nThis will sell all of this item in your inventory."	-- TODO
inventory.drop 							= "Düşür"
inventory.drop_singular 				= "Bir Düşür"
inventory.drop_half 					= "Yarısını Düşür"
inventory.drop_custom 					= "X Düşür"
inventory.drop_helper_description 		= "Bırakmak istediğiniz öğe miktarını belirtin"
inventory.split 						= "Böl"
inventory.split_amount 					= "Ne kadar bölmek istiyorsun (Maks %i)"
inventory.market 						= "Market"
inventory.marketing 					= "Pazarlama"
inventory.marketing_helper_description 	= "Bu öğenin fiyatının ne olmasını istersiniz?"
inventory.form_requirement_number 		= "Form sayısal bir yanıt gerektiriyor."
inventory.form_requirement_string 		= "Form yazısal bir yanıt gerektiriyor."
inventory.form_requirement_specific_length 	= "Form %i uzunlukta bir cevap gerektiriyor"
inventory.custom_title_prompt 			= "Özel bir başlık vermek ister misiniz?"
inventory.remove_attachment 			= "Eki Kaldır"
inventory.delete_item 					= "Kaldır"
inventory.are_you_sure_prompt 			= "Bu eylemi gerçekleştirmek istediğinizden emin misiniz? Bu eylem geri döndürülemez olabilir."
inventory.inventory_load_failure 		= "Envanter verisi yüklenemedi"
inventory.unequip 						= "Çıkar"
inventory.streamer_mode 				= "Yayıncı Modu"
inventory.unknownPlayerName 			= "Bilinmeyen Oyuncu İsmi"
inventory.mainMenuToggle 				= "Ana Menü Geçişi"
inventory.splitIt 						= "Böl!"
inventory.titleLongerThan17 			= "Başlık 17 karakterden uzun olamaz."
inventory.enterValidPrice 				= "Geçerli bir ücret girmelisin."
inventory.hey 							= "Merhaba!"
inventory.promptDelete 					= "Silmek istediğine emin misin?: %s"
inventory.promptHigherHundredThou 		= "Ücret en fazla %s olabilir."
inventory.inventory_open_in_vehicle 	= "Araçtayken envanterinizi açamazsınız."
inventory.inventory_open_handcuffed 	= "Kelepçeliyken envanterinizi açamazsınız."
inventory.inventory_open_surrendered 	= "Teslim olmuşken envanterinizi açamazsınız."
inventory.inventory_open_arrested 		= "Hapisteyken envanterinizi açamazsınız."
inventory.inventory_open_unconscious 	= "Bu eylemi gerçekleştirebilmek için ayakta olmalısınız."
inventory.inventory_going_too_fast		= "You're going too fast. Slow down before opening your inventory again."
inventory.changeVariant					= "Change Variant"

inventory.tooltip_changelog 			= "Güncelleme günlüğümüzü görüntülemek için buraya tıklayın!"
inventory.tooltip_servers 				= "Sunucular sayfamızı görüntülemek için buraya tıklayın!"

inventory.viewer.someonesInventory 		= "ENVANTER (%s)"
inventory.viewer.primary 				= "BIRINCIL SILAH"
inventory.viewer.secondary 				= "IKINCIL SILAH"
inventory.viewer.misc 					= "OZEL ESYALAR"
inventory.viewer.player 				= "OYUNCU"
inventory.viewer.money					= "%s parası var"
inventory.viewer.confiscate 			= "El koy"
inventory.viewer.confiscateIllegal 		= "TÜM İLLEGAL EŞYALARA EL KOY"

inventory.labels.premium 				= "Premium Özel"
inventory.labels.job 					= "Meslek Eşyası"
inventory.labels.exotic 				= "Egzotik" -- TODO: Needs translation.
inventory.labels.soulbound 				= "Ruha bağlı"
inventory.labels.stackSize 				= "Yığın Boyutu: %s"
inventory.labels.thirst 				= "${state} ${amount} Susuzluk"
inventory.labels.hunger 				= "${state} ${amount} Açlık"
inventory.labels.ticket 				= "Bitiş tarihi: ${date}"
inventory.labels.jammed 				= "Sıkışmış"
inventory.labels.stats 					= "Silah İstatistikleri:"
inventory.labels.damage 				= "Hasar: %s"
inventory.labels.supportedAttachments 	= "Desteklediği Ekler:"

-- Labels for varianted items
inventory.labels.variant.haunted		= "Haunted"
inventory.labels.variant.bewitched		= "Bewitched"
inventory.labels.variant.demonic		= "Demonic"
inventory.labels.variant.holly			= "Holly"
inventory.labels.variant.jolly			= "Jolly"
inventory.labels.variant.golly			= "Golly"

inventory.notif.blacklistWeapons 		= "Kara listede iken silah teçhiz edemezsiniz."
inventory.notif.weaponsOnDuty 			= "Görevdeki silahları donatamazsın."
inventory.notif.itemOnDuty 				= "Bu eşyayı görevde kullanamazsınız."
inventory.notif.equipLimit 				= "Bu öğenin %s'sinden fazlasını donatamazsınız."
inventory.notif.cantEquip 				= "Bu eşyayı donatamazsın."
inventory.notif.cantUseItem 			= "Bu öğeyi kullanamazsınız."
inventory.notif.dropPremium 			= "Yalnızca premium öğeleri bırakamazsınız."
inventory.notif.dropSoulbound 			= "Ruha bağlı eşyaları bırakamazsın."
inventory.notif.dropJobItems 			= "İş öğelerini bırakamazsınız"
inventory.notif.alreadyDeployed 		= "Bu öğe zaten konuşlandırıldı. Lütfen önce onu alın."
inventory.notif.cantUseNotEnough 		= "Bu öğeyi kullanamazsınız. (Yeterli değil)"
inventory.notif.failedPickup 			= "Bu öğeyi alamadın."
inventory.notif.marketedStolen 			= "Pazarlanan öğeniz çalındı ​​çünkü ondan çok uzaktasınız!"
inventory.notif.refreshSpam 			= "Spam nedeniyle envanterinizi yenileyemezsiniz."
inventory.notif.cannotSellPremium 		= "Bu premium ürünü satamazsın."
inventory.notif.cannotSellSoulBound 	= "Ruha bağlı eşyalar satamazsın."
inventory.notif.cannotSellJobItems 		= "İş eşyalarını satamazsınız."
inventory.notif.youSoldAnItem 			= "%s için bir ürün sattınız"
inventory.notif.invalidSellPos 			= "Bu eşyayı satmak için çok uzaktasınız."
inventory.notif.confiscatedWeapons 		= "%s'nin donanımlı silahlarına el koydun."
inventory.notif.gainedFromWeapons 		= "%s silahlarından %s kazandın."
inventory.notif.confYourWeapons 		= "%s donanımlı silahlarına el koydu."
inventory.notif.noSpace 				= "Bunu almak için envanter alanınız yok!"
inventory.notif.noDropPremium			= "Bu premium öğeyi bırakamazsınız."
inventory.notif.noDropSoulBound			= "Ruha bağlı eşyaları bırakamazsın."
inventory.notif.noDropThisType			= "Bu tür öğeleri bırakamazsınız."
inventory.notif.noDropJobOnly			= "Yalnızca iş öğelerini bırakamazsınız."
inventory.notif.noDropArrested			= "Tutuklanırken eşya bırakamazsınız."
inventory.notif.maxEntLimit 			= "Maksimum varlık sınırına ulaştınız."
inventory.notif.cantDropThis 			= "Bunu bırakamazsın."
inventory.notif.cantUseInVehicle 		= "Araçtayken eşya kullanamazsınız."
inventory.notif.cantSpawnEnt 			= "%s varlığı ortaya çıkamadı"
inventory.notif.deployed 				= "Varlık başarıyla konuşlandırıldı."
inventory.notif.failDeploy				= "Varlık konuşlandırılamadı."
inventory.notif.patDownNoSpace 			= "You do not have enough inventory space to do this!" -- TODO

inventory.actions.startsSearching 		= "/me çantasını araştırır"
inventory.actions.equips 				= "/me eline %s alır"
inventory.actions.unEquips 				= "/me çantasına %s koyar"
inventory.actions.unEquipDrop 			= "/me yere %s bırakır"
inventory.actions.unEquipping 			= "Silah geri konuluyor..."
inventory.actions.equipping 			= "Silah alınıyor..."
inventory.actions.confiscating 			= "EL KONULUYOR"
inventory.actions.destroys 				= "%s adlı kişi %s yok etti"
inventory.actions.destroyingItem 		= "EŞYA YOK EDİLİYOR"
inventory.actions.soldXofTheir			= "%s adlı kişi ${count} tane \"${name}\" sattı"
inventory.actions.startedSearching 		= "%s adlı kişi %s kişisinin envanterini arar."
inventory.actions.pickingUp 			= "ALINIYOR"
inventory.actions.patDown 				= "%s performs a patdown on %s" -- TODO

inventory.log.equipped 					= "%s eline aldı: %s"
inventory.log.unEquipped 				= "%s un-equipped item: %s"
inventory.log.unEquipDrop 				= "%s un-equipped and dropped item: %s"
inventory.log.deployed 					= "%s konuşlandırdı: %s"
inventory.log.destroyedItem 			= "%s eşya yok etti: %s"
inventory.log.searchedInventory 		= "%s, %s kişisinin envanterini araştırdı"
inventory.log.confiscatedWeapon 		= "%s, %s kişisinin donanılmış silahlarına el koydu"
inventory.log.inventoryPrefix 			= "[Envanter] "
inventory.log.pickupUnknownItem 		= "%s bilinmeyen bir eşya almaya kalkıştı: itemEnt = "
inventory.log.pickedUpItem 				= "%s picked up %sx of item: %s"
inventory.log.pickedUpItem.object 		= "Obje"
inventory.log.dropped					= "%s %sx eşya düşürdü: %s"
inventory.log.usedItem 					= "%s eşya kullandı: %s"
inventory.log.patDown 					= "%s performed a patdown on %s" -- TODO
inventory.log.sellItems     			= "%s sold %s (x%s) to an NPC shop for %s"

inventory.item.consumable 				= "Tüketilebilir"
inventory.item.equipment 				= "Ekipman"
inventory.item.ingredient 				= "Malzeme"
inventory.item.attachment 				= "Ek"
inventory.item.food 					= "Yiyecek"
inventory.item.noDescription 			= "Bu eşyanın bir açıklaması yok"
inventory.item.alreadyEquipped 			= "Zaten bu silahı donanmışsın"
inventory.item.deployingDisabled 		= "Eşya konuşlandırma deaktif: %s"
inventory.item.deployingDisableFallback	= "Sunucu İstikrarsızlığı"
inventory.item.cantDeployThis 			= "Bunu konuşlandıramazsın"
inventory.item.entityLimit 				= "Eşya limitine ulaştın."
inventory.item.inCombat 				= "Savaş anında bunu yapamazsın."
inventory.item.tooClose 				= "Konuşlandırmak için çok uzak"
inventory.item.cantDeployProperty 		= "Bunu bir mülkün dışında konuşlandıramazsın."

inventory.sell_to_store 				= "Sat: %s (%s)"

-- Inventory Containers
invcontainers.gui.container 			= "KONTEYNIR"
invcontainers.gui.subHeader 			= "Bu bir konteynır."
invcontainers.gui.currentObservers 		= "Güncel Gözlemciler:"
invcontainers.gui.storeUpTo 			= "Bu konteynırda en fazla %s öğe saklayabilirsiniz."
invcontainers.gui.money.hint			= "Hold 'SHIFT' and press the Withdraw button!"
invcontainers.gui.money.withdraw		= "How much cash would you like to withdraw from this Container?"
invcontainers.gui.money.deposit			= "How much cash would you like to deposit into this Container?"


invcontainers.notif.somethingWrong 		= "Konteynırı açarken bir şeyler yanlış gitti."
invcontainers.notif.cantUse 			= "Bu konteynırı kullanamazsın."
invcontainers.notif.container 			= "Konteynır"
invcontainers.notif.invalidQuantity 	= "Transfer başarısız. Geçersiz miktar."
invcontainers.notif.failWithdraw 		= "Bu eşyayı çekemedin."
invcontainers.notif.withdrawInvalidAmt 	= "Çekim başarısız. Geçersiz miktar istendi."
invcontainers.notif.confiscatedIllegal 	= "Bu illegal eşyaya el koydun."

invcontainers.lootBox 					= "YAĞMA KUTUSU"
invcontainers.corpse 					= "CESET"
invcontainers.corpse.info 				= "%s kişisinin kalıntıları"
invcontainers.backpack 					= "ÇANTA"

invcontainers.log.deposited 			= "%s deposited item: %s konteynıra%s."
invcontainers.log.ownedBy 				= " sahip: %s"
invcontainers.log.tookItem 				= "%s eşya aldı: %s bir konteynır%s."
invcontainers.log.confiscated 			= "%s eşyaya el koydu: %s konteynır%s"

-- Jobs module
jobs.notif.unknownJob 					= "Bilinmeyen meslek"
jobs.notif.positionsFilled 				= "Tüm pozisyonlar doldu. Lütfen daha sonra tekrar kontrol edin."
jobs.notif.unableBecome 				= "Bu mesleği olamazsın"
jobs.notif.requiresLevelCategory 		= "Bu meslek seviye %s %s gerektiriyor"
jobs.notif.requiresLevel 				= "Bu meslek seviye gerektiriyor"
jobs.notif.tow.inNeedRepair 			= "Bir araba şu anda onarıma ihtiyacı var."
jobs.notif.tow.proceedMarked			= "Lütfen işaretli yere ilerleyin."
jobs.notif.tow.repairmanOnWay 			= "Tamirci yolda!"
jobs.notif.tow.needMoneyToCall 			= "Çekici çağırmak için en az 500$'a ihtiyacınız var."
jobs.notif.tow.nextToVehicle	 		= "Aracınızın yanında olmanız gerekiyor!"
jobs.notif.tow.repairScheduled 			= "Aracınız için zaten bir onarım işi planlandı!"
jobs.notif.tow.unableFindTow 			= "Çekici bulamadık!"
jobs.notif.tow.vehicleRepaired 			= "İyi iş! Araç onarıldı."
jobs.notif.tow.onlyMechanicsRepair 		= "Aracınızı şu anda yalnızca tamirci tamir edebilir."
jobs.notif.employDetained 				= "Gözaltındayken bir mesleğin olamaz."
jobs.notif.blacklistedJob 				= "Bu meslek kategorisinin karalistesindesin."
jobs.notif.blacklistedCat				= "Bu meslek kategorisinin karalistesindesin."
jobs.notif.wanted 						= "Şuanda aranıyorsun. Lütfen başka zaman gel."
jobs.notif.waitBeforeChanging 			= "Meslek değiştirirken lütfen %s bekleyiniz."
jobs.notif.demote.pd					= "Emniyet Müdürlüğü seni yakın zamanda düşürdü. %s saniye beklemelisin"
jobs.notif.demote.tow					= "Çekme Departmanı seni yakın zamanda düşürdü. %s saniye beklemelisin"
jobs.notif.demote.ems					= "EMS Departmanı seni yakın zamanda düşürdü. %s saniye beklemelisin"
jobs.notif.demote.fire					= "İtfaiye Departmanı seni yakın zamanda düşürdü. %s saniye beklemelisin"
jobs.notif.demote.security				= "Güvenlik Departmanı seni yakın zamanda düşürdü. %s saniye beklemelisin"
jobs.notif.demote.mayor					= "Başkan seni yakın zamanda düşürdü. %s saniye beklemelisin"
jobs.notif.demote.doc					= "Ceza İnfaz Kurumu seni yakın zamanda düşürdü. %s saniye beklemelisin"
jobs.notif.premiumOnly 					= "Bu mesleğe geçebilmek için Premium olmalısın."
jobs.notif.quitFromJob 					= "Şuanki mesleğinden ayrıldın"
jobs.notif.reachedJobLevel 				= "Ulaştın: %s seviye: %s."
jobs.notif.demotedToLevel 				= "Düşürüldün: %s, seviye: %s."
jobs.notif.cantSpawnInHouse 			= "!!!BU OLDUĞUNDA BİZİ BİLGİLENDİRİN!!!" -- devul why didnt u add it :(
jobs.notif.unableSWAT					= "SWAT enlistment has not been made available by the Chief." --TODO

jobs.convar.jobmultiplier 				= "Meslek exp çarpanı nedir"

jobs.misc.unknown 						= "Bilinmeyen"
jobs.misc.hospital 						= "Hastane"
jobs.misc.tow.vehicleRepairs 			= "Araç Tamiri"
jobs.misc.noReason 						= "Sebep yok"

jobs.log.became 						= "%s, %s oldu"
jobs.log.quit							= "%s quit from his job as %s"

-- Loadout module
loadout.gui.header 						= "EKIPMAN DOLABI"
loadout.gui.subheader 					= "İmha yöntemlerinizi seçin."
loadout.gui.close 						= "KAPAT ✕"
loadout.gui.purchase 					= "Satın al"
loadout.gui.miscSubhead 				= "Kullanılabilir tüm çeşitli yardımcı programların listesi."
loadout.gui.ammoRefill 					= "MERMI DOLDUR - ${price}"
loadout.gui.armorRefill 				= "ZIRH DOLDUR - ${price}"
loadout.gui.free 						= "Bedava"
loadout.gui.refillSubhead 				= "Mevcut tüm silahlarınızı maksimum kapasiteye kadar doldurun."
loadout.gui.refillCurrentArmor 			= "Mevcut zırhınızı maksimum kapasiteye kadar doldurun."
loadout.gui.refillFilters 				= "Filtreleri maksimum kapasiteye kadar doldurun."  -- TODO: Needs Translation
loadout.gui.gasMaskDesc					= "Toksik kimyasallara karşı geçici koruma sağlar." -- TODO: Needs Translation
loadout.gui.medkitRefill 				= "MEDKIT DOLDUR - ${price}"
loadout.gui.filterRefill 				= "FILTER REFILL - ${price}" -- TODO: Needs Translation
loadout.gui.gasMask 					= "GAZ MASKESI - ${price}" -- TODO: Needs Translation
loadout.gui.grenadeRefill 				= "GRENADE REFILL - ${price}"
loadout.gui.refillJobMedkit 			= "Mesleğinizin medkit'ini doldurun."
loadout.gui.grenadesSubhead 			= "Mevcut tüm el bombalarının listesi."
loadout.gui.currentBudget 				= "ŞUANKİ BÜTÇE"
loadout.gui.numInBudget 				= "Şuanda Belediye Başkanlığı bütçesinde ${price} var."
loadout.gui.budgetNoMayor 				= [[Belediye Başkanı yok - tüm satın alımlar doğrudan
cüzdanından gidecek.]]
loadout.gui.listOfWeapons 				= "Mevcut tüm ${name} silahların bir listesi."
loadout.gui.breachRefill 				= "BREACHING CHARGE - ${price}"
loadout.gui.refillJobBreachCharge 		= "Refill your breaching charge."
loadout.gui.GLRefill 					= "Grenade Launcher Ammo - ${price}"
loadout.gui.refillJobGL 				= "Refill your explosive rounds."
loadout.gui.refillJobGrenade 			= "Refill your grenades."
-- @TODO: How does Monolith.Keybinds.GetBindFromName work?

loadout.grenade.smoke 					= "Sis Bombası"
loadout.grenade.smoke.desc				= "Rakiplerinizi dağıtmak veya bir siper olarak kullanışlı."
loadout.grenade.flash 					= "Kör Edici Bomba"
loadout.grenade.flash.desc				= "Rakiplerinizi yönünü değiştirmek için kullanışlıdır."
loadout.grenade.frag 					= "HE Bombası"
loadout.grenade.frag.desc				= "Büyük bir yıkım için kullanışlı."
-- @TODO: What is "cloud" "certificate" and "bomb" in lines 168-170 sh_loadout.lua?

loadouts.misc.primary 					= "Birincil"
loadouts.misc.secondary 				= "İkincil"
loadouts.misc.grenades 					= "Bombalar"
loadouts.misc.misc 						= "Diğer"

loadout.notif.refillTimeout 			= "Şu anda bu ürünü satın alamıyorsunuz, ${time} saniye içinde satın alabileceksiniz."
loadout.notif.cantFit 					= "Sığamıyorsun"
loadout.notif.deptCantAfford 			= "Departmanın bunu karşılayamaz."
loadout.notif.youCantAfford 			= "Bunu karşılayamıyorsun."
loadout.notif.noSpaceToStore 			= "Bu eşyayı depolayacak boşluğun yok!"
loadout.notif.medkitNoNeedRefill 		= "Medkit'in dolmaya ihtiyacı yok."
loadout.notif.boughtMedkit 				= "Yeni bir meslek medkiti aldın."
loadout.notif.refillJobMedkit 			= "Meslek medkitini doldurdun."
loadout.notif.purchasedXforY 			= "${weapon} eşyasını ${price} fiyata satın aldın."
loadout.notif.purchasedXArmorforY		= "${amount} miktar zırhı ${price} fiyata satın aldın."
loadout.notif.refilledAmmo 				= "Mühimmatını doldurdun."
loadout.notif.refilledFilters 			= "Gaz filtrelerini doldurdun." -- TODO: Needs Translation
loadout.notif.boughtGasMask				= "Bir gaz maskesi satın aldın." -- TODO: Needs Translation
loadout.notif.noNeedtoRefillFilter		= "Zaten izin verilen maksimum filtre miktarına sahipsiniz." -- TODO: Needs Translation
loadout.notif.ownsGasMask				= "Zaten bir gaz maskesine sahipsin." -- TODO: Needs Translation
loadout.notif.breachNoNeedRefill 		= "You already have a breaching charge." -- TODO: Needs Translation
loadout.notif.refillJobBreach 			= "You purchased a breaching charge." -- TODO: Needs Translation
loadout.notif.glNoNeedRefill 		    = "You already have the maximum amount of explosives." -- TODO: Needs Translation
loadout.notif.refillJobGL 			    = "You purchased an explosive round." -- TODO: Needs Translation
loadout.notif.refillJobGrenade 			= "You purchased a grenade refill." -- TODO: Needs Translation


-- Log module
log.sgCommand 							= "%s yürüttü ServerGuard komutu \"%s\" %s"
log.monolithCommand 					= "%s yürüttü Monolith komutu \"%s\" %s"
log.connected							= "%s (%s) sunucuya bağlandı"
log.left 								= "%s (%s) sunucudan ayrıldı (%s)"
log.fallDamage 							= "%s tarafından öldürüldü: düşme hasarı."
log.unknownEntity 						= "%s tarafından öldürüldü: bilinmeyen varlık. Büyük ihtimal harita."
log.suicide 							= "%s tarafından öldürüldü: intihar."
log.killedByXWithY						= "%s tarafından öldürüldü: %s, %s ile"
log.killedByX 							= "%s tarafından öldürüldü: %s"
log.killedByXDriving 					= "%s tarafından öldürüldü: %s araç sürerken."
log.killedByEmptyVehicle 				= "%s tarafından öldürüldü: boş bir araç."
log.killedByEntity 						= "%s tarafından öldürüldü: bir %s"
log.damagedThemself 					= "%s tarafından zarar gördü: kendisine %s hasar"
log.damagedVehicle 						= "%s tarafından zarar gördü bir araç sürücüsü: %s, %s hasar"
log.damagedByXForYWith					= "%s tarafından zarar gördü: %s için %s damage with a %s%s"
log.damagedByXForY 						= "%s tarafından zarar gördü: %s için %s damage%s"
log.damagedVehicleDriven 				= "%s tarafından zarar gördü bir araç sürücüsü: %s %s hasar"
log.damagedLooseVehicle 				= "%s tarafından zarar gördü sahip olduğu araç: %s, %s hasar"
log.spawnSENT 							= "%s spawned sent \"%s\""
log.spawnSWEP 							= "%s spawned SWEP \"%s\""
log.useTool 							= "%s tried to use tool \"%s\" on %s"

log.gui.title							= "Loglar"
log.gui.showing 						= "%s loglar %s için gösteriliyor"
log.gui.showing.all 					= "tümü"
log.gui.noServerName 					= "Sunucu İsmi Yok"
log.gui.liveUpdate 						= "Canlı Güncelleme"
log.gui.searchColon 					= "Ara: "
log.gui.na 								= "N/A"
log.gui.actions 						= "Eylemler"
log.gui.time							= "Zaman"
log.gui.category 						= "Kategori"
log.gui.message							= "Mesaj"
log.gui.copyLog 						= "Kopyala"
log.gui.playerNotOnlineNow 				= "Bu oyuncu şu anda aktif değil"
log.gui.tpToPlayer 						= "Oyuncuya ışınlan"
log.gui.bringPlayer 					= "Oyuncuyu bana çek"
log.gui.filterLogsOf 					= "Filter Logs of %s"
log.gui.noParticipants 					= "Görüntülenecek katılımcı yok"
log.gui.itemsFound 						= "Eşyalar Bulundu"
log.gui.loading 						= "Yükleniyor"
log.gui.allLogs 						= "Tüm Loglar"

log.prompt.pageSelect 					= "Hangi sayfayı seçmek istersin"

log.cat.transaction 					= "işlemler"
log.cat.roleplay 						= "Roleplay"
log.cat.iMessage 						= "İletişim"
log.cat.inventory 						= "Envanter"
log.cat.system 							= "Sistem"
log.cat.police 							= "Polis"
log.cat.deaths 							= "Ölümler"
log.cat.admin 							= "Yönetim"
log.cat.damage 							= "Hasar"
log.cat.commands 						= "Komutlar"
log.cat.connections 					= "Bağlantılar"
log.cat.rrl								= "RRL"
log.cat.spawning 						= "Spawnlama"
log.cat.externalAddons 					= "Diğer Eklentiler"

log.report.created 						= "%s şu mesajı içeren bir rapor (#%s) oluşturdu: %s"
log.report.opened 						= "%s bir rapor açtı (#%s)"
log.report.closed 						= "%s bir raporu kapattı (#%s)"
log.report.claimed 						= "%s bir rapor talep etti (#%s)"
log.report.unclaimed 					= "%s bir raporu sahiplenmedi (#%s)"
log.report.respond 						= "%s bir rapora (#%s) şu iletiyle yanıt verdi: %s"
log.report.removed 						= "%s bir raporu kaldırdı (#%s)"

log.notification.respond				= "A staff member replied to your report (#%s)" --TODO
-- @NOTE: Logs comand sv_logger.lua

-- Money Module
money.notif.withdrawnTooMuch 			= "%s, %s'den fazla kazanmış veya geri çekti."
money.notif.youMustDeclare 				= "Kazancınızı beyan etmek zorundasınız."

-- Nameplates module
nameplates.gui.outOfStock 				= "Stokta yok!"
nameplates.gui.premium 					= "Premium" -- Premium member
nameplates.gui.arrested 				= "Tutuklu"
nameplates.gui.hidden 					= "*GİZLİ*"
nameplates.gui.wanted 					= "ARANIYOR"
nameplates.gui.unknown 					= "Bilinmeyen"

-- Navigation module
nav.gui.below 							= "Altında"
nav.gui.above 							= "Üstünde"
nav.gui.aboveBelowDisplay 				= "%s (%sm)" -- Below (50m)
nav.gui.unknownArea 					= "Bilinmeyen Bölge"

nav.misc.meters 						= "m"

nav.poi.pVehicle 						= "Kişisel Araç"
nav.poi.jVehicle 						= "Meslek Aracı"
nav.poi.myProperty 						= "Mülküm"
nav.poi.gps 							= "GPS"
nav.poi.personalApartment 				= "Kişisel Apartman"

-- New intro module
newintro.gui.map.title 					= "İNTERAKTİF HARİTA"
newintro.gui.map.hint 					= "Orada doğmak için simgeye çift tıklayın."
newintro.gui.dynamic 					= "DİNAMİK"
newintro.gui.buyToSpawnElsewhere 		= "Bir Kişisel Ev satın alarak yeni yerlerde doğabilirsiniz."
newintro.gui.cantSpawnPersonal 			= "Kişisel bir evde doğamazsın."
newintro.gui.static						= "STATİK"
newintro.gui.monolith 					= "MONOLITH ROLEPLAY"
newintro.gui.continueLife 				= "YAŞAMINA %s OLARAK DEVAM ET"
newintro.gui.serverTitle 				= "(%s) VERSİYON %s" -- (SERVER 1) Version 6.0.0
newintro.gui.availableSoon				= "Bu özellik yakında bir güncellemede kullanıma sunulacaktır."

newintro.prompt.spawnHere 				= "Burada doğmak istediğinizden emin misiniz?"

-- Notifications
notify.btn.view 						= "Görünüm"
notify.btn.accept 						= "Kabul etmek"
notify.btn.deny 						= "Reddetmek"
notify.btn.dismiss 						= "Reddet"
notify.btn.joinGarage 					= "Garaja Katıl"
notify.btn.discard 						= "Sil"

notify.title.notice 					= "Bildirim"
notify.title.warning 					= "Uyarı"
notify.title.tip 						= "İpucu"
notify.title.alert 						= "Uyarmak"
notify.title.admin 						= "Yönetici"
notify.title.noticePush 				= "Uyarı (Push)"

notify.notif.unreadNotifications 		= "%s okunmamış bildiriminiz var."

-- NPC module
npc.gui.leave 							= "Ayrılmak"
npc.gui.welcomeTo 						= "HOŞGELDİNİZ"
npc.gui.workersOnDuty 					= "GÖREVLİ İŞÇİLER"
npc.gui.levelX 							= "Seviye %s"
npc.gui.join 							= "KATIL"
npc.gui.quit 							= "ÇIK"
npc.gui.unlockedAt 						= "%s oyuncu seviyesinde açıldı"
npc.gui.unlocksAt 						= "%s oyuncu seviyesinde açılır"
npc.gui.unlockedAtJob					= "%s mesleğinde %s seviyede açıldı" -- Unlocked at Police level 10
npc.gui.unlocksAtJob 					= "%s mesleğinde %s seviyede açılır"
npc.gui.whitelist						= "Requires whitelist"

npc.prompt.leaveJob 					= "%s işinizden ayrılmak istediğinizden emin misiniz?"
npc.prompt.leaveJob.title 				= "Zaten bir işin var"

npc.notif.youNeedToBeX 					= "Buna erişmek için %s olmanız gerekiyor."
npc.notif.invalidRank 					= "Buna erişmek için geçerli bir rütbeniz yok."
npc.notif.noValidCareerLicense 			= "Araç dağıtmak için belediye binasından alınan geçerli bir kariyer ehliyetine ihtiyacınız var."
npc.notif.blacklistedDeployingVehicles 	= "Araç dağıtmaktan kara listeye alındınız."

-- Ownership module
own.gui.onlineFilter 					= "Çevrimiçi Filtre:"
own.gui.onlinePlayers 					= "Çevrimiçi Oyuncular"
own.gui.noOneOnline 					= "Kimse çevrimiçi olarak düzenlenemez"
own.gui.offlinePlayers 					= "Çevrimdışı Oyuncular"
own.gui.addViaSteamID 					= "SteamID ile ekle"
own.gui.promptInsertSteamID 			= "Düzenlemek istediğiniz oyuncunun Steam Kimliği girin"
own.gui.haventEditedOfflinePlayers 		= "Hiçbir çevrimdışı oyuncuyu düzenlemediniz"
own.gui.noOwner 						= "Sahipsiz"
own.gui.sharedAccess 					= "Paylaşılan Erişim"
own.gui.sharedAccessSubhead 			= "Birinin sahip olduğunuz şeylere erişim haklarını değiştirebilirsiniz."

-- Player module
ply.notif.invalidName 					= "Geçersiz bir takma ad girdiniz, tekrar deneyin."
ply.notif.nickProfanity 				= "Takma adınız küfür içeremez."
ply.notif.nickLonger15Chars 			= "Takma adınız 15 karakterden uzun olamaz."

ply.prompt.nickname 					= "Takma ad"
ply.prompt.newNickname 					= "Takma adınızın ne olmasını isterdiniz?"

ply.misc.unknown 						= "Bilinmeyen"

-- Property module
-- @NOTE: We skipped furniture layouts here
-- @TODO: Test this extensively. Cut some corners due to furniture, don't want to see anything missed on the real gamemode.
property.gui.youreRenting 				= "Bunu kiralıyorsun"
property.gui.close 						= "KAPAT ✕"
property.gui.noOwner 					= "Sahipsiz"
property.gui.goBack 					= "< Geri dön"
property.gui.propertyListing 			= "Emlak Listesi"
property.gui.details 					= "DETAYLAR"
property.gui.deedOwner 					= "Tapu Sahibi"
property.gui.rentPrice 					= "Kira Bedeli"
property.gui.pricePerTime 				= "15 dakikada bir %s"
property.gui.rentBuy 					= "Kirala/Satın Al"
property.gui.rentWithPrice 				= "Kirala (%s)"
property.gui.alreadyOwned 				= " (Zaten Alınmış)"
property.gui.deedsLast 					= "Tapuların süresi sona ermeden 14 gün önce."
property.gui.purchaseDeeds				= "Satın Alma Senetleri (%s)"
property.gui.rentingActions 			= "Kiralama İşlemleri"
property.gui.cancelRent 				= "Kirayı İptal Et"
property.gui.deedOwnerActions 			= "Tapu Sahibinin İşlemleri"
property.gui.viewDeedInfo 				= "Senet Bilgisini Görüntüle"
property.gui.listing 					= "${amount} ${plural} listeleniyor."
property.gui.listing.singular 			= "Konut"
property.gui.listing.plural 			= "Konutlar"
property.gui.propDeed 					= "Mülkiyet Belgesi"
property.gui.deedInfo 					= "Tapu Bilgileri"
property.gui.boughtDeed 				= "Bu tapuyu %s satın aldın"
property.gui.deedStartDate 				= "Tapu başlangıç tarihi: %s"
property.gui.unknown 					= "Bilinmeyen"
property.gui.deedEndDate 				= "Tapu bitiş tarihi: %s"
property.gui.earnedFromDeed 			= "Bu senetten %s kazandın"
property.gui.actions 					= "Hareketler"
property.gui.collectUnclaimed 			= "Talep edilmemiş kazançları toplayın (%s)"
property.gui.changeRentPrice 			= "Kira bedelini değiştir (şu anda %s)"
property.gui.currentRenter 				= "Geçerli Kiracı"
property.gui.deed 						= "Tapusu"
property.gui.myDeeds 					= "Benim İşlerim"
property.gui.mapControls 				= [[WASD hareket etmek için kontrol eder. Alternatif olarak, taşımak için tıklayıp sürükleyebilirsiniz.
Yakınlaştırmak/Uzaklaştırmak için tekerleği kaydırın.]]
property.gui.layerX 					= "Katmanlar"
property.gui.viewCurrentDeeds 			= "Şu anki işlerini görüntüle."
property.gui.viewInfoAnyProperty 		= "Herhangi bir mülk hakkındaki bilgileri görüntüleyin."
property.gui.pleaseSelectProperty	 	= "Lütfen bir mülk seçin"
property.gui.propertyView 				= "Mülk Görünümü"
property.gui.propertyFinder 			= "Mülk Bulucu"

property.notif.updatedRentPrice 		= "Bu mülkün kira fiyatını güncellediniz."
property.notif.somethingWentWrong 		= "Bir şeyler ters gitti. Kira bedeli güncellenemedi."
property.notif.deedsDisabled 			= "Senet satın alma özelliği şu anda devre dışı"
property.notif.failedPurchase 			= "Tapu satın alınamadı. Size geri ödeme yapıldı. Daha sonra tekrar deneyin."
property.notif.evicted 					= "Tahliye edildin. %s"
property.notif.alreadyRenting 			= "Zaten bir mülk kiralıyorsunuz."
property.notif.rentFailed 				= "Kira başarısız oldu, kira bedeli için size geri ödeme yapıldı."
property.notif.startedRenting 			= "Bir mülk kiralamaya başladınız"
property.notif.cancelledYourRent 		= "Bir mülk için kiranızı iptal ettiniz"
property.notif.claimedDeedEarnings 		= "Senet kazançlarında %s talep ettin."
property.notif.cantClaimEarnings 		= "Bu kazançları alamazsınız (>0 olmalıdır)"
property.notif.rent 					= "Kiraya ${amount} miktar ödedin. (Kaynak: ${source})"
property.notif.rent.bank				= "depo."
property.notif.rent.pocket 				= "cep."
property.notif.rentUnpaid 				= "Kira ödeyecek kadar paranız yok. Hala ödeme yapamıyorsanız ${time} dakika içinde tahliye edileceksiniz."
property.notif.rentEvicted 				= "Kira ödemediğiniz için mülkünüzden tahliye edildiniz."
property.notif.changedRestricted 		= "Kısıtlı bir işe geçtiniz."

property.prompt.startRent 				= "Bu tesisi %s fiyata her 15 dakikada bir kiralamaya başlamak istediğinizden emin misiniz?"
property.prompt.startRent.title 		= "Mülkiyet Sistemi"
property.prompt.purchaseDeed 			= "Bu mülke tapu almak istediğinizden emin misiniz? NOT: Tapular 14 gün sürer ve süreleri dolar."
property.prompt.rentPriceAdjustment 	= "Bunu hangi fiyatla güncellemek istersiniz? (>0 olmalı)"
property.prompt.rentPriceAdjustment.tit = "Mülk Kira Fiyat Ayarlaması"

property.log.startRent					= "Player %s started renting a property: %s."
property.log.cancelRent					= "Player %s stopped renting a property: %s."
proprety.log.purchaseDeed				= "Player %s purchased a property's deed: %s."

-- Purge module
purge.gui.text 							= [[
Bu bir test değil.

Bu, ABD Hükümeti tarafından onaylanan Yıllık Tasfiyenin başladığını bildiren acil yayın sisteminizdir.

Tasfiye sırasında sınıf 4 ve daha düşük silahların kullanımına izin verilmiştir. Diğer tüm silahlar kısıtlanmıştır.
10'uncu sıradaki hükümet yetkililerine Arınma Gecesi'nden dokunulmazlık tanındı ve onlara zarar verilmeyecek.
Sirenden başlayarak, cinayet de dahil olmak üzere her türlü suç, 12 saat boyunca yasal olacaktır.

Polis, itfaiye ve acil tıbbi hizmetler, Arınma Gecesi sona erene kadar yarın sabah 7'ye kadar kullanılamayacak.

Yeni Kurucu Babalarımız ve Amerika, yeniden doğmuş bir millet olsun.

Tanrı hepinizle olsun.
]]
purge.gui.starting 						= "AMBALAJ BAŞLANGIÇ"
purge.gui.active						= "PURGE ACTIVE"
purge.gui.emergencyBroadcastSystem 		= "Acil Yayın Sistemi"

-- Quests module
quests.gui.complete 					= "GÖREV TAMAMLANDI"
quests.gui.rewardsColon 				= "Ödüller:"
quests.gui.goBack 						= "< Geri dön"
quests.gui.completed 					= "TAMAMLANMIŞ GÖREV"
quests.gui.questInProgress 				= "DEVAM EDEN GÖREV"
quests.gui.description 					= "AÇIKLAMA"
quests.gui.noDescription 				= "Açıklama yok."
quests.gui.requirements 				= "GEREKSİNİMLER"
quests.gui.notApplicable 				= "N/A."
quests.gui.difficulty 					= "ZORLUK"
quests.gui.rewards 						= "ÖDÜL"
quests.gui.noRewards 					= "Ödül yok."
quests.gui.actions 						= "YAPILMASI GEREKENLER"
quests.gui.focusQuest 					= "Görevi seç"
quests.gui.stopFocusingQuest 			= "Görevi bırak"
quests.gui.filter 						= "Filtre"
quests.gui.questLog 					= "Görev Kayıtları"
quests.gui.questLog.subHeader 			= "Butün maceranız tek bir yerde. Daha fazla bilgi görüntülemek için bir Göreve tıklayın."

quests.already_completed_quest = "Bu Görevi zaten tamamladın."
quests.cannot_start_quest = "Bu göreve başlayamazsın."
quests.new_quest = "YENİ GÖREV"

quests.notif.youStartedQuest 			= "Göreve başladın: %s"
quests.notif.drankSomeWater 			= "Biraz su iç. Geçmiş anılarını unutmuş gibisin."

quests.misc.easy 						= "Kolay"

quests.q.tutorial.name 					= "Eğitim Görevi"
quests.q.tutorial.desc 					= "Monolith'e giriş."
quests.q.tutorial.requirements 			= "Gereksinim Yok."
quests.q.tutorial.rewards				= "250 XP, 10.000₺ (Banka Hesabı), 2x Su"
quests.q.tutorial.onFinish 				= "Eğiticiyi tamamladınız! İşte ödülleriniz."
quests.q.tutorial.steps.say 			= "Sohbette bir şey söyle!"
quests.q.tutorial.steps.deposit 		= "Paranızı herhangi bir ATM makinesine yatırın."
quests.q.tutorial.steps.tablet 			= "Mono-Tableti Açın"
quests.q.tutorial.steps.inventory 		= "Envanterinizi açın (Q veya F3 e basın)"
quests.q.tutorial.steps.drink 			= "Biraz Su İç (${amount}/1)"

-- TODO: Translate the firstjob quest.
quests.q.firstjob.name					= "İlk Meslek"
quests.q.firstjob.desc					= "Monolithe hoş geldin bir meslek edin."
quests.q.firstjob.requirements			= "Gereksinim Yok."
quests.q.firstjob.rewards				= "200 Meslek Tecrübesi, 4.000₺ (Banka Hesabı)"
quests.q.firstjob.onFinish				= "Artık bir işin var tebrikler! İşte Ödüllerin."
quests.q.firstjob.steps.join			= "Hastaneye git ve paramedik stajyeri ol."
quests.q.firstjob.steps.heal			= "En az (${amount}/15) vatandaşı iyileştir."

quests.q.mine.name 						= "Emektar Madencilik"
quests.q.mine.desc 						= "Sana nasıl madenci olacağını öğreteceğiz."
quests.q.mine.requirements 				= "Gereksinim Yok."
quests.q.mine.rewards 					= "1000 XP, 2.000₺, 32x Taş"
quests.q.mine.onFinish 					= "Bir görevi bitirdin! İşte ödüller."
quests.q.mine.steps.pickaxe 			= "Nalburdan 1x Kazma Alın (${amount}/1)"
quests.q.mine.steps.stone 				= "Madenlere Git ve 32x Taş Topla (${amount}/32)"

quests.q.wood.name 						= "Eski Dostum Balta"
quests.q.wood.desc 						= "Odun kesmeyi öğretir."
quests.q.wood.requirements 				= "Gereksinim yok."
quests.q.wood.rewards 					= "1000 XP, 2.000₺, 32x Ahşap Kütük"
quests.q.wood.onFinish 					= "Bir görevi bitirdin! İşte ödüllerin."
quests.q.wood.steps.axe 				= "Nalburdan 1x Balta Satın Al (${amount}/1)"
quests.q.wood.steps.logs 				= "Ormana git ve 32x Ahşap Günlüğü Topla (${amount}/32)" -- TODO

quests.q.home.name 						= "Emlak İşi"
quests.q.home.desc 						= "Sizi kendi evinizde yaşamayı tanıtır."
quests.q.home.rewards 					= "1250 XP, 3.000₺, 1x Akıllı TV"
quests.q.home.onFinish 					= "Bir görevi bitirdin! İşte ödüllerin."
quests.q.home.requirements 				= "Bu görevi başlatmak için Seviye 5 olmalısın."
quests.q.home.steps.talkNPC 			= "Bir Emlakçı ile Konuşun"
quests.q.home.steps.rent 				= "Bir daire, ev kiralayın."

quests.q.fish.name 						= "Sudan Çıkmış Bir Balık"
quests.q.fish.desc 						= "Size balıkçılık dünyasını tanıtır."
quests.q.fish.requirements 				= "Gerek yok."
quests.q.fish.rewards 					= "1000 XP, 962 Balıkçılık XP, 1.500₺"
quests.q.fish.onFinish 					= "Bir görevi bitirdin! İşte ödüllerin."
quests.q.fish.steps.talkNPC 			= "Balıkçı Dursunla Konuşun"
quests.q.fish.steps.buyRod 				= "Olta al"
quests.q.fish.steps.buyBait 			= "5 adet küçük yem satın alın."
quests.q.fish.steps.gatherFish 			= "5 adet balık tutun."

quests.q.firstvehicle.name				= "Bas Gaza Aşkım"
quests.q.firstvehicle.desc				= "Ehliyetini ve Arabanı Al."
quests.q.firstvehicle.requirements		= "Bu görevi başlatmak için 10 seviye olmanız gerekli."
quests.q.firstvehicle.rewards			= "300 XP, 3.000₺."
quests.q.firstvehicle.onFinish			= "Bu görevi bitirdin artık İsmail YK Bas Gaza açıp sokaklarda arabanla gezebilirsin! İşte ödüllerin.."
quests.q.firstvehicle.steps.talkNPC		= "Ehliyet Kayıt Sorumlusuyla konuş."
quests.q.firstvehicle.steps.getLicense	= "Ehliyet Sınavına katıl ve sınavı başarılı şekilde tamamla."
quests.q.firstvehicle.steps.salesNPC	= "Araç satıcısıyla konuş."
quests.q.firstvehicle.steps.buyVehicle	= "Araç Satıcısından bir araç al."

quests.log.finishQuest					= "%s görevi bitirdi: %s."

-- Recognition module
recog.unknown 							= "Bilinmeyen"

recog.gui.getID 						= "ID'sini Al"

recog.notif.copiedID 					= "Hedefin kimliği panonuza kopyalandı"
recog.notif.introducedSelf 				= "Kendini tanıt."
recog.notif.theyIntroduced 				= "%s onları size tanıttı."

-- Referrals module (TODO)
referrals.notif.success						= "You entered your referral code successfully!"
referrals.notif.failed						= "We could not process your referral code. Contact a developer."
referrals.notif.invalidCode					= "The SteamID64 you entered is invalid. Please, try again."
referrals.notif.ownCode						= "You cannot use your own SteamID64!"
referrals.notif.failedRedeem				= "Failed to redeem reward for referred player."
referrals.notif.successRedeem				= "Redeemed reward for referred player successfully."
referrals.notif.notEnoughLevel				= "Player's level is not high enough, for you to claim your referral reward."
referrals.notif.notOnTheServer				= "Player must be on the server at the same time, for you to collect the reward."
referrals.notif.waitForAction				= "You must wait %s before performing that action again."

referrals.log.success						= "%s inserted a referral code: %s"
referrals.log.redeemedReward				= "%s redeemed reward for referred player %s"
referrals.log.failed						= "Failed to insert referral code: %s"

referrals.gui.modificationsTitle			= "Referrals Section"
referrals.gui.referralsAccessSubhead		= "You can refer new players to the server in this section."
referrals.gui.onlineFilter 					= "Online Filter: "
referrals.gui.onlinePlayers 				= "Online Players"
referrals.gui.noOneOnline 					= "You cannot redeem any rewards. No players online."
referrals.gui.offlinePlayers 				= "Offline Players"
referrals.gui.noOwner 						= "No Owner"
referrals.gui.redeemReward					= "Redeem Reward"

-- @NOTE: Skipped sit system

-- Skills module
skills.gui.skillMultiplier 					= "%s - (%sx XP, %s)"
skills.gui.allMilestones 					= "%s için tüm dönüm noktaları."
skills.gui.levelX 							= "Seviye %s"

skills.invsec.title 						= "Yetenekler"
skills.invsec.subHeader 					= "Yeteneklerin & Becerilerin. Dönüm noktalarını görüntülemek için bir beceriye tıklayın."

skills.notif.granted						= "%s için size %sx%s deneyim verildi."

skills.s.crafting 							= "Üretim"
skills.s.mining 							= "Maden"
skills.s.farming 							= "Tarım"
skills.s.farming.greenThumb 				= "Usta Çiftçi"
skills.s.cooking 							= "Aşçılık"
skills.s.metal 								= "Metal İşleme"
skills.s.fishing 							= "Balıkçılık"
skills.s.fishing.wormBait 					= "Solucan Yemi (Yılan balığı, Uskumru)"
skills.s.fishing.small 						= "Küçük Yem (Bas, Morina)"
skills.s.fishing.medium 					= "Orta Yem (Mahmahi)"
skills.s.fishing.big 						= "Büyük Cazibesi (Somon)"
skills.s.fishing.tuna 						= "Ton Balığı Yemi (Kılıçbalığı)"
skills.s.fishing.codmeat 					= "Codmeat Bait (Güneş Balığı)"
skills.s.woodcutting 						= "Odunculuk"
skills.s.chemistry 							= "Kimya"
skills.s.chemistry.clumsy					= "Sakar Kimyager"
skills.s.chemistry.handy					= "Dikkatli Kimyager"
skills.s.chemistry.average					= "Ortalama Kimyager"
skills.s.chemistry.trained					= "Eğitimli Kimyager"
skills.s.chemistry.skilled					= "Nitelikli Kimyager"
skills.s.chemistry.experienced				= "Deneyimli Kimyager"
skills.s.chemistry.advanced					= "İleri Kimyager"
skills.s.chemistry.proficient				= "Yetkin Kimyager"
skills.s.chemistry.exceptional				= "Olağanüstü Kimyager"
skills.s.chemistry.master					= "Usta Kimyager"

-- @TODO: Take a look at the spawners module

-- Store Module
store.gui.youCanMove 						= "2. tık ve W, A, S, D tuşlarını basılı tutarak hareket edebilirsiniz"
store.gui.purchasePrice						= "Satın al %s"
store.gui.checkout 							= "SATIN AL"
store.gui.unableToPurchase 					= "Öğe satın alınamıyor. Bir sorunla karşılaşıldı."
store.gui.total 							= "TOPLAM"
store.gui.outOfStock 						= "Bu mağaza stokta yok!"
store.gui.levelColon 						= "SEVİYE: %s"
store.gui.addToCart 						= "KARTA EKLE"
store.gui.wasPrice 							= "%s idi"
store.gui.taxFree 							= "TAX-FREE"

store.prompt.notEnough 						= "Yeterli paranız yok"
store.prompt.notEnough.title 				= "Satın alma işlemini gerçekleştiremiyoruz"
store.prompt.howMuchPurchase.title 			= "Lütfen satın almak istediğiniz miktarı belirtin"
store.prompt.howMuchPurchase 				= "Miktar"
store.prompt.howMuchPurchase.fallback 		= "1"
store.prompt.needLicense 					= "Silah ruhsatına ihtiyacınız var, ancak hiçbir şey sizi kendi silahlarınızı yaratmanıza engellemiyor"
store.prompt.needLicense.title 				= "Silah lisansı gerekli"

store.notif.restrictedPremium 				= "Yalnızca Premium Üyelerle sınırlıdır. (!store)"
store.notif.moreFoodTypes 					= "Daha fazla yiyecek türü açtın!"
store.notif.allFoodTypes 					= "Tüm yiyecek türlerinin kilidini açtın!"
store.notif.levelUp 						= "SEVİYE ATLADIN"
store.notif.pleaseWaitX	 					= "Lütfen %s bekleyin."
store.notif.backpackFull 					= "Sırt çantası dolu! Sizden ücret alınmadı."
store.notif.nonComma 						= "Virgül içermeyen miktarlar kullanmalısınız!"
store.notif.anErrorOccured 					= "Bir hata oluştu."
store.notif.youCantPurchaseX 				= "Bir %s satın alamazsınız"
store.notif.itemOutOfStock 					= "%s ürün stokta yok!"

-- @NOTE: Skipping stormfox until it's re-done.

-- Tablet framework module
tabletfw.gui.moveToDesktop 					= "Masaüstüne sürükle"

-- Transactions module
trans.notif.cantAfford 						= "Bunu satın almaya gücünüz yetmez."
trans.notif.buyerCantAfford					= "Alıcı bu işlemi karşılayamaz."
trans.notif.buyerNoRoom 					= "Alıcının envanterinde yer yok."

trans.misc.unspecified 						= "Belirtilmemiş"

trans.actions.boughtXofY 					= "%s, ${count} tane \"${name}\" sattı"
trans.actions.soldXofY 						= "%s, ${count} tane \"${name}\" sattı"

trans.log.boughtMarketedItem				= "%s pazarlanmış bir eşya aldı: %s, %s, %s kişisinden"
trans.log.boughtMarketedItem.unknownItem 	= "Bilinmeyen Eşya"
trans.log.boughtMarketedItem.unknownPlayer 	= "Bilinmeyen Oyuncu"
trans.log.receivedXFromYforZ				= "%s, %s aldı %s kişisinden (%s)"
trans.log.paidXforY							= "%s, ödeme yaptı: %s, (%s)"
trans.log.receivedFundsFromServer 			= "%s, %s için sunucudan %s miktarında para aldı"
trans.log.purchased							= "%s satın aldı: %s ve %s ödedi %s"
trans.log.purchasedNPC 						= "%s satın aldı: %s ve %s ödedi (NPC Market)"

-- Typing module
-- @NOTE: This is definitely not compatible with client-set language. One of the reasons it should be based on server instance.
typing.yellingEl							= "Bağırıyor..."
typing.whisperingEl							= "Fısıldıyor..."
typing.radioingEl 							= "Radyodan Konuşuyor..."
typing.talkingEl 							= "Konuşuyor..."
typing.typingEl 							= "Yazıyor..."
typing.inInventoryEl 						= "Envanterde..."
typing.performingEl 						= "Yapıyor..."
typing.equippingWeaponEl 					= "Silah Donanıyor..."
typing.unequippingWeaponEl 					= "Silahı Bırakıyor..."

-- Unconscious module
unconscious.gui.warning 					= "Sunucudan ayrılırsan banlanırsın!"
unconscious.gui.currentState				= "ŞUANKİ DURUM:"

unconscious.states.critical 				= "Kritik"
unconscious.states.deceased 				= "Ölü"
unconscious.states.stabilized 				= "Stabilize"
unconscious.states.disoriented 				= "Baygın"
unconscious.states.tazed 					= "Şoklanmış"

unconscious.actions.dying 					= "Ölüyor"
unconscious.actions.standsUp 				= "%s uyandıktan sonra ayağa kalkar."
unconscious.actions.criticalCondition 		= "Kritik Kondisyon"
unconscious.actions.picksSelfUp 			= "%s kendilerini yerden kaldırır."

unconscious.ban.reason 						= "RP durumunda bağlantı kesme (Otomatik Ban) | Temyiz @ https://discord.io/monotr"

unconscious.log.changedState 				= "%s durum değişti: %s -> %s"
unconscious.log.enteredState 				= "%s girdiği durum %s"
unconscious.log.finishedOff 				= "%s işi bitti %s, öldürülüyor"

-- Vehicle Module
vehicle.notif.deployingNearby 				= "Dikkat! Yakında bir araba konuşlandırılıyor."
vehicle.notif.thisLocked 					= "Bu araç kilitli."
vehicle.notif.deployCooldown 				= "Lütfen araç konuşlandırmadan önce ${time} bekleyiniz."
vehicle.notif.disabledMustSell 				= "Bu araç deaktif edildi, satmalısın. Üzgünüz!"
vehicle.notif.allDisabled 					= "Araçlar şuanlık deaktif edildi, Üzgünüz!"
vehicle.notif.blacklistedDeploying 			= "Araç konuşlandırman yasaklandı."
vehicle.notif.cannotPersonalOnDuty 			= "Çalışırken kişisel bir aracı kullanamazsınız."
vehicle.notif.storedBeforeDeployed 			= "Aracınız başka bir konuşlandırmadan önce depolanmalıdır."
vehicle.notif.cannotDeployWhilePending 		= "Beklenen cezan varken bir aracı konuşlandıramazsın"
vehicle.notif.cannotDeployUnderArrest 		= "Tutukluyken bir aracı konuşlandıramazsınız."
vehicle.notif.outOfFuel 					= "Bu araçta yakıt bitmiş."
vehicle.notif.switchSeatsHandcuffed 		= "Kelepçeli iken koltuk değiştiremezsiniz."
vehicle.notif.pleaseWaitBeforeExiting 		= "Lütfen araçtan çıkmadan önce ${time} bekleyiniz."
vehicle.notif.cantExitHandcuffed 			= "Kelepçeli iken araçtan çıkamazsınız."
vehicle.notif.orderReceivedDeploying 		= "Sipariş alındı! Aracınızı açıyorsunuz."
vehicle.notif.enteringCooldown 				= "Lütfen araca girmeden önce ${time} bekleyiniz."
vehicle.notif.enterProned 					= "Yöneltilmişken araca giremezsiniz."
vehicle.notif.tooFarAway 					= "Çok uzakta"
vehicle.notif.youreDead 					= "Öldün"
vehicle.notif.youreNotPolice 				= "Polis değilsin"
vehicle.notif.playerNotHandcuffed 			= "Oyuncu kelepçeli değil"
vehicle.notif.noDriver 						= "Sürücü yok"
vehicle.notif.alreadyInVehicle 				= "Oyuncu zaten bir araçta."
vehicle.notif.cannotEnterWhileTaunt 		= "Alay hareketi sırasında araca giremezsiniz."
vehicle.notif.vehicleIsBeingUnlocked 		= "Aracın kilidi açılıyor."
vehicle.notif.vehicleLocked 				= "Araç kilitli."
vehicle.notif.seatBelt 						= "Emniyet kemerini takmak için J tuşuna basın."
vehicle.notif.turnOnEngine 					= "Motoru çalıştırmak için H düğmesine basın."
vehicle.notif.paidForRepairing 				= "Aracın tamirine %s ödedin."
vehicle.notif.lockedUnlockednside			= "Araç içerden %s"
vehicle.notif.lockedUnlockedInside.locked 	= "kilitli"
vehicle.notif.lockedUnlockedInside.unlocked = "kilidi açık"
vehicle.notif.haveToBeDriver 				= "Bunu yapmak için bir sürücü olmalısınız."
vehicle.notif.noKey 						= "Bu aracın anahtarı yok."
vehicle.notif.noFuelCantDriven 				= "Aracınızda yakıt yok ve sürülemez."
vehicle.notif.destroyedCannotBeDriven 		= "Aracınız yok edildi ve sürülemez."
vehicle.notif.cantAffordDeploy 				= "Bu aracı konuşlandırmayı karşılayamıyorsunuz."
vehicle.notif.error5						= "Araba üretilemedi. ERROR_05"
vehicle.notif.repairBeforeStore 			= "Aracınızı saklamadan önce onarmalısınız!"
vehicle.notif.putIntoGarage 				= "Araban garaja başarıyla yerleştirildi!"
vehicle.notif.waitBeforeDeploying 			= "Lütfen bir aracı konuşlandırmadan önce %s bekleyin."
vehicle.notif.contactADev 					= "Bir geliştiriciyle iletişim kurun."
vehicle.notif.entryCantSpawn 				= "Üzgünüz, giriş seviyesi işler araç üretemez."
vehicle.notif.moveVehicleCloser 			= "Aracınızı yaklaştırmalısınız."
vehicle.notif.cantFindValidSpawnTable 		= "Geçerli bir doğuş tablosu bulunamadı. Bir geliştirici ile iletişim kurun."
vehicle.notif.cantFindValidSpawn 			= "Geçerli bir doğuş noktası bulunamadı."
vehicle.notif.auctionedOffFor				= "%s açık arttırman %s fiyata gitti [%s]!"
vehicle.notif.waitBeforeAuctioning			= "Aracı açık arttırmaya koymadan önce %s bekle."
vehicle.notif.cantAuctionWhileEmployed 		= "Çalışırken araç açık artırması yapamazsınız!"
vehicle.notif.cantAuctionWhileOnSale 		= "Bir aracı satıştayken açık artırmaya çıkaramazsınız!"
vehicle.notif.successfullyPurchased 		= "Başarıyla %s satın aldın! Tebrikler!"
vehicle.notif.waitBeforePurchasing 			= "Lütfen başka araç satın almadan önce %s saniye bekleyiniz."
vehicle.notif.cantBuyDisabled 				= "Bu aracı devre dışı olduğu için satın alamazsınız."
vehicle.notif.cantBuy 						= "Bu aracı satın alamazsınız."
vehicle.notif.cantPurchaseEmployed 			= "Çalışırken bir araç satın alamazsınız!"
vehicle.notif.cantAfford 					= "Bu aracı karşılayamazsın!"
vehicle.notif.hintStuff 					= "Sol tıkla çıkarır, CTRL iptal."
vehicle.notif.unablePurchaseItem 			= "Öğe satın alınamadı. Bir hatayla karşılaşıldı."
vehicle.notif.deployingCosts 				= "Konuşlandırmak departmana %s mâl olur"
vehicle.notif.vehiclesOnSale 				= "Araçlar bayilikte İNDİRİMDE!"
vehicle.notif.vehicleTooFar 				= "Araç çok uzakta!"
vehicle.notif.atLeastSergeant 				= "Bunu yapmak için polis ve en azından bir çavuş olmalısın!"
vehicle.notif.mustBeEmpty 					= "Tekerlek kelepçesini takmadan önce araç boş olmalıdır!"
vehicle.notif.mustBeImmobile 				= "Araç bunu yapmak için hareketsiz olmalı!"
vehicle.notif.cantClampPolice 				= "Polis araçlarını sıkamazsınız!"
vehicle.notif.clampEnter 					= "Tekerlek kelepçesi olan bir araca giremezsiniz!"
vehicle.notif.pushExhausted					= "You're too exhausted to do this right now."
vehicle.notif.pushHandcuffed				= "You can't do this with cuffs on your hands!"

vehicle.bind.engine							= "Motor Açık/Kapalı"
vehicle.bind.seatbelt 						= "Emniyet Kemeri Açık/Kapalı"

vehicle.prompt.sellCarForX 					= "%s fiyata aracınızı satmak istediğinizden emin misiniz?"
vehicle.prompt.vehicleAuction 				= "Araç Açık Arttırma"

vehicle.gui.callMechanic 					= "Bir Mekanik çağır"
vehicle.gui.noVehicles 						= "Hiçbir aracın yok"
vehicle.gui.store 							= "Depola" -- As in, store an item
vehicle.gui.deploy 							= "Konuşlandır"
vehicle.gui.vehicleDeployment 				= "ARAÇ KONUŞLANDIRMA"
vehicle.gui.jobValet 						= "%s VALE"
vehicle.gui.customizeIt						= "Dağıtmak ve özelleştirmek için bir araç seçin."
vehicle.gui.alreadyHaveJobVehicle 			= "Şu anda aktif bir İş Aracınız var. Başka bir yumurtlamadan önce alın."
vehicle.gui.deploy 							= "KONUŞLANDIR"
vehicle.gui.customizeVehicle 				= "ARACI ÖZELLEŞTİR (HENÜZ UYGULANMADI)"
vehicle.gui.ret 							= "GERİ DÖNDÜR"
vehicle.gui.premiumVehicles  				= "PREMIUM ARAÇLAR"
vehicle.gui.vehicles 						= "ARAÇLAR"
vehicle.gui.purchase 						= "Satın al"
vehicle.gui.paintCar 						= "Aracı boya"
vehicle.gui.purchaseWrap 					= "Ambalaj satın al"
vehicle.gui.none 							= "HİÇ"
vehicle.gui.skin							= "Skin #%s"
vehicle.gui.dealershipTitle 				= "ASHEN OTOMATİV GRUP"
vehicle.gui.carOptions	 					= "ARAÇ AYARLARI"
vehicle.gui.originalPrice 					= "ORİJİNAL ÜCRET: %s"
vehicle.gui.premiumIndicator 				= "Premium ★"
vehicle.gui.owned 							= "Satın alınmış"
vehicle.gui.vehicleColor 					= "Araç Rengi"
vehicle.gui.vehicleWrap 					= "Araç Ambalajı"
vehicle.gui.spawn 							= "Çıkar"
vehicle.gui.carDealer 						= "Araç Satıcısı"
vehicle.gui.carsOnSale						= "4 araç şuanda satılık."
vehicle.gui.searchCategory 					= "Hayalinizdeki arabayı aramaya başlamak için lütfen bir kategori seçin."

vehicle.cats.super 							= "Süper"
vehicle.cats.sports 						= "Spor"
vehicle.cats.suv 							= "SUV"
vehicle.cats.classic 						= "Klasik"
vehicle.cats.misc 							= "Diğer" -- Miscellaneous

vehicle.food.text64							= "Metniniz 64 karakterden uzun olamaz."
vehicle.food.text3 							= "Metniniz 3 karakterden uzun olmalıdır."
vehicle.food.textProfanity 					= "Metniniz küfür içeremez."
vehicle.food.wait							= "Lütfen %s bekle."
vehicle.food.title.prompt 					= "Kamyonun üzerinde görüntülenecek bir mesaj koyun (Maks. 64 karakter/Simge yok)"
vehicle.food.title.title 					= "Bir başlık koy"
vehicle.food.title.bite 					= "Lokma Teslimatı"
vehicle.food.gui.gps						= "GPS'inizi gıda alıcısına ayarlamak için [G] tuşuna basın."
vehicle.food.gui.earnings 					= "Kazanç: %s"
vehicle.food.gui.stock 						= "Stok"
vehicle.food.gui.profit 					= "Toplam Kâr: %s"
vehicle.food.gui.currentSession 			= "Şuanki Durum: %s"
vehicle.food.gui.levelMax 					= "Seviye: MAKS"
vehicle.food.gui.level 						= "Seviye: %s (%s/%s)"

vehicle.garage.disabled_warning 			= "Bu şuanda devre dışı."
vehicle.garage.repaired_vehicle 			= "Aracını tamir ettin (%d%%)."
vehicle.garage.deploying_repair_required 	= "Lütfen aracınızı konuşlandırmadan önce onarın."
vehicle.garage.in_combat 					= "Savaştasın."
vehicle.garage.already_inside 				= "Zaten bir garajdasın."
vehicle.garage.player_entered 				= "%s garajınıza girdi."
vehicle.garage.inviting_no_access 			= "%s'nin Garajına oyuncu davet etme izniniz yok."
vehicle.garage.target_too_far 				= "Bu oyuncu bulunduğunuz garaja yeterince yakın değil."
vehicle.garage.invitation_sent 				= "%s kişisine garaj davetiyesi gönderdin."
vehicle.garage.forced_out 					= "%s kişisinin garajından zorla çıkarıldın."
vehicle.garage.vehDoesntExist 				= "Oyuncunun araç envanterinde araç mevcut değil."
vehicle.garage.spawnPointNotFound 			= "Araç çıkma noktası bulunamadı"
vehicle.garage.invitedGarage 				= "${name} kişisinin garajına davet edildin"
vehicle.garage.title 						= "GARAJ"
vehicle.garage.enterGarage 					= "Garaja gir"
vehicle.garage.exitGarage 					= "Garajdan çık"
vehicle.garage.clickToEnter 				= "Girmek için bir garaja tıkla."
vehicle.garage.clickToSell 					= "Satmak için bir garaja tıkla"
vehicle.garage.number 						= "Garaj %s"
vehicle.garage.vehicleMenu 					= "Araç Menüsü"
vehicle.garage.impounded 					= "(El Konuldu) %s"
vehicle.garage.dismiss 						= "REDDET ✕"
vehicle.garage.deployVehicle 				= "ARACI KONUŞLANDIR"
vehicle.garage.sellVehicle 					= "ARACI SAT: %s"
vehicle.garage.promptSell 					= "%s, %s fiyata satmak istediğine emin misin?"
vehicle.garage.promptSell.title 			= "Açık Arttırma (%s)"
vehicle.garage.repairVehicle				= "ARACI TAMİR ET (20%%): %s"
vehicle.garage.aboutVehicle 				= "Araç Hakkında"
vehicle.garage.noDescription 				= "Araç Açıklaması Yok"
vehicle.garage.unknown						= "Bilinmeyen"
vehicle.garage.sitRep 						= "Araç Oturumu"
vehicle.garage.fuelAmount					= "Benzin Miktarı: %s%%"
vehicle.garage.health 						= "Araç Canı: %s%%"
vehicle.garage.damagedParts 				= "Hasarlı Parçalar"
vehicle.garage.none 						= "Yok"
vehicle.garage.invite 						= "Davet et"
vehicle.garage.selectATarget 				= "Bir hedef seçin"
vehicle.garage.employment_restriction 		= "Garajınıza girmek için işinizi bırakmanız gerekiyor."

vehicle.testDrive.licensePlate 				= "TEST"
vehicle.testDrive.timeToTestDrive 			= "Bu aracı test etmek için %s saniyeniz var."
vehicle.testDrive.bringYouBack 				= "Zaman doldu! Sizi bayiliğe geri getirelim ..."
vehicle.testDrive.featureDisabled 			= "Bu özellik şimdilik devre dışı."
vehicle.testDrive.featureUnavailable		= "Bir hata oluştu. Bir geliştiriciye başvurun!"
vehicle.testDrive.cooldown 					= "Test sürüşünü yalnızca %s dakikada bir yapabilirsiniz."
vehicle.testDrive.warranted 				= "Bu aracı garanti altındayken test edemezsiniz."
vehicle.testDrive.vehicleDisabled 			= "Devre dışı bırakıldığından bu aracı süremezsiniz."
vehicle.testDrive.cantTestDrive 			= "Bu aracı süremezsin."
vehicle.testDrive.employed 					= "Bu aracı çalışırken test edemezsiniz."
vehicle.testDrive.minWorth 					= "Test sürüşü için aracın değerinin en az yarısına sahip olmalısınız."
vehicle.testDrive.license 					= "Bir arabayı test etmek için ehliyete ihtiyacınız var!"
vehicle.testDrive.validLicense 				= "Bir arabayı test etmek için geçerli bir lisansa ihtiyacınız var!"
vehicle.testDrive.carDealerButton 			= "Test sürüşü"

vehicle.actions.turnedOnOffCar 				= "%s, aracını %s duruma getirdi."
vehicle.actions.turnedOnOffCar.on 			= "açık"
vehicle.actions.turnedOnOffCar.off 			= "kapalı"
vehicle.actions.startedHotwiring 			= "%s düz kontak yapmaya başladı."
vehicle.actions.succeedHotwire 				= "%s başarıyla bir aracı düzkontak yaptı."
vehicle.actions.failedHotwire 				= "%s aracı düzkontak yapamadı."
vehicle.actions.hotwiringCar 				= "ARAÇ DÜZKONTAK YAPILIYOR"
vehicle.actions.seatbelt 					= "%s kemeri artık %s."
vehicle.actions.seatbelt.putOn				= "takılı"
vehicle.actions.seatbelt.tookOff 			= "takılı değil"

vehicle.section.vehicles 					= "Araçlar"

vehicle.actions.forceIntoPoliceCar			= "%s kapıyı açar, %s kişisini polis arabasına zorlar."
vehicle.actions.puttingOnAWheelClamp		= "TEKERLEK KELEPÇESİNE KOYMA"
vehicle.actions.takingOffWheelClamp 		= "TEKERLEK KELEPÇESİNİN ÇIKARILIYOR"

vehicle.actions.push						= "Push"

vehicle.inventory.trunk 					= "Araç Gövdesi"

vehicle.log.storedVehicle 					= "%s kayıtlı araç %s"
vehicle.log.auctioned 						= "%s, %s için %s aracını açık artırmaya çıkardı"
vehicle.log.purchasedFor 					= "%s, %s için %s satın alınan araç"
vehicle.log.wheelClamp 						= "%s, %s kişisinin %s'sine tekerlek kelepçesi koydu."
vehicle.log.wheelClampOff 					= "%s, %s kişisinin %s'sinden tekerlek kelepçesi aldı."
vehicle.log.wheelClampLockpicked 			= "%s, %s kişisinin %s tekerlek kelepçesini kilitledi"
-- @TODO: sh_foodtruck.lua tDefaultFood and such
-- @TODO: Check out line 960 sv_vehicle.lua

vehicle.taxi.notif.fareStarted 				= "%s ile olan taksimetren başladı."
vehicle.taxi.notif.clientCancelled			= "Mevcut müşteriniz taksi talebini iptal etti."
vehicle.taxi.notif.couldntPay 				= "%s ücretin tamamını ödeyemedi. %s kazandın."
vehicle.taxi.notif.youCouldntPay			= "Tam ücreti ödeyemedin. %s ödedin."
vehicle.taxi.notif.youReceived				= "Taksimetreden %s kazandın."
vehicle.taxi.notif.costYou 					= "Taksimetre sana %s mâl oldu."
vehicle.taxi.notif.pleaseWait				= "Lütfen taksi çağırmadan önce %s bekleyiniz."
vehicle.taxi.notif.mustHave					= "Üzerinde %s olmalı."
vehicle.taxi.notif.calledTaxi				= "%s az önce taksi çağırdı. Hemen yanına git!"
vehicle.taxi.notif.taxiPrefix				= "[TAKSI] "

vehicle.taxi.chat.taxiDriverOnWay			= "Taksi sürücümüz, %s, yolda!"

vehicle.taxi.gui.costColon 					= "Maliyet:"
vehicle.taxi.gui.driverColon				= "Sürücü:"

vehicle.foodtruck.gui.foodTruckTitle 		= "İşte yemeğin."

-- Widget module
widget.app.licenses 						= "Lisanslar"
widget.app.laws								= "Yasalar"
widget.app.skills 							= "Beceriler"
widget.app.monoTablet 						= "Mono-Tablet"
widget.app.monoMap 							= "Mono-Harita"
widget.app.taxi 							= "Taksi"
widget.app.iMessage 						= "İMesaj"
widget.app.trade 							= "Ticaret"

widget.gui.moveToTablet 					= "Tablete taşı"

-- Gas Mask TODO: Needs Translation
gas.notif.filter_empty						= "Filtre boş veya takılı değil"

-- monophone

monophone.cancel = "Cancel"
monophone.name = "Name"
monophone.phone_number = "Phone number"
monophone.confirm = "Confirm"

monophone.app.settings = "Settings"
monophone.app.phone = "Phone"
monophone.app.contacts = "Contacts"
monophone.app.messaging = "Messaging"

monophone.app.contacts.you_have_no_contacts = "You have no contacts"
monophone.app.contacts.add = "Add a new contact"
monophone.app.contacts.copy_to_clipboard = "Copy phone number"



outfits.name = "Outfits"

outfits.ui.current_outfits = "Current Outfits"
outfits.ui.current_outfit_singular = "Current Outfit"
outfits.ui.save_current = "Save Current Outfit"
outfits.ui.equip_new = "Equip Outfit"

outfits.ui.enter_new_name_query = "Please enter a name for your new outfit."
outfits.ui.delete_outfit_query = "Are you sure you would like to delete this outfit?"

outfits.ui.response_mismatched_size = "Outfit name must be greater than 3 characters, and less than 40 characters."
outfits.ui.response_empty = "No outfit name provided."

outfits.notify.changed_outfit = "You changed your outfit."

-- USE THIS.
close_x = "✕"

log.factory.added_item = "%s added an item to a Factory: %s"
log.factory.taken_item = "%s took an item from a Factory: %s"
log.factory.added_to_queue = "%s started crafting in a Factory: %s, it cost: %s"

inventoryviewer.header = "INVENTORY ACTIONS"
inventoryviewer.subHeader = "Actions to manage a player's inventory."
