--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

local gamemode = GAMEMODE or GM

Monolith.LocaleBuilder( "tr", true )

--@TODO: put the translations in subtables (has to be mirrored in the places that use them)

gamemode_version = "Versiyon " .. gamemode.Version -- @TODO: Find all instances and remove

monolith.version 						= "Güncelleme " .. gamemode.Version

ok 										= "TAMAM"
yes 									= "Evet"
no 										= "Hayır"
on 										= "Açık"
off 									= "Kapalı"
cancel 									= "İptal"
err 									= "Hata"
confirm 								= "Doğrula"
moneySymbol 							= "₺"
moneySymbol.side 						= "RIGHT" -- DO NOT TRANSLATE! Enter "LEFT" if you say $100 or "RIGHT" if you display 100$

trash.reward = "Çöp temizlemekten %s kazandın."
trash.isEmpty = "Bu çöplük boş."

init.notif.lifeAlert 					= "Acil durum uyarıcınız hastaneye otomatik 911 çağrısı yaptı!"
init.notif.noParamedics 				= "Şu anda hiç sağlık görevlisi yok. Acil durum uyarıcınız kullanılmayacak."
init.notif.savedMedical 				= "Tıbbi hizmetler tarafından kurtarıldınız, ancak garip bir amnezi vakası yaşıyorsunuz ... (NLR AKTİF)"
init.notif.noPermission					= "Bu işlevi kullanma izniniz yok."

init.dispatch.lifeAlert 				= "Bu otomatik bir hayat uyarısıdır! Mevcut birimler lütfen bu çağrıya cevap verin!"

kernel.ui.elements.noNotifications 		= "Yeni bildiriminiz yok"
kernel.ui.elements.close 				= "KAPAT ✕"
kernel.ui.elements.allNotifications		= "Bütün bildirimler"

kernel.player.waitinglist				= "Sunucu dolu! #%d sıradasın.\nSıranı korumak için sunucuya girmeye devam et."

anims.raiseHands 	= "%s ellerini kaldırıyor."
anims.lowerHands 	= "%s ellerini indiriyor."
anims.log.raiseHands 	= "%s ellerini kaldırdı."
anims.log.lowerHands 	= "%s ellerini indirdi."

taser.low 				= "Düşük voltaj şok için Sol Tıklayın"
taser.high 				= "Yüksek voltaj şok için Sağ Tıklayın"
taser.recharge			= "DOLUYOR"
