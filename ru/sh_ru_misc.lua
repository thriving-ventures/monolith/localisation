--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

local gamemode = GAMEMODE or GM

Monolith.LocaleBuilder( "ru", true )

--@TODO: put the translations in subtables (has to be mirrored in the places that use them)

gamemode_version = "Версия " .. gamemode.Version -- @TODO: Find all instances and remove

monolith.version 						= "Версия " .. gamemode.Version

ok 										= "OK"
yes 									= "Да"
no 										= "Нет"
on 										= "Вкл"
off 									= "Выкл"
cancel 									= "Отмена"
err 									= "Ошибка"
confirm 								= "Подтвердить"
moneySymbol 							= "$"
moneySymbol.side 						= "LEFT" -- DO NOT TRANSLATE! Enter "LEFT" if you say $100 or "RIGHT" if you display 100$

trash.reward = "Вы получили %s за очистку бака."
trash.isEmpty = "Этот мусорный бак пуст."

init.notif.lifeAlert 					= "Ваш датчик здоровья автоматически вызвал СМП!"
init.notif.noParamedics 				= "Нет доступных парамедиков на службе. Ваш датчик здоровья не будет использован."
init.notif.savedMedical 				= "Вас спасли медицинские службы, но у вас странный случай амнезии... (ПРАВИЛО НЛР)"

init.dispatch.lifeAlert 				= "Это автоматический вызов кнопки жизни! Свободные юниты, ответьте на вызов!"

kernel.ui.elements.noNotifications 		= "У вас нет новых уведомлений"
kernel.ui.elements.close 				= "Закрыть ✕"
kernel.ui.elements.allNotifications		= "Все уведомления"

kernel.player.waitinglist				= "Сервер полон! Вы #%d в очереди.\nПродолжайте подключаться, чтобы зайти."

anims.raiseHands 	= "%s raises their hands."
anims.lowerHands 	= "%s lowers their hands."
anims.log.raiseHands 	= "%s raised their hands."
anims.log.lowerHands 	= "%s lowered their hands."