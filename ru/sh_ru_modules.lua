--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

Monolith.LocaleBuilder( "ru", true )

-- Actions Module
actions.timeremaining 					= "ОСТАВШЕЕСЯ ВРЕМЯ"
actions.movingcancel 					= "ПЕРЕДВИЖЕНИЕ ОТМЕНИТ ДЕЙСТВИЕ"
actions.cancelled 						= "Вы передвинулись и текущее действие было отменено."

actions.log.invalid 					= "[ДЕЙСТВИЕ] %s пытался совершить действие с неизвестным предметом."
actions.debug.invalid 					= "[ДЕЙСТВИЕ] Игрок: %s пытался совершить действие с неизвестным предметом"

-- Areas Module
-- @TODO: Map-specific areas

-- Banking Module
banking.gui.deposit = "Внести"
banking.gui.withdraw = "Снять"
banking.gui.specify = "Пожалуйста, укажите, сколько денег вы хотели %s" -- %s = banking.gui.deposit или banking.gui.withdraw
banking.gui.funds = "ВАШИ ДЕНЬГИ"
banking.gui.balance = "Банковский баланс"
banking.gui.account = "БАНКОВСКИЙ СЧЕТ"

banking.notif.combat = "Нельзя сделать это в бою"
banking.notif.full = "Ваш банк переполнен! Вам нужно освободить место перед тем, как добавлять новые вещи"
banking.notif.jobonly = "Вы не можете размещать только рабочие места."
banking.notif.alrdydeployed	= "Указанный предмет уже размещен. Пожалуйста, поднимите его." -- TODO: Proper translation needed for this.
banking.notif.depositInvalidAmount = "Не удалось внести депозит. Недопустимая сумма."
banking.notif.depositInvalidSlip = "Не удалось пополнить счет. Недействительный депозитный чек. Ошибка BANK_01"
banking.notif.withdrawInvalidAmount = "Снятие не удалось. Запрошена неверная сумма."
banking.notif.withdrawInvalidSlip = "Снятие не удалось. Неверное снятие квитанции. Ошибка BANK_02"
banking.notif.deposit = "Вы внесли %s" -- %s = деньги в формате
banking.notif.depositInsufficient = "У вас недостаточно денег для внесения этого депозита."
banking.notif.withdraw = "Вы сняли %s" -- %s = отформатированные деньги
banking.notif.withdrawError = "При выводе произошла ошибка. Код ошибки: BANK_02"
banking.notif.withdrawInsufficient = "У вас недостаточно денег для вывода средств."
banking.notif.loadingError = "Что-то пошло не так при загрузке данных вашего игрока. Пожалуйста, присоединяйтесь."

banking.log.deposit = "%s депозитная банковская позиция: %s"
banking.log.withdraw = "%s забрал позицию банка: %s"
banking.log.depositMoney = "%s внесенных денег %s"
banking.log.withdrawMoney = "%s снял деньги %s"

-- Бюджетный модуль
budget.gui.locker = "Шкафчик оборудования"
budget.gui.equipLoadout = "Снаряженная загрузка"
budget.gui.primary = "Основное оружие"
budget.gui.secondary = "Вторичное оружие"
budget.gui.costs = "Затраты: %s"
budget.gui.deptBudget = "Бюджет отдела %s: %s"
budget.gui.full = "Полный"
budget.gui.locked = "ЗАБЛОКИРОВАНО, НЕТ МЭРА"

budget.notif.ratingGain = "Вы получили %s %% Рейтинга мэра"
budget.notif.ratingLoss = "Вы потеряли %s %% Рейтинга мэра"
budget.notif.tooLow = "Бюджет отдела слишком мал для выполнения этого действия."
budget.notif.refill = "Бюджет департамента ' %s' нуждается в пополнении."
budget.notif.lowCityBudget = "Бюджет города низкий"
budget.notif.lowDepartment = "Бюджет отдела ' %s' низкий, его необходимо пополнить."
budget.notif.lowBudget = "Бюджет отдела слишком мал!"

-- Carm modification module
carmod.gui.noVehicles 					= "У вас нет машин для модификации"
carmod.gui.selectCar 					= "ВЫБЕРИТЕ МАШИНУ"
carmod.gui.modificationsTitle 			= "МОДИФИКАЦИИ"
carmod.gui.bodygroups 					= "Бодигруппы"
carmod.gui.modifications 				= "Модификации для машины"
carmod.gui.purchasePartPrompt 			= "Вы уверены, что хотите приобрести эту часть?"
carmod.gui.noAdditionalParts 			= "Нет модификаций в наличии."
carmod.gui.parts						= "Части"
carmod.gui.engineUpgradePrompt 			= "Вы уверены, что хотите приобрести это улучшение двигателя?"
carmod.gui.engine						= "Двигатель"
carmod.gui.resprayPurchasePrompt 		= "Вы уверены, что хотите приобрести этот раскрас?"
carmod.gui.respray 						= "Раскрас"
carmod.gui.skins 						= "Скины"
carmod.gui.purchase 					= "ПРИОБРЕСТИ"
carmod.gui.wrapNum						= "Пленки # %s"
carmod.gui.wrapPurchasePrompt 			= "Вы уверены, что хотите купить эту пленку?"
carmod.gui.wraps 						= "Пленки"
carmod.gui.underglow 					= "Подсветка"
carmod.gui.underglowPurchasePrompt 		= "Вы уверены, что хотите приобрести эту подсветку?"
carmod.gui.licensePlates 				= "Номерные знаки"
carmod.gui.licensePurchasePrompt 		= "Вы уверены, что хотите приобрести эти номерные знаки?"

carmod.gui.colors.stock 				= "Стоковый"
carmod.gui.colors.white 				= "Белый"
carmod.gui.colors.red 					= "Красный"
carmod.gui.colors.lime 					= "Лаймовый"
carmod.gui.colors.green 				= "Зеленый"
carmod.gui.colors.blue 					= "Синий"
carmod.gui.colors.orange 				= "Оранжевый"
carmod.gui.colors.yellow				= "Желтый"
carmod.gui.colors.cyan					= "Циановый"
carmod.gui.colors.purple 				= "Фиолетовый"
carmod.gui.colors.pink					= "Розовый"

carmod.gui.upgrades.stock 				= "Стоковый"
carmod.gui.upgrades.street				= "Уличный"
carmod.gui.upgrades.race 				= "Гоночный"
carmod.gui.upgrades.pro 				= "Профессиональный"
carmod.gui.upgrades.super 				= "Супер"

carmod.notif.premium = "У вас должен быть Премиум, чтобы купить это. (Напишите !store для получения дополнительной информации)"
carmod.notif.engineUpgrade = "Двигатель вашего автомобиля был обновлен до %s"
carmod.notif.cannotAffordRespray = "Вы не можете позволить себе этот респиратор."
carmod.notif.resprayBought = "Вы купили респиратор для %s"
carmod.notif.cannotAffordEngine = "Вы не можете позволить себе это обновление двигателя."
carmod.notif.currentEngine = "Это ваше текущее обновление двигателя."
carmod.notif.engineReimburse = "Вам возместили %s за текущее обновление двигателя %s"
carmod.notif.engineBought = "Вы купили обновление двигателя %s. Это стоило %s"
carmod.notif.cannotAffordUnderglow = "Вы не можете позволить себе эту подстветку"
carmod.notif.underglowBought = "Вы купили %s подстветку. Это стоило %s"
carmod.notif.cannotAffordBodygroup = "Вы не можете позволить себе эту запчасть"
carmod.notif.bodygroupBought = "Вы купили запчасть. Это стоило %s"
carmod.notif.licenseLong = "Ваш номерной знак слишком длинный. Убедитесь, что он не превышает %s символов."
carmod.notif.licenseShort = "Ваш номерной знак слишком короткий. Убедитесь, что он превышает %s символов."
carmod.notif.licenseProfanity = "Ваш номерной знак не может содержать ненормативную лексику."
carmod.notif.cannotAffordLicense = "Вы не можете позволить себе этот номерной знак"
carmod.notif.purchaseLicense = "Вы купили номерной знак. Это стоило %s"
carmod.notif.skinDisabled = "Скин отключен"
carmod.notif.cannotAffordSkin = "Вы не можете позволить себе этот скин"
carmod.notif.purchaseVehicleSkin = "Вы приобрели скин автомобиля. Это стоило %s"

carmod.keybind.underglow 				= "Неон ВКЛ/ВЫКЛ"

-- Chatbox module
chatbox.keybind.timestamps 				= "Временная метка чата"

-- Christmas module
christmas.gui.title = "Двенадцать дней Рождества"
christmas.gui.titleRewards = "Двенадцать дней Рождества - Награды"
christmas.gui.rewards = "Ваши награды от подарка:"
christmas.gui.gift.opened = "Уже открыт"
christmas.gui.gift.missed = "Пропущенный подарок"
christmas.gui.calendar = "Календарь"

christmas.reward.skillBoost = "%sx %s повышение навыков для %s" -- например, «2x Новогодний Бустер Навыков на 12 часов»

christmas.notif.christmas = "Рождество"
christmas.notif.activeReward = "У вас есть активное вознаграждение, на которое вы можете претендовать! Откройте Моно-Меню, чтобы получить его."
christmas.notif.itemReward = "Вы открыли подарок и получили предмет: %s"
christmas.notif.cashReward = "Вы открыли подарок и получили деньги: %s"

-- Модуль настроек клиента
clientsettings.gui.settings = "Настройки"
clientsettings.gui.keybinds = "Связки клавиш"
clientsettings.gui.other = "Другое"
clientsettings.gui.subheader = "Дом всех опций в Монолите."
clientsettings.gui.confirm = "Подтвердить"
clientsettings.gui.change = "Изменить"

clientsettings.gui.multicore = "Многоядерность"
clientsettings.gui.optimizations = "Оптимизация"
clientsettings.gui.darkMode = "Ночной режим"
clientsettings.gui.performance = "Производительность"
clientsettings.gui.viewRange = "Диапазон просмотра"
clientsettings.gui.viewRange.low = "Низкий"
clientsettings.gui.viewRange.medium = "Средний"
clientsettings.gui.viewRange.high = "Высокий"
clientsettings.gui.apperance = "Внешний вид"
clientsettings.gui.vignette = "Виньетка"
clientsettings.gui.blur = "Размытие"
clientsettings.gui.betaNameplates = "Номерные знаки"
clientsettings.gui.gameHints = "Советы"
clientsettings.gui.minimapStyle = "Стиль мини-карты"
clientsettings.gui.minimapStyle.normal = "Нормальный"
clientsettings.gui.minimapStyle.fancy = "Необычный"
clientsettings.gui.minimapStyle.fancy3D = "Необычный 3D"
clientsettings.gui.streamer = "Режим стримера"
clientsettings.gui.insurance = "Подсказки"
clientsettings.gui.chatSettings = "Настройки чата"

-- Commands module
commands.notif.noPlayer 				= "Игрок не найден"
commands.notif.invalidModel 			= "Неверный путь к модели"
commands.notif.invalidScale 			= "Неверный масштаб"
commands.notif.syncingNames 			= "Синхронизация РП имен..."
commands.notif.cooldown 				= "Вы должны подождать %s секунд, для использования этой комманды."
commands.notif.entitiesFixed 			= "Объекты исправлены! (к счастью)"
commands.notif.noDeaths 				= "Этот игрок не умирал, ему нечего возвращать."
commands.notif.theyRefunded 			= "Вещи успешно возвращены %s."
commands.notif.youRefunded 				= "Администратор вернул твои вещи после смерти! Посетите НПС Возвратов чтобы забрать их."
commands.notif.targetNotPlayer 			= "Вы навели курсор на то, что не является игроком"
commands.notif.targetTooFar 			= "Вы слишком далеко от цели"
commands.notif.cantAfford 				= "Вы не можете себе это позволить, или вы указали неправильную сумму"
commands.notif.youGave 					= "Вы дали кому-то %s"
commands.notif.gaveYou 					= "Кто-то вам дал %s"

commands.roll.prefix 					= "[ШАНС] " -- keep the spaces
commands.roll.rolled 					= " получил шанс "
commands.roll.cooldown 					= "Подожди %s секунд чтобы выбрать роль ещё раз."

commands.unstuck.sitting 				= "Вы не можете сделать это, сидя"
commands.unstuck.quickly 				= "Вы не можете сделать это так быстро"
commands.unstuck.attempting 			= "Мы пытаемся вытащить вас"
commands.unstuck.notStuck 				= "Вы не застряли"
commands.unstuck.couldntFind 			= "Не удалось найти подходящее место. обратитесь к админу."
commands.unstuck.youCantDoThis 			= "You can't do this."

commands.log.refund 					= "%s вернул %s игроку %s за его смерть." -- [admin] refunded [money] to [player] for their death
commands.log.give 						= "%s выдал %s игроку %s через комманду 'give'."
commands.log.unstuck 					= "%s попытался вытащить себя себя"

commands.fps.prompt 					= "Вы хотите, чтобы мы автоматически устанавливали ваши настройки игры, чтобы улучшить частоту кадров? Это может снизить частоту кадров на старых системах. Если вы хотите сохранить эти настройки, напишите 'host_writeconfig' в консоль."
commands.fps.prompt.title 				= "FPS"
commands.fps.notif.applied 				= "Ваш ФПС сейчас должен стать лучше!"
-- @TODO: Should we localize command names too?

-- Communication module
-- @TODO: Should we translate chat commands?
communication.notif.freqLimit 			= "Радиоканалы должны быть в пределах частот 100 и 200"
communication.notif.radioAccess 		= "У вас нет доступа к этому радиоканалу"
communication.notif.radioSet 			= "Вы установили частоту %s"
communication.notif.radioWasSet 		= "Ваша радиочастота установлена на ' %s'"
communication.notif.msgRestrained 		= "Вы не можете писать сообщения будучи закованным."
communication.notif.noComm 				= "У вас с собой ничего нет?"
communication.notif.spam 				= "Прекратите спамить или вам выдадут мут!"
communication.notif.muted 				= "Вам выдан мут на 60 секунд за спам."
communication.notif.unmuted 			= "Вам выдан мут."
communication.notif.oocBlacklist 		= "Вам заблокировано использование OOC."
communication.notif.loocBlacklist 		= "You are blacklisted from using LOOC." -- TODO
communication.notif.generalBlacklist 	= "Вам заблокировано использование этой команды."
communication.notif.cooldown 			= "Подождите %s для использования OOC."
communication.notif.advertCooldown		= "ADs are on cooldown. Please wait %s before creating another one." -- TODO
communication.notif.profanity 			= "Ваш текст не может содержать ненормативную лексику!"
-- @TODO: Add language-specific profanity filter
communication.notif.restrained 			= "Вы не можете общаться, будучи заблокированым"
communication.notif.advertCharge 		= "С вас сняли %s за вашу рекламу."
communication.notif.advertInsufficient	= "У вас нет %s чтобы оплатить рекламу."
communication.notif.noRadio 			= "У вас нет радио."
communication.notif.enableRadio 		= "Вы должны включить свое радио."
communication.notif.radioRestrained 	= "Вы не можете общаться по радио, будучи заблокированым."
communication.notif.needCrew 			= "Вам нужна банда для этого."
communication.notif.orgCantComm 		= "Вы не можете общаться с вашей организацией прямо сейчас."
communication.notif.orgRestrained 		= "Вы не можете общаться с вашей организацией, пока вы заблокированы."
communication.notif.cantDead 			= "Вы не можете сделать это, пока мертвы."

communication.commands.says 			= " говорит "
communication.commands.yells 			= " кричит "
communication.commands.whispers 		= " шепчет "

communication.chat.ooc 					= "OOC" -- не переводить
communication.chat.looc 				= "LOOC"
communication.chat.ad					= "РЕКЛАМА"
communication.chat.radio 				= "РАДИО"
communication.chat.org 					= "ОРГАНИЗАЦИЯ"
communication.chat.gov 					= "ГОС" -- /не переводить
communication.chat.somebody  			= "Кто-то"
communication.chat.unconscious 			= "Вы не можете говорить без сознания или будучи мертвым."
communication.chat.somebody 			= "Кто-то"

communication.channels.publicServices 	= "Гос. служба"
communication.channels.police 			= "ПД Монофорд"
communication.channels.pinePolice 		= "ПД округа Пайн"
communication.channels.guards			= "Тюремная охрана"
communication.channels.emergency 		= "Экстренная служба"
communication.channels.ss 				= "Секретная служба"
communication.channels.security 		= "Охранная служба" -- TODO: Needs Translation
communication.channels.swat 			= "SWAT" -- TODO: Needs Translation
communication.channels.military 		= "Военные"
-- @TODO: Emojis? line 496 @ sh_chat.lua

communication.log.msgSent 				= "%s отправляет сообщение: ' %s' пишет: ' %s'"
communication.log.spamming 				= "%s получает мут за спам."
communication.log.chatCmd 				= "%s вводит команду в чат: %s"

communication.gui.iMessage 				= "Сообщение"
communication.gui.quickReplyBind 		= "Быстрый ответ (F6)"
communication.gui.quickReply 			= "Быстрый ответ"
communication.gui.radio 				= "РАДИО"
communication.gui.close 				= "ЗАКРЫТЬ ✕"
communication.gui.frequency 			= "ЧАСТОТА - %s"
communication.gui.setCustomFreq 		= "Поставить свою частоту"
communication.gui.setFreq 				= "Установить частоту"
communication.gui.inputFreq 			= "Нужно ввести частоту, на которую хочешь переключиться. (между 100 и 200)"
communication.gui.confirmFreq 			= "Вы уверены, что хотите переключить частоту на ' %s'?"
communication.gui.switchFreq 			= "ПЕРЕКЛЮЧЕНИЕ ЧАСТОТЫ ( %s)"
communication.gui.existing 				= "Существующие частоты"

communication.chat.newMessage 			= "Новое сообщение от %s"

communication.binds.radioTalk 			= "Радиопереговоры"

-- Cosmetics module
cosmetics.gui.title 					= "КОСМЕТИЧЕСКИЕ ПРЕДМЕТЫ"
cosmetics.gui.equip 					= "надеть"
cosmetics.gui.unequip 					= "снять"
cosmetics.gui.limited 					= "ИВЕНТ" -- as in, limited edition
cosmetics.gui.free 						= "БЕСПЛАТНО"
cosmetics.gui.promptEquip 				= "Вы уверены, что хотите %s ваш %s" -- Do you wish to [equip] your [item name]
cosmetics.gui.promptPurchase 			= "Вы уверены, что хотите купить %s за %s" -- Do you wish to purchase [name] for [price]
cosmetics.gui.version 					= "ВЕРСИЯ"
cosmetics.gui.updateLog 				= "Нажмите, чтобы посмотреть на лог обновлений!"

-- Cooking module (@TODO)
cooking.ent.pot 						= "Cooking Pot"
cooking.ent.stove 						= "Stove"
cooking.ent.table 						= "Prep Table"

cooking.gui.title 						= "PREP TABLE"
cooking.gui.subTitle 					= "Prepare your ingredients for cooking"
cooking.gui.prepTable 					= "Prep Table"
cooking.gui.cookingPot					= "Cooking Pot"
cooking.gui.insertIngredients			= "Insert your ingredients"

cooking.notif.preparedX 				= "You've prepared %s!"
cooking.notif.noItemInCenter 			= "No item in the center slot to prep!"
cooking.notif.prepTableInUse			= "This prep table is currently in-use."
cooking.notif.tooFarAway 				= "You're too far away!"
cooking.notif.onlyOpenOwn 				= "You can only open your own prep table."
cooking.notif.plugInStove 				= "You must plug the stove in before turning on the burners."
cooking.notif.reservedPremium			= "These burners are reserved for players with Premium%s."
cooking.notif.reservedPremium.level		= " or cooking level %s"
cooking.notif.reservedLevel				= "These burners are reserved for players with cooking level %s%s."
cooking.notif.reservedLevel.premium		= " or Premium"
cooking.notif.cantOperate				= "Only the owner of the stove may operate it."
cooking.notif.emptyPot 					= "All you did was heat an empty pot up, dummy."
cooking.notif.failedRecipe				= "You failed to follow the recipe, and your cooking has resulted in inedible mush."
cooking.notif.goodRecipe				= "You've successfully cooked %s!"
cooking.notif.ownPots					= "You can only use your own cooking pots!"
cooking.notif.farAway					= "You're too far away from the cooking pot!"

-- CPR module
cpr.notif.emergencyOnly 				= "Только скорая помощь может реанимировать людей!"
cpr.notif.stabalized 					= "Этот игрок стабилизирован, и вы можете прочувствовать его пульс."
cpr.notif.noPulse 						= "Вы не чувствуете пульса"
cpr.notif.almostWake 					= "Этот человек в сознании и вот-вот проснется."
cpr.notif.feelPulse 					= "Вы чувствуете %s пульс" -- %s = cpr.strength.XXX
cpr.notif.alreadyStable 				= "Этот игрок уже стабилизирован!"
cpr.notif.tooFarGone 					= "Это не сработает, его уже не спасти."
cpr.notif.alreadyConscious 				= "Вы не можете делать НМС на здоровом человеке!"
cpr.notif.alreadyPerforming 			= "Кто-то уже делает НМС этому игроку!"
cpr.notif.needKit 						= "Вам нужен дефибрилятор для этого!"
cpr.notif.kitRunOut 					= "Ваша рабочая аптечка закончилась, возьмите новую в своем департаменте."
cpr.notif.startedCPR 					= "%s начал делать НМС %s"

cpr.strength.veryStrong 				= "очень сильный"
cpr.strength.barely 					= "едва заметный"
cpr.strength.weak 						= "слабый"
cpr.strength.veryWeak 					= "очень слабый"
cpr.strength.strong 					= "сильный"

cpr.action.checkPulse 					= "%s проверяет пульс %s."
cpr.action.succeedCPR 					= "%s успешно реанимирует %s."
cpr.action.failedCPR 					= "%s не смог реанимировать %s."
cpr.action.youSucceedCPR 				= "Вы успешно реанимировали пациента!"
cpr.action.youFailedCPR 				= "Вы не смогли реанимировать пациента!"
cpr.action.noComplete 					= "%s прерывает процесс реанимации над %s."
cpr.action.performing 					= "СТАБИЛИЗИРУЕМ ИГРОКА"

-- Crafting Module
crafting.gui.title 						= "МЕНЮ КРАФТА"
crafting.gui.queue 						= "ОЧЕРЕДЬ"
crafting.gui.search 					= "Поиск"
crafting.gui.claim 						= "ЗАБРАТЬ ВЕЩИ"
crafting.gui.duration 					= "Длительность: "
crafting.gui.amount 					= "Количество: %s"
crafting.gui.skill 						= "Навык: %s"
crafting.gui.craft 						= "КРАФТ"
crafting.gui.requires 					= "КРАФТ (НЕОБХОДИМ %s)"
crafting.gui.notEnough 					= "КРАФТ (НЕ ХВАТАЕТ РЕСУРСОВ)"
crafting.gui.amt 						= "Кол-во"
crafting.gui.ing 						= "Компонент"
crafting.gui.inv 						= "Инвентарь"
crafting.gui.noCatItems 				= "There are no items in this category. Use a workbench or filter correctly."
crafting.gui.allRecipes 				= "Все рецепты"

crafting.type.crafting 					= "Стол для крафта"
crafting.type.woodcutting 				= "Стол для обработки дерева"
crafting.type.metalworking 				= "Стол для обработки металла"
crafting.type.cooking 					= "Кухонная плита"

crafting.notif.noResources 				= "У вас нет нужных ресурсов для крафта."
crafting.notif.noLevel 					= "У вас недостаточный уровень для крафта."
crafting.notif.queueLimit 				= "Вы достигли лимита очереди."
crafting.notif.someoneStole 			= "%s украл твой верстак!"
crafting.notif.youStole 				= "%s Вы украли верстак %s!"
crafting.notif.youCrafted 				= "Вы скрафтили %s"
crafting.notif.noInventory 				= "У вас нет места в инвентаре, чтобы взять %s"
crafting.notif.cantClaimDidntCraft 		= "Вы не можете забрать вещи из верстака, так как они скрафчены не вами."

-- Crews module
crews.gui.crewName 						= "Название банды"
crews.gui.defaultName 					= "Lorem Ipsum"
crews.gui.listOnline 					= "Список онлайн участников"
crews.gui.invalidRank 					= "Недействительный ранг"
crews.gui.promptKick 					= "Вы уверены, что хотите выгнать %s из вашей банды?"
crews.gui.setRank 						= "Установить ранг"
crews.gui.promptRank 					= "Вы уверены, что хотите сделать %s %s?"
crews.gui.transferLeadership 			= "Передача лидерства"
crews.gui.promptTransfer 				= "Вы уверены, что хотите сделать %s новым лидером?"
crews.gui.invalidName 					= "Недействительное имя"
crews.gui.offlineMembers 				= "УЧАСТНИКИ ОФФЛАЙН"
crews.gui.noOffline 					= "Сейчас нет участников оффлайн."
crews.gui.listOffline 					= "Список участников оффлайн."
crews.gui.loading 						= "Загрузка..."
crews.gui.inviteMembers 				= "ПРИГЛАСИТЬ УЧАСТНИКОВ"
crews.gui.inviteDesc 					= "Место для вербовки новой силы."
crews.gui.search 						= "Поиск: "
crews.gui.promptInvite 					= "Вы уверены, что хотите пригласить %s в вашу банду?\nNotice: This will make them the rank with lowest immunity."
crews.gui.information 					= "Информация"
crews.gui.numMembers 					= "Сейчас %s участников в %s."
crews.gui.yourRank 						= "Вы повышены до ' %s'"
crews.gui.actions 						= "ДЕЙСТВИЯ"
crews.gui.editName 						= "ИЗМЕНИТЬ НАЗВАНИЕ БАНДЫ"
crews.gui.promptEditName 				= "Какое название вы хотите установить?"
crews.gui.editColor 					= "Изменить цвет банды"
crews.gui.color 						= "ИЗМЕНИТЬ ЦВЕТ"
crews.gui.modifyColor 					= "Модифицировать цвет банды."
crews.gui.setColor 						= "Установить цвет"
crews.gui.promptColor 					= "Вы уверены, что хотите сохранить новый цвет банды?"
crews.gui.editLogo 						= "Изменить логотип банды"
crews.gui.editLogoCaps 					= "ИЗМЕНИТЬ ЛОГОТИП"
crews.gui.clickLogo 					= "Нажмите на логотип чтобы сохранить его"
crews.gui.haventSelectedLogo 			= "Вы не выбрали логотип!"
crews.gui.promptLogo 					= "Вы уверены, что хотите сохранить новый логотип?"
crews.gui.viewMembers 					= "Посмотреть список участников"
crews.gui.leaveCrew 					= "Покинуть %s"
crews.gui.promptLeave 					= "Вы уверены, что хотите покинуть %s?"
crews.gui.promptOwnerLeave 				= "Я спрошу вас снова: Как лидер %s, после вашего ухода, ваша банда будет расформирована. Это необратимо. Вы уверены?"
crews.gui.anotherMembersList 			= "%s участников"
crews.gui.crew 							= "Банда"
crews.gui.pending 						= "В ожидании банды"
crews.gui.defaultrank 					= "Owner's Rank Name"
crews.gui.nameColon 					= "Имя: "
crews.gui.colorColon 					= "Цвет: "
crews.gui.logoColon 					= "Логотип: "
crews.gui.createAndCost 				= "Создать ( %s)"
crews.gui.promptCreate 					= "Вы уверены, что хотите создать банду? Это будет стоить: %s"
crews.gui.crews							= "Банды"
crews.gui.sectionSubhead 				= "Управляйте вашей бандой отсюда."

crews.notif.notInCrew 					= "Вы не в банде."
crews.notif.noPermInvite 				= "У вас недостаточно прав для приглашения новых участников."
crews.notif.alreadyInCrew 				= "%s уже в банде."
crews.notif.defaultError 				= "У вас недостаточно прав чтобы выполнить это действие"
crews.notif.youWereInvited 				= "Вы были приглашены ${name} в ${crewName}"
crews.notif.youInvited 					= "${name} пригласил вас в ${crewName}"
crews.notif.noPerm 						= "У вас недостаточно прав чтобы выполнить это действие."
crews.notif.higherImmunity 				= "Ранг, который вы пытаетесь установить этому участнику выше, чем ваш ранг."
crews.notif.updatedRank 				= "Вы обновили ранг участника."
crews.notif.somethingWrong 				= "Что-то пошло не так, когда вы обновляли ранг участника"
crews.notif.cantKickYourself 			= "Вы не можете выгнать себя."
crews.notif.noRightsKick 				= "У вас недостаточно прав, чтобы выгнать этого участника."
crews.notif.youKicked 					= "Вы выгнали кого-то из банды."
crews.notif.gotKicked 					= "Вы были выкинуты из банды ${name}"
crews.notif.youJoined 					= "Вы вступили в ${name}"
crews.notif.acceptedInvitation 			= "Приглашение было принято - ${name}."
crews.notif.noInvite 					= "Сейчас нет приглашений в банду."
crews.notif.transferLeadership 			= "Вы передали лидерство ${crewName}."
crews.notif.nowLeader 					= "Теперь вы лидер ${crewName}, поздравляем!"
crews.notif.nameLongShort 				= "Название банды или слишком длинное, или слишком короткое."
crews.notif.cantAfford 					= "Вы не можете позволить себе создание банды."
crews.notif.crewCreated 				= "Вы создали вашу новую банду!"
crews.notif.disbanded 					= "Вы расформировали %s"
crews.notif.founderRank					= "You cannot invite a player to the founder (default) rank. Make a new rank."


crews.ranks.user 						= "Участник"
crews.ranks.mod 						= "Модератор"
crews.ranks.admin 						= "Администратор"
crews.ranks.leader 						= "Лидер"

-- Daily Rewards
drewards.gui.title 						= "Ежедневные награды"
drewards.gui.rewardsTitle 				= "Сегодняшние награды"
drewards.gui.skillBooster 				= "%s%% бустер для навыка %s" -- 50 %skill booster for 30:00
drewards.gui.possibleRewardsHeader 		= "Награды, которые вы можете получить сегодня"
drewards.gui.rewardsMessage 			= "Награды можно получать каждый день. Каждая подряд полученная награда увеличивает ваш призовой уровень."
drewards.gui.claimReward 				= "Награда получена"

drewrads.rewards.money 					= "Денежное вознаграждение"
drewards.rewards.booster 				= "%s бустер"

drewards.notif.activeReward 			= "У вас есть активная ежедневная награда, которую вы можете получить! Откройте F3 меню чтобы получить её."
drewards.notif.alreadyClaimed 			= "Вы уже получили сегодняшнюю награду!"
drewards.notif.received 				= "Вы получили %s из ежедневной награды!"
drewards.notif.notEnoughSpace			= "You do not have enough space in your inventory to receive the reward. Operation Cancelled." -- TODO

-- Dispatch Module
dispatch.gui.title 						= "Поступил новый вызов!"
dispatch.gui.accept						= "ПРИНЯТЬ (O)"
dispatch.gui.dismiss 					= "ОТКЛОНИТЬ (N)"
dispatch.gui.caller						= "Звонящий: %s"
dispatch.gui.description 				= "Описание: %s"

dispatch.notif.leaveToNotify 			= "Если вы хотите снова получать уведомления о вызовах, откройте приложение 'Службы' и покиньте текущий вызов."
dispatch.notif.newCall 					= "Был совершен новый вызов! Проверьте приложение 'Службы'!"
dispatch.notif.newCallout 				= "Доступен новый вызов! Проверьте свой планшет!"
dispatch.notif.callConcluded 			= "Вызов в котором вы принимали участие, завершен."
dispatch.notif.automatic 				= "Автоматичейский вызов"
dispatch.notif.alreadyInCallout 		= "Вы уже участвуете в каком-либо вызове!"
dispatch.notif.youreResponder 			= "Вы первыми приняли вызов! Не забудьте завершить его, когда закончите с ним!"
dispatch.notif.onlyOne 					= "Только один человек может участвовать в этом вызове!"
dispatch.notif.callDelay 				= "Пожалуйста подождите ${second} секунд перед тем как сделать новый вызов."
dispatch.notif.useRadio 				= "Вы не можете совершить вызов, если вы работаете в правительстве/аварийной службе. Используйте радио чтобы связаться с вашими коллегами!"
dispatch.notif.autoClose 				= "Ваш вызов автоматически завершится через ${minutes} минут!"
dispatch.notif.tooEarly 				= "Вы не можете завершить вызов так рано!"
dispatch.notif.jobChangeClear 			= "Ваши вызовы были очищены из-за смены работы!"

dispatch.action.newCall 				= "Поступил новый вызов от %s!"

dispatch.log.newCall 					= "Поступил новый вызов от %s в ${department} со следующим описанием: \"${description}\""

-- Durability Module
durability.gui.title 					= "РЕМОНТНЫЙ ВЕРСТАК"
durability.gui.subTitle 				= "Положите предмет, чтобы узнать стоимость его починки."
durability.gui.brokenCantRepair 		= "Этот предмет сломан и не может быть починен."
durability.gui.noNeedRepair 			= "Вам не нужно чинить это."
durability.gui.promptRepair 			= "Вы уверены, что хотите починить этот предмет?"
durability.gui.repair 				 	= "Починить"

durability.notif.cantRepair 			= "Вы не можете починить это."
durability.notif.noSelect 				= "Вы не выбрали предмет."
durability.notif.noNeedRepair 			= "Вам не нужно чинить этот предмет."
durability.notif.completelyBroken 		= "Вы не можете починить предмет, который окончательно сломан."
durability.notif.cantAffordRepair 		= "У вас нехватает денег."
durability.notif.youRepaired 			= "Вы починили %s"
durability.notif.thingBroken 			= "%s сломан. Почините его на верстаке!"
durability.notif.equipHandcuff 			= "Вы не можете экипировать вещи будучи закованным"
durability.notif.notJammed 				= "Ваше оружие не заклинило."

durability.action.unJamming 			= "UN-JAMMING"

durability.binds.unjamWeapon 			= "Unjam Weapon"

-- EXP module
exp.gui.xp 								= "xp"
exp.gui.jobEXP 							= "< XP работы"

-- Factory module
factory.gui.close 						= "ЗАКРЫТЬ ✕"
factory.gui.none 						= "Нет"
factory.gui.active 						= "Активен"
factory.gui.notActive 					= "Не активен"
factory.gui.conveyorColon 				= "Конвейер: %s"
factory.gui.powerConsumption 			= "Потребление энергии: %s W"
factory.gui.placed 						= "Размещен"
factory.gui.status						= "СТАТУС"
factory.gui.ingredient 					= "Компоненты"
factory.gui.amount						= "Количество"
factory.gui.available 					= "Доступно"
factory.gui.craft 						= "Крафт"
factory.gui.metalIngots 				= "Металлические слитки"
factory.gui.searchFailed 				= "Вы не нашли предмет"
factory.gui.cantCraftComp 				= "Вы не можете ничего сделать с этими компонентами."
factory.gui.removeQueue 				= "Убрать из очереди"
factory.gui.furnace						= "Печь"
factory.gui.highEndManufacturing 		= "Производство высокого класса."
factory.gui.items						= "Предметы"
factory.gui.listItems 					= "Список предметов, которые можно сделать."
factory.gui.factoryQueue 				= "ОЧЕРЕДЬ КРАФТА"
factory.gui.search 						= "Поиск"
factory.gui.inspect 					= "ПРОВЕРИТЬ"
factory.gui.invItemsSub 				= "Предметы в вашем инвентаре, которые можно добавить для крафта."
factory.gui.components 					= "КОМПОНЕНТЫ"
factory.gui.usedCreateNew 				= "Предметы, которые используются для крафта новых вещей."
factory.gui.take 						= "Взять"
factory.gui.noItemsInFurnace			= "В печи нет предметов"
factory.gui.insert 						= "Положить"
factory.gui.inventory 					= "Инвентарь"
factory.gui.storageBoxColon 			= "Склад: %s"

factory.notif.entFarExist 				= "Энтити не существует или оно находится слишком далеко"
factory.notif.failedCraft 				= "Невозможно скрафтить предмет"
factory.notif.furnaceTakeItems 			= "Вы забрали предметы из печи."
factory.notif.somethingWrong 			= "Что-то пошло не так."
factory.notif.furnaceCancelCratf 		= "Вы отменили крафт предмета. Все компоненты возвращены обратно."
factory.notif.itemNotFound 				= "Предмет не найден."
factory.notif.elseUsing 				= "%s использует вашу фабрику."
factory.notif.usingSomeonesFactory 		= "Вы используете фабрику %s."
factory.notif.noConveyor 				= "Нет конвейера. Установите хотя бы один."
factory.notif.notFactoryItem 			= "Не заводской предмет"
factory.notif.lvlInsuff 				= "Вы не достигли нужного уровня для этого крафта"
factory.notif.coneyorInactive 			= "Конвейер не работает, включите его"
factory.notif.cantAffordManufacture	 	= "Вы не можете сделать предмет"
factory.notif.tooManyQueue 				= "Слишком много предметов в очереди"
factory.notif.addedQueue 				= "Вы добавили предмет в очередь крафта."

factory.misc.energyUnit 				= "J"

-- HUD module
hud.notif.noPlayerFound 				= "Игрок не найден"
hud.notif.noTaxiDrivers 				= "Сейчас нет свободных таксистов."
hud.notif.silentToggle 					= "Включен режим 'без звука'."
hud.notif.blacklistedVoice				= "Вам был отключена возможность голосового чата."

hud.gui.outOfStock 						= "Нет в наличии!"
hud.gui.unknown 						= "Неизвестно"
hud.gui.noAmmo 							= "Нет патронов"
hud.gui.levelStuff 						= "Уровень: %s - опыта осталось: %s"
hud.gui.levelReached 					= "Уровень %s достигнут!"
hud.gui.male 							= "МУЖЧИНА"
hud.gui.female 							= "ЖЕНЩИНА"
hud.gui.noResidence 					= "НЕТ ЖИЛЬЯ"
hud.gui.policeTape 						= "Лента"
hud.gui.nobody 							= "Никто"
hud.gui.frozenHead 						= "Вы были зафрижены админом. Сохраняйте спокойствие"
hud.gui.frozenSub 						= "Если вы считаете это несправедливым, используйте !report."
hud.gui.invisible 						= "Невидимость"

hud.gui.election.header 				= "ВЫБОРЫ МЭРА"
hud.gui.election.text 					= "%s окончится через %s секунд"
hud.gui.election.registration 			= "Регистрация"
hud.gui.election.voting 				= "Голосование"
hud.gui.election.err 					= "ОШИБКА"

hud.gui.emergency.active 				= "Активен"
hud.gui.emergency.header 				= "ЧП"
hud.gui.emergency.reason 				= "Причина: \"%s\""

hud.scoreboard.players 					= "Игроки: %s/%s"
hud.scoreboard.premiumStore 			= "Премиум"
hud.scoreboard.serverUpdates 			= "Апдейты"
hud.scoreboard.guides 					= "Гайды"
hud.scoreboard.forums 					= "Форум"
hud.scoreboard.discord 					= "Дискорд"
hud.scoreboard.steamGroup 				= "Steam"
hud.scoreboard.apply 					= "Заявки"
hud.scoreboard.serverFallback 			= "СЕРВЕР 1"
-- @TODO: Replace serverguard's howtoapply command in cl_scoreboard.lua line 200
-- @TODO: Change spookfest name to localized in line 229 of cl_scoreboard.lua

-- don't know which of these might change w/ your guys' server, so i'm adding them all:
hud.links.servers 						= "https://www.octothorp.team/"
hud.links.premium 						= "https://www.octothorp.team/mono-premium"
hud.links.forums 						= "https://mono.octo.gg/"
hud.links.updates 						= "https://mono.octo.gg/category/1"
hud.links.guides 						= "https://mono.octo.gg/category/4"
hud.links.discord 						= "https://discord.gg/uCEz3Qr"
hud.links.group 						= "https://steamcommunity.com/groups/octothorp-team"

hud.menu.copySteamName 					= "Скопировать ник Steam"
hud.menu.copyRPName 					= "Скопировать РП ник"
hud.menu.copySteamID 					= "Скопировать SteamID"
hud.menu.openProfile 					= "Открыть профиль"
hud.menu.bringPlayer 					= "ТП к себе"
hud.menu.gotoPlayer 					= "ТП к игроку"
hud.menu.cancel 						= "Закрыть"

hud.tablet.taxi 						= "Такси"
hud.tablet.taxi.call					= "ВЫЗВАТЬ ТАКСИ"
hud.tablet.taxi.inside 					= "СЕСТЬ В ТАКСИ"
hud.tablet.licenses 					= "Лицензии"
hud.tablet.licenses.request 			= "Запрос"
hud.tablet.imessage.notify.youblocked	= "You can't send a message to someone you blocked." -- Todo
hud.tablet.imessage.notify.areblocked	= "Failed to send message, you are blocked by the recipient." -- Todo
hud.tablet.imessage.unblock				= "Unblock" -- Todo
hud.tablet.imessage.setblocked			= "Block"	-- Todo
hud.tablet.imessage			 			= "Сообщения"
hud.tablet.imessage.chatOffline 		= "Вы не можете писать игрокам в оффлайн"
hud.tablet.imessage.available 			= "Доступно"
hud.tablet.imessage.online 				= "Онлайн"
hud.tablet.imessage.offline 			= "Оффлайн"
hud.tablet.trading 						= "Торговля"
hud.tablet.trading.selectPlayer 		= "Выберите игрока для торговли"
hud.tablet.trading.sentRequest 			= "Вы отправили запрос игроку %s."
hud.tablet.laws							= "Законы"
hud.tablet.laws.permanent 				= "Постоянные законы"
hud.tablet.laws.temporary 				= "Временные законы"
hud.tablet.monomap 						= "Карта"
hud.tablet.monomap.layer 				= "Уровень %s"
hud.tablet.monotablet 					= "Планшет"
hud.tablet.depttablet					= "Dept-Tablet" -- TODO
hud.tablet.settings 					= "Настройки"
hud.tablet.settings.suppressSMS 		= "Заглушить сообщения"
hud.tablet.emergency 					= "Службы & 911"
hud.tablet.emergency.callouts 			= "ТЕКУЩИЕ ВЫЗОВЫ"
hud.tablet.emergency.endCallout 		= "ЗАКОНЧИТЬ"
hud.tablet.emergency.leave 				= "ПОКИНУТЬ"
hud.tablet.emergency.focus 				= "ОТМЕТИТЬ"
hud.tablet.emergency.noOne 				= "Никто!"
hud.tablet.emergency.info 				= [[Идентификатор вызова: ${calloutID}
Responding units: ${officerAmt}
Первый ответчик: ${firstResponder}
Звонящий: ${caller}
Департамент: ${department}

Description: ${description}]]
hud.tablet.emergency.message			= "ЭКСТРЕННОЕ СООБЩЕНИЕ"
hud.tablet.emergency.dept.fire 			= "Пожарный деп."
hud.tablet.emergency.dept.police 		= "Полицейский деп."
hud.tablet.emergency.dept.ems 			= "СМП деп."
hud.tablet.emergency.dept.tow 			= "Эвакуатор"
hud.tablet.emergency.request 			= "ЗАПРОС ПОМОЩИ"
hud.tablet.emergency.unit 				= "%s Юнит"
hud.tablet.moveDesktop 					= "Перейти на рабочий стол"
hud.tablet.successMove 					= "Успешное перемещение приложения на рабочий стол."

hud.misc.hz 							= "%s Гц" -- Hertz abbreviation
-- @TODO: modules/hud/sh_toast.lua zones

-- Hunger module
hunger.notif.hungry 					= "Вы чувствуете голод."
hunger.notif.thirsty 					= "Вы чувствуете жажду."

-- Instanced housing module
instancedhousing.gui.promptEnter 		= "Вы хотите войти в этот дом?"
instancedhousing.gui.youWillTP			= "Вы будете перемещены в другую локацию."

instancedhousing.notif.entered 			= "Вы зашли в свой дом"
instancedhousing.notif.left 			= "Вы покинули свой дом"

-- Instanced layers module
instancedlayers.notif.cantDeploy 		= "You can't deploy entities while in another instance"
instancedlayers.notif.cantDrop 			= "You can't drop items while in another instance"
instancedlayers.notif.anotherInstance	= "%s is in another instance. You've joined them."
instancedlayers.notif.returning 		= "Returning to your old instance."

-- Insurance module
insurance.gui.unemployed 				= "Безработный"
insurance.gui.understandClose 			= "Я понял, закрыть это."
insurance.gui.treatmentWouldHave		= "Ваше лечение стоило бы %s, но страховка покрыла эту стоимость, за деталями читайте ниже;\n\n"
insurance.gui.fullRate 					= "К оплате %s из кошелька по 100%% ставке.\n"
insurance.gui.someRate 					= "К оплате %s (с учетом скидки) с банковского счёта по 70%% или, в случае с Премиум, по 40%% ставке.\n"
insurance.gui.premiumApplied 			= "Премиум ставка учтена, со счёта будет списано меньше.\n"
insurance.gui.wantedNoDiscount 			= "Вы были в розыске, поэтому никакой скидки.\n"
insurance.gui.tip 						= "\nЗАМЕТКА: Любой платеж через банк будет снижен - чтобы сэкономить деньги, старайтесь платить через банк."

insurance.notif.newbie 					= "Поскольку вы здесь новенький, ваше лечение оплачивает государство. Заметьте, что когда вы достигнете 40 уровня, вам придется оплачивать его самому."
insurance.notif.paidWallet 				= "Вы оплатили стоимость лечения (%s) из своего бумажника."
insurance.notif.paidPartWallet 			= "Вы оплатили часть лечения (%s) из своего бумажника."
insurance.notif.paidPartBank 			= "Вы оплатили часть лечения (%s) со своего счёта."
insurance.notif.paidBank 				= "Вы оплатили стоимость лечения (%s) со своего счёта."

insurance.log.paidTreatment	  			= "%s заплатил %s за свое лечение."

-- Intro Screen Module
intro.gui.startLife 					= "НАЧАТЬ ВСЁ С ЧИСТОГО ЛИСТА"
intro.gui.rpName 						= "ИГРОВОЙ НИК"
intro.gui.fallbackName 					= "Петр Иванов"
intro.gui.warning 						= [[
	 • Введите серьезное имя для ролевой игры.
	 • Имя не может быть знаменитым / мемным.
	 • Отказ следовать этому приведет к запрету.]]
intro.gui.gender 						= "ПОЛ"
intro.gui.face							= "ЛИЦО"
intro.gui.variants 						= " ВАРИАНТЫ (ТОЛЬКО ПРЕМИУМ)"
intro.gui.presents 						= "Представляет..."
intro.gui.spookfest.sponsored 			= "При поддержке..."
intro.gui.spookfest.spooky 				= "Жуткие страшные скелеты"
intro.gui.undocumented 					= "Недокументированный"
intro.gui.citizen 						= "Гражданин"
intro.gui.example 						= "Пример"
intro.gui.name 							= "Имя"
intro.gui.referralsProgram				= "REFERRALS PROGRAM"						-- TODO
intro.gui.referralsQuestion				= "WERE YOU REFERRED TO MONOLITH?"			-- TODO
intro.gui.refereeSteamID				= "REFEREE'S STEAMID64"						-- TODO
-- @TODO: Update profanity filter intro_screen/sh_charactercreation.lua
-- @TODO: Expand invalid characters check in intro_screen/sh_charactercreation.lua

intro.notif.invalidName 				= "Вы ввели неверное имя (Мин. 3 буквы, макс. 15. Только символы А-Я, никаких ругательств.)"
intro.notif.invalidReferral				= "You have entered an invalid steamID64. Please try again." -- TODO

-- Inventory module (imported from legacy)
inventory.inventory 					= "Инвентарь"
inventory.inventory_description 		= "Ваш инвентарь. Он вмещает в себя %i предметов."

inventory.actions 						= "Действия"
inventory.stats 						= "Характеристики"
inventory.confiscatable 				= "Конфискуемо"
inventory.drop_on_death 				= "Падает при смерти"
inventory.deploy_instructions 			= "ЛКМ: спавн; CTR: отмена"
inventory.character_load_failure 		= "Ошибка при загрузке персонажа"
inventory.notifications 				= "Уведомления"
inventory.cant_put_x_on_y 				= "Нельзя надеть %s на %s"

inventory.equip 						= "Экипировать"
inventory.use 							= "Использовать"
inventory.deploy 						= "Разместить"
inventory.consume 						= "Употребить"
inventory.sell_with_price 				= "Продать (%s)"
inventory.sell_all						= "Sell All"	-- TODO
inventory.sell_all_confirmation			= "Are you sure you want to do this?\nThis will sell all of this item in your inventory."	-- TODO
inventory.drop 							= "Выкинуть"
inventory.drop_singular 				= "Выкинуть штуку"
inventory.drop_half 					= "Выкинуть половину"
inventory.drop_custom 					= "Выкинуть X"
inventory.drop_helper_description 		= "Укажите количество, которое вы хотели бы скинуть."
inventory.split 						= "Разделить"
inventory.split_amount 					= "На сколько вы хотите разделить (Макс. %i)"
inventory.market 						= "Магазин"
inventory.marketing 					= "Торговля"
inventory.marketing_helper_description 	= "Какую цену вы хотите поставить для предмета?"
inventory.form_requirement_number 		= "Нужно ввести число."
inventory.form_requirement_string 		= "Form requires a string answer."
inventory.form_requirement_specific_length 	= "Нужно ввести ответ длиной: %i"
inventory.custom_title_prompt 			= "Хотели бы вы задать индивидуальное название?"
inventory.remove_attachment 			= "Убрать обвес"
inventory.delete_item 					= "Удалить"
inventory.are_you_sure_prompt 			= "Вы уверены, что хотите выполнить это действие? Оно может оказаться необратимым."
inventory.inventory_load_failure 		= "Ошибка при загрузке инвентаря"
inventory.unequip 						= "Снять"
inventory.streamer_mode 				= "Стример-мод"
inventory.unknownPlayerName 			= "Неизвестное имя игрока"
inventory.mainMenuToggle 				= "Главное меню"
inventory.splitIt 						= "Разделить!"
inventory.titleLongerThan17 			= "Название не может быть длиннее 17 символов."
inventory.enterValidPrice 				= "Вы должны ввести действительную цену."
inventory.hey 							= "Эй!"
inventory.promptDelete 					= "Вы уверены, что хотите удалить %s?"
inventory.promptHigherHundredThou 		= "Цена не может быть выше, чем %s"
inventory_open_in_vehicle = "Ты не можешь открыть свой инвентарь, находясь в автомобиле."
inventory_open_handcuffed = "Ты не можешь открыть свой инвентарь, находясь в наручниках."
inventory_open_surrendered = "Ты не можешь открыть свой инвентарь, когда ты сдался."
inventory_open_arrested = "Ты не можешь открыть свой инвентарь, когда ты арестован."
inventory_open_unconscious = "Ты должен встать, чтобы выполнить это действие."

inventory.tooltip_changelog 			= "Нажмите здесь, чтобы просмотреть обновления!"
inventory.tooltip_servers 				= "Нажмите здесь, чтобы просмотреть наши сервера!"

inventory.viewer.someonesInventory 		= "ИНВЕНТАРЬ %s"
inventory.viewer.primary 				= "ОСНОВНОЕ ОРУЖИЕ"
inventory.viewer.secondary 				= "ДОПОЛНИТЕЛЬНОЕ ОРУЖИЕ"
inventory.viewer.misc 					= "ПРОЧЕЕ"
inventory.viewer.player 				= "ИГРОК"
inventory.viewer.money					= "У игрока %s"
inventory.viewer.confiscate 			= "Конфисковать"
inventory.viewer.confiscateIllegal 		= "КОНФИСКОВАТЬ НЕЛЕГАЛ"

inventory.labels.premium 				= "Премиум"
inventory.labels.job 					= "Рабочий"
inventory.labels.exotic 				= "Экзотика" -- TODO: Needs translation.
inventory.labels.soulbound 				= "Персональный"
inventory.labels.stackSize 				= "Размер стака: %s"
inventory.labels.thirst 				= "${state} ${amount} Жажда"
inventory.labels.hunger 				= "${state} ${amount} Голод"
inventory.labels.ticket 				= "Истекает: ${date}"
inventory.labels.jammed 				= "Застрял"
inventory.labels.stats 					= "Характеристики оружия:"
inventory.labels.damage 				= "Урон: %s"
inventory.labels.supportedAttachments 	= "	Обвесы:"

inventory.notif.blacklistWeapons 		= "Вы не можете экипировать запрещенное вам оружие."
inventory.notif.weaponsOnDuty 			= "Вы не можете экипировать оружие на работе."
inventory.notif.itemOnDuty 				= "Вы не можете экипировать этот предмет на работе."
inventory.notif.equipLimit 				= "Вы не можете экипировать больше, чем %s ед. этого предмета."
inventory.notif.cantEquip 				= "Вы не можете экипировать это."
inventory.notif.cantUseItem 			= "Вы не можете использовать это."
inventory.notif.dropPremium 			= "Вы не можете выкидывать Премиум вещи."
inventory.notif.dropSoulbound 			= "Вы не можете выкидывать персональные вещи."
inventory.notif.dropJobItems 			= "Вы не можете выкидывать вещи с работы."
inventory.notif.alreadyDeployed 		= "Этот предмет уже размещен, поднимите его для начала."
inventory.notif.cantUseNotEnough 		= "Вы не можете использовать это. (недостаточно)"
inventory.notif.failedPickup 			= "Вы не смогли поднять этот предмет."
inventory.notif.marketedStolen 			= "Ваш товар был украден, потому что вы слишком далеко от него.!"
inventory.notif.refreshSpam 			= "Вы не можете обновить свой инвентарь из-за спама."
inventory.notif.cannotSellPremium 		= "Вы не можете продать эту Премиум вещь."
inventory.notif.cannotSellSoulBound 	= "Вы не можете продать эту персональную вещь."
inventory.notif.cannotSellJobItems 		= "Вы не можете продать вещь с работы."
inventory.notif.youSoldAnItem 			= "Вы продали предмет за %s"
inventory.notif.invalidSellPos 			= "Вы слишком далеко, чтобы продать эту вещь."
inventory.notif.confiscatedWeapons 		= "Вы конфисковали оружие %s."
inventory.notif.gainedFromWeapons 		= "Вы получили %s от оружия %s."
inventory.notif.confYourWeapons 		= "%s конфисковал ваше оружие."
inventory.notif.noSpace 				= "У вас нет места в инвентаре, чтобы поднять это!"
inventory.notif.noDropPremium			= "Вы не можете выкинуть эту Премиум вещь."
inventory.notif.noDropSoulBound			= "Вы не можете выкинуть персональную вещь."
inventory.notif.noDropThisType			= "Вы не можете выкидывать вещи этого типа."
inventory.notif.noDropJobOnly			= "Вы не можете выкидывать вещи для работы."
inventory.notif.noDropArrested			= "Вы не можете выкидывать вещи, будучи арестованным."
inventory.notif.maxEntLimit 			= "Вы достигли лимита энтити."
inventory.notif.cantDropThis 			= "Вы не можете выкинуть это."
inventory.notif.cantUseInVehicle 		= "Вы не можете использовать предметы в машине."
inventory.notif.cantSpawnEnt 			= "Нельзя разместить энт. %s"
inventory.notif.deployed 				= "Энтити успешно размещено."
inventory.notif.failDeploy				= "Ошибка при размещении энтити."
inventory.notif.patDownNoSpace 			= "You do not have enough inventory space to do this!" -- TODO

inventory.actions.startsSearching = "/me начинает рыться в рюкзаке"
inventory.actions.equips = "/me надевает %s"
inventory.actions.unEquips = "/me снимает %s"
inventory.actions.unEquipDrop = "/me сбрасывает %s"
inventory.actions.unEquipping = "Снимает оружие ..."
inventory.actions.equipping = "Экипирует оружие ..."
inventory.actions.confiscating = "КОНФИСКУЕТ"
inventory.actions.destroys = "%s уничтожает %s"
inventory.actions.destroyingItem = "УНИЧТОЖАЮЩИЙ ПУНКТ"
inventory.actions.soldXofTheir = "%s продано ${count} \" ${name} \""
inventory.actions.startedSearching = "%s начинает поиск инвентаря %s."
inventory.actions.pickingUp = "ПОДБИРАЕТ"
inventory.actions.patDown = "%s performs a patdown on %s" -- TODO

inventory.log.equipped = "%s экипированный предмет: %s"
inventory.log.unEquipped = "Не экипированный предмет %s: %s"
inventory.log.unEquipDrop = "%s не экипирован и уронен: %s"
inventory.log.deployed = "%s развернуто %s"
inventory.log.destroyedItem = "%s уничтоженный предмет: %s"
inventory.log.searchedInventory = "%s выполнил поиск в %s"
inventory.log.confiscatedWeapon = "%s конфисковали экипированное оружие %s"
inventory.log.inventoryPrefix = "[Инвентарь]"
inventory.log.pickupUnknownItem = "%s попытался подобрать неизвестный предмет с помощью itemEnt ="
inventory.log.pickedUpItem 	= "%s picked up %sx of item: %s"
inventory.log.pickedUpItem.object = "Объект"
inventory.log.dropped = "%s выпало %sx предмета: %s"
inventory.log.usedItem = "%s использованный элемент: %s"
inventory.log.patDown = "%s performed a patdown on %s" -- TODO

inventory.item.consumable = "Расходуемые"
inventory.item.equipment = "Оборудование"
inventory.item.ingredient = "Ингредиент"
inventory.item.attachment = "Аддон"
inventory.item.food = "Еда"
inventory.item.noDescription = "У этого товара нет описания"
inventory.item.alreadyEquipped = "У вас уже есть это оружие"
inventory.item.deployingDisabled = "Размещение отключено: %s"
inventory.item.deployingDisableFallback = "Нестабильность сервера"
inventory.item.cantDeployThis = "Вы не можете разместить это"
inventory.item.entityLimit = "Вы достигли предела энтити."
inventory.item.inCombat = "Вы не можете сделать это в бою."
inventory.item.tooClose = "Слишком близко для размещения"
inventory.item.cantDeployProperty = "Вы не можете разместить это вне помещения."

inventory.sell_to_store = "Продать %s ( %s)"

-- инвентарные контейнеры
invcontainers.gui.container = "КОНТЕЙНЕР"
invcontainers.gui.subHeader = "Это контейнер."
invcontainers.gui.currentObservers = "Текущие наблюдатели:"
invcontainers.gui.storeUpTo = "Вы можете хранить до %s элементов в этом контейнере."

invcontainers.notif.somethingWrong = "При попытке открыть этот контейнер что-то пошло не так."
invcontainers.notif.cantUse = "Вы не можете использовать этот контейнер."
invcontainers.notif.container = "Контейнер"
invcontainers.notif.invalidQuantity = "Передача не удалась. Неверное количество."
invcontainers.notif.failWithdraw = "Вам не удалось отозвать этот элемент."
invcontainers.notif.withdrawInvalidAmt = "Снятие не удалось. Запрошена неверная сумма."
invcontainers.notif.confiscatedIllegal = "Вы конфисковали этот незаконный предмет."

invcontainers.lootBox = "ЛУТБОКС"
invcontainers.corpse = "ТРУП"
invcontainers.corpse.info = "Остатки %s"
invcontainers.backpack = "РЮКЗАК"

invcontainers.log.deposited = "%s отправленного элемента: %s в контейнер %s."
invcontainers.log.ownedBy = "принадлежит %s"
invcontainers.log.tookItem = "%s взял элемент: %s из контейнера %s."
invcontainers.log.confiscated = "Конфискованный предмет %s: %s из контейнера %s"

-- Модуль работы
jobs.notif.unknownJob = "Неизвестная работа"
jobs.notif.positionsFilled = "Все позиции заполнены. Пожалуйста, зайдите позже."
jobs.notif.unableBecome = "Вы не можете устроиться на эту работу."
jobs.notif.requiresLevelCategory = "Для этого задания требуется уровень %s %s"
jobs.notif.requiresLevel = "Эта работа требует уровня"
jobs.notif.tow.inNeedRepair = "Автомобиль в настоящее время нуждается в ремонте."
jobs.notif.tow.proceedMarked = "Пожалуйста, перейдите к отмеченному местоположению."
jobs.notif.tow.repairmanOnWay = "Ремонтник уже в пути!"
jobs.notif.tow.needMoneyToCall = "Чтобы вызвать эвакуатор, нужно как минимум $500."
jobs.notif.tow.nextToVehicle = "Вы должны быть рядом с вашим автомобилем!"
jobs.notif.tow.repairScheduled = "Ремонт вашего автомобиля уже запланирован!"
jobs.notif.tow.unableFindTow = "Нам не удалось найти эвакуатор!"
jobs.notif.tow.vehicleRepaired = "Отличная работа! Автомобиль был отремонтирован."
jobs.notif.tow.onlyMechanicsRepair = "Только механики могут ремонтировать ваш автомобиль в это время."
jobs.notif.employDetained = "Вы не можете быть наняты во время задержания."
jobs.notif.blacklistedJob = "Вы попали в черный список этой работы."
jobs.notif.blacklistedCat = "Вам была заблокирована эта работа."
jobs.notif.wanted = "Ты сейчас в розыске. Пожалуйста, приходите позже."
jobs.notif.waitBeforeChanging = "Пожалуйста, подождите %s, прежде чем сменить работу."
jobs.notif.demote.pd = "Департамент полиции недавно увольнял вас. Вы должны подождать %s"
jobs.notif.demote.tow = "Департамент буксировки недавно увольнял вас. Вы должны подождать %s"
jobs.notif.demote.ems = "Департамент EMS недавно увольнял вас. Вы должны подождать %s"
jobs.notif.demote.fire = "Отдел пожарной охраны недавно увольнял вас. Вы должны подождать %s"
jobs.notif.demote.security = "Департамент безопасности недавно увольнял вас. Вы должны подождать %s"
jobs.notif.demote.mayor = "Мэр недавно увольнял вас. Вы должны подождать %s"
jobs.notif.premiumOnly = "У вас должен быть Премиум-статус для этой работы."
jobs.notif.quitFromJob = "Вы уволились со своей текущей работы"
jobs.notif.reachedJobLevel = "Вы достигли %s уровня %s."
jobs.notif.demotedToLevel = "Вы были понижены до уровня %s %s."
jobs.notif.cantSpawnInHouse = "!!! ДАЙТЕ НАМ ЗНАТЬ, КОГДА ЭТО ПРОИСХОДИТ !!!" -- Дьявол, почему ты не добавил его :(
jobs.notif.unableSWAT = "SWAT enlistment has not been made available by the Chief." -- TODO

jobs.convar.jobmultiplier = "Что такое множитель опыта работы"

jobs.misc.unknown = "Неизвестно"
jobs.misc.hospital = "Больница"
jobs.misc.tow.vehicleRepairs = "Ремонт автомобилей"
jobs.misc.noReason = "Нет причин"

jobs.log.became = "%s стал %s"
jobs.log.quit							= "%s quit from his job as %s"

-- Loadout module
loadout.gui.header 						= "ЯЩИК С ИНСТРУМЕНТАМИ"
loadout.gui.subheader 					= "Выберите метод уничтожения."
loadout.gui.close 						= "ЗАКРЫТЬ ✕"
loadout.gui.purchase 					= "Покупка"
loadout.gui.miscSubhead 				= "Список всех доступных прочих утилит."
loadout.gui.ammoRefill 					= "ПОПОЛНИТЬ АМУНИЦИЮ - ${price}"
loadout.gui.armorRefill 				= "ПОПОЛНИТЬ БРОНЮ - ${price}"
loadout.gui.free 						= "Бесплатно"
loadout.gui.refillSubhead 				= "Пополните все ваши патроны до максимальной вместимости."
loadout.gui.refillCurrentArmor 			= "Пополните вашу броню до максимума."
loadout.gui.refillFilters 				= "Пополнить кол-во фильтров до максимума."  -- TODO: Needs Translation
loadout.gui.gasMaskDesc					= "Обеспечивает временную защиту от газа и химикатов." -- TODO: Needs Translation
loadout.gui.medkitRefill 				= "ПОПОЛНИТЬ АПТЕЧКУ - ${price}"
loadout.gui.filterRefill 				= "ПОПОЛНИТЬ ФИЛЬТРЫ - ${price}" -- TODO: Needs Translation
loadout.gui.gasMask 					= "ПРОТИВОГАЗ - ${price}" -- TODO: Needs Translation
loadout.gui.refillJobMedkit 			= "Пополните вашу рабочую аптечку."
loadout.gui.grenadesSubhead 			= "Список всех доступных гранат."
loadout.gui.currentBudget 				= "ТЕКУЩИЙ БЮДЖЕТ"
loadout.gui.numInBudget 				= "В настоящее время ${price} в бюджете мэра."
loadout.gui.budgetNoMayor 				= [[В настоящее время мэра нет - все покупки будут напрямую вычтены из вашего кошелька.]]
loadout.gui.listOfWeapons 				= "Список всего доступного ${name} оружия."
loadout.gui.breachRefill 				= "BREACHING CHARGE - ${price}"
loadout.gui.refillJobBreachCharge 		= "Refill your breaching charge."
loadout.gui.GLRefill 					= "Grenade Launcher Ammo - ${price}"
loadout.gui.refillJobGL 				= "Refill your explosive rounds."
-- @TODO: How does Monolith.Keybinds.GetBindFromName work?

loadout.grenade.smoke 					= "Дымовая граната"
loadout.grenade.smoke.desc				= "Полезно для прикрытия."
loadout.grenade.flash 					= "Световая граната"
loadout.grenade.flash.desc				= "Полезно для дизориентации противника."
loadout.grenade.frag 					= "Осколочная граната"
loadout.grenade.frag.desc				= "Полезно для массового уничтожения."
-- @TODO: What is "cloud" "certificate" and "bomb" in lines 168-170 sh_loadout.lua?

loadouts.misc.primary 					= "Основное"
loadouts.misc.secondary 				= "Дополнительное"
loadouts.misc.grenades 					= "Гранаты"
loadouts.misc.misc 						= "Остальное"

loadout.notif.refillTimeout 			= "Пожалуйста, подождите: ${time}."
loadout.notif.cantFit 					= "Вы не можете это надеть."
loadout.notif.deptCantAfford 			= "Ваш департамент не может себе этого позволить."
loadout.notif.youCantAfford 			= "Вы не можете себе это позволить."
loadout.notif.noSpaceToStore 			= "У вас недостаточно места в инвентаре, чтобы купить это!"
loadout.notif.medkitNoNeedRefill 		= "Вашу аптечку не нужно пополнять."
loadout.notif.boughtMedkit 				= "Вы приобрели новую рабочую аптечку."
loadout.notif.refillJobMedkit 			= "Вы пополнили свою рабочую аптечку."
loadout.notif.purchasedXforY 			= "Вы приобрели ${weapon} за ${price}"
loadout.notif.purchasedXArmorforY		= "Вы приобрели ${amount} брони, за ${price}"
loadout.notif.refilledAmmo 				= "Вы пополнили боеприпасы."
loadout.notif.refilledFilters 			= "Вы пополнили запас фильтров." -- TODO: Needs Translation
loadout.notif.boughtGasMask				= "Вы приобрели противогаз." -- TODO: Needs Translation
loadout.notif.noNeedtoRefillFilter		= "У вас уже максимальное количество фильтров." -- TODO: Needs Translation
loadout.notif.ownsGasMask				= "У вас уже есть противогаз." -- TODO: Needs Translation
loadout.notif.breachNoNeedRefill 		= "You already have a breaching charge." -- TODO: Needs Translation
loadout.notif.refillJobBreach 			= "You purchased a breaching charge." -- TODO: Needs Translation
loadout.notif.glNoNeedRefill 		    = "You already have the maximum amount of explosives." -- TODO: Needs Translation
loadout.notif.refillJobGL 			    = "You purchased an explosive round." -- TODO: Needs Translation

-- Log module
log.mnsCommand 							= "%s ran MonoSuite command \"%s\"%s" -- перевод не нужен
log.monolithCommand 					= "%s ran Monolith command \"%s\"%s" -- перевод не нужен
log.connected							= "%s ( %s) подключился к серверу"
log.left 								= "%s ( %s) вышел с сервера ( %s)"
log.fallDamage 							= "%s разбился насмерть."
log.unknownEntity 						= "%s был убит: неизвестный объект. (Возможно, ошибка в карте)."
log.suicide 							= "%s самоубился."
log.killedByXWithY						= "%s был убит: %s при помощи %s"
log.killedByX 							= "%s был убит: %s"
log.killedByXDriving 					= "%s был убит: %s за рулём авто."
log.killedByEmptyVehicle 				= "%s был убит: пустой автомобиль."
log.killedByEntity 						= "%s был убит: a %s"
log.damagedThemself 					= "%s получил урон: нанёс сам себе %s урона"
log.damagedVehicle 						= "%s получил урон машиной, управляемой: %s на %s урона"
log.damagedByXForYWith					= "%s получил урон: %s на %s урона, при помощи %s"
log.damagedByXForY 						= "%s получил урон: %s на %s урона"
log.damagedVehicleDriven 				= "%s получил урон машиной, управляемой: %s на %s урона"
log.damagedLooseVehicle 				= "%s was damaged by a loose vehicle owned by: %s for %s damage"
log.spawnSENT 							= "%s spawned sent \"%s\""
log.spawnSWEP 							= "%s заспавнил SWEP \"%s\""
log.useTool 							= "%s пытался использовать инструмент \"%s\" на %s"

log.gui.title							= "Логи"
log.gui.showing 						= "Показал логи %s игроку %s"
log.gui.showing.all 					= "всё"
log.gui.noServerName 					= "Нет серверного имени"
log.gui.liveUpdate 						= "Обновление в реальном времени"
log.gui.searchColon 					= "Поиск: "
log.gui.na 								= "N/A" -- не требуется
log.gui.actions 						= "Действия"
log.gui.time							= "Время"
log.gui.category 						= "Категория"
log.gui.message							= "Сообщение"
log.gui.copyLog 						= "Скопировать логи"
log.gui.playerNotOnlineNow 				= "Этот игрок не в сети"
log.gui.tpToPlayer 						= "Телепортироваться к игроку"
log.gui.bringPlayer 					= "Телепортировать к себе"
log.gui.filterLogsOf 					= "Фильтр логов %s"
log.gui.noParticipants 					= "No participants to view"
log.gui.itemsFound 						= "Найденные предметы"
log.gui.loading 						= "Загрузка"
log.gui.allLogs 						= "Все логи"

log.prompt.pageSelect 					= "Какую страницу вы хотите открыть?"

log.cat.transaction 					= "Транзакции"
log.cat.roleplay 						= "RP"
log.cat.iMessage 						= "Сообщения"
log.cat.inventory 						= "Инвентарь"
log.cat.system 							= "Система"
log.cat.police 							= "Полиция"
log.cat.deaths 							= "Смерти"
log.cat.admin 							= "Администрация"
log.cat.damage 							= "Урон"
log.cat.commands 						= "Команды"
log.cat.connections 					= "Подключения"
log.cat.rrl								= "RRL"
log.cat.spawning 						= "Спавн (пропов)"
log.cat.externalAddons 					= "Внешние дополнения (аддоны)"

log.report.created 						= "%s создал жалобу (# %s): %s"
log.report.opened 						= "%s открыл жалобу (# %s)"
log.report.closed 						= "%s закрыл жалобу (# %s)"
log.report.claimed 						= "%s занялся жалобой (# %s)"
log.report.unclaimed 					= "%s перестал заниматься жалобой (# %s)"
log.report.respond 						= "%s ответил на жалобу (# %s): %s"
log.report.removed 						= "%s удалил жалобу (# %s)"

log.notification.respond				= "A staff member replied to your report (#%s)" --TODO
-- @NOTE: Logs comand sv_logger.lua

-- Money Module
money.notif.withdrawnTooMuch 			= "%s has earned or withdrawn more than %s"
money.notif.youMustDeclare 				= "You have to declare your earnings."

-- Nameplates module
nameplates.gui.outOfStock 				= "Нет в наличии!"
nameplates.gui.premium 					= "Премиум" -- Premium member
nameplates.gui.arrested 				= "Арестован"
nameplates.gui.hidden 					= "*СПРЯТАН*"
nameplates.gui.wanted 					= "В РОЗЫСКЕ"
nameplates.gui.unknown 					= "Неизвестно"

-- Navigation module
nav.gui.below 							= "1 ур."
nav.gui.above 							= "2 ур."
nav.gui.aboveBelowDisplay 				= "%s ( %sм)" -- Below (50m)
nav.gui.unknownArea 					= "Неизвестное место"

nav.misc.meters 						= "м"

nav.poi.pVehicle 						= "Моя машина"
nav.poi.jVehicle 						= "Рабочая машина"
nav.poi.myProperty 						= "Мой дом"
nav.poi.gps 							= "GPS"
nav.poi.personalApartment 				= "Личная квартира"

-- New intro module
newintro.gui.map.title 					= "ИНТЕРАКТИВНАЯ КАРТА"
newintro.gui.map.hint 					= "2х ЛКМ чтобы заспавниться"
newintro.gui.dynamic 					= "СОБСТВЕННЫЕ"
newintro.gui.buyToSpawnElsewhere 		= "Вы можете спавниться в новом месте с помощью покупки своего Дома."
newintro.gui.cantSpawnPersonal 			= "Вы не можете заспавниться без дома."
newintro.gui.static						= "ГОРОДСКИЕ"
newintro.gui.monolith 					= "МОНОЛИТ РОЛЕПЛЕЙ"
newintro.gui.continueLife 				= "ПРОДОЛЖИТЬ ИГРАТЬ КАК %s"
newintro.gui.serverTitle 				= "( %s) Версия %s" -- (SERVER 1) Version 6.0.0
newintro.gui.availableSoon				= "Это будет добавлено позже"

newintro.prompt.spawnHere 				= "Вы уверены, что хотите заспавниться тут?"

-- Notifications
notify.btn.view 						= "Посмотреть"
notify.btn.accept 						= "Принять"
notify.btn.deny 						= "Отказаться"
notify.btn.dismiss 						= "Уволиться"
notify.btn.joinGarage 					= "Войти в гараж"
notify.btn.discard 						= "Discard" -- ?

notify.title.notice 					= "Уведомление"
notify.title.warning 					= "Предупреждение"
notify.title.tip 						= "Совет"
notify.title.alert 						= "Оповещение"
notify.title.admin 						= "Админ"
notify.title.noticePush 				= "Пуш-уведомление"

notify.notif.unreadNotifications 		= "У вас есть %s непрочитанных оповещений."

-- NPC module
npc.gui.leave 							= "Покинуть"
npc.gui.welcomeTo 						= "ДОБРО ПОЖАЛОВАТЬ"
npc.gui.workersOnDuty 					= "РАБОТНИКОВ"
npc.gui.levelX 							= "Уровень %s"
npc.gui.join 							= "УСТРОИТЬСЯ"
npc.gui.quit 							= "УВОЛИТЬСЯ"
npc.gui.unlockedAt 						= "Открыто на %s уровне игрока"
npc.gui.unlocksAt 						= "Откроется на %s уровне игрока"
npc.gui.unlockedAtJob					= "Открыто на %s уровня %s" -- Unlocked at Police level 10
npc.gui.unlocksAtJob 					= "Откроется на %s уровня %s"
npc.gui.whitelist						= "Requires whitelist"

npc.prompt.leaveJob 					= "Вы уверены, что хотите покинуть работу %s?"
npc.prompt.leaveJob.title 				= "У вас уже есть работа"

npc.notif.youNeedToBeX 					= "Вы должны быть %s, чтобы получить доступ к этому."
npc.notif.invalidRank 					= "Вам нужен ранг выше, чтобы получить доступ к этому."
npc.notif.noValidCareerLicense 			= "Вам нужны действительные грузовые водительские права, полученные в Мэрии."
npc.notif.blacklistedDeployingVehicles 	= "Вам запрещено размещать машины."

-- Ownership module
own.gui.onlineFilter 					= "Онлайн фильтр: "
own.gui.onlinePlayers 					= "Игроки онлайн"
own.gui.noOneOnline 					= "Нет игроков онлайн"
own.gui.offlinePlayers 					= "Игроки оффлайн"
own.gui.addViaSteamID 					= "Добавить через SteamID"
own.gui.promptInsertSteamID 			= "Введите Steam ID игрока, которого хотите изменить"
own.gui.haventEditedOfflinePlayers 		= "Вы не отредактировали оффлайн игроков"
own.gui.noOwner 						= "Нет владельца"
own.gui.sharedAccess 					= "Совместный доступ"
own.gui.sharedAccessSubhead 			= "Вы можете изменять доступ к своим вещам."

-- Player module
ply.notif.invalidName 					= "Ты ввел неверный никнейм, попробуй позже."
ply.notif.nickProfanity 				= "Никнейм не может содержать ненормативную лексику."
ply.notif.nickLonger15Chars 			= "Никнейм не может быть длиннее 15 символов."

ply.prompt.nickname 					= "Никнейм"
ply.prompt.newNickname 					= "Какой ты хочешь никнейм?"

ply.misc.unknown 						= "Неизвестный"

-- Property module
-- @NOTE: We skipped furniture layouts here
-- @TODO: Test this extensively. Cut some corners due to furniture, don't want to see anything missed on the real gamemode.
property.gui.youreRenting 				= "Ты арендуешь это"
property.gui.close 						= "ЗАКРЫТЬ ✕"
property.gui.noOwner 					= "Нет владельца"
property.gui.goBack 					= "< Вернуться"
property.gui.propertyListing 			= "Список помещений"
property.gui.details 					= "ПОДРОБНОСТИ"
property.gui.deedOwner 					= "Арендодатель"
property.gui.rentPrice 					= "Цена аренды"
property.gui.pricePerTime 				= "%s за 15 минут"
property.gui.rentBuy 					= "Арендовать/Приобрести"
property.gui.rentWithPrice 				= "Арендовать (%s)"
property.gui.alreadyOwned 				= " (Уже в собственности)"
property.gui.deedsLast 					= "Договор об аренде длится 14 дней."
property.gui.purchaseDeeds				= "Взять в аренду (%s)"
property.gui.rentingActions 			= "Действия с арендой"
property.gui.cancelRent 				= "Прекратить аренду" -- проверить в игре
property.gui.deedOwnerActions 			= "Действия арендатора"
property.gui.viewDeedInfo 				= "Информация об аренде"
property.gui.listing 					= "В списке ${amount} ${plural}."
property.gui.listing.singular 			= "предложение"
property.gui.listing.plural 			= "предложений"
property.gui.propDeed 					= "Договор о владении"
property.gui.deedInfo 					= "Информация о договоре"
property.gui.boughtDeed 				= "Вы заключили этот договор за %s"
property.gui.deedStartDate 				= "Договор заключен: %s"
property.gui.unknown 					= "Неизвестно"
property.gui.deedEndDate 				= "Окончание срока аренды: %s"
property.gui.earnedFromDeed 			= "Вы получили с арендной платы: %s"
property.gui.actions 					= "Действия"
property.gui.collectUnclaimed 			= "Получено дохода (%s)"
property.gui.changeRentPrice 			= "Изменить цену аренды (текущая %s)"
property.gui.currentRenter 				= "Текущий арендатор"
property.gui.deed 						= "Договор"
property.gui.myDeeds 					= "Мои договоры"
property.gui.mapControls 				= [[WASD клавиши, чтобы двигаться. Вы также можете зажимать ЛКМ. Колёсико мышки чтобы отдалить/приблизить.]]
property.gui.layerX 					= "Уровень %s"
property.gui.viewCurrentDeeds 			= "Посмотреть свои нынешние договоры."
property.gui.viewInfoAnyProperty 		= "Посмотреть информацию о недвижимости."
property.gui.pleaseSelectProperty	 	= "Выберите недвижимость"
property.gui.propertyView 				= "Просмотр недвижимости"
property.gui.propertyFinder 			= "Поиск недвижимости"

property.notif.updatedRentPrice 		= "Вы обновили цену аренды для этого помещения."
property.notif.somethingWentWrong 		= "Что-то пошло не так. Изменить цену аренды не удалось."
property.notif.deedsDisabled 			= "Возможность заключения договора в данный момент ограничена"
property.notif.failedPurchase 			= "Не удалось приобрести собственность. Средства были возвращены. Попробуйте позже."
property.notif.evicted 					= "Вас выселили. %s"
property.notif.alreadyRenting 			= "Вы и так уже снимаете помещение."
property.notif.rentFailed 				= "Не удалось снять помещение, стоимость аренды была возвращена."
property.notif.startedRenting 			= "Вы сдаете помещение в аренду"
property.notif.cancelledYourRent 		= "Вы более не сдаете помещение в аренду"
property.notif.claimedDeedEarnings 		= "Вы получили %s дохода от аренды."
property.notif.cantClaimEarnings 		= "Вы не можете снять эти деньги (должно быть >0)"
property.notif.rent 					= "Вы заплатили за аренду ${amount} ${source}"
property.notif.rent.bank				= "с вашего банковского счета"
property.notif.rent.pocket 				= "из вашего бумажника."
property.notif.rentUnpaid 				= "У вас недостаточно денег для оплаты аренды. Вы будете выселены через ${time} минут если не сможете заплатить."
property.notif.rentEvicted 				= "ы были выселены за неуплату аренды."
property.notif.changedRestricted 		= "Вы были переведены на ограниченные работы."

property.prompt.startRent 				= "Вы уверены что хотите начать сдавать эту недвижимость за %s каждые 15 минут?"
property.prompt.startRent.title 		= "Система недвижимости"
property.prompt.purchaseDeed 			= "Вы уверены что хотите заключить договор аренды этой недвижимости? ЗАМЕТКА: Договор действителен лишь в течении 14 дней."
property.prompt.rentPriceAdjustment 	= "Какую цену вы бы хотели поставить? (должно быть >0)"
property.prompt.rentPriceAdjustment.tit = "Property Rent Price Adjustment"

property.log.startRent					= "Player %s started renting a property: %s."
property.log.cancelRent					= "Player %s stopped renting a property: %s."
proprety.log.purchaseDeed				= "Player %s purchased a property's deed: %s."

-- Purge module
purge.gui.text 							= [[
Это не учебная тревога.

Это система экстренного вещания, объявляющая о начале Ежегодной Чистки, санкционированной правительством США.

Оружие класса 4 и ниже разрешено к применению во время Чистки. Все остальные виды оружия запрещены.
Должностные лица 10 ранга получили иммунитет от чистки и не должны пострадать.
Начиная с сигнала сирены, любые преступления, включая убийства, будут считаться легальными в течении следующих 12 часов.

Полиция, пожарные и скорая медицинская помощь будут недоступны до 7 часов завтрашнего утра, когда Чистка окончится.

Благословенны будут наши новые Отцы-основатели и Америка, возрожденная нация.

Да благославит всех вас Господь.
]]
purge.gui.starting 						= "ЧИСТКА НАЧИНАЕТСЯ"
purge.gui.active						= "ЧИСТКА В ПРОЦЕССЕ"
purge.gui.emergencyBroadcastSystem 		= "Система аварийного оповещения"

-- Quests module
quests.gui.complete 					= "КВЕСТ ЗАВЕРШЕН"
quests.gui.rewardsColon 				= "Награды: "
quests.gui.goBack 						= "< Назад"
quests.gui.completed 					= "ЗАВЕРШЕННЫЙ КВЕСТ"
quests.gui.questInProgress 				= "КВЕСТ В ПРОЦЕССЕ"
quests.gui.description 					= "ОПИСАНИЕ"
quests.gui.noDescription 				= "Нет описания."
quests.gui.requirements 				= "ТРЕБОВАНИЯ"
quests.gui.notApplicable 				= "N/A."
quests.gui.difficulty 					= "Со сложностями"
quests.gui.rewards 						= "НАГРАДЫ"
quests.gui.noRewards 					= "Нет наград."
quests.gui.actions 						= "ДЕЙСТВИЯ"
quests.gui.focusQuest 					= "Следить за квестом"
quests.gui.stopFocusingQuest 			= "Перестать следить за квестом"
quests.gui.filter 						= "Сортировать"
quests.gui.questLog 					= "Лог квестов"
quests.gui.questLog.subHeader 			= "Твои приключения, все в одном месте. Нажми на Квест, чтобы узнать подробности."

quests.already_completed_quest = "Ты уже закончил этот квест."
quests.cannot_start_quest = "Ты не можешь начать этот квест."
quests.new_quest = "НОВЫЙ КВЕСТ"

quests.notif.youStartedQuest 			= "Ты начал квест: %s"
quests.notif.drankSomeWater 			= "Ты выпил воды. Кажется, ты забыл некоторые из своих воспоминаний."

quests.misc.easy 						= "Легко"

quests.q.tutorial.name 					= "Вступительный квест"
quests.q.tutorial.desc 					= "Знакомство с Монолитом."
quests.q.tutorial.requirements 			= "Нет требований."
quests.q.tutorial.rewards				= "250 XP, $10,000 (Банковский счет), 2x бутылки воды"
quests.q.tutorial.onFinish 				= "Вы завершили туториал! Вот ваша награда."
quests.q.tutorial.steps.say 			= "Скажите что-нибудь в чат!"
quests.q.tutorial.steps.deposit 		= "Внесите деньги на свой банковский счет с помощью терминала."
quests.q.tutorial.steps.tablet 			= "Откройте Планшет"
quests.q.tutorial.steps.drink 			= "Выпей бутилированной воды (${amount}/1)"

-- TODO: Translate the firstjob quest.
quests.q.firstjob.name					= "First Job"
quests.q.firstjob.desc					= "Get your first job in Monolith"
quests.q.firstjob.requirements			= "No requirements."
quests.q.firstjob.rewards				= "200 JobXP, $4,000 (Bank Account)"
quests.q.firstjob.onFinish				= "You finished this quest! Here are your rewards."
quests.q.firstjob.steps.join			= "Go to the hospital and become a Health Professional!"
quests.q.firstjob.steps.heal			= "Heal at least (${amount}/15) citizens."

quests.q.mine.name 						= "Рождён копать"
quests.q.mine.desc 						= "Учит тебя добыче полезных ископаемых."
quests.q.mine.requirements 				= "Нет требований."
quests.q.mine.rewards 					= "1000 XP, $2,000, 32x камня"
quests.q.mine.onFinish 					= "Вы завершили квест! Вот ваша награда."
quests.q.mine.steps.pickaxe 			= "Купи 1x кирку у кассира с инструментами (${amount}/1)"
quests.q.mine.steps.stone 				= "Отправляйся в шахту и собери 32x куска камня (${amount}/32)"

quests.q.wood.name 						= "Жизнь дровосека"
quests.q.wood.desc 						= "Учит тебя добыче дерева."
quests.q.wood.requirements 				= "Нет требований."
quests.q.wood.rewards 					= "1000 XP, $2,000, 32x бревна"
quests.q.wood.onFinish 					= "Вы завершили квест! Вот ваша награда."
quests.q.wood.steps.axe 				= "Купи 1x топор лесоруба у кассира с инструментами (${amount}/1)"
quests.q.wood.steps.logs 				= "Отправляйся на лесопилку и добудь 32x бревна (${amount}/32)" -- TODO

quests.q.home.name 						= "Домашние компромиссы"
quests.q.home.desc 						= "Знакомит тебя с жизнью в собственном доме."
quests.q.home.rewards 					= "1250 XP, $3,000, 1x Smart TV"
quests.q.home.onFinish 					= "Вы завершили квест! Вот ваша награда."
quests.q.home.requirements 				= "Ты должен быть 5 уровня, чтобы начать этот квест."
quests.q.home.steps.talkNPC 			= "Поговори с агентом по недвижимости."
quests.q.home.steps.rent 				= "Арендуй резиденцию, дом или квартиру."

quests.q.fish.name 						= "Как рыба из воды"
quests.q.fish.desc 						= "Знакомит тебя с миром рыбалки."
quests.q.fish.requirements 				= "Нет требований."
quests.q.fish.rewards 					= "1000 XP, 962 XP, $1,500"
quests.q.fish.onFinish 					= "Вы завершили квест! Вот ваша награда."
quests.q.fish.steps.talkNPC 			= "Поговори с поставщиком товаров для рыбалки"
quests.q.fish.steps.buyRod 				= "Купить удочку"
quests.q.fish.steps.buyBait 			= "Buy Small Lure Bait 5x"
quests.q.fish.steps.gatherFish 			= "Go to a fishing spot and Gather 5x Fish"

quests.log.finishQuest					= "%s finished a quest: %s."

-- Recognition module
recog.unknown 							= "Неизвестно"

recog.gui.getID 						= "Скопировать ID"

recog.notif.copiedID 					= "ID цели скопирован в буфер обмена."
recog.notif.introducedSelf 				= "Вы представились игроку."
recog.notif.theyIntroduced 				= "Игрок %s представился вам."

-- Referrals module (TODO)
referrals.notif.success						= "You entered your referral code successfully!"
referrals.notif.failed						= "We could not process your referral code. Contact a developer."
referrals.notif.invalidCode					= "The SteamID64 you entered is invalid. Please, try again."
referrals.notif.ownCode						= "You cannot use your own SteamID64!"
referrals.notif.failedRedeem				= "Failed to redeem reward for referred player."
referrals.notif.successRedeem				= "Redeemed reward for referred player successfully."
referrals.notif.notEnoughLevel				= "Player's level is not high enough, for you to claim your referral reward."
referrals.notif.notOnTheServer				= "Player must be on the server at the same time, for you to collect the reward."
referrals.notif.waitForAction				= "You must wait %s before performing that action again."

referrals.log.success						= "%s inserted a referral code: %s"
referrals.log.redeemedReward				= "%s redeemed reward for referred player %s"
referrals.log.failed						= "Failed to insert referral code: %s"

referrals.gui.modificationsTitle			= "Referrals"
referrals.gui.referralsAccessSubhead		= "You can refer new players to the server in this section."
referrals.gui.onlineFilter 					= "Online Filter: "
referrals.gui.onlinePlayers 				= "Online Players"
referrals.gui.noOneOnline 					= "You cannot redeem any rewards. No players online."
referrals.gui.offlinePlayers 				= "Offline Players"
referrals.gui.noOwner 						= "No Owner"
referrals.gui.redeemReward					= "Redeem Reward"

-- @NOTE: Skipped sit system

-- Skills module
skills.gui.skillMultiplier 					= "%s - ( %s опыта за %s)"
skills.gui.allMilestones 					= "Весь прогресс для %s."
skills.gui.levelX 							= "Уровень %s"

skills.invsec.title 						= "Навыки"
skills.invsec.subHeader 					= "Ваши умения & способности. Нажмите на навык, чтобы посмотреть его прогресс."

skills.notif.granted						= "Вы получили %sx бустер опыта для %s на %s."

skills.s.crafting 							= "Крафтинг"
skills.s.mining 							= "Рудокопство"
skills.s.farming 							= "Фермерство"
skills.s.farming.greenThumb 				= "Зеленый палец"
skills.s.cooking 							= "Кулинария"
skills.s.metal 								= "Литье стали"
skills.s.fishing 							= "Рыболовство"
skills.s.fishing.wormBait 					= "Приманка-червь (Угорь, скумбрия)"
skills.s.fishing.small 						= "Маленькая приманка (Окунь, треска)"
skills.s.fishing.medium 					= "Средняя приманка (Рыба махи-махи)"
skills.s.fishing.big 						= "Большая приманка (Лосось)"
skills.s.fishing.tuna 						= "Наживка из тунца (Меч-рыба)"
skills.s.fishing.codmeat 					= "Наживка из трески (Рыба-солнце)"
skills.s.woodcutting 						= "Лесорубство"
skills.s.chemistry 							= "Химия"
skills.s.chemistry.clumsy					= "Неуклюжий химик"
skills.s.chemistry.handy					= "Рукастый химик"
skills.s.chemistry.average					= "Обычный химик"
skills.s.chemistry.trained					= "Обученный химик"
skills.s.chemistry.skilled					= "Умелый химик"
skills.s.chemistry.experienced				= "Бывалый химик"
skills.s.chemistry.advanced					= "Продвинутый химик"
skills.s.chemistry.proficient				= "Опытный химик"
skills.s.chemistry.exceptional				= "Исключительный химик"
skills.s.chemistry.master					= "Мастер-химик"

-- @TODO: Take a look at the spawners module

-- Store Module
store.gui.youCanMove 						= "Вы можете передвигаться с помощью клавиш WASD."
store.gui.purchasePrice						= "Купить %s"
store.gui.checkout 							= "ОПЛАТИТЬ"
store.gui.unableToPurchase 					= "Невозможно приобрести товар. Была обнаружена ошибка."
store.gui.total 							= "ВСЕГО"
store.gui.outOfStock 						= "На складе нет этого товара!"
store.gui.levelColon 						= "УРОВЕНЬ: %s"
store.gui.addToCart 						= "В КОРЗИНУ"
store.gui.wasPrice 							= "было %s"

store.prompt.notEnough 						= "У вас недостаточно денег"
store.prompt.notEnough.title 				= "Вы не можете совершить покупку"
store.prompt.howMuchPurchase.title 			= "Укажите количество приобретаемого товара"
store.prompt.howMuchPurchase 				= "Количество"
store.prompt.howMuchPurchase.fallback 		= "1"
store.prompt.needLicense 					= "Вам нужна лицензия на оружие, в ином случае вас никто не остановит от производства собственного"
store.prompt.needLicense.title 				= "Требуется лицензия на оружие"

store.notif.restrictedPremium 				= "Разрешено для Премиум игроков. (!store)"
store.notif.moreFoodTypes 					= "Вы разблокировали больше видов еды!"
store.notif.allFoodTypes 					= "Вы разблокировали все виды еды!"
store.notif.levelUp 						= "ПОВЫШЕНИЕ УРОВНЯ"
store.notif.pleaseWaitX	 					= "Подождите %s."
store.notif.backpackFull 					= "Инвентарь полон!"
store.notif.nonComma 						= "Вы должны использовать натуральные числа!"
store.notif.anErrorOccured 					= "Была обнаружена ошибка."
store.notif.youCantPurchaseX 				= "Вы не можете приобрести %s"
store.notif.itemOutOfStock 					= "Товар %s отсутствует на складе!"

-- @NOTE: Skipping stormfox until it's re-done.

-- Tablet framework module
tabletfw.gui.moveToDesktop 					= "Перейти на рабочий стол"

-- Transactions module
trans.notif.cantAfford 						= "Вы не можете себе это позволить."
trans.notif.buyerCantAfford					= "Покупатель не может себе этого позволить."
trans.notif.buyerNoRoom 					= "У покупателя нет места в инвентаре."

trans.misc.unspecified 						= "Неопределенный"

trans.actions.boughtXofY 					= "%s приобрел ${count} ед. товара \"${name}\""
trans.actions.soldXofY 						= "%s продал ${count} ед. товара \"${name}\""

trans.log.boughtMarketedItem				= "%s приобрел товар: %s за %s у %s"
trans.log.boughtMarketedItem.unknownItem 	= "Неизвестный товар"
trans.log.boughtMarketedItem.unknownPlayer 	= "неизвестный игрок"
trans.log.receivedXFromYforZ				= "%s получил %s от %s за %s"
trans.log.paidXforY							= "%s заплатил %s за %s"
trans.log.receivedFundsFromServer 			= "%s получил доход от сервера %s за %s"
trans.log.purchased							= "%s приобрел %s и заплатил %s игроку %s"
trans.log.purchasedNPC 						= "%s приобрел %s и заплатил %s в магазин НПС"

-- Typing module
-- @NOTE: This is definitely not compatible with client-set language. One of the reasons it should be based on server instance.
typing.yellingEl							= "Кричит..."
typing.whisperingEl							= "Шепчет..."
typing.radioingEl 							= "Передает по радио..."
typing.talkingEl 							= "Говорит..."
typing.typingEl 							= "Печатает..."
typing.inInventoryEl 						= "В инвентаре..."
typing.performingEl 						= "Выполняет..."
typing.equippingWeaponEl 					= "Достает оружие..."
typing.unequippingWeaponEl 					= "Убирает оружие..."

-- Unconscious module
unconscious.gui.warning 					= "Если ты покинешь сервер сейчас, то попадешь в бан!"
unconscious.gui.currentState				= "Текущее состояние:"

unconscious.states.critical 				= "В критическом состоянии"
unconscious.states.deceased 				= "Умирающий"
unconscious.states.stabilized 				= "Стабилизированный"
unconscious.states.disoriented 				= "Дезориентированный"
unconscious.states.tazed 					= "Обездвиженный"

unconscious.actions.dying 					= "Умирает"
unconscious.actions.standsUp 				= "%s встает после пробуждения."
unconscious.actions.criticalCondition 		= "Критическое состояние"
unconscious.actions.picksSelfUp 			= "%s поднимается с земли."

unconscious.ban.reason 						= "Отключение во время РП-ситуации (Автоматический бан) | Оспорить @ mono.octo.gg"

unconscious.log.changedState 				= "%s сменил состояние с %s на %s"
unconscious.log.enteredState 				= "%s вошел в состояние %s"
unconscious.log.finishedOff 				= "%s вырубил %s, тем самым убив."

-- Vehicle Module
vehicle.notif.deployingNearby 				= "Внимание! Машина расположилась неподалеку."
vehicle.notif.thisLocked 					= "Эта машина заперта."
vehicle.notif.deployCooldown 				= "Подождите ${time} перед тем, как пригнать машину."
vehicle.notif.disabledMustSell 				= "Эта машина не активна. Вам нужно продать её. Извините!"
vehicle.notif.allDisabled 					= "Машины временно не активны, извините!"
vehicle.notif.blacklistedDeploying 			= "Вам запрещено размещать машины."
vehicle.notif.cannotPersonalOnDuty 			= "Вам нельзя размещать личный транспорт будучи на работе."
vehicle.notif.storedBeforeDeployed 			= "Вам необходимо загнать предыдущую машину."
vehicle.notif.cannotDeployWhilePending 		= "Вы не можете разместить машину с активной заявкой."
vehicle.notif.cannotDeployUnderArrest 		= "Вы не можете разместить машину будучи под арестом."
vehicle.notif.outOfFuel 					= "В этой машине нет топлива."
vehicle.notif.switchSeatsHandcuffed 		= "Вы не можете пересесть будучи связанным."
vehicle.notif.pleaseWaitBeforeExiting 		= "Подождите ${time} перед выходом из машины."
vehicle.notif.cantExitHandcuffed 			= "Вы не можете покинуть машину будучи связанным."
vehicle.notif.orderReceivedDeploying 		= "Заказ получен! Размещаем вашу машину."
vehicle.notif.enteringCooldown 				= "Подождите ${time} перед посадкой в машину."
vehicle.notif.enterProned 					= "Вы не можете сесть в машину сидя."
vehicle.notif.tooFarAway 					= "Слишком далеко"
vehicle.notif.youreDead 					= "Вы мертвы"
vehicle.notif.youreNotPolice 				= "Вы не из полиции"
vehicle.notif.playerNotHandcuffed 			= "Игрок не связан"
vehicle.notif.noDriver 						= "Нет водителя"
vehicle.notif.alreadyInVehicle 				= "Игрок уже в машине."
vehicle.notif.cannotEnterWhileTaunt 		= "Вы не можете сесть в машину используя анимацию."
vehicle.notif.vehicleIsBeingUnlocked 		= "Машина открывается."
vehicle.notif.vehicleLocked 				= "Машина заперта."
vehicle.notif.seatBelt 						= "Нажмите J чтобы надеть ремень безопасности."
vehicle.notif.turnOnEngine 					= "Нажмите H чтобы завести двигатель."
vehicle.notif.paidForRepairing 				= "Вы заплатили %s за починку машины."
vehicle.notif.lockedUnlockednside			= "Вы %s машину изнутри"
vehicle.notif.lockedUnlockedInside.locked 	= "заблокировали"
vehicle.notif.lockedUnlockedInside.unlocked = "разблокировали"
vehicle.notif.haveToBeDriver 				= "Вам нужно быть водителем для этого."
vehicle.notif.noKey 						= "У вас нет ключей от этой машины."
vehicle.notif.noFuelCantDriven 				= "В вашей машине нет топлива, она не поедет."
vehicle.notif.destroyedCannotBeDriven 		= "Ваша машина уничтожена, она не поедет."
vehicle.notif.cantAffordDeploy 				= "Вы не можете позволить себе разместить машину."
vehicle.notif.error5						= "Невозможно заспавнить машину. ERROR_05"
vehicle.notif.repairBeforeStore 			= "Вы должны починить машину перед возвращением в гараж!"
vehicle.notif.putIntoGarage 				= "Ваша машина была успешно помещена в гараж!"
vehicle.notif.waitBeforeDeploying 			= "Подождите %s перед размещением машины."
vehicle.notif.contactADev 					= "Свяжитесь с разработчиком."
vehicle.notif.entryCantSpawn 				= "Извините, игроки с начальным уровнем профессии не могут размещать машины."
vehicle.notif.moveVehicleCloser 			= "Вам нужно подогнать машину ближе."
vehicle.notif.cantFindValidSpawnTable 		= "Невозможно найти место для спавна. Свяжитесь с разработчиком."
vehicle.notif.cantFindValidSpawn 			= "Нет места для спавна."
vehicle.notif.auctionedOffFor				= "Ваш %s был продан на аукционе за %s [%]!"
vehicle.notif.waitBeforeAuctioning			= "Подождите %s перед продажей на аукционе следующей машины."
vehicle.notif.cantAuctionWhileEmployed 		= "Вы не можете выставлять машину на аукцион будучи на работе!"
vehicle.notif.cantAuctionWhileOnSale 		= "Вы не можете выставлять машину на аукцион пока она на продаже!"
vehicle.notif.successfullyPurchased 		= "Вы успешно приобрели %s! Поздравляем!"
vehicle.notif.waitBeforePurchasing 			= "Подождите %s перед покупкой следующей машины."
vehicle.notif.cantBuyDisabled 				= "Вы не можете купить эту машину, так как она не активна."
vehicle.notif.cantBuy 						= "Вы не можете купить эту машину."
vehicle.notif.cantPurchaseEmployed 			= "Вы не можете купить машину будучи на работе!"
vehicle.notif.cantAfford 					= "Вы не можете позволить себе эту машину!"
vehicle.notif.hintStuff 					= "ЛКМ для спавна, CTRL для отмены."
vehicle.notif.unablePurchaseItem 			= "Невозможно приобрести предмет. Была обнаружена ошибка."
vehicle.notif.deployingCosts 				= "Размещение будет стоить %s"
vehicle.notif.vehiclesOnSale 				= "Распродажа у автодилера!"
vehicle.notif.vehicleTooFar 				= "Машина слишком далеко!"
vehicle.notif.atLeastSergeant 				= "Вы должны быть хотя бы Сержантом полиции для этого!"
vehicle.notif.mustBeEmpty 					= "Машина должна быть пуста перед установкой блокиратора колес!"
vehicle.notif.mustBeImmobile 				= "Машина должна стоять неподвижно перед установкой блокиратора!"
vehicle.notif.cantClampPolice 				= "Вы не можете установить блокиратор на полицейскую машину!"
vehicle.notif.clampEnter 					= "Вы не можете сесть в машину, пока на ней блокиратор колес!"

vehicle.bind.engine							= "Двигатель Вкл/Выкл"
vehicle.bind.seatbelt 						= "Ремень Вкл/Выкл"

vehicle.prompt.sellCarForX 					= "Вы уверены что хотите продать вашу машину за %s?"
vehicle.prompt.vehicleAuction 				= "Авто-аукцион"

vehicle.gui.callMechanic 					= "Вызвать механика"
vehicle.gui.noVehicles 						= "У вас нет машин"
vehicle.gui.store 							= "Гараж" -- As in, store an item
vehicle.gui.deploy 							= "Пригнать"
vehicle.gui.vehicleDeployment 				= "РАЗМЕЩЕНИЕ АВТО"
vehicle.gui.jobValet 						= "%s"
vehicle.gui.customizeIt						= "Выберите машину"
vehicle.gui.alreadyHaveJobVehicle 			= "У вас уже есть служебная машина. Верните её перед тем, как пригнать другую."
vehicle.gui.deploy 							= "ПРИГНАТЬ"
vehicle.gui.customizeVehicle 				= "Кастомизировать авто (НЕ РЕАЛИЗОВАНО)"
vehicle.gui.ret 							= "ВЕРНУТЬСЯ"
vehicle.gui.premiumVehicles  				= "ПРЕМИУМ АВТО"
vehicle.gui.vehicles 						= "АВТОМОБИЛИ"
vehicle.gui.purchase 						= "Приобрести"
vehicle.gui.paintCar 						= "Покрасить авто"
vehicle.gui.purchaseWrap 					= "Приобрести тюнинг"
vehicle.gui.none 							= "НЕТ"
vehicle.gui.skin							= "Скин #%s"
vehicle.gui.dealershipTitle 				= "АВТОЦЕНТР КОБА"
vehicle.gui.carOptions	 					= "НАСТРОЙКИ АВТО"
vehicle.gui.originalPrice 					= "ЦЕНА: %s"
vehicle.gui.premiumIndicator 				= "Премиум ★"
vehicle.gui.owned 							= "Куплено"
vehicle.gui.vehicleColor 					= "Цвет авто"
vehicle.gui.vehicleWrap 					= "Тюнинг авто"
vehicle.gui.spawn 							= "Спавн"
vehicle.gui.carDealer 						= "Авто-дилер"
vehicle.gui.carsOnSale						= "4 на продаже в данный момент."
vehicle.gui.searchCategory 					= "Выберите категорию, чтобы найти машину своей мечты."

vehicle.cats.super 							= "Суперкар"
vehicle.cats.sports 						= "Спорткар"
vehicle.cats.suv 							= "Джип"
vehicle.cats.classic 						= "Классика"
vehicle.cats.misc 							= "Прочее" -- Miscellaneous

vehicle.food.text64							= "Ваш текст должен быть менее 64 символов."
vehicle.food.text3 							= "Ваш текст должен быть более 3 символов."
vehicle.food.textProfanity 					= "Ваш текст не должен содержать нецензурной лексики."
vehicle.food.wait							= "Подождите %s."
vehicle.food.title.prompt 					= "Напишите сообщение, которое будет написано над вашим грузовичком.(максимум 64 символа)"
vehicle.food.title.title 					= "Разместите заголовок"
vehicle.food.title.bite 					= "Байт-Доставка"
vehicle.food.gui.gps						= "Нажмите [G] чтобы установить GPS на поставщика еды."
vehicle.food.gui.earnings 					= "Заработок: %s"
vehicle.food.gui.stock 						= "Склад"
vehicle.food.gui.profit 					= "Весь заработок: %s"
vehicle.food.gui.currentSession 			= "Текущая сессия: %s"
vehicle.food.gui.levelMax 					= "Уровень: MAX"
vehicle.food.gui.level 						= "Уровень: %s (%s/%s)"

vehicle.garage.disabled_warning 			= "Это временно отключено."
vehicle.garage.repaired_vehicle 			= "Вы починили вашу машину за %d%%."
vehicle.garage.deploying_repair_required 	= "Почините вашу машину перед тем как её разместить."
vehicle.garage.in_combat 					= "Вы в бою."
vehicle.garage.already_inside 				= "Вы уже в гараже."
vehicle.garage.player_entered 				= "%s вошел в ваш гараж."
vehicle.garage.inviting_no_access 			= "У вас нет прав приглашать других игроков в гараж игрока %s."
vehicle.garage.target_too_far 				= "Этот игрок недостаточно близко к гаражу, в котором вы находитесь."
vehicle.garage.invitation_sent 				= "Вы отправили приглашение игроку %s."
vehicle.garage.forced_out 					= "Вас выгнали из гаража %s."
vehicle.garage.vehDoesntExist 				= "Этой машины нет во владении игрока."
vehicle.garage.spawnPointNotFound 			= "Точка спавна машины не найдена."
vehicle.garage.invitedGarage 				= "Вы были приглашены в гараж ${name}"
vehicle.garage.title 						= "ГАРАЖ"
vehicle.garage.enterGarage 					= "Войти в гараж"
vehicle.garage.exitGarage 					= "Выйти из гаража"
vehicle.garage.clickToEnter 				= "Нажмите на гараж, чтобы войти."
vehicle.garage.clickToSell 					= "Нажмите на машину, чтобы продать её"
vehicle.garage.number 						= "Гараж %s"
vehicle.garage.vehicleMenu 					= "Меню машин"
vehicle.garage.impounded 					= "(Конфискована) %s"
vehicle.garage.dismiss 						= "ОТКАЗАНО ✕"
vehicle.garage.deployVehicle 				= "ПРИГНАТЬ МАШИНУ"
vehicle.garage.sellVehicle 					= "ПРОДАТЬ МАШИНУ: %s"
vehicle.garage.promptSell 					= "Вы уверены что хотите продать %s за %s?"
vehicle.garage.promptSell.title 			= "Аукцион (%s)"
vehicle.garage.repairVehicle				= "ПОЧИНИТЬ МАШИНУ (20%%): %s"
vehicle.garage.aboutVehicle 				= "Об этой машине"
vehicle.garage.noDescription 				= "Нет описания"
vehicle.garage.unknown						= "Неизвестно"
vehicle.garage.sitRep 						= "Сведения о машине"
vehicle.garage.fuelAmount					= "Запас топлива: %s%%"
vehicle.garage.health 						= "Состояние автомобиля: %s%%"
vehicle.garage.damagedParts 				= "Поврежденные части"
vehicle.garage.none 						= "Нет"
vehicle.garage.invite 						= "Пригласить"
vehicle.garage.selectATarget 				= "Выберите цель"
vehicle.garage.employment_restriction 		= "Вам нужно покинуть работу прежде чем зайти в гараж."

vehicle.testDrive.licensePlate 				= "ТЕСТ-ДРАйВ"
vehicle.testDrive.timeToTestDrive 			= "У вас есть %s секунд для тест-драйва этой машины."
vehicle.testDrive.bringYouBack 				= "Время! Давайте-ка вернем вас обратно..."
vehicle.testDrive.featureDisabled 			= "Эта возможность сейчас отключена."
vehicle.testDrive.featureUnavailable		= "Произошла ошибка. Свяжитесь с разработчиком!"
vehicle.testDrive.cooldown 					= "Вы можете проводить тест-драйв лишь раз в %s минут."
vehicle.testDrive.warranted 				= "Вы не можете проводить тест-драйв будучи в розыске."
vehicle.testDrive.cantTestDrive 			= "Вы не можете проводить тест-драйв этой машины."
vehicle.testDrive.employed 					= "Вы не можете проводить тест-драйв будучи на работе."
vehicle.testDrive.minWorth 					= "У вас должна быть по крайней мере половина от стоимости машины для её тест-драйва."
vehicle.testDrive.license 					= "Вам нужны водительские права для тест-драйва!"
vehicle.testDrive.validLicense 				= "Вам нужны действительные права для тест-драйва!"
vehicle.testDrive.carDealerButton 			= "Тест-драйв"

vehicle.actions.turnedOnOffCar 				= "%s %s машину."
vehicle.actions.turnedOnOffCar.on 			= "завёл"
vehicle.actions.turnedOnOffCar.off 			= "заглушил"
vehicle.actions.startedHotwiring 			= "%s замыкает провода в машине."
vehicle.actions.succeedHotwire 				= "%s удачно замкнул провода в машине."
vehicle.actions.failedHotwire 				= "%s не смог замкнуть провода в машине."
vehicle.actions.hotwiringCar 				= "ЗАМЫКАНИЕ ПРОВОДОВ"
vehicle.actions.seatbelt 					= "%s %s ремень безопасности."
vehicle.actions.seatbelt.putOn				= "надел"
vehicle.actions.seatbelt.tookOff 			= "снял"

vehicle.section.vehicles 					= "Машины"

vehicle.actions.forceIntoPoliceCar			= "%s открыл дверь, заталкивая %s в полицейскую машину."
vehicle.actions.puttingOnAWheelClamp		= "УСТАНАВЛИВАЕТ БЛОКИРАТОР КОЛЕС"
vehicle.actions.takingOffWheelClamp 		= "СНИМАЕТ БЛОКИРАТОР КОЛЕС"

vehicle.inventory.trunk 					= "Багажник"

vehicle.log.storedVehicle 					= "%s загнал машину %s"
vehicle.log.auctioned 						= "%s продал на аукционе машину %s за %s"
vehicle.log.purchasedFor 					= "%s приобрел машину %s за %s"
vehicle.log.wheelClamp 						= "%s установил блокиратор колес %s %s."
vehicle.log.wheelClampOff 					= "%s снял блокиратор колес %s %s."
vehicle.log.wheelClampLockpicked 			= "%s взломал блокиратор колес %s %s"
-- @TODO: sh_foodtruck.lua tDefaultFood and such
-- @TODO: Check out line 960 sv_vehicle.lua

vehicle.taxi.notif.fareStarted 				= "Ваша работа началась. Тариф: %s."
vehicle.taxi.notif.clientCancelled			= "Клиент отменил вызов."
vehicle.taxi.notif.couldntPay 				= "%s не смог заплатить полную цену поездки. Вы заработали %s."
vehicle.taxi.notif.youCouldntPay			= "Вы не смогли оплатить поездку.  %s."
vehicle.taxi.notif.youReceived				= "Вы выручили %s за эту поездку."
vehicle.taxi.notif.costYou 					= "Вызов такси обошелся вам в %s."
vehicle.taxi.notif.pleaseWait				= "Подождите %s перед вызовом такси."
vehicle.taxi.notif.mustHave					= "У вас должно быть по-крайней мере %s."
vehicle.taxi.notif.calledTaxi				= "%s вызвал такси. Скорее подбери пассажира!"
vehicle.taxi.notif.taxiPrefix				= "[ТАКСИ] "

vehicle.taxi.chat.taxiDriverOnWay			= "Наш водитель такси, %s, уже на пути к вам!"

vehicle.taxi.gui.costColon 					= "Стоимость:"
vehicle.taxi.gui.driverColon				= "Водитель:"

vehicle.foodtruck.gui.foodTruckTitle 		= "Здесь твоя еда."

-- Widget module
widget.app.licenses 						= "Лицензии"
widget.app.laws								= "Законы"
widget.app.skills 							= "Навыки"
widget.app.monoTablet 						= "Планшет"
widget.app.monoMap 							= "Моно-Map"
widget.app.taxi 							= "Такси"
widget.app.iMessage 						= "Сообщения"
widget.app.trade 							= "Сделка"

widget.gui.moveToTablet 					= "Move to tablet"

-- Gas Mask TODO: Needs Translation
gas.notif.filter_empty						= "Фильтр не установлен или израсходован"
