--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

--[[ @NOTE: Addon language configurations locations for the following addons are diplayed:
		- CasinoKit: casinokit/lua/casinokit/custom/langs
			- Also for each sub addon
		- DHeists: dheists_/lua/dheists/config/language
		- gsigns: gsigns/lua/metasign_config.lua
		- RRL: rrl/lua/rrl/config
]]

-- @TODO:
		-- firetruck-ladder
		-- blue's casino stuff
		-- fishingmod
		-- raptor-ui
		-- [any SWEP PrintNames]
		-- [any AddNotification]
		-- rrl
		-- v-fire
		-- Zero's arcade pack
		-- Check GetJobCategory again.

Monolith.LocaleBuilder( "ru", true )

-- CasinoKit
addon.casino.notif.allowedMax5					= "Разрешено делать не больше 5 ставок на одном столе."

-- Tow Truck Driver
addon.towtruck.notif.noVehicle					= "За эвакуатором нет транспортного средства!"
addon.towtruck.notif.attached 					= "Транспортное средство успешно прикреплено!"
addon.towtruck.notif.unattached					= "Транспортное средство успешно откреплено!"
addon.towtruck.notif.lookTowTruck				= "Тебе нужно смотреть на эвакуатор, чтобы прикрепить близлежащее транспортное средство!"

-- Firetruck Ladder
addon.ftladder.notif.needFirefighter		= "Нужно быть пожарным, чтобы сделать это!"

-- Fishingmod
addon.fishing.notif.hookedCaught				= "You hooked a fish! It's now in your inventory."
addon.fishing.notif.hookedAway 					= "You hooked a fish, but it got away."
addon.fishing.notif.noSpace						= "You do not have space in your inventory!"
addon.fishing.notif.noCatch						= "You didnt catch anything."
addon.fishing.notif.noBait						= "You have no more bait."
addon.fishing.notif.pleaseWait					= "Please wait %s to cast again."
addon.fishing.notif.rmbReel						= "[RMB] REEL IN"


-- GSigns
addon.gsigns.notif.err 							= "Произошла ошибка. Пожалуйста, проверьте консоль!"

addon.gsigns.action.pickingUp 					= "ПОДНЯТИЕ"

-- RRL
addon.rrl.notif.removedWarning 					= "Вы сняли предупреждение с %s"
addon.rrl.notif.warnedBy						= "Вы получили предупреждение от %s за %s, у вас теперь %s предупреждений."
addon.rrl.notif.willKicked						= "Вы будете кикнуты с сервера, если вы получите еще %s предупреждений"
addon.rrl.notif.willKicked.any 					= "any" -- Контекст
addon.rrl.notif.youWarned 						= "Вы выдали предупреждение %s за %s."
addon.rrl.notif.reportDoesntExist 				= "Жалоба, которую вы пытаетесь открыть, не существует."
addon.rrl.notif.noAccess						= "У вас нет доступа к данному свойству"
addon.rrl.notif.removedReportTicket 			= "Вы сняли жалобу с (#%s)"
addon.rrl.notif.supportTicketRemoved			= "Ваше сообщение в поддержку было снято (#%s)"
addon.rrl.notif.cantCloseOwnReport				= "Вы не можете закрыть свою собственную жалобу."
addon.rrl.notif.reportTicketClosed 				= "Ваша жалоба была закрыта (#%s)"
addon.rrl.notif.cannotOpenOwnReport				= "Вы не можете открыть свою собственную жалобу."
addon.rrl.notif.yourReportTicketOpened 			= "Ваша жалоба была открыта (#%s)"
addon.rrl.notif.alreadyClaimed 					= "Вы не можете взять на рассмотрение данную жалобу, так как она уже рассматривается"
addon.rrl.notif.cannotClaimOwn 					= "Вы не можете рассмотреть свою собственную жалобу."
addon.rrl.notif.staffMemberClaimed 				= "Член администрации взял вашу жалобу на рассмотрение (#%s)"
addon.rrl.notif.claimedReportTicket				= "Вы взяли жалобу на рассмотрение (#%s)"
addon.rrl.notif.staffMemberUnclaimed			= "Член администрации больше не рассматривает вашу жалобу (#%s)"
addon.rrl.notif.youUnclaimed					= "Вы больше не рассматриваете жалобу (#%s)"
addon.rrl.notif.submittedNewReport				= "Вы подали новую жалобу (#%s)"

-- Whomekit_media
addon.whomekit_media.notif.needConnect			= "Вам нужно подключить прибор к розетке."

addon.whomekit_media.log.startedPlaying 		= "%s начал проигрывать: %s по медиа-источнику. (%s)"

-- zcrga
addon.zcrga.notif.cantAfford					= "Вы не можете это себе позволить!"
addon.zcrga.notif.youWonX						= "Вы выиграли %s!"
