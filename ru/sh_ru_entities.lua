--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

Monolith.LocaleBuilder( "ru", true )

--[[
	SWEPS
--]]

-- Hands
sweps.handcuffsinhand.printName 			= "Наручники"

sweps.handcuffsinhand.actions.beginCuffing 	= "%s начинает надевать наручники на руки %s."
sweps.handcuffsinhand.actions.placedCuffs	= "%s надел наручники на руки %s."
sweps.handcuffsinhand.actions.handCuffing	= "НАДЕВАЕМ НАРУЧНИКИ НА \n%s"
sweps.handcuffsinhand.actions.removeCuffs	= "%s снимает наручники с рук %s."

sweps.handcuffsinhand.gui.checkOwner 		= "Нажмите R чтобы узнать влдельца"
sweps.handcuffsinhand.gui.noOwner			= "Нет владельца"

sweps.handcuffsinhand.log.cuffed			= "%s был закован в наручники %s"
sweps.handcuffsinhand.log.unCuffed			= "%s был раскован %s"

-- Restrained

sweps.handcuffsonplayer.printName 			= "Задержаный"

-- Bong
sweps.sentBong.gui.weed 					= "Марихуана"
sweps.sentBong.gui.quality					= "Качество"

-- Zipties
sweps.zipties.printName 					= "Верёвка"

sweps.zipties.actions.beginZip 				= "%s начал связывать верёвкой %s."
sweps.zipties.actions.finishZip 			= "%s закончил связывать %s."
sweps.zipties.actions.zipTie 				= "СВЯЗЫВАНИЕ %s"

sweps.zipties.log.ziptied					= " СВЯЗАН "

-- Axe
sweps.axe.printName 						= "Топор"

-- Defib
sweps.defib.actions.stabalizing				= "СТАБИЛИЗАЦИЯ ${name}"

sweps.defib.gui.dead 						= "Пациент не подаёт признаков жизни."
sweps.defib.gui.stable						= "Он уже стабилизирован!"
sweps.defib.gui.critical					= "Пациент должен быть в критическом состоянии!"
sweps.defib.gui.downtime					= "Вы прибыли сюда достаточно быстро! Поэтому у вас больше шансов на выживание пациента!"
sweps.defib.gui.hospitalwarning				= "Похоже он живой, доставьте его в больницу!"

sweps.defib.logger.revived 					= " воскрешен: "

-- Axe
sweps.fireaxe.gui.assistance 				= "Ваша помощь здесь не требуется!"

-- Keys
sweps.keys.printName 						= "Ключи"

sweps.keys.gui.state						= "Вы хотите ${state} доступ к вашим машинам/дверям?"
sweps.keys.gui.stateRevoke					= "ограничить"
sweps.keys.gui.stateGrant					= "предоставить"
sweps.keys.gui.access						= "Доступ"
sweps.keys.gui.thisPerson					= "этот игрок"
sweps.keys.gui.yes							= "Да"
sweps.keys.gui.no							= "Нет"
sweps.keys.gui.revokeOwner					= "Вы убрали владельца "
sweps.keys.gui.grantOwner					= "Вы добавили владельца "

--weapon_lockpick
sweps.lockpick.printName         = "Отмычка"

--weapon_lockpick
sweps.extinguisher.printName 		= "Огнетушитель"

--weapon_mono_citizen_id
sweps.citizenid.printName        = "ID карта"

--weapon_mono_department
sweps.departmentTablet.printName = "Полицейский планшет"
sweps.departmentTablet.desc      = "Используй это, чтобы кайфануть" -- ? ...

--weapon_fireaxe
sweps.fireaxe.printName 		= "Пожарный топор"

-- weapon_defibrillator
sweps.defibrillator.printName 	= "Дефибриллятор"

--weapon_mono_fists
sweps.fists.printName            = "Руки"
sweps.fists.instructions         = "ЛКМ: удар; ПКМ: постучать/подобрать"
sweps.fists.purpose              = "Бить и стучать в двери"

-- Medkit
sweps.medkit.printName 						= "Аптечка Перваой помощи"

sweps.medkit.gui.healing					= "ЛЕЧЕНИЕ"
sweps.medkit.gui.sterilizing 				= "ОБРАБОТКА РАН"
sweps.medkit.gui.cauterizing 				= "ПРИЖИГАНИЕ РАН"
sweps.medkit.gui.applyingBandages 			= "НАНЕСЕНИЕ БИНТА"
sweps.medkit.gui.healingMe 					= "САМОЛЕЧЕНИЕ"

--weapon_mono_police_id
sweps.policeid.printName   = "Жетон"

--weapon_mono_tablet
sweps.monotablet.printName = "Планшет"

--weapon_pickaxe
sweps.pickaxe.printName    = "Кирка"
sweps.pickaxe.instructions = "ЛКМ: взмах; ПКМ: удар"

--weapon_radargun
sweps.radargun.printName   = "Радар"
sweps.radargun.purpose     = "Проверить скорость"

--weapon_ramdoor
sweps.ramdoor.printName = "Таран"
sweps.ramdoor.purpose = "Помощь"

--weapon_taser
sweps.taser.printName = "Тазер"

-- weapon_mono_vape
-- @TODO configure the flavor juices in the weapon_mono_vape.lua directly in the JuicyVapeJuices table
sweps.monovape.printName 					= "Вэйп"
sweps.monovape.gui.loadedFlavorJuice 		= "Заправил %s вкус в вэйп."

sweps.monovape.juices.dew				= "Mountain Dew"
sweps.monovape.juices.cheetos				= "Cheetos"
sweps.monovape.juices.razzleberry				= "Ягодный"
sweps.monovape.juices.banana				= "Банановый"
sweps.monovape.juices.blacklicorice				= "Лакричный"
sweps.monovape.juices.churro				= "Чуррос"
sweps.monovape.juices.skittles				= "Skittles"
sweps.monovape.juices.normal				= "Обычный"

-- weapon_nightstick
sweps.nightstick.printName 					= "Дубинка"
sweps.nightstick.log.knockOut 				= "%s вырублен %s с помощью полицейской дубинки."

-- weapon_policetape
sweps.policetape.printName 					= "Лента"

sweps.policetape.gui.distance				= "Дистанция"
sweps.policetape.gui.rightClickCancel		= "ПКМ для отмены."
sweps.policetape.gui.ropeCantBeThatLong 	= "Ваша лента не настолько длинная!"
sweps.policetape.gui.pickedUpSegmentTape 	= "Вы подобрали кусок ленты."
sweps.policetape.gui.clearedAllTapes 		= "Вы убрали всю свою ленту."
sweps.policetape.gui.maxTapes 				= "Вы достигли лимита лент!" -- @TODO money symbol line 139
sweps.policetape.log.hasPlacedRope 			= "%s разместил ленту на %s"
sweps.policetape.log.removedPoliceTape 		= "%s убрал ленту с %s"
sweps.policetape.log.clearedAllTapes		= "%s убрал всю полицейскую ленту."

-- weapon_radargun
-- @TODO maybe we should add the speed unit in this file

-- weapon_ticket
sweps.weaponticket.printName 					= "Штрафы"
sweps.weaponticket.purpose 						= "Используйте это, чтобы выписывать штрафы."

sweps.weaponticket.gui.cantGiveTicketToPlayer = "Вы не можете выписать штраф этому игроку в течении %s секунд."
sweps.weaponticket.gui.cantGiveTicket		  = "Вы не можете выписать штраф в течении %s секунд."
sweps.weaponticket.gui.officierIssuedIt 	  = "Офицер, выдавший его"
sweps.weaponticket.gui.timeSinceIssued		  = "Время с момента выпуска"
sweps.weaponticket.gui.lawyer 				  = "Адвокат"
sweps.weaponticket.gui.selectTicket 		  = "Выберите штраф"
sweps.weaponticket.gui.police 				  = "Полиция"
sweps.weaponticket.gui.minutesAgo 			  = "%s минут(а) назад"
sweps.weaponticket.gui.resolve				  = "Решить"
sweps.weaponticket.gui.ticket				  = "Штраф"
sweps.weaponticket.gui.payTicket			  = "Заплатите за штраф ($%s)"
sweps.weaponticket.gui.pay					  = "Заплатить"
sweps.weaponticket.gui.deny					  = "Отклонить"
sweps.weaponticket.gui.appealTicketIfUnfair   = "Если вы не согласны с причиной штрафа, вы можете его оспорить."
sweps.weaponticket.gui.smallViolation 		  = "Первое/незначительное нарушение"
sweps.weaponticket.gui.repeated 			  = "Повторное/умеренное"
sweps.weaponticket.gui.excessive			  = "Постоянное/тяжкое"
sweps.weaponticket.gui.appeal 				  = "Аппеляция"
sweps.weaponticket.gui.amountToPay 			  = "Сумма к оплате" -- @TODO money symbol line 246
sweps.weaponticket.gui.ticketsFromHim 		  = "Штраф от "
sweps.weaponticket.gui.insertText 			  = "<Впишите текст>"
sweps.weaponticket.gui.beMoreDescriptive 	  = "Напишите больше о причине."
sweps.weaponticket.gui.insertNumber 		  = "Вы должны написать число!"
sweps.weaponticket.gui.ok 					  = "ОК"
sweps.weaponticket.gui.cantFineMore 		  = "Вы не можете оштрафовать больше, чем на $4,500!" -- @TODO maybe adding this to a configuration or something like that line 313 and 227
sweps.weaponticket.gui.cantFineMoreThan		  = "Вы не можете штрафовывать больше, чем на %s!"
sweps.weaponticket.gui.areYouSure 			  = "Вы уверены, что хотите выписать штраф?"
sweps.weaponticket.gui.cancel 				  = "Отклонить"
sweps.weaponticket.gui.accept 				  = "Принять"
sweps.weaponticket.gui.id 					  = "ID"
sweps.weaponticket.gui.kind 				  = "Kind" -- ??
sweps.weaponticket.gui.targetName			  = "Name" -- TODO

-- weapon_trowel
sweps.weapontrowel.printName 				  = "Садовая лопатка"
sweps.weapontrowel.gui.cantPlantHere 		  = "Вы не можете выращивать здесь."
sweps.weapontrowel.gui.tooClose 			  = "Вы пытаетесь посадить очень близко к чему-то!"

-- weapon_watercan
sweps.watercan.printName = "Лейка"
sweps.watercan.purpose = "Используется для полива."


--[[
	ENTITIES
--]]

-- areacreator
tools.areacreator.name = "Area Creator"
tools.areacreator.notDefined = "Not Defined"

-- nodecreator
tools.nodecreator.name = "Node Creator"
tools.nodecreator.instructions = "LMB: Create node - RMB: Set Path"
tools.nodecreator.selectNode = "Select another node"

-- permaprops
tools.permaprops.name = "PermaProps"
tools.permaprops.description = "Save a props permanently"
tools.permaprops.instructions = "LeftClick: Add RightClick: Remove Reload: Update"
tools.permaprops.adminCan = "Admin can touch permaprops"
tools.permaprops.adminCant = "Admin can't touch permaprops !"
tools.permaprops.superAdminCan = "SuperAdmin can touch PermaProps"
tools.permaprops.superAdminCant = "SuperAdmin can't touch permaprops !"
tools.permaprops.onlySA = "Only Super Admin can touch permaprops"
tools.permaprops.inValidEnt = "That is not a valid entity !"
tools.permaprops.isPlayer = "That is a player !"
tools.permaprops.already = "That entity is already permanent !"
tools.permaprops.saved = "You saved %s with model %s to the database."
tools.permaprops.erased = "You erased %s with a model of %s from the database."
tools.permaprops.reload = "You have reload all PermaProps !"
tools.permaprops.updated = "You updated the %s you selected in the database."
tools.permaprops.saveProps = "Save a props for server restarts\nBy Malboro"
tools.permaprops.header = "------ Configuration ------"
tools.permaprops.header2 = "-------- Functions --------"
tools.permaprops.removeAll = "Remove all PermaProps"
tools.permaprops.erasedAll = "You erased all props from the map"

-- propertycreator
tools.propertycreator.name = "Property Creator"
tools.propertycreator.desc = "Edit The Property Data"
tools.propertycreator.propertyName = "Name: "
tools.propertycreator.hammerIdentifier = "Hammer ID: "
tools.propertycreator.propertyUID = "UID: "
tools.propertycreator.propertyCategory = "Category: "
tools.propertycreator.propertyPrice = "Price: "
tools.propertycreator.propertyJobCat = "Job Category: "
tools.propertycreator.propertyElectricMode = "Electricity Node: "
tools.propertycreator.noDataGiven = "No data is given"
tools.propertycreator.noDataCamGiven = "No cam data is given"
tools.propertycreator.noDataBoundGiven = "No boundarie data is given"
tools.propertycreator.noDataSendGiven = "No send data is given"
tools.propertycreator.noDataIDGiven = "No id is given"
tools.propertycreator.noDataNameGiven = "No name is given"
tools.propertycreator.noDataPriceGiven = "No price is given"
tools.propertycreator.noDataCatGiven = "No category is given"
tools.propertycreator.noDataJCatGiven = "No jobCategory given on unownable property"
tools.propertycreator.savedCon = "Saved %s as existing configuration"
tools.propertycreator.failedSave = "Failed to save %s as existing configuration. ERROR: "
tools.propertycreator.noError = "no error"
tools.propertycreator.saved = "Saved "
tools.propertycreator.reload = "Reload: Open Property Editor"
tools.propertycreator.leftC = "Left-Click: "
tools.propertycreator.rightC = "Right-Click: "
tools.propertycreator.none = "None"

tools.propertycreator.gui.propertyEditor = "Property Creator"
tools.propertycreator.gui.editBound = "Edit Boundaries"
tools.propertycreator.gui.setMaxPos = "Set Max Pos"
tools.propertycreator.gui.setMinPos = "Set Min Pos"
tools.propertycreator.gui.editDoors = "Edit Doors"
tools.propertycreator.gui.noDoorFound = "No door found at hit location. Make sure you are aiming at the door"
tools.propertycreator.gui.addedDoor = "Added door to property"
tools.propertycreator.gui.unableToRmove = "Unable to remove door position from property. Door position not found in property door table"
tools.propertycreator.gui.addDoor = "Add Door"
tools.propertycreator.gui.removeDoor = "Remove Door"
tools.propertycreator.gui.editCam = "Edit Camera"
tools.propertycreator.gui.setCam = "Set Camera Position"
tools.propertycreator.gui.editPower = "Edit Power Outlets"
tools.propertycreator.gui.addOutlet = "Add Outlet"
tools.propertycreator.gui.removeOutlet = "Remove Outlet"
tools.propertycreator.gui.editBins = "Edit Trash Bins"
tools.propertycreator.gui.addBin = "Add Bin"
tools.propertycreator.gui.removeBin = "Remove Removebin"
tools.propertycreator.gui.editPrisoner = "Edit Prisoner Toilets"
tools.propertycreator.gui.addPrisoner = "Add Prisoner Toilet"
tools.propertycreator.gui.removePrisoner = "Remove Prisoner Toilet"
tools.propertycreator.gui.editTask = "Edit Prisoner Tasks"
tools.propertycreator.gui.addTask = "Add Prisoner Task"
tools.propertycreator.gui.removeTask = "Remove Prisoner Task"
tools.propertycreator.gui.saveProperty = "Save Property Changes"
tools.propertycreator.gui.deselectProperty = "Exit Property"
tools.propertycreator.gui.addChildren = "Add Children Houses"
tools.propertycreator.gui.setParentID = "Set The Parent ID"
tools.propertycreator.gui.putParentID = "Put the Parent ID here."
tools.propertycreator.gui.setChildrenCount = "Set The Children Count"
tools.propertycreator.gui.setChildrenCountBelow = "Set the children count of the properys below."
tools.propertycreator.gui.editParentID = "Edit Parent ID"
tools.propertycreator.gui.putNewParentID = "Put the new Parent ID here."
tools.propertycreator.gui.quickPropertyPicker = "Quick Property Picker"
tools.propertycreator.gui.automaticDoors = "Automatically Set Doors"

-- textscreen
tools.textscreen.name = "3D2D Textscreen"
tools.textscreen.desc = "Create a textscreen with multiple lines, font colours and sizes."
tools.textscreen.instructions = "Left Click: Spawn a textscreen Right Click: Update textscreen with settings"
tools.textscreen.undoneText = "Undone textscreen"
tools.textscreen.textScreens = "Textscreens"
tools.textscreen.cleanedUpText = "Cleaned up all textscreens"
tools.textscreen.hitLimit = "You've hit the textscreen limit!"

tools.textscreen.gui.reset = "Reset all"
tools.textscreen.gui.resetColors = "Reset colors"
tools.textscreen.gui.resetSizes = "Reset sizes"
tools.textscreen.gui.resetText = "Reset textboxes"
tools.textscreen.gui.resetEverything = "Reset everything"
tools.textscreen.gui.resetLine = "Reset line"
tools.textscreen.gui.resetAllLines = "Reset all lines"
tools.textscreen.gui.line = "Line "
tools.textscreen.gui.fontColor = " font color"
tools.textscreen.gui.fontSize = "Font size"

--[[
	ENTITIES
--]]

-- sent_workbench
entities.workbench.printName                = "Верстак"

-- sent_woodworkbench
entities.woodworkbench.printName            = "Деревообрабатывающий стол"

-- sent_textscreen
entities.textscreen.printName               = "Экран с текстом"
entities.textscreen.editTextTitle           = "Изменить текст"
entities.textscreen.editColors              = "Изменить цвета"
entities.textscreen.abort                   = "Откат"
entities.textscreen.setColor                = "Установить цвет"
entities.textscreen.colorChanged            = "Цвет экрана изменён"
entities.textscreen.edit                    = "Изменить"
entities.textscreen.textLengthError         = "Текст слишком короткий/длинный (<2 >18)"
entities.textscreen.pressEToChange          = "Нажмите E чтобы изменить"

-- sent_stretcher
entities.stretcher.printName                = "Носилки"
entities.stretcher.patientStabilise         = "Пациент сперва должен быть стабилизирован!"
entities.stretcher.ledByAnyone              = "Отпустите носилки чтобы сделать это!"
entities.stretcher.strappedOntop            = "%s положил тело %s на носилки"
entities.stretcher.unstrapped               = "%s снял тело %s с носилок"
entities.stretcher.sameVehicle              = "Вы должны положить носилки в ту же машину, из которой достали!"
entities.stretcher.backDoorClosed           = "Задние двери закрыты! Сперва откройте их."
entities.stretcher.notEMSPolice             = "Вы не можете сделать это, если вы не являетесь сотрудником СМП/полицейским!"
entities.stretcher.notYourVehicle           = "Это транспортное средство вам не принадлежит!"
entities.stretcher.notYourAmbulance         = "Это не ваш автомобиль СМП!"
entities.stretcher.blockingDeploy           = "Что-то блокирует место высадки!"

-- sent_road_sign
entities.roadsign.printName                 = "Полицейский дорожный знак"
entities.roadsign.policeDepartment          = "Полицейский Департамент"
entities.roadsign.mustBePolice              = "Вы должны служить в полиции, чтобы изменить этот знак!"
entities.roadsign.pickingUp                 = "ПОДНЯТИЕ"
entities.roadsign.policeSign                = "Полицейский знак"
entities.roadsign.update                    = "Обновление"

entities.roadsignsmall.log.changeText		= "%s changed a roadsign's text to: %s."

-- sent_road_sign_small
entities.roadsignsmall.printName            = "Маленький дорожный знак"
entities.roadsignsmall.roadClosed           = "ДОРОГА ЗАКРЫТА"
entities.roadsignsmall.memberPolice         = "Вы должны служить в полиции, чтобы изменить этот знак!"
entities.roadsignsmall.policeSign           = "Полицейский знак"
entities.roadsignsmall.update               = "Обновление"

-- sent_residential_extension_outlet
entities.extensionoutlet.printName          = "Расширение кол-ва розеток в помещении"
entities.extensionoutlet.errorAttempt       = "Attempt to use SetBreaker() on a sent_residential_extension_outlet"

-- sent_repair_bench
entities.repairbench.printName              = "Ремонтный верстак"

-- sent_property_bin
entities.propertybin.printName              = "Мусорное ведро"
entities.propertybin.garbageCan             = "Мусорное ведро"
entities.propertybin.percentFull            = "% заполнено"

-- sent_power_source
entities.powersource.printName              = "Источник питания"

-- sent_power_outlet
entities.poweroutlet.printName              = "Розетка"

-- sent_power_converter
entities.powerconverter.printName           = "Конвертер (двусторонний)"

-- sent_pot
entities.pot.printName                      = "Горшок"
entities.pot.weedBag                        = "Сумка для марихуаны"
entities.pot.useOtherPots                   = "Вы не можете использовать чужие горшки."
entities.pot.dehydrated                     = "Обезвоженный"
entities.pot.thirsty                        = "Жаждущий"
entities.pot.neutral                        = "Нормальный"
entities.pot.healthy                        = "Здоровый"
entities.pot.insertSoil                     = "Засыпьте почву"
entities.pot.insertSeed                     = "Посадите семена"
entities.pot.harvest                        = "Урожай"
entities.pot.properSeeds                    = "У вас нет подходящих семян."
entities.pot.gardeningSoil                  = "У вас нет почвы."
entities.pot.plantedSeed                    = "Вы посадили семя в горшок."
entities.pot.filledPot                      = "Вы наполнили горшок почвой."
entities.pot.gardeningPot                   = "Садовый горшок"
entities.pot.alreadySoil                    = "Горшок полон почвы"
entities.pot.soilAvailable                  = "У вас есть %s почвы"
entities.pot.seedsAvailable                 = "У вас есть %s семян %s, доступных для посадки."
entities.pot.plant                          = "Растение"
entities.pot.properSeeds                    = "У вас нет подходящих семян для посадки."
entities.pot.addSoil                        = "Добавьте почву"

-- sent_plug
entities.plug.printName                     = "Вилка" -- нужен доп. контекст

-- sent_money
entities.money.printName                    = "Деньги"
entities.money.youGrabbed                   = "Вы награбили $"
entities.money.pickedUp                     = " подобрано "

-- sent_metalworkbench
entities.metalworkbench.printName           = "Металлообрабатывающий стол"

-- sent_metalworkbench
entities.metalworkbench.printName           = "Металлообрабатывающий стол"

-- sent_lsd
entities.lsd.printName                      = "ЛСД"

-- sent_lsd_pyro
entities.lsdpyro.printName                  = "Горелка Бунзена"

-- sent_lsd_powder
entities.lsdpowder.printName                = "Порошок ЛСД" -- нужен доп. контекст

-- sent_lsd_paper
entities.lsdpaper.printName                 = "Лист бумаги" -- нужен доп. контекст

-- sent_lsd_gas
entities.lsdgas.printName                   = "Канистра с бензином"

-- sent_lsd_freezer
entities.lsdfreezer.printName               = "Холодильник"
entities.lsdfreezer.holdPickUp              = "(Удерживать) Поднять "
entities.lsdfreezer.insertFlask             = "Вставьте колбу"
entities.lsdfreezer.takeOut                 = "Я замерзаю! Вытащи меня"

-- sent_lsd_flask
entities.lsdflask.printName                 = "Колба"
entities.lsdflask.stage1                    = "Внесите порошок"
entities.lsdflask.stage2                    = "Мне нужно тепло"
entities.lsdflask.stage3                    = "Дождитесь завершения работы горелки"
entities.lsdflask.stage4                    = "Добавьте жидкость"
entities.lsdflask.stage5                    = "Осторожно потряси меня"
entities.lsdflask.stage6                    = "Охлади меня"
entities.lsdflask.stage7                    = "Положи меня на лист бумаги"

-- sent_lsd_flank_support
entities.lsdflanksupport.printName          = "Подставка для колб"
entities.lsdflanksupport.fireStarted        = "[Квест] Процесс нагрева начался!"
entities.lsdflanksupport.insertBurner       = "Вставьте горелку"
entities.lsdflanksupport.insertGas          = "Вставьте канистру с бензином"
entities.lsdflanksupport.insertFlask        = "Вставьте колбу"
entities.lsdflanksupport.progress           = "Прогресс: %"

-- sent_lsd_bottle
entities.lsdbottle.printName                = "LSD Bottle material"

-- sent_infinite_power_source
entities.infinitepower.printName            = "Бесконечный источник питания"
entities.infinitepower.powerSource          = "Источник питания"
entities.infinitepower.infinite             = "Бесконечный"

-- sent_fire_spark
entities.firespark.printName                = "Наркотик"

-- sent_fire_source
entities.firesource.printName               = "Огонь"

-- sent_factory_furnace
entities.factoryfurnace.printName           = "Фабричная печь"
entities.factoryfurnace.busy                = "Печь занята."
entities.factoryfurnace.takeAllComp         = "Вам нужно достать все компоненты, прежде чем поднять это."
entities.factoryfurnace.readyForUse         = "Готово к использованию"
entities.factoryfurnace.needsConveyor       = "Нужен конвейер"
entities.factoryfurnace.needsPower          = "Нужен источник питания"

-- sent_factory_crusher
entities.factorycrusher.printName           = "Дробилка"
entities.factorycrusher.busyCrushing        = "Дробилка занята переработкой предмета."
entities.factorycrusher.takeAllBefore       = "Вам нужно достать все предметы из контейнера, прежде чем поднять это."
entities.factorycrusher.preciousMats        = "Контейнер для хранения драгоценных материалов из дробилки."
entities.factorycrusher.turnOffNotif        = "Дробилка выключится после того как закончит."
entities.factorycrusher.crushing            = "Дробление "
entities.factorycrusher.insertChunks        = "Вставить куски"
entities.factorycrusher.unplugged           = "Обесточено" -- нужен контекст

-- sent_factory_conveyor
entities.factoryconveyor.printName          = "Конвеер"
entities.factoryconveyor.factoryBusy        = "Фабрика занята."
entities.factoryconveyor.turnMeOn           = "Включи меня"
entities.factoryconveyor.active             = "Активировать"
entities.factoryconveyor.secs               = " секунд"

-- sent_factory_cell
entities.factorycell.printName              = "Батарея"

-- sent_factory_box
entities.factorybox.printName               = "Хранилище"
entities.factorybox.containerHold           = "Контейнер для хранения заводских предметов из печи."
entities.factorybox.takeAllItems            = "Вы должны достать все предметы из ящика для хранения перед тем, как взять его."

-- sent_factory_base
entities.factorybase.printName              = "Factory Base"

-- sent_drug_bag
entities.drugbag.printName                  = "Сумка для марихуаной"

-- sent_door_charge
entities.doorcharge.printName               = "Дверной заряд"

-- sent_cooktable
entities.cooktable.printName                = "Кухонная плита"

-- sent_cocaine_stove
entities.cocainestove.printName             = "Электрическая плита"

-- sent_cocaine_pot
entities.cocainpot.printName                = "Кастрюля" -- @TODO: Change
entities.cocainpot.temperature              = "Температура: "
entities.cocainpot.waitAbout                = "Секунд до завершения: %d"

-- sent_cocaine_packingbox
entities.cocainepackingbox.printName        = "Упаковочная коробка"
entities.cocainepackingbox.cocaLeaves       = "Листьев коки: "
entities.cocainepackingbox.boxFull          = "Коробка заполнена"
entities.cocainepackingbox.packing          = "Упаковка"
entities.cocainepackingbox.notEnoughLeaves  = "У вас недостаточно листьев."
entities.cocainepackingbox.boxFullEmpty     = "Коробка заполнена, освободите в ней место."
entities.cocainepackingbox.noLeaves         = "У вас нет листьев."

-- sent_cocaine_kerosene
entities.cocainkerosene.printName           = "Керосин"
entities.cocainkerosene.shakeIt             = "Потрясите: "
entities.cocainkerosene.readyToUse          = "Готово к испольованию"

-- sent_cocaine_jerrycan
entities.cocainejerrycan.printName          = "Канистра"
entities.cocainejerrycan.ready              = "Готово"
entities.cocainejerrycan.pleaseWait         = "Секунд до завершения: %d"

-- sent_cocaine_gas
entities.cocainegas.printName               = "Бензин"
entities.cocainegas.filled                  = "Заполнено: "

-- sent_cocaine_drafted
entities.cocainedrafted.printName           = "Готовые листья"
entities.cocainedrafted.shakeIt             = "Потрясите: "
entities.cocainedrafted.ready               = "Готово к использованию"
entities.cocainedrafted.pleaseWait          = "Секунд до завершения: %d"

-- sent_cocaine_acid
entities.cocaineacid.printName              = "Серная кислота"
entities.cocaineacid.pleaseWait             = "Секунд до завершения: %d"
entities.cocaineacid.ready                  = "Готово к использованию"

-- sent_christmas_tree
entities.christmastree.printName            = "Рождественская ёлка"

-- sent_brewingbarrelbench
entities.brewingbarrelbench.printName       = "Пивоварная бочка"

-- sent_bountyboard
entities.bountyboard.printName              = "Доска наград"
entities.bountyboard.placeBounty            = "Установите награду"
entities.bountyboard.name                   = "Имя"
entities.bountyboard.reward                 = "Награда"
entities.bountyboard.dateAssigned           = "Назначенная дата"

-- sent_base_lsd
entities.baselsd.printName                  = "Наркотик"

-- base_item_stackable
entities.baseitemstackable.printName        = "Base item"
entities.baseitemstackable.beingArrested    = "Вы не можете подбирать предметы, когда арестованы."
entities.baseitemstackable.inventorySpace   = "У вас недостаточно места в инвентаре!"
entities.baseitemstackable.boughtItem       = "Вы преобрели товар. Вы заплатили: %s."
entities.baseitemstackable.shopFull         = "Этот магазин уже заполнен.!"
entities.baseitemstackable.dontHaveItem     = "У вас нет этого предмета в инвентаре!"
entities.baseitemstackable.onlyHadThis      = "У вас было только %s от этой вещи!"

--ent_atm_screen
entities.atmscreen.printName                = "ЭКРАН БАНКОМАТА"
entities.atmscreen.withdraw                 = "Снять"
entities.atmscreen.deposit                  = "Внести"
entities.atmscreen.monoBank                 = "МОНО-БАНК"
entities.atmscreen.balance                  = "Баланс: "
entities.atmscreen.accountHolder            = "Владелец счета: %s"
entities.atmscreen.fiftyUSD                 = "$50" -- idk what you expect me to use here
entities.atmscreen.fiveHundredUSD           = "$500"
entities.atmscreen.oneThousandUSD           = "$1000"
entities.atmscreen.tenThousandUSD           = "$10000"
entities.atmscreen.clear                    = "C"
entities.atmscreen.lessThan                 = "<"
entities.atmscreen.USD                      = "$"
entities.atmscreen.accessDenied             = "Доступ запрещен."
entities.atmscreen.insufficientFunds        = "У вас недостаточно средств."
entities.atmscreen.confirm                  = "ОК"

--ent_atm
entities.atm.printName                      = "Банкомат"

--ent_boat_base
entities.boatbase.printName                 = "Лодочная база"
entities.boatbase.containerDesc             = "Контейнер для хранения груза в трюме лодки."

--ent_boat_motor_engine
entities.boatmotorengine.printName          = "Лодочный мотор"

--ent_boat_rhib
entities.boatrhib.printName                 = "ЖНЛ"

--ent_boat_rowboat
entities.rowboat.printName                  = "Гребная лодка"


--ent_callsign_display
entities.callsigndisplay.printName          = "Табло позывных"
entities.callsigndisplay.yourCallsign       = "Ваш позывной %s"
entities.callsigndisplay.noCallsign         = "У вас нет позывного"
entities.callsigndisplay.deactivate         = "Детектив"

--ent_carspawnsign
entities.carspawnsign.warnning              = "— ВНИМАНИЕ —" --written 2 years ago by kyle goodale not me
entities.carspawnsign.seriousInjury         = "Чтобы избежать серьезных травм,"
entities.carspawnsign.remain10ft            = "Осталось 5 метров"
entities.carspawnsign.fromThisSign          = "от этого знака..."
entities.carspawnsign.ty                    = "Спасибо."

--ent_circle_trigger
entities.circletrigger.printName            = "Точка доставки"
entities.circletrigger.depotPrintName       = "Точка возврата"

--ent_detector_trigger
entities.detectortrigger.printName          = "Металлодетектор" -- ???

--ent_teleporter_trigger
entities.teleportertrigger.printName		= "Shooting Range Trigger"	-- TODO
entities.teleportertrigger.text				= "Validating ID Card"	   -- TODO
entities.teleportertrigger.notAllowed		= "You are not allowed to access the shooting range at this time."	-- TODO

--ent_fire_hydrant
entities.firehydrant.printName              = "Пожарный гидрант"

--ent_fire_pumphose
entities.pumphose.printName                 = "Шланговый насос"
entities.pumphose.detachingHose             = "ОТСОЕДИНЕНИЕ ШЛАНГА"

--ent_fire_waterhose
entities.waterhose.printName                = "Водяной шланг"
entities.waterhose.unequipFirst             = "Сначала распакуйте шланг!"

--ent_garage_tv
entities.garagetv.printName                 = "ТВ для гаража"

--ent_garage
entities.garage.printName                   = "Гараж"
entities.garage.modeDisabled                = "Этот режим сейчас отключен. Пожалуйста, используйте старую систему автомобилей нажатием F3." -- нужен доп. контекст

--ent_gascan
entities.gascan.printName                   = "Канистра бензина"
entities.gascan.pourThis                    = "Залей это в свою машину."

--ent_gaspump
entities.gaspump.printName                  = "Бензин"
entities.gaspump.poor                       = "Вы не можете себе этого позволить!"
entities.gaspump.needToWait                 = "Секунд, еще нужно подождать: %s."

--ent_house_alarm
entities.housealarm.printName               = "Карта" -- weir name for a house alarm

--ent_instanced_apt_circle
entities.instancedaptcircle.printName       = "Тестовый текст"

--ent_invisible_window
entities.invisibleWindow.printName          = "Невидимое окно"

--ent_itembox
entities.itembox.printName                  = "Премиум ящик"
entities.itembox.restricted                 = "Только для премиум игроков. (!store)"

--ent_map
entities.map.printName                      = "Карта"
entities.map.cityHall                       = "Мэрия"
entities.map.superMarket                    = "Супермаркет"
entities.map.policeStation                  = "Полицейский участок"
entities.map.fireStation                    = "Пожарный участок"
entities.map.clothingStore                  = "Магазин одежды"
entities.map.generalHospital                = "Госпиталь"
entities.map.modShop                        = "Тюнинг"
entities.map.realtyOffice                   = "Риелторское бюро"
entities.map.realEstateOffice               = "Агенство недвижимости"
entities.map.carDealership                  = "Автосалон"
entities.map.cityBank                       = "Банк"
entities.map.miningRavine                   = "Шахтерское ущелье" -- Может быть и ущелье/что-либо другое, не помешал бы Контекст
entities.map.transitCentre                  = "Транзитный центр"
entities.map.impoundLot                     = "Штрафстоянка"
entities.map.vehicleImpound                 = "Конфискация ТС"
entities.map.fishingDock                    = "Пристань"
entities.map.biteRestaurant                 = "Ресторан Байт"
entities.map.woodcuttingForest              = "Лес" -- Контекст
entities.map.gasStation                     = "АЗС"
entities.map.gasStationCity                 = "АЗС (город)"
entities.map.gasStationCountry              = "АЗС (пригород)"
entities.map.gasStationIndustiral           = "АЗС (промзона)"
entities.map.deliveryDepot                  = "Склад доставки"
entities.map.drugAlley                      = "Подворотня драгдилера"
entities.map.cityMines                      = "Городская шахта"
entities.map.highEndMines                   = "Высокотехнологичная шахта"
entities.map.fishingStore                   = "Рыболовный магазин"
entities.map.casino                         = "Казино"
entities.map.monoRailStop                   = "Монорельс"
entities.map.gunStore                       = "Оружейный магазин"
entities.map.hardwareStore                  = "Бытовой магазин"
entities.map.woodCuttingPark                = "Парк" -- Контекст
entities.map.marketStreet                   = "Рынок"
entities.map.garage                         = "Гараж"
entities.map.cityGarage                     = "Автогараж"
entities.map.miningRow                      = "Mining Row" -- Контекст
entities.map.woodcuttingRow                 = "Woodcutting Row" -- Контекст
entities.map.resourceHaven                  = "Resource Haven" -- Контекст
entities.map.mineshaft                      = "Шахта"
entities.map.hospital                       = "Госпиталь"
entities.map.mechanicShop                   = "Мастерская"
entities.map.automobileDealer               = "Aвтодилер"
entities.map.drugDealers                    = "Барыга"
entities.map.truckersCo                     = "Грузовики Ко."
entities.map.deliveryService                = "Служба доставки"
entities.map.mexiGrill                      = "Мекси Грилль" -- Контекст, но скорее всего именительное
entities.map.ronnies                      	= "У Ронни"
entities.map.bank                           = "Банк"
entities.map.lake                           = "Озеро"
entities.map.truenorthCounty                = "Округ Трунорз"
entities.map.pineCounty                		= "Округ Пайн"
entities.map.monofordCounty					= "Округ Монофорд"
entities.map.truenorthCentral               = "Трунорз Централ"
entities.map.taxiCompany                    = "Служба такси"
entities.map.openMap                        = "Открой карту!"
entities.map.MonoMap                        = "КАРТА"
entities.map.legend                         = "ЛЕГЕНДА"
entities.map.you                            = "Вы"
entities.map.setDestination                 = "Указать назначение"
entities.map.mcc                			= "Convention Center"

--ent_metal_detector
entities.metalDetector.printName            = "Металлодетектор"

--ent_police_database
entities.policeDatabase.printName           = "БД Полиции"
entities.policeDatabase.accessPoliceDB      = "Доступ к БД Полиции" -- Может быть и глагол. Контекст!
entities.policeDatabase.noAccess            = "Нет доступа"

--ent_police_jailer
entities.policeJailer.printName             = "Полицейский компьютер"
entities.policeJailer.accessPolicePC        = "Доступ к полицейскому компьютеру" --   -||-
entities.policeJailer.noAccess              = "Нет доступа"

--ent_prisoner_toiler
entities.prisonerToilet.printName           = "Туалетный ёршик"
entities.prisonerToilet.digIn               = "Поищи на удачу!"
entities.prisonerToilet.ewDisgusting        = "Фу, отвратительно!"
entities.prisonerToilet.looksEmpty          = "Этот толчок выглядит пустым..." -- может быть "уборная"
entities.prisonerToilet.noWay               = "Ни за что не дотронусь до него!"
entities.prisonerToilet.cannotSearch        = "Вы не можете обыскивать туалеты, если на посту нет Охранников."
entities.prisonerToilet.alreadyHave         = "У меня уже есть отмычка, зачем мне это делать?"
entities.prisonerToilet.startedDigging      = "%s начал рыскать в содержимом унитаза."
entities.prisonerToilet.dangIt              = "Я нашел отмычку, но она выскользнула и упала в толчок... Чёрт."
entities.prisonerToilet.dugUpLockpick       = "Вы отыскали отмычку! Замечательно!"
entities.prisonerToilet.foundNothing        = "Вы ничего не нашли."
entities.prisonerToilet.diggingThrough      = "РЫТЬЕ"

--ent_prisoner_task
entities.prisonerTask.printName				= "Работа для заключенных"  -- Не знаю, насколько корректно. Важно знать, как работает система в игре.
entities.prisonerTask.notif.notYou			= "К счастью, эта работа не для тебя."
entities.prisonerTask.notif.justDone 		= "Эта работка была только что выполнена. Найди другую или подожди ещё немного."
entities.prisonerTask.notif.cooldown		= "Подожди ещё несколько секунд перед тем, как начать выполнять другую работу (%s)"

entities.prisonerTask.laundry.hintText		= "Постирай грязное белье"
entities.prisonerTask.laundry.actionText	= "Стирка белья..."
entities.prisonerTask.dishes.hintText 		= "Помой грязную посуду"
entities.prisonerTask.dishes.actionText 	= "Мытье посуды..."
entities.prisonerTask.cooking.hintText 		= "Приготовь чего-нибудь скверного"
entities.prisonerTask.cooking.actionText 	= "Готовка..."
entities.prisonerTask.fallbackHintText		= "Поработай в тюряге"

--ent_respawner
entities.respawner.printName                = "Ящик полиции"
entities.respawner.cannotOpen               = "Вы не можете открыть этот ящик."
entities.respawner.readyToUse               = "Готово к использованию"
entities.respawner.seconds                  = "секунд" -- нужен контекст
entities.respawner.noAccess                 = "Нет доступа"

--ent_source_base
entities.sourceBase.printName               = "Источник ресурсов" -- нужен контекст
entities.sourceBase.gatherWeaponName        = "Инструмент добычи" -- нужен контекст
entities.sourceBase.cannotGather            = "Вы не можете добывать ресурсы будучи на работе!"
entities.sourceBase.noMoreResources         = "Этот источник ресурсов иссяк!"
entities.sourceBase.skillLevelRequired      = "Необходим %s уровень навыка %s!"
entities.sourceBase.source                  = "%s" -- нужен контекст
entities.sourceBase.sourceAvailable         = "Доступно %s: %s%%"
entities.sourceBase.level                   = "Уровень %s"

--ent_source_rock
entities.sourceRock.printName               = "Булыжник"
entities.sourceRock.resourceName            = "Камень"
entities.sourceRock.cannotMine              = "Вы не можете добывать руду, когда вы на работе!"
entities.sourceRock.noMoreStone             = "В этом булыжнике больше нет камня."
entities.sourceRock.higherSkillLevel        = "Ваш уровень навыка недостаточно высок!"

--ent_source_tree
entities.sourceTree.printName               = "Дерево"
entities.sourceTree.resourceName            = "Древесина"
entities.sourceTree.gatherWeaponName        = "Топор"
entities.sourceTree.messageEmployed         = "Вы не можете рубить дерево, когда вы на работе!"
entities.sourceTree.noMoreWood              = "Вы больше не можете получить древесину из этого дерева."

--ent_spawned_furniture
entities.spawnedFurniture.printName         = "Мебель" -- нужен контекст

--ent_spawned_prop
entities.spawnedProp.printName              = "Проп" -- нужен контекст
entities.spawnedProp.pickingUp              = "ПОДНЯТИЕ"
entities.spawnedProp.health                 = "Прочность: %s%%"

--ent_spray_plate
entities.sprayPlate.printName               = "Балончик с краской"
entities.sprayPlate.sprayingTag             = "ТЭГ"
entities.sprayPlate.pleaseWait              = "Пожалуйста подождите перед тем как снова применить балончик."
entities.sprayPlate.tagYourCrew             = "Нанесите ваш тэг на стену."

--ent_tapedummy
entities.tapeDummy.printName                = "Tape Dummy Object" -- нужен контекст

--ent_unarrest_trigger
entities.unarrestTrigger.printName          = "Unarrest Trigger" -- проверить в игре

--ent_use_trigger
entities.useTrigger.printName               = "Use Trigger" -- проверить в игре

--ent_vehicle_oopvs  ( specifies it's disabled but adding it in just in case )
entities.vehicleOOPVS.printName             = "Vehicle OOPVS View" -- не известен перевод.

--ent_vehicle_showcase
entities.vehicleShowcase.printName          = "Автопоказ"

--ent_voting
entities.votingComputer.printName           = "Компьютер для голосования"
entities.votingComputer.cannotVote          = "Вы не можете голосовать до начала следующего периода выборов."
entities.votingComputer.mayorRating         = "Рейтинг мэра: "
entities.votingComputer.generalTaxes        = "Общий налог:"
entities.votingComputer.yourJobTaxes        = "НДФЛ:"
entities.votingComputer.aveJobTaxes         = "Ср. налог:"

--ent_wheel_repair_kit
entities.wheelRepairKit.printName           = "Комплект для ремонта колёс"

--mono_camera
entities.monoCamera.printName               = "Личная камера"

--monolith_npc
entities.monoNPC.printName                  = "НПС" -- в переводе не нуждается

--npc_injuredciv
entities.injuredCiv.printName               = "Раненый человек"
entities.injuredCiv.innocentCivilian        = "Этот человек невиновен."

--npc_robber
entities.npcRobber.criminal 				= "Криминал"
entities.npcRobber.mustSurrenderFirst       = "Грабитель, должно быть, сдался первым."

--sent_base_gonzo
entities.baseGonzo.printName                = "Наркотик"

-- the_weed_foil
entities.weedfoil.printName                 = "Упаковочная машина" -- нужен контекст

-- the_weed_foildryhash
entities.weedfoildryhash.printName          = "Foil Dry Hash"
entities.weedfoildryhash.foiledDryHash      = "Foiled Dry Hash"
entities.weedfoildryhash.consumeWeed        = "You can't consume foiled weed."

-- the_weed_hash
entities.weedhash.printName                 = "Влажный гашиш"
entities.weedhash.wetWeed                   = "Вы не можете использовать это, для начала нужно высушить травку."

-- the_weed_jar
entities.weedjar.printName                  = "Банка"

-- the_weed_microwave
entities.weedmicrowave.printName            = "Микроволновка"
entities.weedmicrowave.connectApp           = "Необходимо подключить вилку прибора к розетке."

-- the_weed_plant
entities.weedplant.printName                = "Горшок"
entities.weedplant.soilInPot                = "Вы положили немного почвы в горшок."
entities.weedplant.soilPack                 = "Для этого в вашем инвентаре должен быть пакет с почвой."
entities.weedplant.plantedSeed              = "Вы посадили семя в горшок."
entities.weedplant.marjIsBigSeed            = "Для этого вам понадобится пакет с семенами марихуаны в вашем инвентаре."
entities.weedplant.harvestSuccess           = "Вы успешно собрали травку растение."
entities.weedplant.badTeam                  = "You are not the correct team to harvest the plant."
entities.weedplant.somethingStrange         = "Вы чувствуете какой-то странный запах. Вы останавливаетесь, чтобы прочистить горло."

-- the_weed_tent
entities.weedtent.printName                 = "Парник с освещением"
entities.weedtent.sockResi                  = "Необходимо подключить вилку прибора к розетке."
entities.weedtent.toggleSystem              = "Вы переключили систему: "
entities.weedtent.on                        = "вкл."
entities.weedtent.off                       = "выкл."
entities.weedtent.bigBarrel                 = "Вам нужна большая бочка воды для этого."
entities.weedtent.refillWater               = "Вам нужно налить воду."
entities.weedtent.waterSys                  = "Поливочная система ("
entities.weedtent.ON                        = "ВКЛ)"
entities.weedtent.OFF                       = "ВЫКЛ)"
entities.weedtent.waterLeft                 = "Воды осталось"
entities.weedtent.liters                    = " литров"
entities.weedtent.refill                    = "Нажмите E на экран чтобы пополнить"
entities.weedtent.altE                      = "Зажмите ALT и E чтобы начать процесс полива"

--ent_dirt_pile
entities.dirtpile.printName                 = "Участок"
entities.dirtpile.harvesting                = "СБОР УРОЖАЯ"
entities.dirtpile.stealing                  = "КРАЖА"
entities.dirtpile.noInventorySpace          = "У вас нет места в инвентаре, чтобы собрать этот урожай.!"
entities.dirtpile.soilQualityNoBueno        = "Качество почвы настолько плохое, что вы случайно уничтожили свой участок!"
entities.dirtpile.stealingAttempt           = "/me пытается украсть урожай с участка" -- NOTE: Keep the /me prefix, this formats the text when typed in the chat
entities.dirtpile.stealingSeeds             = "КРАЖА\nСЕМЯН"

--ent_easter_egg
entities.easteregg.printName                = "Пасхальное яйцо"
entities.easteregg.claimPrize               = "Нажмите\n\n\nИ заберите ваш приз!"
entities.easteregg.nothing                  = "ничего"
entities.easteregg.youReceived              = "Вы получили %s"
entities.easteregg.congratsYouReceived      = "Поздравляю! Вы получили %s"

--ent_wol_machine
entities.wolmachine.printName               = "Колесо Удачи"
entities.wolmachine.congratsYouWon          = "Поздравляю! Вы только что выиграли %s!"
entities.wolmachine.chatPrefix              = "[Колесо Удачи]"
entities.wolmachine.justWonTheJackpot       = "только что выиграл джекпот"
entities.wolmachine.onTheWheelOfLuck        = "на Колесе Удачи!"
entities.wolmachine.cantWhenWorking         = "Вы не можете использовать это на работе."
entities.wolmachine.currDisabled            = "Колесо Удачи сейчас не активно."
entities.wolmachine.pleaseWait              = "Подождите %s секунд перед сменой автоматов."
entities.wolmachine.pleaseWaitTurn          = "Подождите %s пока Колесо не остановится, или же найдите другое."
entities.wolmachine.cannotAfford            = "Вы не можете использовать Колесо, вам нужно хотя бы %s."

--ent_wol_reel
entities.wolreel.printName                  = "Барабан колеса удачи"

--the_weed_dirt
entities.theweeddirt.printName              = "Пакет грунта"
entities.theweeddirt.chargesLeft            = "%s - %s частей осталось"

--the_weed_dryhash
entities.theweeddryhash.printName           = "Гашиш"

--ent_pplant_barrel
entities.pplantBarrel.printName           	= "Бочка"
entities.pplantBarrel.extractAttempt        = "Перекачиваем химикаты..."
entities.pplantBarrel.empty           		= "Эта бочка уже пустая."
entities.pplantBarrel.startExtract			= "%s перекачивает химикаты из бочки."
entities.pplantBarrel.siphon				= "Перекачка химикатов"
entities.pplantBarrel.noSiphon				= "У вас нет сифонного насоса."
entities.pplantBarrel.police				= "You cant do this." -- TODO: Needs Translation
entities.pplantBarrel.notEnough				= "Warning: Not enough security online" -- TODO: Needs Translation
entities.pplantBarrel.broken				= "Siphon pump is broken." -- TODO: Needs Translation