--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

local gamemode = GAMEMODE or GM

Monolith.LocaleBuilder( "de", true )

--@TODO: put the translations in subtables (has to be mirrored in the places that use them)

gamemode_version = "Version " .. gamemode.Version -- @TODO: Find all instances and remove

monolith.version 						= "Update " .. gamemode.Version

ok 										= "Ok"
yes 									= "Ja"
no 										= "Nein"
on 										= "An"
off 									= "Aus"
cancel 									= "Abbrechen"
err 									= "Fehler"
confirm 								= "Bestätigen"
moneySymbol 							= "$"
moneySymbol.side 						= "LEFT" -- DO NOT TRANSLATE! Enter "LEFT" if you say $100 or "RIGHT" if you display 100$

trash.reward = "Du erhältst %s für das Reinigen der Straßen."
trash.isEmpty = "Diese Mülltonne wurde bereits geleert."

init.notif.lifeAlert 					= "Dein Lebensalarm hat einen automatischen Notruf ans Krankenhaus getätigt!"
init.notif.noParamedics 				= "Derzeit sind keine Sanitäter verfügbar."
init.notif.lifeAlertNotUsed 			= "Deine Lebensalarm-Gebühr wird nicht berechnet."
init.notif.savedMedical 				= "Du wurdest von einen Sanitäter gerettet, jedoch erlebst du einen seltsamen Fall von Amnesie. (NLR AKTIV)"

init.dispatch.lifeAlert 				= "Dies ist eine automatische Warnung, verfügbare Einheiten sollen bitte auf diesen Notruf antworten!"

kernel.ui.elements.noNotifications 		= "Du hast keine neuen Benachrichtigungen"
kernel.ui.elements.close 				= "Schließen ✕"

kernel.player.waitinglist				= "Der Server ist überfüllt! Du bist Platz #%d in der Warteschlange.\nBleib am Ball um deinen Platz zu sichern."

anims.raiseHands 	= "%s hebt seine Fäuste."
anims.lowerHands 	= "%s nimmt seine Fäuste runter."
anims.log.raiseHands 	= "%s hebt seine Fäuste."
anims.log.lowerHands 	= "%s nimmt seine Fäuste runter."

kernel.ui.elements.noNotifications 		= "Du hast keine neuen Benachrichtigungen."
kernel.ui.elements.close 				= "SCHLIEßEN ✕"
kernel.ui.elements.allNotifications		= "Alle Benachrichtigungen"
