--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

Monolith.LocaleBuilder( "de", true )

--[[
	SWEPS
--]]

-- Hands
sweps.handcuffsinhand.printName 			= "Handschellen"

sweps.handcuffsinhand.actions.beginCuffing 	= "%s fängt an %s Handschellen anzulegen."
sweps.handcuffsinhand.actions.placedCuffs	= "%s legt %s Handschellen an."
sweps.handcuffsinhand.actions.handCuffing	= "LEGE \n%s HANDSCHELLEN AN"
sweps.handcuffsinhand.actions.removeCuffs	= "%s legt %s Handschellen an."

sweps.handcuffsinhand.gui.checkOwner 		= "Drücke R um der Besitzer zu werden"
sweps.handcuffsinhand.gui.noOwner			= "Kein Besitzer"

sweps.handcuffsinhand.log.cuffed			= "%s hat %s Handschellen angelegt"
sweps.handcuffsinhand.log.unCuffed			= "%s hat %s die Handschellen abgenommen"

-- Restrained

sweps.handcuffsonplayer.printName 			= "Gefesselt"

-- Bong
sweps.sentBong.gui.weed 					= "Gras"
sweps.sentBong.gui.quality					= "Qualität"

-- Zipties
sweps.zipties.printName 					= "Kabelbinder"

sweps.zipties.actions.beginZip 				= "%s fängt an %s mit Kabelbindern zu fesseln."
sweps.zipties.actions.finishZip 			= "%s hat %s mit Handschellen gefesselt."
sweps.zipties.actions.zipTie 				= "FESSLE \n%s"

sweps.zipties.log.ziptied					= "%s hat %s mit Kabelbindern gefesselt"

-- Axe
sweps.axe.printName 						= "Holzaxt"

-- Defib
sweps.defib.actions.stabalizing				= "STABILISIERE\n${name}"

sweps.defib.gui.dead 						= "Der Patient ist verstorben."
sweps.defib.gui.stable						= "Er ist bereits stabilisiert!"
sweps.defib.gui.critical					= "Der Patient muss sich im kritischen Status befinden!"
sweps.defib.gui.downtime					= "Du warst schnell genug hier! Der Patient hat keine schweren Verletzungen erlitten."
sweps.defib.gui.hospitalwarning				= "Der Patient ist stabilisiert, bring ihn ins Krankenhaus!"

sweps.defib.logger.revived 					= "%s hat %s wiederbelebt"

-- Axe
sweps.fireaxe.gui.assistance 				= "Deine Hilfe wird nicht benötigt!"

-- Keys
sweps.keys.printName 						= "Schlüssel"

sweps.keys.gui.state						= "Möchtest du ${state} zugriff auf dein Auto & Türen geben?"
sweps.keys.gui.stateRevoke					= "Widerrufen"
sweps.keys.gui.stateGrant					= "Erlauben"
sweps.keys.gui.access						= "Zugriff"
sweps.keys.gui.thisPerson					= "Diese Person"
sweps.keys.gui.yes							= "Ja"
sweps.keys.gui.no							= "Nein"
sweps.keys.gui.revokeOwner					= " hat nun keine Zugriffsrechte mehr" 
sweps.keys.gui.grantOwner					= " hat nun Zugriffsrechte"

--weapon_lockpick
sweps.lockpick.printName         = "Dietrich"

--weapon_lockpick
sweps.extinguisher.printName 		= "Feuerlöscher"

--weapon_mono_citizen_id
sweps.citizenid.printName        = "Ausweis"

--weapon_mono_department
sweps.departmentTablet.printName = "Department Tablet"
sweps.departmentTablet.desc      = "Nutze es für den Rausch" -- ? ...

--weapon_fireaxe
sweps.fireaxe.printName 		= "Feuerwehraxt"

-- weapon_defibrillator
sweps.defibrillator.printName 	= "AED"

--weapon_mono_fists
sweps.fists.printName            = "Hände"
sweps.fists.instructions         = "Primärtaste: Schlagen\nSekundärtaste: Klopfen/Aufheben"
sweps.fists.purpose              = "Zum Schlagen, Klopfen und Aufheben von Dingen."

-- Medkit
sweps.medkit.printName 						= "Erstes-Hilfe-Kit"

sweps.medkit.gui.healing					= "HEILE"
sweps.medkit.gui.sterilizing 				= "REINIGE\nWUNDEN"
sweps.medkit.gui.cauterizing 				= "SCHNEIDE\nWUNDEN"
sweps.medkit.gui.applyingBandages 			= "LEGE\nBANDAGEN AN"
sweps.medkit.gui.healingMe 					= "SELBER\nHEILEN"

--weapon_mono_police_id
sweps.policeid.printName   = "Polizeimarke"

--weapon_mono_tablet
sweps.monotablet.printName = "Mono-Tablet"

--weapon_pickaxe
sweps.pickaxe.printName    = "Spitzhacke"
sweps.pickaxe.instructions = "Primärtaste: Schwingen\nSekundärtaste: Schieben"

--weapon_radargun
sweps.radargun.printName   = "Radarpistole"
sweps.radargun.purpose     = "Primärtaste: Geschwindigkeit messen"

--weapon_ramdoor
sweps.ramdoor.printName = "Rammbock"
sweps.ramdoor.purpose = "Hilfe"

--weapon_taser
sweps.taser.printName = "Taser"

-- weapon_mono_vape
-- @TODO configure the flavor juices in the weapon_mono_vape.lua directly in the JuicyVapeJuices table
sweps.monovape.printName 					= "Vape"
sweps.monovape.gui.loadedFlavorJuice 		= "%s Geschmack"

sweps.monovape.juices.dew				= "Mountain Dew"
sweps.monovape.juices.cheetos				= "Cheetos"
sweps.monovape.juices.razzleberry				= "Razzleberry"
sweps.monovape.juices.banana				= "Banane"
sweps.monovape.juices.blacklicorice				= "Schwarzes Süßholz"
sweps.monovape.juices.churro				= "Churro"
sweps.monovape.juices.skittles				= "Skittles"
sweps.monovape.juices.normal				= "Normal"

-- weapon_nightstick
sweps.nightstick.printName 					= "Schlagstock"
sweps.nightstick.log.knockOut 				= "%s hat %s mit seinem Schlagstock bewusstlos geschlagen."

-- weapon_policetape
sweps.policetape.printName 					= "Absperrband"

sweps.policetape.gui.distance				=  "Entfernung"
sweps.policetape.gui.rightClickCancel		= "Rechtsklick zum Abbrechen."
sweps.policetape.gui.ropeCantBeThatLong 	= "Das ist zu lang!"
sweps.policetape.gui.pickedUpSegmentTape 	= "Du hast einen Teil deines Absperrbands eingesammelt."
sweps.policetape.gui.clearedAllTapes 		= "Du hast dein gesamtes Absperrband eingesammelt."
sweps.policetape.gui.maxTapes 				= "Du kannst nicht mehr Absperrband anbringen!" -- @TODO money symbol line 139
sweps.policetape.log.hasPlacedRope 			= "%s hat Absperrband an %s angebracht."
sweps.policetape.log.removedPoliceTape 		= "%s hat sein Absperrband von %s entfernt."
sweps.policetape.log.clearedAllTapes		= "%s hat sein gesamtes Absperrband  entfernt."

-- weapon_radargun
-- @TODO maybe we should add the speed unit in this file

-- weapon_ticket
sweps.weaponticket.printName 					= "Strafzettelbuch"
sweps.weaponticket.purpose 						= "Hiermit kannst du Strafzettel schreiben"

sweps.weaponticket.gui.cantGiveTicketToPlayer = "Du musst %s warten, bevor du den nächsten Strafzettel vergibst."
sweps.weaponticket.gui.cantGiveTicket		  = "Du musst %s warten, bevor du den nächsten Strafzettel schreibst."
sweps.weaponticket.gui.officierIssuedIt 	  = "Geschrieben von"
sweps.weaponticket.gui.timeSinceIssued		  = "Vergangene Zeit"
sweps.weaponticket.gui.lawyer 				  = "Anwalt"
sweps.weaponticket.gui.selectTicket 		  = "Einen Strafzettel wählen"
sweps.weaponticket.gui.police 				  = "Polizei"
sweps.weaponticket.gui.minutesAgo 			  = "vor %s Minuten"
sweps.weaponticket.gui.resolve				  = "Problem lösen"
sweps.weaponticket.gui.ticket				  = "Strafzettel"
sweps.weaponticket.gui.payTicket			  = "Strafzettel bezahlen ($%s)"
sweps.weaponticket.gui.pay					  = "Bezahlen"
sweps.weaponticket.gui.deny					  = "Ablehnen"
sweps.weaponticket.gui.appealTicketIfUnfair   = "Reiche Einspruch gegen den Strafzettel ein, wenn du denkst dass er nicht rechtens ist."
sweps.weaponticket.gui.smallViolation 		  = "Erster/Kleiner Verstoß"
sweps.weaponticket.gui.repeated 			  = "Wiederholter/Mittlerer Verstoß"
sweps.weaponticket.gui.excessive			  = "Schwerer Verstoß"
sweps.weaponticket.gui.appeal 				  = "Einspruch einreichen"
sweps.weaponticket.gui.amountToPay 			  = "Zu bezahlen" -- @TODO money symbol line 246
sweps.weaponticket.gui.ticketsFromHim 		  = "Strafzettel von ihm"
sweps.weaponticket.gui.insertText 			  = "<Hier schreiben>"
sweps.weaponticket.gui.beMoreDescriptive 	  = "Dein Grund muss detaillierter sein."
sweps.weaponticket.gui.insertNumber 		  = "Du musst eine Zahl angeben!"
sweps.weaponticket.gui.ok 					  = "Ok"
sweps.weaponticket.gui.cantFineMore 		  = "Der Strafzettel darf nicht mehr als $4,500 sein!" -- @TODO maybe adding this to a configuration or something like that line 313 and 227
sweps.weaponticket.gui.cantFineMoreThan		  = "Der Strafzettel darf nicht mehr als %s sein!"
sweps.weaponticket.gui.areYouSure 			  = "Bist du dir sicher, dass du den Strafzettel schreiben möchtest?"
sweps.weaponticket.gui.cancel 				  = "Abbrechen"
sweps.weaponticket.gui.accept 				  = "Akzeptieren"
sweps.weaponticket.gui.id 					  = "ID"
sweps.weaponticket.gui.kind 				  = "Typ"
sweps.weaponticket.gui.invalidReason		  = "Dein Strafzettel braucht einen Text."
sweps.weaponticket.gui.targetName			  = "Name"

-- weapon_trowel
sweps.weapontrowel.printName 				  = "Gartenschaufel"
sweps.weapontrowel.gui.cantPlantHere 		  = "Du kannst hier nichts pflanzen."
sweps.weapontrowel.gui.tooClose 			  = "Du bist zu nah, um hier zu pflanzen!"

-- weapon_watercan
sweps.watercan.printName = "Gießkanne"
sweps.watercan.purpose = "Hiermit kannst du deine Pflanzen gießen"


--[[
	ENTITIES
--]]

-- areacreator
tools.areacreator.name = "Area Creator"
tools.areacreator.notDefined = "Not Defined"

-- nodecreator
tools.nodecreator.name = "Node Creator"
tools.nodecreator.instructions = "LMB: Create node - RMB: Set Path"
tools.nodecreator.selectNode = "Select another node"

-- permaprops
tools.permaprops.name = "PermaProps"
tools.permaprops.description = "Save a props permanently"
tools.permaprops.instructions = "LeftClick: Add RightClick: Remove Reload: Update"
tools.permaprops.adminCan = "Admin can touch permaprops"
tools.permaprops.adminCant = "Admin can't touch permaprops !"
tools.permaprops.superAdminCan = "SuperAdmin can touch PermaProps"
tools.permaprops.superAdminCant = "SuperAdmin can't touch permaprops !"
tools.permaprops.onlySA = "Only Super Admin can touch permaprops"
tools.permaprops.inValidEnt = "That is not a valid entity !"
tools.permaprops.isPlayer = "That is a player !"
tools.permaprops.already = "That entity is already permanent !"
tools.permaprops.saved = "You saved %s with model %s to the database."
tools.permaprops.erased = "You erased %s with a model of %s from the database."
tools.permaprops.reload = "You have reload all PermaProps !"
tools.permaprops.updated = "You updated the %s you selected in the database."
tools.permaprops.saveProps = "Save a props for server restarts\nBy Malboro"
tools.permaprops.header = "------ Configuration ------"
tools.permaprops.header2 = "-------- Functions --------"
tools.permaprops.removeAll = "Remove all PermaProps"
tools.permaprops.erasedAll = "You erased all props from the map"

-- propertycreator
tools.propertycreator.name = "Property Creator"
tools.propertycreator.desc = "Edit The Property Data"
tools.propertycreator.propertyName = "Name: "
tools.propertycreator.hammerIdentifier = "Hammer ID: "
tools.propertycreator.propertyUID = "UID: "
tools.propertycreator.propertyCategory = "Category: "
tools.propertycreator.propertyPrice = "Price: "
tools.propertycreator.propertyJobCat = "Job Category: "
tools.propertycreator.propertyElectricMode = "Electricity Node: "
tools.propertycreator.noDataGiven = "No data is given"
tools.propertycreator.noDataCamGiven = "No cam data is given"
tools.propertycreator.noDataBoundGiven = "No boundarie data is given"
tools.propertycreator.noDataSendGiven = "No send data is given"
tools.propertycreator.noDataIDGiven = "No id is given"
tools.propertycreator.noDataNameGiven = "No name is given"
tools.propertycreator.noDataPriceGiven = "No price is given"
tools.propertycreator.noDataCatGiven = "No category is given"
tools.propertycreator.noDataJCatGiven = "No jobCategory given on unownable property"
tools.propertycreator.savedCon = "Saved %s as existing configuration"
tools.propertycreator.failedSave = "Failed to save %s as existing configuration. ERROR: "
tools.propertycreator.noError = "no error"
tools.propertycreator.saved = "Saved "
tools.propertycreator.reload = "Reload: Open Property Editor"
tools.propertycreator.leftC = "Left-Click: "
tools.propertycreator.rightC = "Right-Click: "
tools.propertycreator.none = "None"

tools.propertycreator.gui.propertyEditor = "Property Creator"
tools.propertycreator.gui.editBound = "Edit Boundaries"
tools.propertycreator.gui.setMaxPos = "Set Max Pos"
tools.propertycreator.gui.setMinPos = "Set Min Pos"
tools.propertycreator.gui.editDoors = "Edit Doors"
tools.propertycreator.gui.noDoorFound = "No door found at hit location. Make sure you are aiming at the door"
tools.propertycreator.gui.addedDoor = "Added door to property"
tools.propertycreator.gui.unableToRmove = "Unable to remove door position from property. Door position not found in property door table"
tools.propertycreator.gui.addDoor = "Add Door"
tools.propertycreator.gui.removeDoor = "Remove Door"
tools.propertycreator.gui.editCam = "Edit Camera"
tools.propertycreator.gui.setCam = "Set Camera Position"
tools.propertycreator.gui.editPower = "Edit Power Outlets"
tools.propertycreator.gui.addOutlet = "Add Outlet"
tools.propertycreator.gui.removeOutlet = "Remove Outlet"
tools.propertycreator.gui.editBins = "Edit Trash Bins"
tools.propertycreator.gui.addBin = "Add Bin"
tools.propertycreator.gui.removeBin = "Remove Removebin"
tools.propertycreator.gui.editPrisoner = "Edit Prisoner Toilets"
tools.propertycreator.gui.addPrisoner = "Add Prisoner Toilet"
tools.propertycreator.gui.removePrisoner = "Remove Prisoner Toilet"
tools.propertycreator.gui.editTask = "Edit Prisoner Tasks"
tools.propertycreator.gui.addTask = "Add Prisoner Task"
tools.propertycreator.gui.removeTask = "Remove Prisoner Task"
tools.propertycreator.gui.saveProperty = "Save Property Changes"
tools.propertycreator.gui.deselectProperty = "Exit Property"
tools.propertycreator.gui.addChildren = "Add Children Houses"
tools.propertycreator.gui.setParentID = "Set The Parent ID"
tools.propertycreator.gui.putParentID = "Put the Parent ID here."
tools.propertycreator.gui.setChildrenCount = "Set The Children Count"
tools.propertycreator.gui.setChildrenCountBelow = "Set the children count of the properys below."
tools.propertycreator.gui.editParentID = "Edit Parent ID"
tools.propertycreator.gui.putNewParentID = "Put the new Parent ID here."
tools.propertycreator.gui.quickPropertyPicker = "Quick Property Picker"
tools.propertycreator.gui.automaticDoors = "Automatically Set Doors"

-- textscreen
tools.textscreen.name = "3D2D Textscreen"
tools.textscreen.desc = "Create a textscreen with multiple lines, font colours and sizes."
tools.textscreen.instructions = "Left Click: Spawn a textscreen Right Click: Update textscreen with settings"
tools.textscreen.undoneText = "Undone textscreen"
tools.textscreen.textScreens = "Textscreens"
tools.textscreen.cleanedUpText = "Cleaned up all textscreens"
tools.textscreen.hitLimit = "You've hit the textscreen limit!"

tools.textscreen.gui.reset = "Reset all"
tools.textscreen.gui.resetColors = "Reset colors"
tools.textscreen.gui.resetSizes = "Reset sizes"
tools.textscreen.gui.resetText = "Reset textboxes"
tools.textscreen.gui.resetEverything = "Reset everything"
tools.textscreen.gui.resetLine = "Reset line"
tools.textscreen.gui.resetAllLines = "Reset all lines"
tools.textscreen.gui.line = "Line "
tools.textscreen.gui.fontColor = " font color"
tools.textscreen.gui.fontSize = "Font size"

--[[
	ENTITIES
--]]

-- ent_dumpster
entities.dumpster.waitBeforeDiving = "Du musst %s warten, bevor du die nächste Mülltonne durchsuchen kannst."
entities.dumpster.dumpsterSearched = "Die Mülltonne wurde bereits durchsucht. Du musst %s warten."
entities.dumpster.foundNothing = "Du hast nichts in der Mülltonne gefunden."
entities.dumpster.foundX = "Du hast %s in der Mülltonne gefunden!"
entities.dumpster.dumpsterDiving = "Mülltonnen tauchen..."

-- sent_workbench
entities.workbench.printName                = "Werkbank"

-- sent_woodworkbench
entities.woodworkbench.printName            = "Holzwerkbank"

-- sent_textscreen
entities.textscreen.printName               = "Textbildschirm"
entities.textscreen.editTextTitle           = "Textbildschirm bearbeiten"
entities.textscreen.editColors              = "Farbe bearbeiten"
entities.textscreen.abort                   = "Abbrechen"
entities.textscreen.setColor                = "Farbe setzen"
entities.textscreen.colorChanged            = "Textbildschirm Farbe geändert."
entities.textscreen.edit                    = "Bearbeiten"
entities.textscreen.textLengthError         = "Dein Text ist zu lang/kurz (<2 >18)"
entities.textscreen.pressEToChange          = "Drücke E zum Wechseln"

-- sent_stretcher
entities.stretcher.printName                = "Trage"
entities.stretcher.patientStabilise         = "Der Patient muss hierfür stabilisiert sein!"
entities.stretcher.ledByAnyone              = "Die Trage muss stillstehen!"
entities.stretcher.strappedOntop            = "%s hat %s's Körper auf der Trage angeschnallt."
entities.stretcher.unstrapped               = "%s hat %s's Körper auf der Trage abgeschnallt."
entities.stretcher.sameVehicle              = "Die Trage muss ins selbe Fahrzeug, wie davor!"
entities.stretcher.backDoorClosed           = "Die hinteren Türen sind zu, öffne sie erst."
entities.stretcher.notEMSPolice             = "Du musst Sanitäter/Polizist hierfür sein."
entities.stretcher.notYourVehicle           = "Das ist nicht dein Fahrzeug!"
entities.stretcher.notYourAmbulance         = "Das ist nicht dein Krankenwagen!"
entities.stretcher.blockingDeploy           = "Irgendwas blockiert die Trage!"

-- sent_road_sign
entities.roadsign.printName                 = "Polizei-Straßenschild"
entities.roadsign.policeDepartment          = "Polizei"
entities.roadsign.mustBePolice              = "Nur Polizisten können das hier bearbeiten"
entities.roadsign.pickingUp                 = "HEBE AUF"
entities.roadsign.policeSign                = "Polizeischild"
entities.roadsign.update                    = "Aktualisieren"

-- sent_road_sign_small
entities.roadsignsmall.printName            = "Kleines Polizei-Straßenschild"
entities.roadsignsmall.roadClosed           = "STRAßE GESPERRT"
entities.roadsignsmall.memberPolice         = "Nur Polizisten können das hier bearbeiten"
entities.roadsignsmall.policeSign           = "Polizeischild"
entities.roadsignsmall.update               = "Aktualisieren"

entities.roadsignsmall.log.changeText		= "%s hat den Text des Polizei-Straßenschildes zu: %s geändert."

-- sent_residential_extension_outlet
entities.extensionoutlet.printName          = "Erweiterte Wohnungssteckdose"
entities.extensionoutlet.errorAttempt       = "Attempt to use SetBreaker() on a sent_residential_extension_outlet"

-- sent_repair_bench
entities.repairbench.printName              = "Reparaturbank"

-- sent_property_bin
entities.propertybin.printName              = "Grundstück Eimer"
entities.propertybin.garbageCan             = "Mülltonne"
entities.propertybin.percentFull            = "% voll"

-- sent_power_source
entities.powersource.printName              = "Grundrückenenergiequelle"

-- sent_power_outlet
entities.poweroutlet.printName              = "Steckdose"

-- sent_power_converter
entities.powerconverter.printName           = "Stromwandler (Zweiweg)"

-- sent_pot
entities.pot.printName                      = "Gartentopf"
entities.pot.weedBag                        = "Grastüte"
entities.pot.useOtherPots                   = "Du kannst nur deine eigenen Gartentöpfe benutzen."
entities.pot.dehydrated                     = "Dehydriert"
entities.pot.thirsty                        = "Durstig"
entities.pot.neutral                        = "Neutral"
entities.pot.healthy                        = "Gesund"
entities.pot.insertSoil                     = "Gartenerde einfügen"
entities.pot.insertSeed                     = "Samen einfügen"
entities.pot.harvest                        = "Fällen"
entities.pot.properSeeds                    = "Du hast die falschen Samen."
entities.pot.gardeningSoil                  = "Du brauchst Gartenerde hierfür."
entities.pot.plantedSeed                    = "Du musst Samen hierein Pflanzen."
entities.pot.filledPot                      = "Du hast Gartenerde in den Gartentopf geschüttet."
entities.pot.gardeningPot                   = "Gartentopf"
entities.pot.alreadySoil                    = "Der Gartentopf ist bereits voll"
entities.pot.soilAvailable                  = "Du hast %s Gartenerde zur Verfügung"
entities.pot.seedsAvailable                 = "Du hast %s Samen zur Verfügung" -- %s war hier drin
entities.pot.plant                          = "Pflanzen"
entities.pot.properSeeds                    = "Du hast die falschen Samen."
entities.pot.addSoil                        = "Gartenerde hinzufügen"

-- sent_plug
entities.plug.printName                     = "Stecker"

-- sent_money
entities.money.printName                    = "Geld"
entities.money.youGrabbed                   = "Du hast aufgehoben: $"
entities.money.pickedUp                     = " aufgehoben "

-- sent_metalworkbench
entities.metalworkbench.printName           = "Metallwerkbank"

-- sent_metalworkbench
entities.metalworkbench.printName           = "Metallwerkbank"

-- sent_lsd
entities.lsd.printName                      = "LSD"

-- sent_lsd_pyro
entities.lsdpyro.printName                  = "Gasbrenner"

-- sent_lsd_powder
entities.lsdpowder.printName                = "LSD Pulvermaterial"

-- sent_lsd_paper
entities.lsdpaper.printName                 = "Abenteuer Papier"

-- sent_lsd_gas
entities.lsdgas.printName                   = "Gaskanister"

-- sent_lsd_freezer
entities.lsdfreezer.printName               = "Kühlschrank"
entities.lsdfreezer.holdPickUp              = "(Halten ) Aufheben "
entities.lsdfreezer.insertFlask             = "Erlenmeyerkolben einsetzen"
entities.lsdfreezer.takeOut                 = "Mir ist kalt! Nimm mich raus."

-- sent_lsd_flask
entities.lsdflask.printName                 = "Erlenmeyerkolben"
entities.lsdflask.stage1                    = "Pulver hinzufügen"
entities.lsdflask.stage2                    = "Ich brauch wärme"
entities.lsdflask.stage3                    = "Warte bis der Brenner fertig ist"
entities.lsdflask.stage4                    = "Flüssigkeit hinzufügen"
entities.lsdflask.stage5                    = "Schüttel mich"
entities.lsdflask.stage6                    = "Kühl mich"
entities.lsdflask.stage7                    = "Wickel mich in Papier"

-- sent_lsd_flank_support
entities.lsdflanksupport.printName          = "Gasbrenner Stütze"
entities.lsdflanksupport.fireStarted        = "[Feuer Quest] Das LSD Feuer hat angefangen!"
entities.lsdflanksupport.insertBurner       = "Gasbrenner einsetzen"
entities.lsdflanksupport.insertGas          = "Gasflasche einsetzen"
entities.lsdflanksupport.insertFlask        = "Erlenmeyerkolben einsetzen"
entities.lsdflanksupport.progress           = "Fortschritt: "

-- sent_lsd_bottle
entities.lsdbottle.printName                = "LSD Flaschenmaterial"

-- sent_infinite_power_source
entities.infinitepower.printName            = "Unbegrenzte Energiequelle"
entities.infinitepower.powerSource          = "Energiequelle"
entities.infinitepower.infinite             = "Unbegrenzt"

-- sent_fire_spark
entities.firespark.printName                = "Droge"

-- sent_fire_source
entities.firesource.printName               = "Feuer"

-- sent_factory_furnace
entities.factoryfurnace.printName           = "Ofen"
entities.factoryfurnace.busy                = "Die Fabrik ist beschäftigt."
entities.factoryfurnace.takeAllComp         = "Du musst alle Items herausnehmen bevor du es aufhebst."
entities.factoryfurnace.readyForUse         = "Bereit"
entities.factoryfurnace.needsConveyor       = "Braucht ein Förderband"
entities.factoryfurnace.needsPower          = "Braucht eine Energiequelle"

-- sent_factory_crusher
entities.factorycrusher.printName           = "Zerkleinerer"
entities.factorycrusher.busyCrushing        = "Der Zerkleiner arbeitet gerade."
entities.factorycrusher.takeAllBefore       = "Du musst alle Items aus der Box nehmen, bevor du es aufhebst."
entities.factorycrusher.preciousMats        = "Lagert die zerkleinerten Items."
entities.factorycrusher.turnOffNotif        = "Der Zerkleinerer schaltet sich von alleine aus, wenn er fertig ist."
entities.factorycrusher.crushing            = "Zerkleinert "
entities.factorycrusher.insertChunks        = "Steinbrocken einfügen"
entities.factorycrusher.unplugged           = "Ausgesteckt"

-- sent_factory_conveyor
entities.factoryconveyor.printName          = "Förderband"
entities.factoryconveyor.factoryBusy        = "Die Fabrik ist beschäftigt."
entities.factoryconveyor.turnMeOn           = "Schalt mich an"
entities.factoryconveyor.active             = "Aktiv"
entities.factoryconveyor.secs               = " sek"

-- sent_factory_cell
entities.factorycell.printName              = "Energiezelle"

-- sent_factory_box
entities.factorybox.printName               = "Lagerbox"
entities.factorybox.containerHold           = "Lagert Items, die aus der Fabrik kommen."
entities.factorybox.takeAllItems            = "Du musst alle Items aus der Lagerbox nehmen, bevor du es aufhebst."

-- sent_factory_base
entities.factorybase.printName              = "Fabrikbasis"

-- sent_drug_bag
entities.drugbag.printName                  = "Grastüte"

-- sent_door_charge
entities.doorcharge.printName               = "Türsprengsatz"

-- sent_cooktable
entities.cooktable.printName                = "Kochplatte"

-- sent_cocaine_stove
entities.cocainestove.printName             = "E-Herd"

-- sent_cocaine_pot
entities.cocainpot.printName                = "Kochtopf"
entities.cocainpot.temperature              = "Temperatur: "
entities.cocainpot.waitAbout                = "Warte ca. %ds"

-- sent_cocaine_packingbox
entities.cocainepackingbox.printName        = "Pappbox"
entities.cocainepackingbox.cocaLeaves       = "Kokablätter: "
entities.cocainepackingbox.boxFull          = "Die Box ist voll"
entities.cocainepackingbox.packing          = "Packe"
entities.cocainepackingbox.notEnoughLeaves  = "Du hast nicht genug Blätter."
entities.cocainepackingbox.boxFullEmpty     = "Die Pappbox ist voll."
entities.cocainepackingbox.noLeaves         = "Du hast keine Blätter."

-- sent_cocaine_kerosene
entities.cocainkerosene.printName           = "Kerosin"
entities.cocainkerosene.shakeIt             = "Schüttel es: "
entities.cocainkerosene.readyToUse          = "Bereit für die Benutzung"

-- sent_cocaine_jerrycan
entities.cocainejerrycan.printName          = "Kanister"
entities.cocainejerrycan.ready              = "Bereit"
entities.cocainejerrycan.pleaseWait         = "Warte %ds"

-- sent_cocaine_gas
entities.cocainegas.printName               = "Gas"
entities.cocainegas.filled                  = "Gefüllt: "

-- sent_cocaine_drafted
entities.cocainedrafted.printName           = "Getrocknete Blätter"
entities.cocainedrafted.shakeIt             = "Schüttle es: "
entities.cocainedrafted.ready               = "Bereit für die Benutzung"
entities.cocainedrafted.pleaseWait          = "Warte %d Sekunden"

-- sent_cocaine_acid
entities.cocaineacid.printName              = "Schwefelsäure"
entities.cocaineacid.pleaseWait             = "Warte %d Sekunden"
entities.cocaineacid.ready                  = "Bereit für die Benutzung"

-- sent_christmas_tree
entities.christmastree.printName            = "Weihnachtsbaum"

-- sent_brewingbarrelbench
entities.brewingbarrelbench.printName       = "Braufaß"

-- sent_bountyboard
entities.bountyboard.printName              = "Bountyboard"
entities.bountyboard.placeBounty            = "Ein Kopfgeld aussetzen"
entities.bountyboard.name                   = "Name"
entities.bountyboard.reward                 = "Belohnung"
entities.bountyboard.dateAssigned           = "Datum"

-- sent_base_lsd
entities.baselsd.printName                  = "Droge"

-- base_item_stackable
entities.baseitemstackable.printName        = "Basis Item"
entities.baseitemstackable.beingArrested    = "Du kannst keine Items aufheben, solange du verhaftet bist."
entities.baseitemstackable.inventorySpace   = "Du hast nicht genug Platz in deinem Inventar!"
entities.baseitemstackable.boughtItem       = "Du hast ein Item gekauft. Du hast %s bezahlt."
entities.baseitemstackable.shopFull         = "Der Shop ist bereits voll!"
entities.baseitemstackable.dontHaveItem     = "Du hast dieses Item nicht in deinem Inventar!"
entities.baseitemstackable.onlyHadThis      = "Du hast %s von diesem Item in deinem Inventar!"

--ent_atm_screen
entities.atmscreen.printName                = "ATM BILDSCHIRM"
entities.atmscreen.withdraw                 = "Abheben"
entities.atmscreen.deposit                  = "Einzahlen"
entities.atmscreen.monoBank                 = "MONO-BANK"
entities.atmscreen.balance                  = "Guthaben: "
entities.atmscreen.accountHolder            = "Kontoinhaber: %s"
entities.atmscreen.fiftyUSD                 = "$50" -- idk what you expect me to use here
entities.atmscreen.fiveHundredUSD           = "$500"
entities.atmscreen.tenThousandUSD           = "$10000"
entities.atmscreen.clear                    = "C"
entities.atmscreen.lessThan                 = "<"
entities.atmscreen.USD                      = "$"
entities.atmscreen.accessDenied             = "ATM Zugriff abgelehnt."
entities.atmscreen.insufficientFunds        = "Du hast nicht genug Geld."
entities.atmscreen.confirm                  = "Bestätigen"

--ent_atm
entities.atm.printName                      = "ATM"

--ent_boat_base
entities.boatbase.printName                 = "Boot Basis"
entities.boatbase.containerDesc             = "Hier kannst du Items einlagern."

--ent_boat_motor_engine
entities.boatmotorengine.printName          = "Bootsmotor"

--ent_boat_rhib
entities.boatrhib.printName                 = "RHIB"

--ent_boat_rowboat
entities.rowboat.printName                  = "Reihenboot"


--ent_callsign_display
entities.callsigndisplay.printName          = "Rufzeichentabelle"
entities.callsigndisplay.yourCallsign       = "Dein Rufzeichen ist %s"
entities.callsigndisplay.noCallsign         = "Du hast kein Rufzeichen"
entities.callsigndisplay.deactivate         = "Detektiv"

--ent_carspawnsign
entities.carspawnsign.warnning              = "— ACHTUNG —" --written 2 years ago by kyle goodale not me
entities.carspawnsign.seriousInjury         = "Um schwere Verletzungen zu vermeiden,"
entities.carspawnsign.remain10ft            = "halte zehn Meter Abstand"
entities.carspawnsign.fromThisSign          = "von diesem Schild..."
entities.carspawnsign.ty                    = "Danke."

--ent_circle_trigger
entities.circletrigger.printName            = "Lieferpunkt"
entities.circletrigger.depotPrintName       = "Rückgabepunkt"

--ent_busstop_trigger
entities.busstoptrigger.printName			= "Bushaltestelle"

--ent_teleporter_trigger
entities.teleportertrigger.printName		= "Shooting Range Trigger" -- TODO
entities.teleportertrigger.text				= "Validating ID Card"	   -- TODO
entities.teleportertrigger.notAllowed		= "You are not allowed to access the shooting range at this time."	-- TODO

--ent_detector_trigger
entities.detectortrigger.printName          = "Metalldetektor-Trigger"

--ent_fire_hydrant
entities.firehydrant.printName              = "Feuerhydrant"

--ent_fire_pumphose
entities.pumphose.printName                 = "Pumpe"
entities.pumphose.detachingHose             = "KUPPLE SCHLAUCH AB"

--ent_fire_waterhose
entities.waterhose.printName                = "Schlauch"
entities.waterhose.unequipFirst             = "Lege deinen Schlauch erst beiseite!"

--ent_garage_tv
entities.garagetv.printName                 = "Garagen Fernseher"

--ent_garage
entities.garage.printName                   = "Garage"
entities.garage.modeDisabled                = "Das ist momentan deaktiviert. Nutze das F3 Fahrzeugmenü."

--ent_gascan
entities.gascan.printName                   = "Benzinkanister"
entities.gascan.pourThis                    = "Hiermit kannst du dein Fahrzeug auftanken."

--ent_gaspump
entities.gaspump.printName                  = "Tankstelle"
entities.gaspump.poor                       = "Du kannst dir das nicht leisten!"
entities.gaspump.needToWait                 = "Du musst %s Sekunden warten!"

--ent_house_alarm
entities.housealarm.printName               = "Map" -- weird name for a house alarm

--ent_instanced_apt_circle
entities.instancedaptcircle.printName       = "Test-Text"

--ent_invisible_window
entities.invisibleWindow.printName          = "Unsichtbares Fenster"

--ent_itembox
entities.itembox.printName                  = "Premium Spind"
entities.itembox.restricted                 = "Nur für Premium. (!store)"

--ent_map
entities.map.printName                      = "Map"
entities.map.cityHall                       = "Rathaus"
entities.map.superMarket                    = "Supermarkt"
entities.map.policeStation                  = "Polizeiwache"
entities.map.fireStation                    = "Feuerwache"
entities.map.clothingStore                  = "Klamottenladen"
entities.map.generalHospital                = "Krankenhaus"
entities.map.modShop                        = "Mod Shop"
entities.map.realtyOffice                   = "Immobilienbüro"
entities.map.realEstateOffice               = "Immobilienbüro"
entities.map.carDealership                  = "Autohaus"
entities.map.cityBank                       = "Bank"
entities.map.miningRavine                   = "Minenschacht"
entities.map.transitCentre                  = "Transit Center"
entities.map.impoundLot                     = "Abschlepphof"
entities.map.vehicleImpound                 = "Fahrzeug Beschlagnahmung"
entities.map.fishingDock                    = "Angelsteg"
entities.map.biteRestaurant                 = "Restaurant"
entities.map.woodcuttingForest              = "Wald"
entities.map.gasStation                     = "Tankstelle"
entities.map.gasStationCity                 = "Tankstelle (Stadt)"
entities.map.gasStationCountry              = "Tankstelle (Country)"
entities.map.gasStationIndustiral           = "Tankstelle (Industrie)"
entities.map.deliveryDepot                  = "Lierferdepot"
entities.map.drugAlley                      = "Drogengasse"
entities.map.cityMines                      = "Stadtminen"
entities.map.highEndMines                   = "High-End-Minen"
entities.map.fishingStore                   = "Angelgeschäft"
entities.map.casino                         = "Casino"
entities.map.monoRailStop                   = "Mono-Rail Haltestelle"
entities.map.gunStore                       = "Waffenladen"
entities.map.hardwareStore                  = "Baumarkt"
entities.map.woodCuttingPark                = "Holzfällerpark"
entities.map.marketStreet                   = "Marktstraße"
entities.map.garage                         = "Garage"
entities.map.cityGarage                     = "Stadtgarage"
entities.map.miningRow                      = "Bergbau"
entities.map.woodcuttingRow                 = "Holzfällerei"
entities.map.resourceHaven                  = "Ressourcenhafen"
entities.map.mineshaft                      = "Minenschacht"
entities.map.hospital                       = "Krankenhaus"
entities.map.mechanicShop                   = "Werkstatt"
entities.map.automobileDealer               = "Fahrzeughändler"
entities.map.drugDealers                    = "Drogendealer"
entities.map.truckersCo                     = "Truckers Co."
entities.map.deliveryService                = "Lieferdienst"
entities.map.mexiGrill                      = "Mexi Grill"
entities.map.ronnies                      	= "Ronnie's"
entities.map.bank                           = "Bank"
entities.map.lake                           = "See"
entities.map.truenorthCounty                = "Truenorth County"
entities.map.pineCounty                		= "Pine County"
entities.map.monofordCounty					= "Monoford County"
entities.map.truenorthCentral               = "Truenorth Central"
entities.map.taxiCompany                    = "Taxiunternehmen"
entities.map.openMap                        = "Karte öffnen!"
entities.map.MonoMap                        = "MONO-MAP"
entities.map.legend                         = "LEGENDE"
entities.map.you                            = "DU"
entities.map.setDestination                 = "Zielort setzen"
entities.map.mcc                			= "Kongresszentrum"

--ent_metal_detector
entities.metalDetector.printName            = "Metalldetektor"

--ent_police_database
entities.policeDatabase.printName           = "Polizeidatenbank"
entities.policeDatabase.accessPoliceDB      = "Auf Polizeidatenbank zugreifen"
entities.policeDatabase.noAccess            = "Kein Zugriff!"

--ent_police_jailer
entities.policeJailer.printName             = "Polizei Computer"
entities.policeJailer.accessPolicePC        = "Auf Polizeicomputer zugreifen"
entities.policeJailer.noAccess              = "Kein Zugriff!"

--ent_prisoner_toiler
entities.prisonerToilet.printName           = "Gefängnis-Toilettenbürste"
entities.prisonerToilet.digIn               = "Greif rein für eine Überraschung"
entities.prisonerToilet.ewDisgusting        = "Ih, widerlich!"
entities.prisonerToilet.looksEmpty          = "Die Toilette ist leer..."
entities.prisonerToilet.noWay               = "Ugh, Ich kann mir nicht mal vorstellen die Toilette anzufassen!"
entities.prisonerToilet.cannotSearch        = "Du kannst die Toilette nicht durchsuchen, da keine Gefängniswärter da sind."
entities.prisonerToilet.alreadyHave         = "Ich hab bereits eine Dietrich, warum brauche ich noch einen?"
entities.prisonerToilet.startedDigging      = "%s hat angefangen die Toilette zu durchsuchen."
entities.prisonerToilet.dangIt              = "Ich habe eine Dietrich gefunden, aber er ist mir entglitten... Verdammt."
entities.prisonerToilet.dugUpLockpick       = "Du hast eine Dietrich gefunden! Nett!"
entities.prisonerToilet.foundNothing        = "Du hast nichts gefunden."
entities.prisonerToilet.diggingThrough      = "DURCHWÜHLT"

--ent_prisoner_task
entities.prisonerTask.printName				= "Gefangenenaufgabe"
entities.prisonerTask.notif.notYou			= "Das ist nichts für dich!"
entities.prisonerTask.notif.justDone 		= "Diese Aufgabe wurde bereits erledigt, Finde eine andere oder warte etwas."
entities.prisonerTask.notif.cooldown		= "Warte %s Sekunden bevor du die nächste Aufgabe erledigst."

entities.prisonerTask.laundry.hintText		= "Mach die Wäsche."
entities.prisonerTask.laundry.actionText	= "Wäscht Wäsche..."
entities.prisonerTask.dishes.hintText 		= "Reinige das Geschirr."
entities.prisonerTask.dishes.actionText 	= "Reinigt Geschirr..."
entities.prisonerTask.cooking.hintText 		= "Koch das Essen."
entities.prisonerTask.cooking.actionText 	= "Kocht..."
entities.prisonerTask.fallbackHintText		= "Gefängnisaufgabe machen."

--ent_respawner
entities.respawner.printName                = "AUSRÜSTUNG"
entities.respawner.cannotOpen               = "Du kannst den Spind nicht öffnen."
entities.respawner.readyToUse               = "Bereit"
entities.respawner.seconds                  = "Sekunden"
entities.respawner.noAccess                 = "Kein Zugriff"

--ent_source_base
entities.sourceBase.printName               = "Ressourcenbasis"
entities.sourceBase.gatherWeaponName        = "Waffen"
entities.sourceBase.cannotGather            = "Du kannst das nicht tun, solange du angestellt hast!"
entities.sourceBase.noMoreResources         = "Hier gibt es nichts mehr zu holen"
entities.sourceBase.skillLevelRequired      = "Level %s %s benötigt!"
entities.sourceBase.source                  = "%s-Quelle"
entities.sourceBase.sourceAvailable         = "Verfügbar - %s: %s%%"
entities.sourceBase.level                   = "Level %s"

--ent_source_rock
entities.sourceRock.printName               = "Felsen"
entities.sourceRock.resourceName            = "Stein"
entities.sourceRock.cannotMine              = "Du kannst das nicht tun, solange du angestellt hast!"
entities.sourceRock.noMoreStone             = "Der Stein hat keine Ressourcen mehr"
entities.sourceRock.higherSkillLevel        = "Dein Level ist zu niedrig!"

--ent_source_tree
entities.sourceTree.printName               = "Baum"
entities.sourceTree.resourceName            = "Holz"
entities.sourceTree.gatherWeaponName        = "Axt"
entities.sourceTree.messageEmployed         = "Du kannst das nicht tun, solange du angestellt hast!"
entities.sourceTree.noMoreWood              = "Der Baum hat keine Ressourcen mehr"

--ent_spawned_furniture
entities.spawnedFurniture.printName         = "Möbel"

--ent_spawned_prop
entities.spawnedProp.printName              = "Prop"
entities.spawnedProp.pickingUp              = "HEBE AUF"
entities.spawnedProp.health                 = "Gesundheit: %s%%"

--ent_spray_plate
entities.sprayPlate.printName               = "Sprühflasche"
entities.sprayPlate.sprayingTag             = "SPRÜHE TAG"
entities.sprayPlate.pleaseWait              = "Bitte warte, bevor du wieder sprühst."
entities.sprayPlate.tagYourCrew             = "Sprühe deinen Crewtag an die Wand"

--ent_tapedummy
entities.tapeDummy.printName                = "Tape Dummy Objekt"

--ent_unarrest_trigger
entities.unarrestTrigger.printName          = "Entlassungstrigger"

--ent_use_trigger
entities.useTrigger.printName               = "Nutze Trigger"

--ent_vehicle_oopvs  ( specifies it's disabled but adding it in just in case )
entities.vehicleOOPVS.printName             = "Fahrzeug OOPVS Ansicht"

--ent_vehicle_showcase
entities.vehicleShowcase.printName          = "Fahrzeug Showcase"

--ent_voting
entities.votingComputer.printName           = "Abstimmungs-PC"
entities.votingComputer.cannotVote          = "Du kannst im Moment nicht abstimmen."
entities.votingComputer.mayorRating         = "Bürgermeisterwertung: "
entities.votingComputer.generalTaxes        = "Allgemeine Steuern:"
entities.votingComputer.yourJobTaxes        = "Deine Berufsteuer:"
entities.votingComputer.aveJobTaxes         = "Durchschn. Berufsteuer:"

--ent_wheel_repair_kit
entities.wheelRepairKit.printName           = "Reifen-Reparatur-Kit"

--mono_camera
entities.monoCamera.printName               = "Persönliche CCTV Kamera"

--monolith_npc
entities.monoNPC.printName                  = "NPC"

--npc_injuredciv
entities.injuredCiv.printName               = "Verletzter Zivilist"
entities.injuredCiv.innocentCivilian        = "Der Zivilist ist unschuldig."

--npc_robber
entities.npcRobber.criminal 				= "Verbrecher"
entities.npcRobber.mustSurrenderFirst       = "Der Räuber musst sich erst ergeben."

--sent_base_gonzo
entities.baseGonzo.printName                = "Droge"

-- the_weed_foil
entities.weedfoil.printName                 = "Foliermaschine"

-- the_weed_foildryhash
entities.weedfoildryhash.printName          = "Foliere trockenes Gras"
entities.weedfoildryhash.foiledDryHash      = "Foliertes trockenes Gras"
entities.weedfoildryhash.consumeWeed        = "Du kannst foliertes Grass nicht konsumieren."

-- the_weed_hash
entities.weedhash.printName                 = "Nasses Gras"
entities.weedhash.wetWeed                   = "Du musst das Gras erst trocknen."

-- the_weed_jar
entities.weedjar.printName                  = "Glas"

-- the_weed_microwave
entities.weedmicrowave.printName            = "Mikrowelle"
entities.weedmicrowave.connectApp           = "Du musst die Mikrowelle an der Steckdose anschließen."

-- the_weed_plant
entities.weedplant.printName                = "Gartentopf"
entities.weedplant.soilInPot                = "Du musst Gartenerde in den Gartentopf packen."
entities.weedplant.soilPack                 = "Du musst Gartenerde in deinem Inventar hierfür haben."
entities.weedplant.plantedSeed              = "Du hast Samen in den Gartentopf gepflanzt."
entities.weedplant.marjIsBigSeed            = "Du brauchst Marihuana Samen in deinem Inventar hierfür."
entities.weedplant.harvestSuccess           = "Du hast die Pflanze erfolgreich eingesammelt."
entities.weedplant.badTeam                  = "Du bist im falschen Team um die Pflanze einzusammeln."
entities.weedplant.somethingStrange         = "Du riechst irgendetwas komisches."

-- the_weed_tent
entities.weedtent.printName                 = "Zelt"
entities.weedtent.sockResi                  = "Du musst es an die Steckdose anschließen."
entities.weedtent.toggleSystem              = "Du hast das Systen "
entities.weedtent.on                        = "eingeschaltet."
entities.weedtent.off                       = "ausgeschaltet."
entities.weedtent.bigBarrel                 = "Du brauchst einen großes Wasserfass hierfür."
entities.weedtent.refillWater               = "Du hast dein Wasserfass nachgefüllt."
entities.weedtent.waterSys                  = "Bewässerungssystem ("
entities.weedtent.ON                        = "AN)"
entities.weedtent.OFF                       = "AUS)"
entities.weedtent.waterLeft                 = "Verbleibendes Wasser"
entities.weedtent.liters                    = " Liter"
entities.weedtent.refill                    = "Zum Nachfüllen drücke E auf dem Monitor"
entities.weedtent.altE                      = "Drücke ALT + E, um die Bewässerung ein- und ausschalten"

--ent_dirt_pile
entities.dirtpile.printName                 = "Erdhaufen"
entities.dirtpile.harvesting                = "ERNTE"
entities.dirtpile.stealing                  = "STEHLT"
entities.dirtpile.noInventorySpace          = "Du hast nicht genug Platz im Inventar, um das hier zu ernten!"
entities.dirtpile.soilQualityNoBueno        = "Die Gartenerde war so schlecht, dass du die Pflanzen aus Versehen zerstört hat!"
entities.dirtpile.stealingAttempt           = "/me Versucht die Pflanzen zu stehlen." -- NOTE: Keep the /me prefix, this formats the text when typed in the chat
entities.dirtpile.stealingSeeds             = "STEHLE\nSAMEN"

--ent_easter_egg
entities.easteregg.printName                = "Easter Egg"
entities.easteregg.claimPrize               = "Drücke \n\n\nUnd sammle deinen Preis ein!"
entities.easteregg.nothing                  = "nichts"
entities.easteregg.youReceived              = "Du hast %s erhalten"
entities.easteregg.congratsYouReceived      = "Glückwunsch! Du hast\n%s erhalten."

--ent_wol_machine
entities.wolmachine.printName               = "Wheel Of Luck"
entities.wolmachine.congratsYouWon          = "Glückwunsch! Du hast %s gewonnen."
entities.wolmachine.chatPrefix              = "[Wheel Of Money]"
entities.wolmachine.justWonTheJackpot       = "Hat den Jackpot gewonnen"
entities.wolmachine.onTheWheelOfLuck        = "vom Wheel of Luck"
entities.wolmachine.cantWhenWorking         = "Du kannst diese Maschine nicht nutzen, solange du angestellt bist!"
entities.wolmachine.currDisabled            = "The Wheel Of Luck ist im Moment deaktiviert."
entities.wolmachine.pleaseWait              = "Bitte warte %s Sekunden bevor du die Maschine wechselt."
entities.wolmachine.pleaseWaitTurn          = "Bitte warte bis %s fertig ist oder finde eine neue Maschine."
entities.wolmachine.cannotAfford            = "Du kannst dir das nicht leisten, du brauchst mindestens %s."

--ent_wol_reel
entities.wolreel.printName                  = "Wheel Of Luck Rolle"

--the_weed_dirt
entities.theweeddirt.printName              = "Gartenerde"
entities.theweeddirt.chargesLeft            = "%s - %s Ladungen verbleibend"

--the_weed_dryhash
entities.theweeddryhash.printName           = "Gras"

--ent_pplant_barrel
entities.pplantBarrel.printName           	= "Kraftwerk Fass"
entities.pplantBarrel.extractAttempt        = "Sauge Chemikalien..."
entities.pplantBarrel.empty           		= "Das fast ist im Moment leer."
entities.pplantBarrel.startExtract			= "%s hat angefangen Chemikalien aus dem Fass abzusaugen."
entities.pplantBarrel.siphon				= "Um Chemikalien abzusaugen"
entities.pplantBarrel.noSiphon				= "Du hast keine Saugpumpe."
entities.pplantBarrel.police				= "Du kannst das nicht tun."
entities.pplantBarrel.notEnough				= "Achtung: Es sind nicht genug Sicherheitsleute online"
entities.pplantBarrel.broken				= "Deine Saugpumpe ist beschädigt."
