--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

--[[ @NOTE: Addon language configurations locations for the following addons are diplayed:
		- CasinoKit: casinokit/lua/casinokit/custom/langs
			- Also for each sub addon
		- DHeists: dheists_/lua/dheists/config/language
		- gsigns: gsigns/lua/metasign_config.lua
		- RRL: rrl/lua/rrl/config
]]

-- @TODO:
-- firetruck-ladder
-- blue's casino stuff
-- fishingmod
-- raptor-ui
-- [any SWEP PrintNames]
-- [any AddNotification]
-- rrl
-- v-fire
-- Zero's arcade pack
-- Check GetJobCategory again.

Monolith.LocaleBuilder( "de", true )

-- CasinoKit
addon.casino.notif.allowedMax5					= "Du darfst maximal 5 Wetten pro Tisch platzieren."

-- Tow Truck Driver
addon.towtruck.notif.noVehicle					= "Es steht kein Fahrzeug hinter dem Abschleppwagen!"
addon.towtruck.notif.attached 					= "Fahrzeug erfolgreich aufgeladen!"
addon.towtruck.notif.unattached					= "Fahrzeug erfolgreich abgeladen!"
addon.towtruck.notif.lookTowTruck				= "Du musst auf einen Abschleppwagen schauen um Fahrzeuge aufzuladen/abzuladen!"

-- Firetruck Ladder
addon.ftladder.notif.needFirefighter		= "Du musst ein Feuerwehrmann sein, um dies tun zu können!"

-- Fishingmod
addon.fishing.notif.hookedCaught				= "Du hast einen Fisch am Haken! Er befindet sich jetzt in deinem Inventar."
addon.fishing.notif.hookedAway 					= "Du hast einen Fisch am Haken, aber er kam davon."
addon.fishing.notif.noSpace						= "Du hast nicht genügend Platz in deinem Inventar!"
addon.fishing.notif.noCatch						= "Du hast nichts gefangen."
addon.fishing.notif.noBait						= "Du hast keine Köder mehr."
addon.fishing.notif.pleaseWait					= "Bitte warte %s, bevor du die Angel erneut auswirfst."
addon.fishing.notif.rmbReel						= "[RMB] EINHOLEN"

addon.fishing.notif.wrongBait 					= "Falscher Köder!"
addon.fishing.notif.fishInLine					= "Ein Fisch kaut an der Angelschnur."
addon.fishing.notif.noBaitEquipped				= "Kein Köder ausgerüstet!"
--addon.fishing.notif.noSpace						= "Du hast nicht genügend Platz in deinem Inventar!"
addon.fishing.notif.caughtFish					= "Du hast einen Fisch gefangen!"
addon.fishing.notif.cantUnderwater				= "Du kannst die Angelrute unter Wasser nicht benutzen."
addon.fishing.notif.needBait 					= "Du musst einen Köder ausgerüstet haben."

-- GSigns
addon.gsigns.notif.err 							= "Ein Fehler ist aufgetreten, überprüfe deine Konsole!"

addon.gsigns.action.pickingUp 					= "ÜBERNEHMEN"

-- RRL
addon.rrl.notif.removedWarning 					= "Du hast eine Verwarnung von %s entfernt."
addon.rrl.notif.warnedBy						= "Du wurdest von %s für %s verwarnt. Du hast nun %s Verwarnungspunkte."
addon.rrl.notif.willKicked						= "Du wirst vom Server gekickt, wenn du %s Punkte mehr erhältst."
addon.rrl.notif.willKicked.any 					= "jemanden"
addon.rrl.notif.youWarned 						= "Du hast %s für %s verwarnt."
addon.rrl.notif.reportDoesntExist 				= "Das Support Ticket auf das du versuchst zuzugreifen existiert nicht mehr."
addon.rrl.notif.noAccess						= "Du hast keine Berechtigung zu dieser Funktion."
addon.rrl.notif.removedReportTicket 			= "Du hast ein Support Ticket (#%s) entfernt."
addon.rrl.notif.supportTicketRemoved			= "Dein Support Ticket wurde entfernt (#%s)."
addon.rrl.notif.cantCloseOwnReport				= "Du kannst dein eigenes Ticket nicht schließen."
addon.rrl.notif.reportTicketClosed 				= "Dein Support Ticket wurde geschlossen (#%s)"
addon.rrl.notif.cannotOpenOwnReport				= "Du kannst dein eigenes Support Ticket nicht öffnen"
addon.rrl.notif.yourReportTicketOpened 			= "Dein Support Ticket wurde geöffnet (#%s)."
addon.rrl.notif.alreadyClaimed 					= "Du kannst das Support Ticket nicht übernehmen, da es bereits bearbeitet wird "
addon.rrl.notif.cannotClaimOwn 					= "Du kannst dein eigenes Support Ticket nicht bearbeiten."
addon.rrl.notif.staffMemberClaimed 				= "Ein Teammitglied übernimmt dein Support Ticket (#%s)."
addon.rrl.notif.claimedReportTicket				= "Du übernimmst ein Support Ticket (#%s)."
addon.rrl.notif.staffMemberUnclaimed			= "Das Teammitglied übernimmt nun nicht mehr dein Support Ticket (#%s)."
addon.rrl.notif.youUnclaimed					= "Du legst ein Support Ticket ab (#%s)"
addon.rrl.notif.submittedNewReport				= "Du hast ein neues Support Ticket (#%s) eröffnet."

-- Whomekit_media
addon.whomekit_media.notif.needConnect			= "Du musst den Stecker an eine Steckdose anschließen."

addon.whomekit_media.log.startedPlaying 		= "%s spielt auf einer Medienquelle ab: %s. (%s)"

-- zcrga
addon.zcrga.notif.cantAfford					= "Du kannst dir dies nicht leisten!"
addon.zcrga.notif.youWonX						= "Du hast %s gewonnen!"
		
