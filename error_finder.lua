-- THIS IS A TOOL MEANT TO FIND LOCALIZATION ISSUE.
-- IT MUST BE RUN USING A STANDALONE INTERPRETER, NOT THE GAME.
if gmod then return end

-- Usage: lua error_finder.lua [--ignore] [<lang>]

-- If you get a "NOT TRANSLATED" error but the name is correct, add the id to this list
-- If you do not want to see those errors at all, use the --ignore flag.
local ignored_missing = {
	fr = {
		"ok",
		"close_x",
		"gamemode_version",
		"admin.gui.teams",
		"admin.gui.tickets",
		"admin.chat.adminChatPrefix",
		"animation.taunts.robot",
		"animation.taunts.zombie",
		"blacklist.time.permanent",
		"blacklist.gui.steamIDColon",
		"carmod.gui.colors.cyan",
		"carmod.gui.colors.orange",
		"carmod.gui.modificationsTitle",
		"carmod.gui.upgrades.super",
		"cfg.cosmetics.fedora",
		"cfg.cosmetics.kevlar",
		"cfg.cosmetics.monocle",
		"cfg.cosmetics.moustache",
		"cfg.cosmetics.sombrero",
		"cfg.cosmetics.thomas_punk",
		"cfg.cosmetics.toque_blanche",
		"cfg.item.attachment_anpeq",
		"cfg.item.attachment_eotech",
		"cfg.item.attachment_extris",
		"cfg.item.attachment_m203",
		"cfg.item.attachment_x2lam",
		"cfg.item.drug_anti_drug",
		"cfg.item.drug_lsd",
		"cfg.item.ent_lsd_gas",
		"cfg.item.ent_tv",
		"cfg.item.food_bacon",
		"cfg.item.food_bread_baguette",
		"cfg.item.food_bread_croissant",
		"cfg.item.food_burger",
		"cfg.item.food_cheeseburger",
		"cfg.item.food_cola",
		"cfg.item.food_cookies",
		"cfg.item.food_doritos",
		"cfg.item.food_fristos",
		"cfg.item.food_mountaindew",
		"cfg.item.food_pepsi",
		"cfg.item.food_pizzafull",
		"cfg.item.food_popcorn",
		"cfg.item.food_raw_orange",
		"cfg.item.food_superburger",
		"cfg.item.food_taco",
		"cfg.item.food_toffifee",
		"cfg.item.food_tropicalchips",
		"cfg.item.furniture_table3",
		"cfg.item.tool_vape",
		"cfg.item.desc.food_bread_baguette",
		"cfg.item.desc.food_bread_croissant",
		"cfg.job.category.police",
		"cfg.job.prefix.asschief",
		"cfg.job.prefix.lt",
		"cfg.job.prefix.sergeant",
		"cfg.npc.casino_bartender",
		"cfg.npc.clothing_cashier",
		"cfg.npc.cosmetics_cashier",
		"cfg.npc.foodtruck",
		"cfg.npc.gun_dealer",
		"cfg.npc.premium_dealer",
		"cfg.npc.tow_truck_recruiter",
		"cfg.npc.tow_truck_valet",
		"cfg.veh.cat.suv",
		"cfg.veh.desc.ford_raptor",
		"clientsettings.gui.minimapStyle.normal",
		"clientsettings.gui.performance",
		"clientsettings.gui.vignette",
		"clothing.style.parka",
		"commands.fps.prompt.title",
		"communication.gui.iMessage",
		"communication.gui.radio",
		"crews.gui.actions",
		"crews.gui.information",
		"context.menu.moderating.freeze",
		"context.menu.admin.kick",
		"context.menu.admin.ban",
		"context.prompt.ban.title",
		"cosmetics.gui.version",
		"entities.atmscreen.clear",
		"entities.atmscreen.lessThan",
		"entities.atmscreen.monoBank",
		"entities.garage.printName",
		"entities.lsd.printName",
		"entities.map.casino",
		"entities.map.garage",
		"entities.map.mexiGrill",
		"entities.map.MonoMap",
		"exp.gui.xp",
		"farming.gui.type",
		"farming.item.cropMelon",
		"farming.item.cropOrange",
		"hud.gui.invisible",
		"hud.scoreboard.discord",
		"hud.tablet.taxi",
		"hud.tablet.licenses",
		"hud.tablet.imessage",
		"hud.tablet.monomap",
		"hud.tablet.emergency",
		"hud.tablet.emergency.focus",
		"hud.misc.hz",
		"inventory.actions",
		"inventory.notifications",
		"inventory.marketing",
		"loadouts.misc.grenades",
		"log.gui.na",
		"log.gui.actions",
		"log.gui.message",
		"log.cat.transaction",
		"log.cat.roleplay",
		"log.cat.iMessage",
		"log.cat.police",
		"log.cat.admin",
		"market.gui.item",
		"market.gui.total",
		"market.gui.type",
		"mayor.app.budget",
		"mayor.app.management",
		"mayor.gui.votesColon",
		"monophone.app.contacts",
		"monophone.app.messaging",
		"monophone.app.taxi",
		"monophone.app.taxi.mono",
		"monophone.app.taxi.distance",
		"monophone.banking.timestamp",
		"monophone.app.monomarket.category.transactions",
		"monophone.app.darkweb.info.title",
		"monophone.app.tweeter.tweet",
		"monophone.app.tweeter.comment",
		"monophone.app.tweeter.like",
		"nameplates.gui.premium",
		"nav.gui.aboveBelowDisplay",
		"nav.misc.meters",
		"newintro.gui.serverTitle",
		"notify.title.admin",
		"pcomp.gui.na",
		"pcomp.gui.actions",
		"pcomp.gui.date",
		"pcomp.gui.scan",
		"pcomp.gui.toggle.on",
		"pcomp.gui.toggle.off",
		"police.callsigns.patrol",
		"police.callsign.sergeant",
		"police.callsign.lt",
		"police.callsign.pinedeputy",
		"police.callsign.capt",
		"police.callsign.chief",
		"police.gui.bot",
		"police.reason.kidnapping",
		"property.gui.actions",
		"quests.gui.actions",
		"quests.gui.description",
		"quests.gui.notApplicable",
		"quests.q.cook_1.name",
		"refunds.gui.steamIDColon",
		"security.app.monoSecure",
		"skillbooks.page_x",
		"store.gui.total",
		"sweps.sentBong.gui.weed",
		"sweps.monotablet.printName",
		"sweps.monovape.juices.cheetos",
		"sweps.monovape.juices.dew",
		"sweps.monovape.juices.normal",
		"sweps.monovape.juices.skittles",
		"sweps.policetape.gui.distance",
		"sweps.taser.printName",
		"sweps.weaponticket.gui.ok",
		"sweps.weaponticket.gui.police",
		"tickets.frame.tickets",
		"vehicle.gui.skin",
		"vehicle.gui.premiumIndicator",
		"vehicle.cats.super",
		"vehicle.cats.sports",
		"vehicle.cats.suv",
		"vehicle.food.gui.stock",
		"vehicle.garage.title",
		"vehicle.testDrive.licensePlate",
		"vehicle.taxi.notif.taxiPrefix",
		"widget.app.licenses",
		"widget.app.taxi",
		"widget.app.iMessage",
	},
	de = {},
	ru = {},
	tr = {}
}

local folder_to_load = {
	"de",
	"en",
	"fr",
	"ru",
	"tr"
}

---------------------------------
-- Do not edit past this point --
---------------------------------

local lang, lang2 = ...
local ignoreallnt
if lang == "--ignore" then
	ignoreallnt = true
	lang = lang2
elseif lang2 == "--ignore" then
	ignoreallnt = true
end

local ignorent = {}
for lang, v in pairs(ignored_missing) do
	ignorent[lang] = {}
	for i, id in ipairs(v) do
		ignorent[lang][id] = true
	end
end

local function listfiles()
	local files = {}
	for i, folder in ipairs(folder_to_load) do
		local f
		local sOS = os.getenv("OS")
		if sOS and sOS == "Windows_NT" then
			f = io.popen("dir " .. folder) -- Pattern only safe on Windows
		else
			f = io.popen("ls -l " .. folder) -- Unix/Linux
		end
		local dt = f:read("*a")
		f:close()
		for name in dt:gmatch("(sh_[^ ]+%.lua)") do
			table.insert( files, folder .. "/" .. name )
		end
	end
	return files
end

Monolith = {}
Monolith.Phrases = {}

local langrefs = {}
local currentfile

local function makesig(str)
	local sig = {}
	for pattern in str:gsub("%%%%",""):gmatch("(%%[^%%])") do
		table.insert(sig, pattern)
	end
	return table.concat(sig,"-")
end

function Monolith.LocaleBuilder( sLanguage, bSetFEnv )
	local tData = {}
	local tDataReverse = {}

	local tLang = Monolith.Phrases[ sLanguage ] or {}
	Monolith.Phrases[ sLanguage ] = tLang

	local tMeta
	tMeta = {
		__index = function( self, sSubCategory )
			local sID = string.format( "%s%s.", tDataReverse[ self ] or "", sSubCategory )
			if tData[ sID ] then return tData[ sID ] end

			local tNewCategory = setmetatable( {}, tMeta )
			tDataReverse[ tNewCategory ] = sID
			tData[ sID ] = tNewCategory

			return tNewCategory
		end,
		__newindex = function( self, sKey, sValue )
			local sID = string.format( "%s%s", tDataReverse[ self ] or "", sKey )
			if sLanguage == "en" and not tLang[ sID ] then
				table.insert(langrefs, {file = currentfile, id = sID, signature = makesig(sValue), value = sValue})
			end
			tLang[ sID ] = sValue
		end
	}

	local tBuilder = setmetatable( {}, tMeta )

	if bSetFEnv then setfenv( 2, tBuilder ) end

	return tBuilder
end

GAMEMODE = {Version = "ERROR FINDER"}

local failed = {}
for i, name in ipairs(listfiles()) do
	local f = io.open(name, "r")
	if f then
		local data = f:read("*a")
		f:close()
		if data:sub(1,3) == "\239\187\191" then
			data = data:sub(4)
		end
		local f, err = loadstring(data, name)
		if not f then
			print(name, err)
			table.insert(failed, {name = name, err = err})
		else
			currentfile = name
			f()
		end
	else
		print("COULD NOT OPEN FILE: " .. name)
	end
end

local tocheck = {}
if lang and lang ~= "en" then
	tocheck[1] = lang
else
	for k,v in pairs(Monolith.Phrases) do
		if k ~= "en" then
			table.insert(tocheck, k)
		end
	end
end

local missing = 0
local untranslated = 0
local invalid = 0

for i, lang in ipairs(tocheck) do
	local thislang = Monolith.Phrases[lang]
	if thislang then
		for i, ref in ipairs(langrefs) do
			if not thislang[ref.id] then
				if not ignorent[lang] or not ignorent[lang][ref.id] then
					print(string.format("MISSING(%s): %s (%s)", lang, ref.id, ref.file or "Unknown File"))
					missing = missing + 1
				end
			elseif thislang[ref.id] == ref.value and ref.value ~= "" then
				if not ignoreallnt and ( not ignorent[lang] or not ignorent[lang][ref.id] ) then
					print(string.format("NOT TRANSLATED(%s): %s (%s)", lang, ref.id, ref.value))
					untranslated = untranslated + 1
				end
			else
				local sig = makesig(thislang[ref.id])
				if sig ~= ref.signature then
					print(string.format("INVALID FORMAT(%s): %s (has format %s, expected %s)", lang, ref.id, sig, ref.signature))
					invalid = invalid + 1
				end
			end
		end
	end
end

if #failed > 0 then
	print()
	print("One or more files failed to load:")
	for i, err in ipairs(failed) do
		print(err.name, err.err)
	end
end

print()
print(string.format("Found %d missing entries", missing))
print(string.format("Found %d invalid entries", invalid))
if not ignoreallnt then
	print(string.format("Found %d untranslated entries", untranslated))
	if untranslated > 0 then
		print()
		print(" To suppress 'NOT TRANSLATED' errors, add --ignore to the command line.")
	end
end
if missing > 0 then
	print(" Missing entries and untranslated ones have the same effect in-game; the english version is displayed.")
end
