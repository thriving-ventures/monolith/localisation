--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

local gamemode = GAMEMODE or GM

Monolith.LocaleBuilder( "fr", true )

--@TODO: put the translations in subtables (has to be mirrored in the places that use them)

gamemode_version = "Version " .. gamemode.Version -- @TODO: Find all instances and remove

time_long_format = "%H:%M:%S - %e %B %Y"
time_abrev_format = "%H:%M:%S - %d/%m/%Y"

monolith.version 						= "Version " .. gamemode.Version

ok 										= "OK"
yes 									= "Oui"
no 										= "Non"
on 										= "Allumé"
off 									= "Eteint"
cancel 									= "Annuler"
err 									= "Erreur"
confirm 								= "Confirmer"
moneySymbol 							= "$"
moneySymbol.side 						= "LEFT" -- DO NOT TRANSLATE! Enter "LEFT" if you say $100 or "RIGHT" if you display 100$

trash.reward = "Vous avez reçu %s pour avoir vidé la poubelle."
trash.isEmpty = "Cette poubelle est vide."

init.notif.lifeAlert 					= "Votre alerte de vie a fait un appel automatique au 911!"
init.notif.noParamedics 				= "Il n'y a pas de médecin disponible actuellement. Votre charge d'alerte de vie ne sera pas utilisée."
init.notif.savedMedical 				= "Vous avez été sauvé par les services médicaux, mais vous rencontrez un cas étrange d'amnésie ... (NLR ACTIVE)"
init.notif.noPermission					= "Vous n'avez pas la permission pour utiliser cette fonctionnalité."

init.dispatch.lifeAlert 				= "Ceci est une alerte de vie automatique! Unités disponibles, veuillez répondre à cet appel!"

init.notif.panicButton 					= "Vous avez appuyé sur l'alarme ! Une alerte d'urgence a été faite à la police !"
init.dispatch.panicButton				= "Il s'agit d'une alarme automatique ! Les unités disponibles doivent être référencées sur la carte informatique de la police !"

weapon.lockpick.notif.dispatch 			= "Une effraction a été détectée à cet endroit. Les unités disponibles sont priées de répondre !"

kernel.ui.elements.noNotifications 		= "Vous n'avez pas de notification"
kernel.ui.elements.close 				= "FERMER ✕"
kernel.ui.elements.allNotifications		= "Toutes les notifications"

kernel.player.waitinglist				= "Le serveur est plein! Vous etes #%d dans la file d'attente.\nContinuez a vous connecter pour garder votre place."

anims.raiseHands 	= "%s a levé ses mains."
anims.lowerHands 	= "%s a baissé ses mains."
anims.log.raiseHands 	= "%s a levé ses mains."
anims.log.lowerHands 	= "%s a baissé ses mains."

taser.low 				= "Clic gauche pour une décharge faible"
taser.high 				= "Clic droit pour une décharge forte"
taser.recharge			= "RECHARGEMENT"

gambling.notAllowed = "Vous n'êtes pas autorisé à jouer."

admin.actionNotAllowed = "Vous n'êtes pas autorisé à effectuer cette action."
admin.characterReset = "Les données de votre personnage ont été réinitialisées."
admin.missingId = "SteamId manquant."
admin.characterResetCooldown = "Vous ne pouvez effacer votre personnage qu'une fois par mois."
