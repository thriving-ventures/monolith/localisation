--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

Monolith.LocaleBuilder( "fr", true )

--[[
	SWEPS
--]]

-- Hands
sweps.handcuffsinhand.printName 			= "Menottes"

sweps.handcuffsinhand.actions.beginCuffing 	= "%s commence à mettre les menottes aux poignets de %s."
sweps.handcuffsinhand.actions.placedCuffs	= "%s a mis les menottes aux poignets de %s."
sweps.handcuffsinhand.actions.handCuffing	= "MENOTTAGE \n%s"
sweps.handcuffsinhand.actions.removeCuffs	= "%s enlève les menottes aux poignets de %s."

sweps.handcuffsinhand.gui.checkOwner 		= "Appuyez sur R pour obtenir le propriétaire"
sweps.handcuffsinhand.gui.noOwner			= "Aucun propriétaire"

sweps.handcuffsinhand.log.cuffed			= "%s a menotté %s"
sweps.handcuffsinhand.log.unCuffed			= "%s a dé-menotté %s"

-- Restrained

sweps.handcuffsonplayer.printName 			= "Menotté"

-- Bong
sweps.sentBong.gui.weed 					= "Weed"
sweps.sentBong.gui.quality					= "Qualité"

-- Zipties
sweps.zipties.printName 					= "Menottes"

sweps.zipties.actions.beginZip 				= "%s a commencé à menotter %s."
sweps.zipties.actions.finishZip 			= "%s a terminé de menotter %s."
sweps.zipties.actions.zipTie 				= "EN TRAIN DE MENOTTER \n%s"

sweps.zipties.log.ziptied					= "%s a menotté %s"


-- Defib
sweps.defib.actions.stabalizing				= "STABILISATION\n${name}"

sweps.defib.gui.dead 						= "Le patient semble décédé."
sweps.defib.gui.stable						= "Il est déjà stabilisé!"
sweps.defib.gui.critical					= "Le patient doit être dans un état critique pour être stabilisé!"
sweps.defib.gui.downtime					= "Tu es arrivé assez vite! Grâce à cela le patient n'a pas subi de grosses blessures."
sweps.defib.gui.hospitalwarning				= "On dirait qu'il est stabilisé, emmenez-le à l'hôpital!"

sweps.defib.logger.revived 					= "%s a réanimé: %s"

-- Axe
sweps.axe.printName 						= "Hache en bois"
sweps.fireaxe.gui.assistance 				= "Votre aide n'est pas requise ici!"

-- Keys
sweps.keys.printName 						= "Clés"

sweps.keys.gui.state						= "Voulez-vous ${state} l'accès à vos voitures/portes?"
sweps.keys.gui.stateRevoke					= "révoquer"
sweps.keys.gui.stateGrant					= "allouer"
sweps.keys.gui.access						= "Accès"
sweps.keys.gui.thisPerson					= "cette personne"
sweps.keys.gui.yes							= "Oui"
sweps.keys.gui.no							= "Non"
sweps.keys.gui.revokeOwner					= "Vous avez révoqué la propriété à "
sweps.keys.gui.grantOwner					= "Vous avez alloué la propriété à "

--weapon_lockpick
sweps.lockpick.printName         			= "Crochet"

sweps.extinguisher.printName 				= "Extincteur"

--weapon_mono_citizen_id
sweps.citizenid.printName        = "Carte d'identité civile"

--weapon_mono_department
sweps.departmentTablet.printName = "Tablette de la police"
sweps.departmentTablet.desc      = "Utilisez-le pour planer" -- ? ...

--weapon_fireaxe
sweps.fireaxe.printName 		= "Hache de pompier"

-- weapon_defibrillator
sweps.defibrillator.printName 	= "Défibrillateur"

--weapon_mono_fists
sweps.fists.printName            = "Mains"
sweps.fists.instructions         = "Attaque principal: Coup de poing\nAttaque secondaire: Frapper/Ramasser"
sweps.fists.purpose              = "Frapper des choses et toquer aux portes."

-- Medkit
sweps.medkit.printName 						= "Premiers secours"

sweps.medkit.gui.healing					= "GUÉRISON"
sweps.medkit.gui.sterilizing 				= "STÉRILISATION\nDES PLAIES"
sweps.medkit.gui.cauterizing 				= "CAUTÉRISATION\nDES PLAIES"
sweps.medkit.gui.applyingBandages 			= "POSE DE\nBANDAGES"
sweps.medkit.gui.healingMe 					= "AUTO-MÉDICATION"
sweps.medkit.notif.playerInCombat			= "On ne peut pas soigner en étant au combat!"
sweps.medkit.notif.targetInCombat			= "La cible est au combat!"

--weapon_mono_police_id
sweps.policeid.printName   = "Carte d'identité policière"

--weapon_mono_tablet
sweps.monotablet.printName = "Mono-Tablet"

--weapon_pickaxe
sweps.pickaxe.printName    = "Pioche"
sweps.pickaxe.instructions = "Attaque primaire: Coup droit - Attaque secondaire : Coup gauche"

--weapon_radargun
sweps.radargun.printName   = "Pistolet radar"
sweps.radargun.purpose     = "Gauche pour scanner la vitesse"

--weapon_ramdoor
sweps.ramdoor.printName = "Bélier"
sweps.ramdoor.purpose = "Aide"

--weapon_taser
sweps.taser.printName = "Taser"
sweps.taser.log = "%s a tasé %s."

-- weapon_mono_vape
-- @TODO configure the flavor juices in the weapon_mono_vape.lua directly in the JuicyVapeJuices table
sweps.monovape.printName 					= "Cigarette électronique"
sweps.monovape.gui.loadedFlavorJuice 		= "Liquide arôme %s chargé"

sweps.monovape.juices.dew				= "Mountain Dew"
sweps.monovape.juices.cheetos				= "Cheetos"
sweps.monovape.juices.razzleberry				= "Tarte à la framboise"
sweps.monovape.juices.banana				= "Banane"
sweps.monovape.juices.blacklicorice				= "Réglisse noire"
sweps.monovape.juices.churro				= "Churros"
sweps.monovape.juices.skittles				= "Skittles"
sweps.monovape.juices.normal				= "Normal"

-- weapon_nightstick
sweps.nightstick.printName 					= "Matraque"
sweps.nightstick.log.knockOut 				= "%s a assommé %s avec une matraque de police."

-- weapon_policetape
sweps.policetape.printName 					= "Ruban de police"

sweps.policetape.gui.distance				=  "Distance"
sweps.policetape.gui.rightClickCancel		= "Clic droit pour annuler."
sweps.policetape.gui.ropeCantBeThatLong 	= "Ton ruban ne doit pas être si long!"
sweps.policetape.gui.pickedUpSegmentTape 	= "Vous avez récupéré ce segment de ruban."
sweps.policetape.gui.clearedAllTapes 		= "Vous avez retiré tous vos rubans de police."
sweps.policetape.gui.maxTapes 				= "Vous avez atteint la limite maximale de rubans!"
sweps.policetape.log.hasPlacedRope 			= "%s a placé un ruban de police à %s"
sweps.policetape.log.removedPoliceTape 		= "%s a retiré son ruban de police à %s"
sweps.policetape.log.removedOtherPoliceTape = "%s a retiré son ruban de police de %s à %s"
sweps.policetape.log.clearedAllTapes		= "%s a retiré tous ses rubans de police."

-- weapon_radargun
-- @TODO maybe we should add the speed unit in this file

-- weapon_ticket
sweps.weaponticket.printName 					= "Carnet d'amendes"
sweps.weaponticket.purpose 						= "Utilisez-le pour écrire des amendes, etc."

sweps.weaponticket.gui.cantGiveTicketToPlayer = "Vous ne pouvez pas donner d'amende à ce joueur avant %s secondes."
sweps.weaponticket.gui.cantGiveTicket		  = "Vous ne pouvez pas donner d'amende avant %s secondes."
sweps.weaponticket.gui.officierIssuedIt 	  = "Officier qui l'a délivrée"
sweps.weaponticket.gui.timeSinceIssued		  = "Temps depuis l'émission"
sweps.weaponticket.gui.lawyer 				  = "Avocat"
sweps.weaponticket.gui.selectTicket 		  = "Sélectionnez une amende"
sweps.weaponticket.gui.police 				  = "Police"
sweps.weaponticket.gui.minutesAgo 			  = "Il y a %s minute(s)"
sweps.weaponticket.gui.resolve				  = "Résoudre"
sweps.weaponticket.gui.ticket				  = "Amende"
sweps.weaponticket.gui.payTicket			  = "Payer cette amende ($%s)"
sweps.weaponticket.gui.pay					  = "Payer"
sweps.weaponticket.gui.deny					  = "Refuser"
sweps.weaponticket.gui.appealTicketIfUnfair   = "Faites appel pour cette amende si vous croyez qu'elle est injuste.."
sweps.weaponticket.gui.smallViolation 		  = "Première/Petite infraction"
sweps.weaponticket.gui.repeated 			  = "Répétée/Modérée"
sweps.weaponticket.gui.excessive			  = "Excessive/Grave"
sweps.weaponticket.gui.appeal 				  = "Faire appel"
sweps.weaponticket.gui.amountToPay 			  = "Montant à payer" -- @TODO money symbol line 246
sweps.weaponticket.gui.ticketsFromHim 		  = "Amendes de sa part"
sweps.weaponticket.gui.insertText 			  = "<Insérer du texte>"
sweps.weaponticket.gui.beMoreDescriptive 	  = "Vous devez être plus descriptif dans la raison."
sweps.weaponticket.gui.insertNumber 		  = "Vous devez insérer un nombre!"
sweps.weaponticket.gui.ok 					  = "Ok"
sweps.weaponticket.gui.cantFineMore 		  = "Vous ne pouvez pas verbaliser au-delà de $4,500!" -- @TODO maybe adding this to a configuration or something like that line 313 and 227
sweps.weaponticket.gui.cantFineMoreThan		  = "Vous ne pouvez pas verbaliser au-delà de %s!"
sweps.weaponticket.gui.areYouSure 			  = "Êtes-vous sûr de vouloir donner cette amende?"
sweps.weaponticket.gui.cancel 				  = "Annuler"
sweps.weaponticket.gui.accept 				  = "Accepter"
sweps.weaponticket.gui.id 					  = "Carte d'identité"
sweps.weaponticket.gui.kind 				  = "Genre"
sweps.weaponticket.gui.invalidReason		  = "Vous ne pouvez pas émettre d'amendes avec la raison par défaut."
sweps.weaponticket.gui.targetName			  = "Nom"
sweps.weaponticket.gui.creatorName			  = "Officier"

-- weapon_Lawbook
sweps.lawbook.printName			  				= "Livre de droit"
sweps.lawbook.purpose 							= "Clic gauche pour ouvrir la dernière version du livre de droit"

-- weapon_trowel
sweps.weapontrowel.printName 				  = "Truelle de jardinage"
sweps.weapontrowel.gui.cantPlantHere 		  = "Vous ne pouvez pas planter ici."
sweps.weapontrowel.gui.tooClose 			  = "Vous essayez de planter trop près de quelque chose!"

-- weapon_watercan
sweps.watercan.printName = "Arrosoir"
sweps.watercan.purpose = "Une arme utilisée pour arroser les amas de terre"


--[[
	ENTITIES
--]]

-- areacreator
tools.areacreator.name = "Area Creator"
tools.areacreator.notDefined = "Not Defined"

-- nodecreator
tools.nodecreator.name = "Node Creator"
tools.nodecreator.instructions = "LMB: Create node - RMB: Set Path"
tools.nodecreator.selectNode = "Select another node"

-- permaprops
tools.permaprops.name = "PermaProps"
tools.permaprops.description = "Save a props permanently"
tools.permaprops.instructions = "LeftClick: Add RightClick: Remove Reload: Update"
tools.permaprops.adminCan = "Admin can touch permaprops"
tools.permaprops.adminCant = "Admin can't touch permaprops !"
tools.permaprops.superAdminCan = "SuperAdmin can touch PermaProps"
tools.permaprops.superAdminCant = "SuperAdmin can't touch permaprops !"
tools.permaprops.onlySA = "Only Super Admin can touch permaprops"
tools.permaprops.inValidEnt = "That is not a valid entity !"
tools.permaprops.isPlayer = "That is a player !"
tools.permaprops.already = "That entity is already permanent !"
tools.permaprops.saved = "You saved %s with model %s to the database."
tools.permaprops.erased = "You erased %s with a model of %s from the database."
tools.permaprops.reload = "You have reload all PermaProps !"
tools.permaprops.updated = "You updated the %s you selected in the database."
tools.permaprops.saveProps = "Save a props for server restarts\nBy Malboro"
tools.permaprops.header = "------ Configuration ------"
tools.permaprops.header2 = "-------- Functions --------"
tools.permaprops.removeAll = "Remove all PermaProps"
tools.permaprops.erasedAll = "You erased all props from the map"

-- propertycreator
tools.propertycreator.name = "Créateur de propriété"
tools.propertycreator.desc = "Modifier les données de la propriétée"
tools.propertycreator.propertyName = "Nom : "
tools.propertycreator.hammerIdentifier = "ID Hammer : "
tools.propertycreator.propertyUID = "UID : "
tools.propertycreator.propertyCategory = "Catégorie : "
tools.propertycreator.propertyPrice = "Prix : "
tools.propertycreator.propertyJobCat = "Catégorie du travail : "
tools.propertycreator.propertyElectricMode = "Nœud électrique : "
tools.propertycreator.noDataGiven = "Aucune donnée n'a été donnée"
tools.propertycreator.noDataCamGiven = "Aucune donnée caméra n'a été donnée"
tools.propertycreator.noDataBoundGiven = "Aucune limite n'a été donnée"
tools.propertycreator.noDataSendGiven = "Aucune donnée d'envoi n'a été donnée"
tools.propertycreator.noDataIDGiven = "Aucun ID n'a été donné"
tools.propertycreator.noDataNameGiven = "Aucun nom n'a été donné"
tools.propertycreator.noDataPriceGiven = "Aucun prix n'a été donné"
tools.propertycreator.noDataCatGiven = "Aucune catégorie n'a été donné"
tools.propertycreator.noDataJCatGiven = "Aucune catégore de travail n'a été donné sur la propriété"
tools.propertycreator.savedCon = "%s sauvegardé en tant que configuration existante"
tools.propertycreator.failedSave = "Echec de la sauvegarde de %s. ERREUR : "
tools.propertycreator.noError = "aucune erreur"
tools.propertycreator.saved = "Sauvegardé "
tools.propertycreator.reload = "Recharger: Ouvrir l'éditeur de propriété"
tools.propertycreator.leftC = "Clic gauche : "
tools.propertycreator.rightC = "Clic droit : "
tools.propertycreator.none = "Aucun"

tools.propertycreator.gui.propertyEditor = "Créateur de propriété"
tools.propertycreator.gui.editBound = "Editer les limites"
tools.propertycreator.gui.setMaxPos = "Régler la position max"
tools.propertycreator.gui.setMinPos = "Régler la position min"
tools.propertycreator.gui.editDoors = "Modifier les portes"
tools.propertycreator.gui.noDoorFound = "Aucune porte trouvée. Soyez sûr de viser une porte"
tools.propertycreator.gui.addedDoor = "Porte ajoutée à la propriété"
tools.propertycreator.gui.unableToRmove = "Impossible de retirer la position de la porte de la propriété. La position de la porte n'a pas été trouvée dans la table de la propriété"
tools.propertycreator.gui.addDoor = "Ajouter porte"
tools.propertycreator.gui.removeDoor = "Retirer porte"
tools.propertycreator.gui.editCam = "Modifier caméra"
tools.propertycreator.gui.setCam = "Régler position caméra"
tools.propertycreator.gui.editPower = "Modifier les prises électrique"
tools.propertycreator.gui.addOutlet = "Ajouter prise"
tools.propertycreator.gui.removeOutlet = "Retirer prise"
tools.propertycreator.gui.editBins = "Modifier les poubelles"
tools.propertycreator.gui.addBin = "Ajouter poubelle"
tools.propertycreator.gui.removeBin = "Retirer poubelle"
tools.propertycreator.gui.editPrisoner = "Modifier les toilettes des prisoniers"
tools.propertycreator.gui.addPrisoner = "Ajouter toilettes prisoniers"
tools.propertycreator.gui.removePrisoner = "Retirer toilettes prisoniers"
tools.propertycreator.gui.editTask = "Modifier les tâches des prisoniers"
tools.propertycreator.gui.addTask = "Ajouter une tâche aux prisoniers"
tools.propertycreator.gui.removeTask = "Supprimer une tâche des prisoniers"
tools.propertycreator.gui.saveProperty = "Sauvegarder les changements de la propriété"
tools.propertycreator.gui.deselectProperty = "Quitter propriété"
tools.propertycreator.gui.addChildren = "Ajouter des maisons enfant"
tools.propertycreator.gui.setParentID = "Régler l'ID parent"
tools.propertycreator.gui.putParentID = "Mettre ID parent ici."
tools.propertycreator.gui.setChildrenCount = "Régler le nombre d'enfants"
tools.propertycreator.gui.setChildrenCountBelow = "Régler le nombre d'enfant des propriétés en dessous."
tools.propertycreator.gui.editParentID = "Modifier ID parent"
tools.propertycreator.gui.putNewParentID = "Mettez l'ID parent ici."
tools.propertycreator.gui.quickPropertyPicker = "Menu rapide de sélection de propriété"
tools.propertycreator.gui.automaticDoors = "Régler les portes automatiquement"

-- textscreen
tools.textscreen.name = "3D2D Textscreen"
tools.textscreen.desc = "Créez un textscreen avec plusieurs lignes, couleurs de police et tailles."
tools.textscreen.instructions = "Clic gauche: créer un textscreen Clic droit: mettre à jour le textscreen avec les paramètres"
tools.textscreen.undoneText = "Undone textscreen"
tools.textscreen.textScreens = "Textscreens"
tools.textscreen.cleanedUpText = "Cleaned up all textscreens"
tools.textscreen.hitLimit = "Vous avez atteint la limite des textscreens !"

tools.textscreen.gui.reset = "Réinitialiser"
tools.textscreen.gui.resetColors = "Réinitialiser les couleurs"
tools.textscreen.gui.resetSizes = "Réinitialiser les tailles"
tools.textscreen.gui.resetText = "Réinitialiser les boites de texte"
tools.textscreen.gui.resetEverything = "Tout réinitialiser"
tools.textscreen.gui.resetLine = "Réinitialiser la ligne"
tools.textscreen.gui.resetAllLines = "Réinitialiser toutes les lignes"
tools.textscreen.gui.line = "Ligne "
tools.textscreen.gui.fontColor = " couleur de la police"
tools.textscreen.gui.fontSize = "Taille de la police"

--[[
	ENTITIES
--]]

-- ent_dumpster
entities.dumpster.waitBeforeDiving = "Vous devez patienter %s avant de fouiller à nouveau dans les bennes à ordures."
entities.dumpster.dumpsterSearched = "Cette benne a déjà été fouillée récemment. Vous devez patienter %s"
entities.dumpster.foundNothing = "Vous n'avez rien trouvé dans cette benne à ordures."
entities.dumpster.foundX = "Vous avez trouvé un(e) %s dans cette benne à ordures !"
entities.dumpster.dumpsterDiving = "Fouille en cours..."

-- sent_workbench
entities.workbench.printName                = "Établi"

-- sent_woodworkbench
entities.woodworkbench.printName            = "Établi de menuiserie"

-- sent_textscreen
entities.textscreen.printName               = "Text Screen"
entities.textscreen.editTextTitle           = "Éditer Text Screen"
entities.textscreen.editColors              = "Éditer couleurs"
entities.textscreen.abort                   = "Abandonner"
entities.textscreen.setColor                = "Définir la couleur"
entities.textscreen.colorChanged            = "Le Text Screen a changé de couleur"
entities.textscreen.edit                    = "Éditer"
entities.textscreen.textLengthError         = "Texte trop court/long (<2 >18)"
entities.textscreen.pressEToChange          = "Appuyez sur E pour modifier"

-- sent_stretcher
entities.stretcher.printName                = "Civière"
entities.stretcher.patientStabilise         = "Le patient doit d'abord être stabilisé pour faire cela!"
entities.stretcher.ledByAnyone              = "Le civière ne doit pas être conduit par quelqu'un pour faire cela!"
entities.stretcher.strappedOntop            = "%s a sanglé le corps de %s sur un civière"
entities.stretcher.unstrapped               = "%s a détaché le corps de %s d'un civière"
entities.stretcher.sameVehicle              = "Vous devez remettre la civière dans le même véhicule d'où elle vient!"
entities.stretcher.backDoorClosed           = "Les portes arrières sont fermées! Ouvrez-les d'abord."
entities.stretcher.notEMSPolice             = "Vous ne pouvez pas faire ça si vous n'êtes EMS/Policier."
entities.stretcher.notYourVehicle           = "Ce n'est pas votre véhicule!"
entities.stretcher.notYourAmbulance         = "Ce n'est pas votre ambulance!"
entities.stretcher.blockingDeploy           = "Il y a quelque chose qui bloque la position de déploiement!"

-- sent_road_sign
entities.roadsign.printName                 = "Panneau de signalisation policière"
entities.roadsign.policeDepartment          = "Service de police"
entities.roadsign.mustBePolice              = "Vous devez faire partie de la police pour éditer ce panneau"
entities.roadsign.pickingUp                 = "RAMASSAGE"
entities.roadsign.policeSign                = "Panneau de police"
entities.roadsign.update                    = "Actualiser"

-- sent_road_sign_small
entities.roadsignsmall.printName            = "Panneau de signalisation policière - Petit"
entities.roadsignsmall.roadClosed           = "ROUTE FERMÉE"
entities.roadsignsmall.memberPolice         = "Vous devez faire partie de la police pour éditer ce panneau"
entities.roadsignsmall.policeSign           = "Panneau de police"
entities.roadsignsmall.update               = "Actualiser"

entities.roadsignsmall.log.changeText		= "%s changed a roadsign's text to: %s."

-- sent_residential_extension_outlet
entities.extensionoutlet.printName          = "Extension de prise résidentielle"
entities.extensionoutlet.errorAttempt       = "Tentative d'utilisation de SetBreaker() sur sent_residential_extension_outlet"

-- sent_repair_bench
entities.repairbench.printName              = "Établi de réparation"

-- sent_property_bin
entities.propertybin.printName              = "Poubelle"
entities.propertybin.garbageCan             = "Benne à ordures"
entities.propertybin.percentFull            = "% pleine"

-- sent_power_source
entities.powersource.printName              = "Source d'Énergie de la Propriété"

-- sent_power_outlet
entities.poweroutlet.printName              = "Prise Électrique"

-- sent_power_converter
entities.powerconverter.printName           = "Transformateur (à double sens)"

-- sent_pot
entities.pot.printName                      = "Pot"
entities.pot.weedBag                        = "Sac de Weed"
entities.pot.useOtherPots                   = "Vous ne pouvez pas utiliser les pots appartenant à quelqu'un d'autre."
entities.pot.dehydrated                     = "Déshydraté"
entities.pot.thirsty                        = "Assoiffé"
entities.pot.neutral                        = "Neutre"
entities.pot.healthy                        = "Sain"
entities.pot.insertSoil                     = "Insérer terreau"
entities.pot.insertSeed                     = "Insérer graine"
entities.pot.harvest                        = "Récolter"
entities.pot.properSeeds                    = "Vous n'avez pas de graines à planter."
entities.pot.gardeningSoil                  = "Vous n'avez pas de terreau à utiliser."
entities.pot.plantedSeed                    = "Vous avez planté une graine dans le pot."
entities.pot.filledPot                      = "Vous avez rempli le pot avec du terreau."
entities.pot.gardeningPot                   = "Pot de jardinage"
entities.pot.alreadySoil                    = "Le pot a déjà du terreau"
entities.pot.soilAvailable                  = "Vous disposez de %s de terreau à utiliser"
entities.pot.seedsAvailable                 = "Vous avez %s graine%s prête(s) à être plantées"
entities.pot.plant                          = "Planter"
entities.pot.properSeeds                    = "Vous n'avez pas de graines à planter." -- @TODO Remove this line? duplication of line 384
entities.pot.addSoil                        = "Ajouter du terreau"

-- sent_plug
entities.plug.printName                     = "Prise"

-- sent_money
entities.money.printName                    = "Argent"
entities.money.youGrabbed                   = "Tu as pris $"
entities.money.pickedUp                     = " a récupéré "

-- sent_metalworkbench
entities.metalworkbench.printName           = "Établi de métallurgie"

-- sent_metalworkbench
entities.metalworkbench.printName           = "Établi de métallurgie" -- @TODO Remove this line? duplication of line 405

-- sent_lsd
entities.lsd.printName                      = "LSD"

-- sent_lsd_pyro
entities.lsdpyro.printName                  = "Bec Bunsen"

-- sent_lsd_powder
entities.lsdpowder.printName                = "Poudre de LSD"

-- sent_lsd_paper
entities.lsdpaper.printName                 = "Buvard"

-- sent_lsd_gas
entities.lsdgas.printName                   = "Bidon d'essence"

-- sent_lsd_freezer
entities.lsdfreezer.printName               = "Réfrigérateur"
entities.lsdfreezer.holdPickUp              = "(Maintenir) Ramasser "
entities.lsdfreezer.insertFlask             = "Insérez une fiole"
entities.lsdfreezer.takeOut                 = "Je suis gelé ! Sortez-moi de là"

-- sent_lsd_flask
entities.lsdflask.printName                 = "Fiole"
entities.lsdflask.stage1                    = "Insérez de la poudre"
entities.lsdflask.stage2                    = "Besoin de chaleur"
entities.lsdflask.stage3                    = "Attendez que le brûleur soit terminé"
entities.lsdflask.stage4                    = "Insérez du liquide"
entities.lsdflask.stage5                    = "Remue-moi doucement"
entities.lsdflask.stage6                    = "Refroidis-moi"
entities.lsdflask.stage7                    = "Mettez-moi sur un buvard"

-- sent_lsd_flank_support
entities.lsdflanksupport.printName          = "Support de fiole"
entities.lsdflanksupport.fireStarted        = "[Quête Feu] Un incendie de LSD a commencé !"
entities.lsdflanksupport.insertBurner       = "Insérez un brûleur"
entities.lsdflanksupport.insertGas          = "Insérez une bonbonne de gaz"
entities.lsdflanksupport.insertFlask        = "Insérez une fiole"
entities.lsdflanksupport.progress           = "Progression: %"

-- sent_lsd_bottle
entities.lsdbottle.printName                = "Bouteille de LSD"

-- sent_infinite_power_source
entities.infinitepower.printName            = "Source d'énergie infinie"
entities.infinitepower.powerSource          = "Source d'énergie"
entities.infinitepower.infinite             = "Infinie"

-- sent_fire_spark
entities.firespark.printName                = "Drogue"

-- sent_fire_source
entities.firesource.printName               = "Feu"

-- sent_factory_furnace
entities.factoryfurnace.printName           = "Chaudière"
entities.factoryfurnace.busy                = "Usine occupée"
entities.factoryfurnace.takeAllComp         = "Vous devez enlever tous les composants avant de ramasser ceci."
entities.factoryfurnace.readyForUse         = "Prêt à l'emploi"
entities.factoryfurnace.needsConveyor       = "Nécessite un convoyeur"
entities.factoryfurnace.needsPower          = "Nécessite une source d'énergie"

-- sent_factory_crusher
entities.factorycrusher.printName           = "Broyeur"
entities.factorycrusher.busyCrushing        = "Le broyeur est occupé à écraser un objet."
entities.factorycrusher.takeAllBefore       = "Vous devez sortir tout le contenu avant de ramasser ceci."
entities.factorycrusher.preciousMats        = "Un récipient pour contenir les matériaux précieux donnés par le broyeur."
entities.factorycrusher.turnOffNotif        = "Le broyeur s'éteindra une fois qu'il aura terminé."
entities.factorycrusher.crushing            = "Broyage "
entities.factorycrusher.insertChunks        = "Insérer pièces"
entities.factorycrusher.unplugged           = "Débranché"

-- sent_factory_conveyor
entities.factoryconveyor.printName          = "Convoyeur"
entities.factoryconveyor.factoryBusy        = "L'usine est occupée"
entities.factoryconveyor.turnMeOn           = "Allumez-moi"
entities.factoryconveyor.active             = "Actif"
entities.factoryconveyor.secs               = " sec"

-- sent_factory_cell
entities.factorycell.printName              = "Cellule de puissance"

-- sent_factory_box
entities.factorybox.printName               = "Boite de stockage"
entities.factorybox.containerHold           = "Un conteneur pour ranger les articles d'usine fournis par la chaudière."
entities.factorybox.takeAllItems            = "Vous devez sortir tous les articles de la boîte avant de les ramasser."

-- sent_factory_base
entities.factorybase.printName              = "Base de l'usine"

-- sent_drug_bag
entities.drugbag.printName                  = "Sac de Weed"

-- sent_door_charge
entities.doorcharge.printName               = "Charge explosive"

-- sent_cooktable
entities.cooktable.printName                = "Cuisinière"

-- sent_cocaine_stove
entities.cocainestove.printName             = "Gazinière"
entities.cocainestove.gas                   = "Gaz: "
entities.cocainestove.plates                = "Plaques: "
entities.cocainestove.insertGas             = "Insérez bonbonne de gaz"

-- sent_cocaine_pot
entities.cocainpot.printName                = "Casserole"
entities.cocainpot.temperature              = "Température: "
entities.cocainpot.waitAbout                = "Attendez environ %d secondes"

-- sent_cocaine_packingbox
entities.cocainepackingbox.printName        = "Boîte d'emballage"
entities.cocainepackingbox.cocaLeaves       = "Feuilles de coca: "
entities.cocainepackingbox.boxFull          = "La boîte est pleine"
entities.cocainepackingbox.packing          = "Emballage"
entities.cocainepackingbox.notEnoughLeaves  = "Vous n'avez pas assez de feuilles."
entities.cocainepackingbox.boxFullEmpty     = "La boîte est pleine, veuillez la vider."
entities.cocainepackingbox.noLeaves         = "Tu n'as aucune feuilles."

-- sent_cocaine_kerosene
entities.cocainkerosene.printName           = "Kérosène"
entities.cocainkerosene.shakeIt             = "Secouez-le: "
entities.cocainkerosene.readyToUse          = "Prêt à l'emploi"

-- sent_cocaine_jerrycan
entities.cocainejerrycan.printName          = "Jerrican"
entities.cocainejerrycan.ready              = "Prêt"
entities.cocainejerrycan.pleaseWait         = "Veuillez patienter %d Secondes"

-- sent_cocaine_gas
entities.cocainegas.printName               = "Gaz"
entities.cocainegas.filled                  = "Rempli: "

-- sent_cocaine_drafted
entities.cocainedrafted.printName           = "Feuilles déséchées"
entities.cocainedrafted.shakeIt             = "Secouez-le: "
entities.cocainedrafted.ready               = "Prêt à l'emploi"
entities.cocainedrafted.pleaseWait          = "Veuillez patienter %d Secondes"

-- sent_cocaine_acid
entities.cocaineacid.printName              = "Acide sulfurique"
entities.cocaineacid.pleaseWait             = "Veuillez patienter %d Secondes"
entities.cocaineacid.ready                  = "Prêt à l'emploi"

-- sent_christmas_tree
entities.christmastree.printName            = "Arbre de Noël"

-- sent_brewingbarrelbench
entities.brewingbarrelbench.printName       = "Baril de brassage"

-- sent_bountyboard
entities.bountyboard.printName              = "Tableau de primes"
entities.bountyboard.placeBounty            = "Placer une prime"
entities.bountyboard.name                   = "Nom"
entities.bountyboard.reward                 = "Récompense"
entities.bountyboard.dateAssigned           = "Date fixée"

-- sent_base_lsd
entities.baselsd.printName                  = "Drogue"

-- base_item_stackable
entities.baseitemstackable.printName        = "Objet de base"
entities.baseitemstackable.beingArrested    = "Vous ne pouvez pas ramasser d'articles pendant votre arrestation."
entities.baseitemstackable.inventorySpace   = "Vous n'avez pas assez d'espace dans votre inventaire pour acheter ça !"
entities.baseitemstackable.boughtItem       = "Vous avez acheté un article. %s ont été soustraits de votre portefeuille."
entities.baseitemstackable.shopFull         = "Cette boutique est déjà pleine !"
entities.baseitemstackable.dontHaveItem     = "Vous n'avez pas cet article dans votre inventaire !"
entities.baseitemstackable.onlyHadThis      = "Tu n'as que %s de cet item !"

--ent_atm_screen
entities.atmscreen.printName                = "ECRAN DE L'ATM"
entities.atmscreen.withdraw                 = "Retrait"
entities.atmscreen.withdrawAll				= "Tout retirer"
entities.atmscreen.deposit                  = "Dépôt"
entities.atmscreen.depositAll               = "Tout déposer"
entities.atmscreen.monoBank                 = "MONO-BANK"
entities.atmscreen.balance                  = "Solde: "
entities.atmscreen.accountHolder            = "Titulaire du compte: %s"
entities.atmscreen.fiftyUSD                 = "$50" -- idk what you expect me to use here
entities.atmscreen.fiveHundredUSD           = "$500"
entities.atmscreen.tenThousandUSD           = "$10000"
entities.atmscreen.clear                    = "C"
entities.atmscreen.lessThan                 = "<"
entities.atmscreen.USD                      = "$"
entities.atmscreen.accessDenied             = "Accès au distributeur refusé."
entities.atmscreen.insufficientFunds        = "Vous n'avez pas les fonds suffisants."
entities.atmscreen.UseWithdraw				= "Veuillez utiliser l'option de retrait pour cela."
entities.atmscreen.UseDeposit				= "Veuillez utiliser l'option de dépôt pour cela."
entities.atmscreen.confirm                  = "Confirmer"

--ent_atm
entities.atm.printName                      = "GAB"

--ent_atm_craftable
entities.atmcraftable.printName                      = "ATM créable"

--ent_boat_base
entities.boatbase.printName                 = "Base de bateau"
entities.boatbase.containerDesc             = "Un conteneur pour une cargaison dans la cale d'un bateau."

--ent_boat_motor_engine
entities.boatmotorengine.printName          = "Moteur de bateau"

--ent_boat_rhib
entities.boatrhib.printName                 = "Bateau semi-rigide"

--ent_boat_rowboat
entities.rowboat.printName                  = "Bateau à rames"


--ent_callsign_display
entities.callsigndisplay.printName          = "Tableau d'affichage des indicatifs radio"
entities.callsigndisplay.yourCallsign       = "Votre indicatif radio est %s"
entities.callsigndisplay.noCallsign         = "Vous n'avez pas d'indicatif radio"
entities.callsigndisplay.deactivate         = "Détective"

--ent_carspawnsign
entities.carspawnsign.warnning              = "— ATTENTION —" --written 2 years ago by kyle goodale not me
entities.carspawnsign.seriousInjury         = "Pour éviter des blessures graves,"
entities.carspawnsign.remain10ft            = "restez à au moins 3 mètres"
entities.carspawnsign.fromThisSign          = "de distance de ce signe..."
entities.carspawnsign.ty                    = "Merci."

--ent_circle_trigger
entities.circletrigger.printName            = "Point de livraison"
entities.circletrigger.depotPrintName       = "Point de retour"

--ent_busstop_trigger
entities.busstoptrigger.printName			= "Arrêt de bus"

--ent_teleporter_trigger
entities.teleportertrigger.printName		= "Stand de tir"
entities.teleportertrigger.text				= "Validation de la carte d'identité"
entities.teleportertrigger.notAllowed		= "Vous n'êtes pas autorisé à accéder au stand de tir pour le moment."

--ent_detector_trigger
entities.detectortrigger.printName          = "Détecteur de métaux"

--ent_fire_hydrant
entities.firehydrant.printName              = "Bouche d'incendie"

--ent_fire_pumphose
entities.pumphose.printName                 = "Pompe à tuyaux"
entities.pumphose.detachingHose             = "DETACHE LE TUYAU"

--ent_fire_waterhose
entities.waterhose.printName                = "Tuyau"
entities.waterhose.unequipFirst             = "Déséquipez votre tuyau d'abord !"

--ent_garage_tv
entities.garagetv.printName                 = "TV du Garage"

--ent_garage
entities.garage.printName                   = "Garage"
entities.garage.modeDisabled                = "Ce mode est actuellement désactivé. Veuillez utiliser le système de véhicules de série en appuyant sur F3."

--ent_gascan
entities.gascan.printName                   = "Bidon d'essence"
entities.gascan.pourThis                    = "Versez ceci dans votre véhicule."

--ent_gaspump
entities.gaspump.printName                  = "Pompe à essence"
entities.gaspump.poor                       = "Vous n'avez pas les fonds nécessaires!"
entities.gaspump.needToWait                 = "Vous devez attendre %s secondes!"

--ent_house_alarm
entities.housealarm.printName               = "Alarme domestique"

--ent_instanced_apt_circle
entities.instancedaptcircle.printName       = "Texte de test"

--ent_invisible_window
entities.invisibleWindow.printName          = "Fenêtre invisible"

--ent_itembox
entities.itembox.printName                  = "Casier Premium"
entities.itembox.restricted                 = "Réservé aux membres Premium uniquement. (!store)"

--ent_map
entities.map.printName                      = "Carte"
entities.map.cityHall                       = "Hôtel de ville"
entities.map.superMarket                    = "Supermarché"
entities.map.policeStation                  = "Poste de Police"
entities.map.fireStation                    = "Caserne de Pompiers"
entities.map.clothingStore                  = "Magasin de Vêtements"
entities.map.generalHospital                = "Hôpital Général"
entities.map.modShop                        = "Mod Shop"
entities.map.realtyOffice                   = "Immeubles de bureaux"
entities.map.realEstateOffice               = "Agence immobilière"
entities.map.carDealership                  = "Concessionnaire"
entities.map.cityBank                       = "Banque Municipale"
entities.map.miningRavine                   = "Ravin minier"
entities.map.transitCentre                  = "Centre de Transit"
entities.map.impoundLot                     = "Mise en fourrière"
entities.map.vehicleImpound                 = "Fourrière"
entities.map.fishingDock                    = "Quai de Pêche"
entities.map.biteRestaurant                 = "Restaurant"
entities.map.woodcuttingForest              = "Forêt"
entities.map.gasStation                     = "Station Essence"
entities.map.gasStationCity                 = "Station Essence (Ville)"
entities.map.gasStationCountry              = "Station Essence (Campagne)"
entities.map.gasStationIndustiral           = "Station Essence (Zone Industrielle)"
entities.map.deliveryDepot                  = "Entrepôt de Livraison"
entities.map.drugAlley                      = "Ruelle de la drogue"
entities.map.cityMines                      = "Mines Municipales"
entities.map.highEndMines                   = "Mines haut de gamme"
entities.map.fishingStore                   = "Magasin de pêche"
entities.map.casino                         = "Casino"
entities.map.monoRailStop                   = "Station Mono-Rail"
entities.map.gunStore                       = "Armurerie"
entities.map.hardwareStore                  = "Quincaillerie"
entities.map.woodCuttingPark                = "Parc forestier"
entities.map.marketStreet                   = "Rue du Marché"
entities.map.garage                         = "Garage"
entities.map.cityGarage                     = "Garage Municipal"
entities.map.miningRow                      = "Exploitation Minière"
entities.map.woodcuttingRow                 = "Exploitation Forestière"
entities.map.resourceHaven                  = "Havre de ressource"
entities.map.mineshaft                      = "Puits de mine"
entities.map.hospital                       = "Hôpital"
entities.map.mechanicShop                   = "Magasin de Mécanique"
entities.map.automobileDealer               = "Concessionnaire Automobile"
entities.map.drugDealers                    = "Trafiquants de drogue"
entities.map.truckersCo                     = "Cie. de Transport Routier"
entities.map.deliveryService                = "Services de Livraison"
entities.map.mexiGrill                      = "Mexi Grill"
entities.map.ronnies						= "Chez Ronnie"
entities.map.bank                           = "Banque"
entities.map.lake                           = "Lac"
entities.map.truenorthCounty                = "Comté de Truenorth"
entities.map.pineCounty                		= "Comté de pin"
entities.map.monofordCounty					= "Comté de Monoford"
entities.map.truenorthCentral               = "Truenorth Centre"
entities.map.taxiCompany                    = "Compagnie de Taxi"
entities.map.openMap                        = "Ouvrir la carte!"
entities.map.MonoMap                        = "MONO-MAP"
entities.map.legend                         = "LÉGENDE"
entities.map.you                            = "Vous"
entities.map.setDestination                 = "Définir la destination"
entities.map.mcc                			= "Convention Center"
entities.map.applianceStore                	= "Boutique d'Appareil"

--ent_metal_detector
entities.metalDetector.printName            = "Détecteur de métaux"

--ent_police_database
entities.policeDatabase.printName           = "Base de données de la police"
entities.policeDatabase.accessPoliceDB      = "Accès à la base de données de la police"
entities.policeDatabase.noAccess            = "Inaccessible"

--ent_police_jailer
entities.policeJailer.printName             = "Ordinateur de police"
entities.policeJailer.accessPolicePC        = "Accès à l'ordinateur de police"
entities.policeJailer.noAccess              = "Inaccessible"

--ent_prisoner_toiler
entities.prisonerToilet.printName           = "Brosse à WC de prison"
entities.prisonerToilet.digIn               = "Fouillez pour avoir une surprise"
entities.prisonerToilet.ewDisgusting        = "Ew, Dégoûtant!"
entities.prisonerToilet.looksEmpty          = "Les toilettes semblent vides..."
entities.prisonerToilet.noWay               = "Ugh, je ne peux même pas imaginer toucher ces toilettes!"
entities.prisonerToilet.cannotSearch        = "Vous ne pouvez pas fouiller les toilettes à la recherche de crochets quand il n'y a pas de gardiens de prison en service.."
entities.prisonerToilet.alreadyHave         = "J'ai déjà un crochet, pourquoi je le ferais ça?"
entities.prisonerToilet.startedDigging      = "%s a commencé à fouiller le contenu des toilettes."
entities.prisonerToilet.dangIt              = "J'ai trouvé un crochet de serrure mais il a glissé dans les toilettes.... Merde, c'est pas vrai."
entities.prisonerToilet.dugUpLockpick       = "Vous avez trouvé un crochet de serrure! Super!"
entities.prisonerToilet.foundNothing        = "Vous n'avez rien trouvé."
entities.prisonerToilet.diggingThrough      = "CREUSE À TRAVERS"

--ent_prisoner_task
entities.prisonerTask.printName 			= "Tâches des prisonniers"
entities.prisonerTask.notif.notYou 			= "Par chance, cette tâche n'est pas pour vous."
entities.prisonerTask.notif.justDone 		= "Cette tache vient d'être terminé. Trouvez en une autre, ou alors patientez."
entities.prisonerTask.notif.cooldown 		= "Patientez %s secondes avant de faire une autre tâche de prisonnier."

entities.prisonerTask.laundry.hintText 		= "Nettoyer le linge sale."
entities.prisonerTask.laundry.actionText 	= "Nettoyage du linge..."
entities.prisonerTask.dishes.hintText 		= "Nettoyer la vaisselle crade."
entities.prisonerTask.dishes.actionText 	= "Faire la vaisselle..."
entities.prisonerTask.cooking.hintText 		= "Cuisiner de méchants plats."
entities.prisonerTask.cooking.actionText 	= "Cuisine..."
entities.prisonerTask.fallbackHintText 		= "Effectuer une tâche de prisonnier."

--ent_respawner
entities.respawner.printName                = "Casier d'Équipement"
entities.respawner.cannotOpen               = "Vous ne pouvez pas ouvrir ce casier."
entities.respawner.readyToUse               = "Prêt à l'emploi"
entities.respawner.seconds                  = "secondes"
entities.respawner.noAccess                 = "Inaccessible"

--ent_source_base
entities.sourceBase.printName               = "Ressource de Base"
entities.sourceBase.gatherWeaponName        = "Arme"
entities.sourceBase.cannotGather            = "Vous ne pouvez pas récolter alors que vous n'avez pas débauché de votre métier actuel!"
entities.sourceBase.noMoreResources         = "Cette source n'a pas plus de ressources"
entities.sourceBase.skillLevelRequired      = "Niveau %s %s requis!"
entities.sourceBase.source                  = "Source de %s"
entities.sourceBase.sourceAvailable         = "Disponible %s: %s%%"
entities.sourceBase.level                   = "Niveau %s"

--ent_source_rock
entities.sourceRock.printName               = "Rocher"
entities.sourceRock.resourceName            = "Pierre"
entities.sourceRock.cannotMine              = "Vous ne pouvez pas miner alors que vous n'avez pas débauché de votre métier!"
entities.sourceRock.noMoreStone             = "Ce rocher n'a pas plus de pierre"
entities.sourceRock.higherSkillLevel        = "Vous avez besoin d'un niveau de compétence plus élevé pour exploiter ce minerai!"

--ent_source_tree
entities.sourceTree.printName               = "Arbre"
entities.sourceTree.resourceName            = "Bois"
entities.sourceTree.gatherWeaponName        = "Hache"
entities.sourceTree.messageEmployed         = "Vous ne pouvez pas couper de bois alors que vous n'avez pas débauché de votre métier actuel!"
entities.sourceTree.noMoreWood              = "Cet arbre n'a plus de bois"

--ent_spawned_furniture
entities.spawnedFurniture.printName         = "Mobilier"

--ent_spawned_prop
entities.spawnedProp.printName              = "Objet"
entities.spawnedProp.pickingUp              = "RAMASSAGE"
entities.spawnedProp.health                 = "Santé: %s %"

--ent_spray_plate
entities.sprayPlate.printName               = "Passer du spray sur la plaque"
entities.sprayPlate.sprayingTag             = "SPRAY"
entities.sprayPlate.pleaseWait              = "Veuillez attendre avant d'appliquer à nouveau du spray, en cours de rechargement.."
entities.sprayPlate.tagYourCrew             = "Taguez votre crew sur les murs"

--ent_tapedummy
entities.tapeDummy.printName                = "Objet factice de bande"

--ent_towtruck_bed
entities.towtruck.reelTheWinch				= "Enroulez le treuil avant de verrouiller le lit."
entities.towtruck.slideTheBack				= "Faites glisser le lit vers l'arrière avant de verrouiller le lit."
entities.towtruck.attachToSelf				= "Interdit."

--ent_unarrest_trigger
entities.unarrestTrigger.printName          = "Déclencheur de libération"

--ent_use_trigger
entities.useTrigger.printName               = "Utiliser le déclencheur"

--ent_vehicle_oopvs  ( specifies it's disabled but adding it in just in case )
entities.vehicleOOPVS.printName             = "Vue d'un véhicule OOPVS"

--ent_vehicle_showcase
entities.vehicleShowcase.printName          = "Vitrine de Véhicules"

--ent_voting
entities.votingComputer.printName           = "Ordinateur de Vote"
entities.votingComputer.cannotVote          = "Vous ne pouvez pas voter avant la prochaine période électorale."
entities.votingComputer.mayorRating         = "Évaluation du maire: "
entities.votingComputer.generalTaxes        = "Taxes générales:"
entities.votingComputer.yourJobTaxes        = "Impôts sur le travail:"
entities.votingComputer.aveJobTaxes         = "Ave. Taxes sur les métiers:"

--ent_wheel_repair_kit
entities.wheelRepairKit.printName           = "Kit de réparation de roue"

--mono_camera
entities.monoCamera.printName               = "Caméra CCTV personnelle"

--monolith_npc
entities.monoNPC.printName                  = "PNJ"

--npc_injuredciv
entities.injuredCiv.printName               = "Citoyen blessé"
entities.injuredCiv.innocentCivilian        = "Ce civil est innocent."

--npc_robber
entities.npcRobber.criminal 				= "Criminel"
entities.npcRobber.mustSurrenderFirst       = "Le voleur a dû se rendre en premier."

--sent_base_gonzo
entities.baseGonzo.printName                = "Drogue"

-- the_weed_foil
entities.weedfoil.printName                 = "Machine à Cellophaner"

-- the_weed_foildryhash
entities.weedfoildryhash.printName          = "Emballer weed sèche"
entities.weedfoildryhash.foiledDryHash      = "Hachisch Cellophané"
entities.weedfoildryhash.consumeWeed        = "Vous ne pouvez pas consommer du hachisch cellophané."

-- the_weed_hash
entities.weedhash.printName                 = "Hachisch Humide"
entities.weedhash.wetWeed                   = "Vous ne pouvez pas encore consommer le hachisch, vous devez le faire sécher avant."

-- the_weed_jar
entities.weedjar.printName                  = "Bocal"

-- the_weed_microwave
entities.weedmicrowave.printName            = "Micro-ondes"
entities.weedmicrowave.connectApp           = "Vous devez brancher la prise de cet appareil à une prise secteur."

-- the_weed_plant
entities.weedplant.printName                = "Pot"
entities.weedplant.soilInPot                = "Vous avez mis de la terre dans le pot."
entities.weedplant.soilPack                 = "Vous devez avoir de la terre dans votre inventaire pour utiliser ça."
entities.weedplant.plantedSeed              = "Vous avez planté une graine dans le pot."
entities.weedplant.marjIsBigSeed            = "Vous avez besoin d'un paquet de graines de weed dans votre inventaire pour utiliser ça."
entities.weedplant.harvestSuccess           = "Vous avez récolté avec succès la plant de weed."
entities.weedplant.badTeam                  = "Vous n'avez pas le bon métier pour récolter de la weed."
entities.weedplant.somethingStrange         = "Vous sentez quelque chose d'étrange, vous prenez un moment pour vous éclaircir la gorge."

-- the_weed_tent
entities.weedtent.printName                 = "Tente"
entities.weedtent.sockResi                  = "Vous devez brancher la prise de cet appareil à une prise secteur."
entities.weedtent.toggleSystem              = "Vous avez enclenché le système "
entities.weedtent.on                        = "marche."
entities.weedtent.off                       = "arrêt."
entities.weedtent.bigBarrel                 = "Vous avez besoin d'un grand baril d'eau pour faire ça."
entities.weedtent.refillWater               = "Vous avez à nouveau rempli le réservoir d'eau."
entities.weedtent.waterSys                  = "Système d'Arrosage ("
entities.weedtent.ON                        = "MARCHE)"
entities.weedtent.OFF                       = "ARRÊT)"
entities.weedtent.waterLeft                 = "Eau restante"
entities.weedtent.liters                    = " litres"
entities.weedtent.refill                    = "Appuyez sur E sur l'écran pour remplir"
entities.weedtent.altE                      = "Maintenez ALT et E sur l'écran pour actionner l'arrosage"

--ent_dirt_pile
entities.dirtpile.printName                 = "Tas de terre"
entities.dirtpile.harvesting                = "RÉCOLTE"
entities.dirtpile.stealing                  = "VOL"
entities.dirtpile.noInventorySpace          = "Vous n'avez pas assez d'espace d'inventaire pour récolter ce produit!"
entities.dirtpile.soilQualityNoBueno        = "La qualité du sol est si mauvaise que vous avez détruit votre parcelle par accident!"
entities.dirtpile.stealingAttempt           = "/me tente de voler les récoltes d'un tas de terre." -- NOTE: Keep the /me prefix, this formats the text when typed in the chat
entities.dirtpile.stealingSeeds             = "VOL DE\nGRAINES"

--ent_easter_egg
entities.easteregg.printName                = "Oeuf de Pâques"
entities.easteregg.claimPrize               = "Appuyez sur\n\n\nEt réclamez votre prix!"
entities.easteregg.nothing                  = "Rien"
entities.easteregg.youReceived              = "Vous avez reçu %s"
entities.easteregg.congratsYouReceived      = "Félicitations ! Vous avez reçu\n%s"

--ent_wol_machine
entities.wolmachine.printName               = "Roue de la Chance"
entities.wolmachine.congratsYouWon          = "Félicitations! Vous venez de gagner %s"
entities.wolmachine.chatPrefix              = "[Roue de la Chance]"
entities.wolmachine.justWonTheJackpot       = "vient de gagner le jackpot de"
entities.wolmachine.onTheWheelOfLuck        = "sur la Roue de la Chance!"
entities.wolmachine.cantWhenWorking         = "Tu ne peux pas utiliser cette machine quand tu travailles!"
entities.wolmachine.currDisabled            = "La Roue de la Chance est désactivée pour le moment."
entities.wolmachine.pleaseWait              = "Veuillez attendre %s secondes avant de changer de machine."
entities.wolmachine.pleaseWaitTurn          = "Veuillez attendre que %s termine son tour ou cherchez une nouvelle machine."
entities.wolmachine.cannotAfford            = "Vous ne pouvez pas jouer sur cette machine, vous avez besoin d'au moins %s."

--ent_wol_reel
entities.wolreel.printName                  = "Bobine Roue de la Chance"

--the_weed_dirt
entities.theweeddirt.printName              = "Paquet de terre"
entities.theweeddirt.chargesLeft            = "%s - %s Charges Restantes"

--the_weed_dryhash
entities.theweeddryhash.printName           = "Hachisch"

--ent_pplant_barrel
entities.pplantBarrel.printName           	= "Baril de centrale électrique"
entities.pplantBarrel.extractAttempt        = "Siphonage des produits chimiques..."
entities.pplantBarrel.empty           		= "Ce baril semble être vide."
entities.pplantBarrel.startExtract			= "%s a commencé le siphonage des produits chimiques de ce baril."
entities.pplantBarrel.siphon				= "Pour siphoner les produits chimiques"
entities.pplantBarrel.noSiphon				= "Vous n'avez de pompe de siphon."
entities.pplantBarrel.police				= "Vous ne pouvez pas faire ceci."
entities.pplantBarrel.notEnough				= "Attention : Il n'y a pas d'agents de sécurité."
entities.pplantBarrel.broken				= "La pompe à siphon ne fonctionne plus."

-- Monoford keycard entities
entities.keypad.startSwiping				= "%s passe une carte-clé sur la serrure électronique de la porte."
entities.keypad.swipeAttempt				= "Insertion de la carte-clé..."

-- mailbox shit

entities.mailboxlocker.printName			= "Casier de boîte aux lettres"
entities.mailbox.printName					= "Boîte aux lettres"

-- DHeist entities
entities.shipCrate.foundX 					= "Vous avez trouvé un(e) %s dans la caisse !"
entities.shipCrate.foundBP 					= "Vous avez trouvé un plan %s dans la caisse !"
entities.robEntity.log.received				= "%s a reçu un(e) %s (x%s)"

-- Meth entities
entities.zmlab2.table.pickupFailed 			= "Vous ne pouvez pas ramasser cette table tant qu'elle contient des objets."
