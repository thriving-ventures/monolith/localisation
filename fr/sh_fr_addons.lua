--[[
	© 2023 Thriving Ventures AB, do not share, re-distribute or modify

	without permission of its author (gustaf@thrivingventures.com).
]]

--[[ @NOTE: Addon language configurations locations for the following addons are diplayed:
		- CasinoKit: casinokit/lua/casinokit/custom/langs
			- Also for each sub addon
		- DHeists: dheists_/lua/dheists/config/language
		- gsigns: gsigns/lua/metasign_config.lua
		- RRL: rrl/lua/rrl/config
]]

-- @TODO:
		-- firetruck-ladder
		-- blue's casino stuff
		-- fishingmod
		-- raptor-ui
		-- [any SWEP PrintNames]
		-- [any AddNotification]
		-- rrl
		-- v-fire
		-- Zero's arcade pack
		-- Check GetJobCategory again.

Monolith.LocaleBuilder( "fr", true )

-- CasinoKit
addon.casino.notif.allowedMax5					= "Vous n'êtes autorisé à placer que 5 paris maximum par table."

-- Tow Truck Driver
addon.towtruck.notif.noVehicle					= "Il n'y a pas de véhicule derrière la dépanneuse!"
addon.towtruck.notif.attached 					= "Véhicule attaché!"
addon.towtruck.notif.unattached					= "Véhicule détaché!"
addon.towtruck.notif.lookTowTruck				= "Vous devez regarder une dépanneuse pour attacher les véhicules à proximité!"

-- Firetruck Ladder
addon.ftladder.notif.needFirefighter			= "Vous devez être Pompier pour faire ça!"

-- Fishingmod
addon.fishing.notif.hookedCaught				= "Vous avez attrapé un poisson! Il est maintenant dans votre inventaire."
addon.fishing.notif.hookedAway 					= "Vous avez attrapé un poisson mais il s'est échappé."
addon.fishing.notif.noSpace						= "Vous n'avez pas de place dans votre inventaire!"
addon.fishing.notif.noCatch						= "Vous n'avez rien attrapé."
addon.fishing.notif.noBait						= "Vous n'avez plus d'appât."
addon.fishing.notif.pleaseWait					= "Veuillez patienter %s pour relancer."
addon.fishing.notif.rmbReel						= "[RMB] ENROULER"

-- GSigns
addon.gsigns.notif.err 							= "Il y a eu une erreur, vérifiez votre console!"
addon.gsigns.action.pickingUp 					= "RAMASSER"

-- RRL
addon.rrl.notif.removedWarning 					= "Vous avez supprimé un avertissement de %s"
addon.rrl.notif.warnedBy						= "Vous avez reçu un avertissement de %s pour %s, vous avez %s points d'avertissement."
addon.rrl.notif.willKicked						= "Vous serez exclu si vous recevez encore %s points"
addon.rrl.notif.willKicked.any 					= "un"
addon.rrl.notif.youWarned 						= "Vous avez averti %s pour %s."
addon.rrl.notif.reportDoesntExist 				= "Le signalement auquel vous essayez d'accéder n'existe pas."
addon.rrl.notif.noAccess						= "Vous n'avez pas accès à cette fonctionnalité."
addon.rrl.notif.removedReportTicket 			= "Vous avez supprimé un ticket de signalement (#%s)"
addon.rrl.notif.supportTicketRemoved			= "Votre ticket de signalement a été supprimé (#%s)"
addon.rrl.notif.cantCloseOwnReport				= "Vous ne pouvez pas clore votre propre signalement."
addon.rrl.notif.reportTicketClosed 				= "Votre ticket de signalement a été clos (#%s)"
addon.rrl.notif.cannotOpenOwnReport				= "Vous ne pouvez pas ouvrir votre propre signalement."
addon.rrl.notif.yourReportTicketOpened 			= "Votre ticket de signalement a été ouvert (#%s)"
addon.rrl.notif.alreadyClaimed 					= "Vous ne pouvez pas prendre ce signalement car il est déjà pris."
addon.rrl.notif.cannotClaimOwn 					= "Vous ne pouvez pas prendre votre propre signalement."
addon.rrl.notif.staffMemberClaimed 				= "Un membre du staff a pris votre signalement (#%s)"
addon.rrl.notif.claimedReportTicket				= "Vous avez pris un ticket de signalement (#%s)"
addon.rrl.notif.staffMemberUnclaimed			= "Un membre du staff a laché votre signalement (#%s)"
addon.rrl.notif.youUnclaimed					= "Vous avez laché un signalement (#%s)"
addon.rrl.notif.submittedNewReport				= "Vous avez envoyé un nouveau signalement (#%s)"

-- Whomekit_media
addon.whomekit_media.notif.needConnect			= "Vous devez brancher la prise de cet appareil à une prise résidentielle."
addon.whomekit_media.notif.blacklist 			= "Vous êtes blacklisté de la lecture de tout contenu."
addon.whomekit_media.log.startedPlaying 		= "%s a commencé à jouer: %s sur une source de media. (%s)"

-- zcrga
addon.zcrga.notif.cantAfford					= "Vous n'avez pas assez d'argent pour ça!"
addon.zcrga.notif.youWonX						= "Vous avez gagné %s!"	